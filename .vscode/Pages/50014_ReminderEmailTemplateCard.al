page 50014 "Reminder Email Template Card"
{
    PageType = Card;
    SourceTable = "Reminder Email Template";
    ApplicationArea = All;
    UsageCategory = Administration;
    CaptionML = ENU = 'Reminder Email Template';

    layout
    {

        area(Content)
        {
            //Defines a FastTab that has the heading 'General'.
            group(General)
            {
                field("Language Code"; Rec."Language Code")
                {
                    ApplicationArea = All;

                }
                field("Email Subject"; Rec."Email Subject")
                {
                    ApplicationArea = All;

                }
                field("Email Body"; EmailBodyHTML)
                {
                    ApplicationArea = All;
                    MultiLine = true;
                    Width = 300;

                    trigger OnValidate()
                    begin
                        Rec.SetEmailBody(EmailBodyHTML);
                    end;


                }
                field("Email Body Text Lvl 4"; EmailBodyHTMLLvl4)
                {
                    ApplicationArea = All;
                    MultiLine = true;
                    Width = 300;

                    trigger OnValidate()
                    begin
                        Rec.SetEmailBodyLvl4(EmailBodyHTMLLvl4);
                    end;


                }
            }
        }

    }

    trigger OnAfterGetRecord()
    begin

        EmailBodyHTML := Rec.GetEmailBody();
        EmailBodyHTMLLvl4 := Rec.GetEmailBodyLvl4();
    end;

    trigger OnOpenPage()
    begin


    end;

    var
        EmailBodyHTML: Text[10000];

        EmailBodyHTMLLvl4: Text[10000];
        outStr: OutStream;
        inSTr: InStream;





}