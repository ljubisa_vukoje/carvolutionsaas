page 50019 UpdateCostObject
{

    Extensible = false;
    PageType = StandardDialog;
    Caption = 'Update Cost Object';

    layout
    {
        area(Content)
        {
            field(OldDimensionValue; OldDimCode)
            {
                ApplicationArea = all;
                Caption = 'Old Dimension Value';
                ToolTip = 'Specify old dimension which will be corrected to new dimension';

                trigger OnLookup(var Text: Text): Boolean
                var
                    OldDim: Record "Dimension Value";
                begin
                    OldDim.Reset();
                    OldDim.SetRange("Global Dimension No.", 2);
                    OldDim.SetRange(Blocked, false);
                    OldDim.SetRange("Dimension Value Type", OldDim."Dimension Value Type"::Standard);
                    if Page.RunModal(Page::"Dimension Value List", OldDim) = Action::LookupOK then
                        OldDimCode := OldDim.Code;

                end;

            }

            field(NewDimensionValue; NewDimCode)
            {
                ApplicationArea = all;
                Caption = 'New Dimension Value';
                ToolTip = 'Specify new dimension';

                trigger OnLookup(var Text: Text): Boolean
                var
                    NewDim: Record "Dimension Value";
                begin
                    NewDim.Reset();
                    NewDim.SetRange("Global Dimension No.", 2);
                    NewDim.SetRange(Blocked, false);
                    NewDim.SetRange("Dimension Value Type", NewDim."Dimension Value Type"::Standard);
                    if Page.RunModal(Page::"Dimension Value List", NewDim) = Action::LookupOK then
                        NewDimCode := NewDim.Code;

                end;

            }
        }
    }

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin

    end;

    procedure GetOldDimension(): Code[25]
    begin
        Exit(OldDimCode);
    end;

    procedure GetNewDimension(): Code[25]
    begin
        Exit(NewDimCode);
    end;

    var


        OldDimCode: COde[20];
        NewDimCode: Code[20];



}