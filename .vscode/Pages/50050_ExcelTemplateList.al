/// <summary>
/// Page ExcelTemplateList (ID 50050).
/// </summary>
page 50050 ExcelTemplateList
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "Excel Template Storage";
    Caption = 'Excel Template';

    layout
    {
        area(Content)
        {
            repeater(GroupName)
            {
                field(Name; Rec.TemplateName)
                {
                    ApplicationArea = All;
                    Caption = 'Template Name';
                }
                field("File Name"; Rec.TemplateFileName)
                {
                    ApplicationArea = All;
                    Caption = 'Template Filename';
                }
                field("Has Content"; Rec.TemplateFile.HasValue)
                {
                    ApplicationArea = All;
                    Caption = 'Template Exists';
                    /*                     Visible = FALSE; */
                }

                field(SheetName; rec.SheetName)
                {
                    ApplicationArea = All;
                    Caption = 'Excel Sheetname';
                    trigger OnAssistEdit()
                    begin
                        ExcelHelper.SelectSheetName(rec);
                    end;
                }
                field(DefaultFileName; rec.DefaultFileName)
                {
                    ApplicationArea = All;
                    Caption = 'Default Filename';
                }
                field(DataExchangeCode; rec.DataExchangeCode)
                {
                    ApplicationArea = All;
                    Caption = 'Data Exchange Def. Code';
                    Visible = FALSE;


                }
            }
        }
    }

    actions
    {
        area(Creation)
        {
            action(ImportExcelTemplate)
            {
                ApplicationArea = All;
                Caption = 'Import Excel Template';
                Promoted = true;
                PromotedCategory = New;
                Image = ImportExcel;

                trigger OnAction()
                begin
                    ExcelHelper.ImportExcelTemplate();
                end;
            }
        }
        area(Processing)
        {

            action(ExportExcelTemplate)
            {
                ApplicationArea = All;
                Caption = 'Export Excel Template';
                Promoted = true;
                PromotedCategory = Process;
                Image = ExportToExcel;

                trigger OnAction()
                begin
                    ExcelHelper.ExportExcelTemplate(Rec);
                end;
            }
        }
    }
    var
        ExcelHelper: Codeunit "Excel Management";
}

