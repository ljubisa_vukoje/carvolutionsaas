/// <summary>
/// CU for recreating API records
/// Vukoje Ljubisa vukoje 06.08.2021
/// </summary>
pageextension 50102 APiSetupPageExt extends "API Setup"
{
    actions
    {

        addafter(IntegrateAPIs)
        {
            action(GraphTool)
            {
                Caption = 'Run Graph. Tool';
                Image = Setup;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = all;
                trigger OnAction()
                begin
                    if Confirm(ConfirmApiSetupQst) then
                        CODEUNIT.Run(CODEUNIT::"Graph Mgt - General Tools");
                end;

            }


        }
    }

    var
        ConfirmApiSetupQst: Label 'This action will populate the integration tables for all APIs and may take several minutes to complete. Do you want to continue?';



}


