page 50031 "Replacement part template list"
{
    PageType = List;
    SourceTable = "Car Replacement template";
    CardPageId = 50032;
    ApplicationArea = All;
    UsageCategory = Lists;
    CaptionML = ENU = 'Car Replacement email templates';
    InsertAllowed = false;
    ModifyAllowed = false;

    layout
    {

        area(Content)
        {
            // Sets the No., Name, Contact, and Phone No. fields in the Customer table to be displayed as columns in the list. 
            repeater(Group)
            {
                field("Language Code"; Rec."Language Code")
                {
                    ApplicationArea = All;
                    Enabled = false;

                }
                field("Email Subject"; Rec."Email Subject")
                {
                    ApplicationArea = All;
                    Enabled = false;

                }

            }
        }

    }


}