
/// <summary>
/// Page "AddDocumentAttachment" (ID 50100).
/// </summary>
page 50100 AddDocumentAttachment
{
    PageType = API;
    Caption = 'addDocumentAttachment';
    APIPublisher = 'HolyERP';
    APIGroup = 'app1';
    APIVersion = 'v2.0';
    EntityName = 'addDocumentAttachment';
    EntitySetName = 'addDocumentAttachment';
    SourceTable = BlobValues;
    SourceTableTemporary = true;
    DelayedInsert = true;
    ODataKeyFields = "File Name";

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field("Filename"; rec."File Name")
                {
                    ApplicationArea = all;
                }
                field(SystemId; rec.SystemId)
                {
                    ApplicationArea = all;
                }
                Field(DocumentType; rec."Document Type")
                {
                    ApplicationArea = all;
                }
                field(Base64String; Base64String)
                {
                    ApplicationArea = all;
                }

            }
        }
    }
    actions
    {

    }
    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    var
        Attach: Codeunit AddAttachment;
    begin
        Attach.Attach(rec.SystemId, rec."Document Type", Base64String, rec."File Name")
    end;

    trigger OnDeleteRecord(): Boolean
    begin
        rec.Delete(true);
    end;

    var
        Base64String: Text;
        Base64Convert: Codeunit "Base64 Convert";
        FinalOStream: OutStream;
        attachment: Record "Document Attachment";
        FinalInstream: InStream;
        Filename: Text;
        BlobTable: record BlobValues;
        today: Date;
        SalesInv: Record "Sales Invoice Header";
        PurchInv: Record "Purch. Inv. Header";
        actionContext: WebServiceActionContext;
        Message: Text;
}