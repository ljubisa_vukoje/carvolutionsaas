page 50018 "Cost Object COrrection List"
{
    PageType = List;
    SourceTable = CostObjectCorrectionTable;
    ApplicationArea = All;
    UsageCategory = Lists;
    Caption = 'Cost Object Correction List - Sales';

    layout
    {

        area(Content)
        {
            repeater(General)
            {

                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;

                    trigger OnValidate()
                    var
                        CostCorrObj: Record CostObjectCorrectionTable;
                    begin
                        if CostCorrObj."Entry No." <> 0 then begin
                            CostCorrObj.Reset();
                            CostCorrObj.SetRange("Document No.", Rec."Document No.");
                            CostCorrObj.SetFilter("Entry No.", '<>%1', Rec."Entry No.");
                            CostCorrObj.SetFilter(CostCorrObj."Cost Object Status", '%1|%2', CostCorrObj."Cost Object Status"::"In Process", CostCorrObj."Cost Object Status"::Open);
                            if CostCorrObj.find('-') then
                                Error(StrSubstNo(DoubleDocCheckLbl, Rec."Document No.", Format(Rec."Cost Object Status")));
                        end;
                    end;

                }
                field(Carvolution_ID; Rec.Carvolution_ID)
                {
                    ApplicationArea = All;

                }
                field(Corrected; Rec.Corrected)
                {
                    ApplicationArea = all;
                    Editable = false;
                }

                field("Cost Journal Posted"; Rec."Cost Journal Posted")
                {
                    ApplicationArea = all;
                    Editable = false;
                }
                field("Error Text"; Rec."Error Text")
                {
                    ApplicationArea = all;
                    Editable = false;
                }

                field("Cost Object Status"; Rec."Cost Object Status")
                {
                    ApplicationArea = All;
                    Editable = true;
                    StyleExpr = StyleExprTxt;
                    trigger OnValidate()
                    begin
                        StyleExprTxt := ChangeCOlorCU.ChangeCostCorrColor(Rec);
                    end;



                }

                field("DimensionCorrection ID"; Rec."DimensionCorrection ID")
                {
                    ApplicationArea = All;
                    Editable = false;


                    trigger OnDrillDown()
                    var
                        DimCorrPage: Page "Dimension Correction";
                        DimCorrRec: Record "Dimension Correction";
                    begin
                        Clear(DimCorrPage);
                        DimCorrRec.Reset();
                        DimCorrRec.SetRange("Entry No.", Rec."DimensionCorrection ID");
                        if DimCorrRec.Find('-') then begin
                            DimCorrPage.SetTableView(DimCorrRec);
                            DimCorrPage.RunModal();
                        end;

                    end;


                }

                field(SalesInvAmt; Rec.SalesInvAmt)
                {
                    ApplicationArea = All;
                    trigger OnDrillDown()
                    var
                        TGlEntry: Record "G/L Entry";
                        PGlEntry: Page "General Ledger Entries";
                        TGlAcc: Record "G/L Account";
                    begin
                        TGlAcc.Reset();
                        TGlEntry.Reset();
                        TGlEntry.SetRange("Document No.", Rec."Document No.");

                        if TGlEntry.Find('-') then begin
                            repeat
                                TGlAcc.Get(TGlEntry."G/L Account No.");
                                if TGlAcc."Income/Balance" = TGlAcc."Income/Balance"::"Income Statement" then
                                    TGlEntry.Mark(true);
                            until TGlEntry.Next() = 0;

                            TGlEntry.MarkedOnly(true);
                            PGlEntry.LookupMode := true;
                            PGlEntry.SetTableView(TGlEntry);
                            PGlEntry.SetRecord(TGlEntry);
                            Commit();
                            PGlEntry.RunModal();
                        end;

                    end;
                }
                field(CostEntriesAmt; Rec.CostEntriesAmt)
                {
                    ApplicationArea = All;
                }
                field(DiffAmount; Rec.DiffAmount)
                {
                    ApplicationArea = All;
                    Caption = 'Diff. Amount';
                    Editable = false;
                }
                field(CostCorrCount; Rec.CostCorrCount)
                {
                    ApplicationArea = All;
                    Caption = 'Cost Correction Count';
                    Editable = false;
                }



            }
        }



    }

    actions
    {
        area(Processing)
        {
            action(Run)
            {
                Caption = 'Run cost object correction';
                Promoted = true;
                PromotedIsBig = true;
                PromotedCategory = Process;//test push 
                Image = Process;
                RunObject = codeunit CostObjectCorrectionCU;
                ApplicationArea = all;


            }
            action(FixAmt)
            {
                Caption = 'Try To Fix Amount';
                Promoted = true;
                PromotedIsBig = true;
                PromotedCategory = Process;//test push 
                Image = Process;
                ApplicationArea = all;
                trigger OnAction()
                var
                    CostCorrCU: codeunit CostObjectCorrectionCU;
                    CostCorrection: Record CostObjectCorrectionTable;
                begin
                    Message('Disabled !');
                    exit;
                    Clear(CostCorrCU);
                    CLear(CostCorrection);
                    CurrPage.SetSelectionFilter(CostCorrection);

                    if CostCorrection.Find('-') then begin
                        repeat
                        //CostCorrCU.FixCorrectionLineAmt(CostCorrection);
                        //CostCorrCU.VoidDuplicateAmount();
                        until CostCorrection.Next() = 0;
                    end;

                    Message('Finished !');
                end;


            }
            action(RunSelected)
            {
                /*
                Caption = 'Run cost object correction for selected';
                Promoted = true;
                PromotedIsBig = true;
                PromotedCategory = Process;//test push 
                Image = Process;
                ApplicationArea = all;
                trigger OnAction()
                var
                    CostObjCorr: codeunit CostObjectCorrectionCU;
                begin
                    CostObjCorr.CorrectDimension(Rec);
                end;*/


            }
            action(ExportToExcel)
            {
                Caption = 'Export to Excel';
                ApplicationArea = All;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = Export;

                trigger OnAction()
                var
                begin
                    ExportCustLedgerEntries(Rec);
                end;
            }
            action(VoidToZero)
            {
                Caption = 'Void Amount To Zero From Excel Temp';
                ApplicationArea = All;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = Export;

                trigger OnAction()
                var
                begin
                    VoidToZeroTemp();
                end;
            }
        }


    }

    local procedure ExportCustLedgerEntries(var CostObject: Record "CostObjectCorrectionTable")
    var
        TempExcelBuffer: Record "Excel Buffer" temporary;
        CustLedgerEntriesLbl: Label 'Cost Object Correction List';
        ExcelFileName: Label 'Cost Object Correction List_%1_%2';
    begin
        TempExcelBuffer.Reset();
        TempExcelBuffer.DeleteAll();
        TempExcelBuffer.NewRow();
        TempExcelBuffer.AddColumn(CostObject.FieldCaption("Document No."), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption(Carvolution_ID), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption(Corrected), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption("Cost Journal Posted"), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption("Error Text"), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption("Cost Object Status"), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption("DimensionCorrection ID"), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption(SalesInvAmt), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption(CostEntriesAmt), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption(DiffAmount), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
        if CostObject.FindSet() then
            repeat
                TempExcelBuffer.NewRow();
                TempExcelBuffer.AddColumn(CostObject."Document No.", false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject.Carvolution_ID, false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject.Corrected, false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject."Cost Journal Posted", false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject."Error Text", false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject."Cost Object Status", false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject."DimensionCorrection ID", false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject.SalesInvAmt, false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
                TempExcelBuffer.AddColumn(CostObject.CostEntriesAmt, false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
                TempExcelBuffer.AddColumn(CostObject.DiffAmount, false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
            until CostObject.Next() = 0;
        TempExcelBuffer.CreateNewBook(CustLedgerEntriesLbl);
        TempExcelBuffer.WriteSheet(CustLedgerEntriesLbl, CompanyName, UserId);
        TempExcelBuffer.CloseBook();
        TempExcelBuffer.SetFriendlyFilename(StrSubstNo(ExcelFileName, CurrentDateTime, UserId));
        TempExcelBuffer.OpenExcel();
    end;

    trigger OnAfterGetRecord()
    begin
        StyleExprTxt := ChangeCOlorCU.ChangeCostCorrColor(Rec);

    end;

    trigger OnOpenPage()
    begin
        CheckAmt();
        if Rec.Find('+') then;
    end;

    procedure VoidToZeroTemp()
    begin
        ReadExcelSheet();
        ImportExcelData();
    end;

    local procedure ImportExcelData()
    var

        RowNo: Integer;
        ColNo: Integer;
        LineNo: Integer;
        MaxRowNo: Integer;
        EntryNo: Integer;
        DummyBoolean: Boolean;
        CostEntries: Record "Cost Entry";

    begin

        RowNo := 0;
        ColNo := 0;
        MaxRowNo := 0;


        TempExcelBuffer.Reset();
        if TempExcelBuffer.FindLast() then begin
            MaxRowNo := TempExcelBuffer."Row No.";
        end;

        for RowNo := 2 to MaxRowNo do begin
            DummyBoolean := Evaluate(EntryNo, GetValueAtCell(RowNo, 10));

            CostEntries.Reset();
            CostEntries.SetRange("Entry No.", EntryNo);
            if CostEntries.Find('-') then begin
                CostEntries."Debit Amount" := 0;
                CostEntries."Credit Amount" := 0;
                CostEntries.Amount := 0;
                CostEntries."Voided Comment" := 'Voided : ' + Format(Today);
                CostEntries.Modify();
            end;



        end;
        Message(ExcelImportSucess);
    end;

    local procedure ReadExcelSheet()
    var
        FileMgt: Codeunit "File Management";
        IStream: InStream;
        FromFile: Text[100];
    begin
        UploadIntoStream(UploadExcelMsg, '', '', FromFile, IStream);
        if FromFile <> '' then begin
            FileName := FileMgt.GetFileName(FromFile);
            SheetName := TempExcelBuffer.SelectSheetsNameStream(IStream);
        end else
            Error(NoFileFoundMsg);
        TempExcelBuffer.Reset();
        TempExcelBuffer.DeleteAll();
        TempExcelBuffer.OpenBookStream(IStream, SheetName);
        TempExcelBuffer.ReadSheet();
    end;

    local procedure GetValueAtCell(RowNo: Integer; ColNo: Integer): Text
    begin

        TempExcelBuffer.Reset();
        If TempExcelBuffer.Get(RowNo, ColNo) then
            exit(TempExcelBuffer."Cell Value as Text")
        else
            exit('');
    end;

    procedure CheckAmt()
    var
        TCostCorrection: Record CostObjectCorrectionTable;
    begin
        TCostCorrection.Reset();
        TCostCorrection.SetRange(SalesInvAmt, 0);
        TCostCorrection.SetFilter("Entry No.", '<>%1', 0);
        if TCostCorrection.Find('-') then begin
            repeat
                TCostCorrection.SalesInvAmt := TCostCorrection.CalcInvAmount();
                TCostCorrection.CalcFields(CostEntriesAmt);
                TCostCorrection.DiffAmount := TCostCorrection.SalesInvAmt + TCostCorrection.CostEntriesAmt;
                TCostCorrection.Modify();
            until TCostCorrection.Next() = 0;
            Commit();
        end;

        TCostCorrection.Reset();
        TCostCorrection.SetFilter(DiffAmount, '<>%1', 0);
        TCostCorrection.SetFilter("Entry No.", '<>%1', 0);
        if TCostCorrection.Find('-') then begin
            repeat
                TCostCorrection.CalcFields(CostEntriesAmt);
                TCostCorrection.DiffAmount := TCostCorrection.SalesInvAmt + TCostCorrection.CostEntriesAmt;
                TCostCorrection.Modify();
            until TCostCorrection.Next() = 0;
            Commit();
        end;
        CurrPage.Update(false);
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        Commit();
    end;

    var
        BatchName: Code[10];
        FileName: Text[100];
        SheetName: Text[100];
        DimensionValues: Record "Dimension Value";
        CostObjects: Record "Cost Object";

        TempExcelBuffer: Record "Excel Buffer" temporary;

        StyleExprTxt: Text[50];
        ChangeCOlorCU: Codeunit ChangeColor;

        DoubleDocCheckLbl: Label 'Cost correction with document %1 , and status %2 already exists !';

        UploadExcelMsg: Label 'Please Choose the Excel file.';
        NoFileFoundMsg: Label 'No Excel file found!';
        BatchISBlankMsg: Label 'Batch name is blank';
        ExcelImportSucess: Label 'Excel is successfully imported.';





}