page 50013 "Reminder Email Template List"
{
    PageType = List;
    SourceTable = "Reminder Email Template";
    CardPageId = 50014;
    ApplicationArea = All;
    UsageCategory = Lists;
    CaptionML = ENU = 'Reminder Email Templates';
    InsertAllowed = false;
    ModifyAllowed = false;

    layout
    {

        area(Content)
        {
            // Sets the No., Name, Contact, and Phone No. fields in the Customer table to be displayed as columns in the list. 
            repeater(Group)
            {
                field("Language Code"; Rec."Language Code")
                {
                    ApplicationArea = All;
                    Enabled = false;

                }
                field("Email Subject"; Rec."Email Subject")
                {
                    ApplicationArea = All;
                    Enabled = false;

                }

            }
        }

    }


}