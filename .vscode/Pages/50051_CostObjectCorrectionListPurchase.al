page 50051 "Cost Object Corr. List Purch."
{
    PageType = List;
    SourceTable = CostObjectCorrectionPurchTable;
    ApplicationArea = All;
    UsageCategory = Lists;
    Caption = 'Cost Object Correction List - Purchase';

    layout
    {

        area(Content)
        {
            repeater(General)
            {

                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;


                    trigger OnValidate()
                    var
                        CostCorrObj: Record CostObjectCorrectionPurchTable;
                    begin
                        CostCorrObj.Reset();
                        CostCorrObj.SetRange("Document No.", Rec."Document No.");
                        CostCorrObj.SetFilter("Entry No.", '<>%1', Rec."Entry No.");
                        CostCorrObj.SetFilter(CostCorrObj."Cost Object Status", '%1|%2', CostCorrObj."Cost Object Status"::"In Process", CostCorrObj."Cost Object Status"::Open);
                        if CostCorrObj.find('-') then
                            Error(StrSubstNo(DoubleDocCheckLbl, Rec."Document No.", Format(Rec."Cost Object Status")));

                    end;

                }
                field(Carvolution_ID; Rec.Carvolution_ID)
                {
                    ApplicationArea = All;

                }
                field(Corrected; Rec.Corrected)
                {
                    ApplicationArea = all;
                    Editable = false;
                }

                field("Cost Journal Posted"; Rec."Cost Journal Posted")
                {
                    ApplicationArea = all;
                    Editable = false;
                }
                field("Error Text"; Rec."Error Text")
                {
                    ApplicationArea = all;
                    Editable = false;
                }
                field("Cost Object Status"; Rec."Cost Object Status")
                {
                    ApplicationArea = All;
                    Editable = true;
                    StyleExpr = StyleExprTxt;

                    trigger OnValidate()
                    begin
                        StyleExprTxt := ChangeCOlorCU.ChangeCostCorrColorPurch(Rec);
                    end;
                }
                field("DimensionCorrection ID"; Rec."DimensionCorrection ID")
                {
                    ApplicationArea = All;
                    Editable = false;


                    trigger OnDrillDown()
                    var
                        DimCorrPage: Page "Dimension Correction";
                        DimCorrRec: Record "Dimension Correction";
                    begin
                        Clear(DimCorrPage);
                        DimCorrRec.Reset();
                        DimCorrRec.SetRange("Entry No.", Rec."DimensionCorrection ID");
                        if DimCorrRec.Find('-') then begin
                            DimCorrPage.SetTableView(DimCorrRec);
                            DimCorrPage.RunModal();
                        end;

                    end;


                }
                field(PurchInvAmt; Rec.PurchInvAmt)
                {
                    ApplicationArea = All;
                    trigger OnDrillDown()
                    var
                        TGlEntry: Record "G/L Entry";
                        PGlEntry: Page "General Ledger Entries";
                        TGlAcc: Record "G/L Account";
                    begin
                        TGlAcc.Reset();
                        TGlEntry.Reset();
                        TGlEntry.SetRange("Document No.", Rec."Document No.");

                        if TGlEntry.Find('-') then begin
                            repeat
                                TGlAcc.Get(TGlEntry."G/L Account No.");
                                if TGlAcc."Income/Balance" = TGlAcc."Income/Balance"::"Income Statement" then
                                    TGlEntry.Mark(true);
                            until TGlEntry.Next() = 0;

                            TGlEntry.MarkedOnly(true);
                            PGlEntry.LookupMode := true;
                            PGlEntry.SetTableView(TGlEntry);
                            PGlEntry.SetRecord(TGlEntry);
                            Commit();
                            PGlEntry.RunModal();
                        end;

                    end;
                }
                field(CostEntriesAmt; Rec.CostEntriesAmt)
                {
                    ApplicationArea = All;
                }
                field(DiffAmount; Rec.DiffAmount)
                {
                    ApplicationArea = All;
                    Caption = 'Diff. Amount';
                    Editable = false;
                }
                field(CostCorrCount; Rec.CostCorrCount)
                {
                    ApplicationArea = All;
                    Caption = 'Cost Correction Count';
                    Editable = false;
                }
                field("Special Case Document"; Rec."Special Case Document")
                {
                    ApplicationArea = All;
                    Caption = 'Special Case Document';
                    Editable = false;
                }


            }
        }



    }

    actions
    {
        area(Processing)
        {
            action(Run)
            {
                Caption = 'Run cost object correction';
                Promoted = true;
                PromotedIsBig = true;
                PromotedCategory = Process;//test push 
                Image = Process;
                RunObject = codeunit CostObjectCorrectionPurchCU;
                ApplicationArea = all;
            }
            action(FixAmt)
            {
                Caption = 'Try To Fix Amount';
                Promoted = true;
                PromotedIsBig = true;
                PromotedCategory = Process;//test push 
                Image = Process;
                ApplicationArea = all;
                trigger OnAction()
                var
                    CostCorrPurchCU: codeunit CostObjectCorrectionPurchCU;
                    CostCorrection: Record CostObjectCorrectionPurchTable;
                    CostCorrCU: codeunit CostObjectCorrectionCU;
                begin
                    Message('Disabled !');
                    exit;
                    Clear(CostCorrPurchCU);
                    CLear(CostCorrection);
                    CLear(CostCorrCU);
                    /*
                    CurrPage.SetSelectionFilter(CostCorrection);

                    if CostCorrection.Find('-') then begin
                        repeat
                            CostCorrPurchCU.FixCorrectionLineAmt(CostCorrection);
                        until CostCorrection.Next() = 0;
                    end;*/
                    CostCorrCU.VoidDuplicateAmount();
                    Message('Finished !');
                    CurrPage.Update(false);
                end;


            }
            action(UpdateStatusToComplete)
            {
                Caption = 'Update status to complete';
                Promoted = true;
                PromotedIsBig = true;
                PromotedCategory = Process;
                Image = Process;
                ApplicationArea = all;
                trigger OnAction()
                begin
                    TriggerUpdateStatusToComplete();
                end;


            }
            action(ExportToExcel)
            {
                Caption = 'Export to Excel';
                ApplicationArea = All;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = Export;

                trigger OnAction()
                var
                begin
                    ExportCustLedgerEntries(Rec);
                end;
            }


        }
    }
    local procedure ExportCustLedgerEntries(var CostObject: Record CostObjectCorrectionPurchTable)
    var
        TempExcelBuffer: Record "Excel Buffer" temporary;
        CustLedgerEntriesLbl: Label 'Cost Object Correction Purchase List';
        ExcelFileName: Label 'Cost Object Correction Purchase List_%1_%2';
    begin
        TempExcelBuffer.Reset();
        TempExcelBuffer.DeleteAll();
        TempExcelBuffer.NewRow();
        TempExcelBuffer.AddColumn(CostObject.FieldCaption("Document No."), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption(Carvolution_ID), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption(Corrected), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption("Cost Journal Posted"), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption("Error Text"), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption("Cost Object Status"), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption("DimensionCorrection ID"), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption(PurchInvAmt), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption(CostEntriesAmt), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
        TempExcelBuffer.AddColumn(CostObject.FieldCaption(DiffAmount), false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
        if CostObject.FindSet() then
            repeat
                TempExcelBuffer.NewRow();
                TempExcelBuffer.AddColumn(CostObject."Document No.", false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject.Carvolution_ID, false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject.Corrected, false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject."Cost Journal Posted", false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject."Error Text", false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject."Cost Object Status", false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject."DimensionCorrection ID", false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Text);
                TempExcelBuffer.AddColumn(CostObject.PurchInvAmt, false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
                TempExcelBuffer.AddColumn(CostObject.CostEntriesAmt, false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
                TempExcelBuffer.AddColumn(CostObject.DiffAmount, false, '', false, false, false, '', TempExcelBuffer."Cell Type"::Number);
            until CostObject.Next() = 0;
        TempExcelBuffer.CreateNewBook(CustLedgerEntriesLbl);
        TempExcelBuffer.WriteSheet(CustLedgerEntriesLbl, CompanyName, UserId);
        TempExcelBuffer.CloseBook();
        TempExcelBuffer.SetFriendlyFilename(StrSubstNo(ExcelFileName, CurrentDateTime, UserId));
        TempExcelBuffer.OpenExcel();
    end;

    procedure TriggerUpdateStatusToComplete()
    var
        CostCorr: record CostObjectCorrectionTable;
        CostCorrPurch: Record CostObjectCorrectionPurchTable;
    begin
        CostCorr.Reset();
        CostCorr.SetFilter("Cost Object Status", '%1|%2', CostCorr."Cost Object Status"::Open, CostCorr."Cost Object Status"::"In Process");
        if CostCorr.Find('-') then
            CostCorr.ModifyAll(CostCorr."Cost Object Status", CostCorr."Cost Object Status"::Complete);

        CostCorrPurch.Reset();
        CostCorrPurch.SetFilter("Cost Object Status", '%1|%2', CostCorrPurch."Cost Object Status"::Open, CostCorrPurch."Cost Object Status"::"In Process");
        if CostCorrPurch.Find('-') then
            CostCorrPurch.ModifyAll(CostCorrPurch."Cost Object Status", CostCorrPurch."Cost Object Status"::Complete);

        Message('Finished !');

    end;

    trigger OnAfterGetRecord()
    begin
        StyleExprTxt := ChangeCOlorCU.ChangeCostCorrColorPurch(Rec);


    end;

    trigger OnOpenPage()
    begin
        CheckAmt();
        if Rec.Find('+') then;
    end;

    procedure CheckAmt()
    var
        TCostCorrection: Record CostObjectCorrectionPurchTable;
    begin
        TCostCorrection.Reset();
        TCostCorrection.SetRange(PurchInvAmt, 0);
        if TCostCorrection.Find('-') then begin
            repeat
                TCostCorrection.PurchInvAmt := TCostCorrection.CalcInvAmount();
                TCostCorrection.CalcFields(CostEntriesAmt);
                TCostCorrection.DiffAmount := TCostCorrection.PurchInvAmt - TCostCorrection.CostEntriesAmt;
                TCostCorrection.Modify();
            until TCostCorrection.Next() = 0;
            Commit();
        end;
        TCostCorrection.Reset();
        TCostCorrection.SetFilter(DiffAmount, '<>%1', 0);
        if TCostCorrection.Find('-') then begin
            repeat
                TCostCorrection.CalcFields(CostEntriesAmt);
                TCostCorrection.DiffAmount := TCostCorrection.PurchInvAmt - TCostCorrection.CostEntriesAmt;
                TCostCorrection.Modify();
            until TCostCorrection.Next() = 0;
            Commit();
        end;
        CurrPage.Update(false);

    end;

    var
        StyleExprTxt: Text[50];
        ChangeCOlorCU: Codeunit ChangeColor;
        DoubleDocCheckLbl: Label 'Cost correction with document %1 , and status %2 already exists !';




}