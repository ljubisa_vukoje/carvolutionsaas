page 50035 "QR IBAN Swift bank list"
{
    PageType = List;
    SourceTable = "QR IBAN Swift bank list";
    CardPageId = 50035;
    ApplicationArea = All;
    UsageCategory = Lists;
    CaptionML = ENU = 'QR IBAN Swift bank list';
    InsertAllowed = true;
    ModifyAllowed = true;

    layout
    {

        area(Content)
        {
            // Sets the No., Name, Contact, and Phone No. fields in the Customer table to be displayed as columns in the list. 
            repeater(Group)
            {
                field("Bank Code"; Rec."Bank code")
                {
                    ApplicationArea = All;
                    Enabled = true;

                }
                field("Country code"; Rec."Country code")
                {
                    ApplicationArea = All;
                    Enabled = true;

                }
                field("Description"; Rec."Description")
                {
                    ApplicationArea = All;
                    Enabled = true;

                }
                field("Swift code"; Rec."Swift code")
                {
                    ApplicationArea = All;
                    Enabled = true;

                }
                field("Place"; Rec."Place")
                {
                    ApplicationArea = All;
                    Enabled = true;

                }
            }
        }

    }


}