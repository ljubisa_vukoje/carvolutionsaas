page 50032 "Replacement part template card"
{
    PageType = Card;
    SourceTable = "Car Replacement template";
    ApplicationArea = All;
    UsageCategory = Administration;
    CaptionML = ENU = 'Car Replacement email template';

    layout
    {

        area(Content)
        {
            //Defines a FastTab that has the heading 'General'.
            group(General)
            {
                field("Language Code"; Rec."Language Code")
                {
                    ApplicationArea = All;

                }
                field("Email Subject"; Rec."Email Subject")
                {
                    ApplicationArea = All;

                }
                field("Email Body"; EmailBodyHTML)
                {
                    ApplicationArea = All;
                    MultiLine = true;
                    Width = 300;

                    trigger OnValidate()
                    begin
                        Rec.SetEmailBody(EmailBodyHTML);
                    end;


                }
            }
        }

    }

    trigger OnAfterGetRecord()
    begin

        EmailBodyHTML := Rec.GetEmailBody();
    end;

    trigger OnOpenPage()
    begin


    end;

    var
        EmailBodyHTML: Text[10000];
        outStr: OutStream;
        inSTr: InStream;





}