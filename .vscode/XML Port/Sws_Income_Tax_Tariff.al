xmlport 50112 SwSIncomeTaxTariff
{
    Format = Xml;
    Direction = Import;



    schema
    {
        textelement(Root)
        {
            //tableelement(SwsOutTax; "SwS Income Tax Tariff")
            tableelement(SwsOutTax; "SwS Tax At Source Tariff")
            {

                fieldelement(p1; SwsOutTax.County)
                {

                }
                fieldelement(p2; SwsOutTax."Church Tax")
                {

                }
                fieldelement(p3; SwsOutTax."Number of Children")
                {

                }
                fieldelement(p4; SwsOutTax."Valid from")
                {

                }
                fieldelement(p5; SwsOutTax."Valid to")
                {

                }
                fieldelement(p6; SwsOutTax.Gender)
                {

                }
                fieldelement(p7; SwsOutTax."From Amount")
                {

                }
                /*
                fieldelement(p8; SwsOutTax."Commuter Tariff")
                {

                }*/
                fieldelement(p9; SwsOutTax.Tariff)
                {

                }
                fieldelement(p10; SwsOutTax."Tax Amount")
                {

                }
                fieldelement(p11; SwsOutTax."Tax Percent")
                {

                }
                /*
                fieldelement(p12; SwsOutTax."Modified by")
                {

                }
                fieldelement(p13; SwsOutTax."Modified Date")
                {

                }*/
            }
        }
    }

    trigger OnPreXmlPort()
    begin

    end;


}