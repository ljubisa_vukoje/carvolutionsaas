xmlport 50113 SwSLedgerEntries
{
    Format = Xml;
    Direction = Import;



    schema
    {
        textelement(Root)
        {
            tableelement(SwsSalaryEntry; "SwS Salary Entry")
            {

                fieldelement(p1; SwsSalaryEntry."Line No.")
                {

                }
                fieldelement(p2; SwsSalaryEntry."Employee No.")
                {

                }
                fieldelement(p3; SwsSalaryEntry."Salary Type No.")
                {

                }
                fieldelement(p4; SwsSalaryEntry.Type)
                {

                }
                fieldelement(p5; SwsSalaryEntry.Date)
                {

                }
                fieldelement(p6; SwsSalaryEntry."Document Date")
                {

                }
                fieldelement(p7; SwsSalaryEntry.Text)
                {

                }
                fieldelement(p8; SwsSalaryEntry.Code)
                {

                }
                fieldelement(p9; SwsSalaryEntry."Code 2")
                {

                }
                fieldelement(p10; SwsSalaryEntry.Amount)
                {

                }
                fieldelement(p11; SwsSalaryEntry.Quantity)
                {

                }

            }
        }
    }

    trigger OnPreXmlPort()
    begin

    end;


}