pageextension 50018 ExtPaymentJournal extends "Payment Journal"
{

    layout
    {

        addafter("Posting Date")
        {
            field("Debit Bank"; Rec."Debit Bank")
            {
                ApplicationArea = all;
            }

        }
    }


    actions
    {


        addlast("&DTA")
        {
            action(GenerateISOFile)
            {
                Caption = 'Generate ISO file';
                Promoted = true;
                PromotedIsBig = true;
                PromotedCategory = Category12;
                PromotedOnly = true;
                image = FileContract;
                ApplicationArea = all;
                trigger OnAction();
                begin
                    RunReportWithCurrentRec(REPORT::ISOFIle);

                end;
            }
            action(VendorBankAcc)
            {
                Caption = 'Vendor Bank Accounts List';
                Promoted = true;
                PromotedIsBig = true;
                PromotedCategory = Category12;
                PromotedOnly = true;
                ApplicationArea = all;
                Image = BankAccount;
                trigger OnAction();
                begin
                    Page.Run(Page::"Vendor Bank Account List");
                end;
            }
        }





    }


    procedure RunReportWithCurrentRec(ReportID: Integer)
    var
        TempGenJnlLine: Record "Gen. Journal Line";
    begin
        TempGenJnlLine.COPY(Rec);
        TempGenJnlLine.SETRANGE("Journal Template Name", Rec."Journal Template Name");
        TempGenJnlLine.SETRANGE("Journal Batch Name", Rec."Journal Batch Name");

        REPORT.RUN(ReportID, TRUE, FALSE, TempGenJnlLine);
    end;
}




