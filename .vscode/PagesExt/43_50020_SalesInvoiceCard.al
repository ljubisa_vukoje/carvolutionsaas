/// <summary>
/// Vukoje Ljubisa 01.27.2022
/// </summary>
pageextension 50020 SalesInvoice extends "Sales Invoice"
{

    layout
    {
        addafter("Work Description")
        {
            field(Remark; Rec.Remark)
            {

                Editable = true;
                ApplicationArea = all;
                MultiLine = true;
            }
        }


        modify("Shortcut Dimension 2 Code")
        {

            trigger OnLookup(var Text: Text): Boolean
            var
                GlobalDimension2: Record "Dimension Value";
                DimensionValuesListPage: Page "Dimension Value List";
            begin
                Clear(DimensionValuesListPage);
                DimensionValuesListPage.LookupMode := true;
                GlobalDimension2.Reset();
                GlobalDimension2.SetRange("Global Dimension No.", 2);
                GlobalDimension2.SetRange("Dimension Value Type", GlobalDimension2."Dimension Value Type"::Standard);
                GlobalDimension2.SetRange(Blocked, false);

                DimensionValuesListPage.SetTableView(GlobalDimension2);
                if DimensionValuesListPage.RunModal() = Action::LookupOK then begin
                    DimensionValuesListPage.GetRecord(GlobalDimension2);
                    Rec.Validate("Shortcut Dimension 2 Code", GlobalDimension2.Code);
                end;

            end;
        }
    }


}