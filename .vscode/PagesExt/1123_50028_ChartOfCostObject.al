/// <summary>
/// Vukoje Ljubisa 20.01.2020
/// </summary>
pageextension 50028 ChartOfCostObjectPageExt extends "Chart Of Cost Objects"
{
    layout
    {
        addafter(Comment)
        {
            field(VIN; Rec.VIN)
            {
                ApplicationArea = all;
            }

            field("Vehicle Status"; Rec."Vehicle Status")
            {
                ApplicationArea = all;
            }
            field("Vehicle Category"; Rec."Vehicle Category")
            {
                ApplicationArea = all;
            }
            field("Carvolution Model ID"; Rec."Carvolution Model ID")
            {
                ApplicationArea = all;
            }
            field("Carvolution ID"; Rec."Carvolution ID")
            {
                ApplicationArea = all;
            }
            field("Licence plate"; Rec."Licence plate")
            {
                ApplicationArea = all;
            }
        }
    }

    actions
    {
        addafter("Get Cost Objects From Dimension")
        {
            action(CostObjectCorrectionsList)
            {
                ApplicationArea = all;
                Image = CostEntries;
                Caption = 'Cost Object Corrections List - Sales';
                RunObject = Page "Cost Object COrrection List";

            }
            action(CostObjectCorrectionsPurchList)
            {
                ApplicationArea = all;
                Image = CostEntries;
                Caption = 'Cost Object Corrections List - Purchase';
                RunObject = Page "Cost Object Corr. List Purch.";

            }
            action(ChangeCostObjectToAnother)
            {
                ApplicationArea = all;
                Image = CostLedger;
                Caption = 'Update Cost Object';

                trigger OnAction()
                var
                    UpdateCostObjectDialog: Page UpdateCostObject;
                    OldDim: Code[25];
                    NewDim: Code[25];
                    COCorrectionCU: Codeunit CostObjectCorrectionCU;
                    DimValues: Record "Dimension Value";
                    DimValues2: Record "Dimension Value";
                begin
                    Clear(COCorrectionCU);
                    Clear(UpdateCostObjectDialog);
                    DimValues.Reset();
                    DimValues2.Reset();
                    if UpdateCostObjectDialog.RunModal() = Action::OK then begin
                        NewDim := UpdateCostObjectDialog.GetNewDimension();
                        OldDim := UpdateCostObjectDialog.GetOldDimension();
                        if ((OldDim = '') OR (NewDim = '')) then
                            Error(EmptyDimensionLbl);

                        if (OldDim = NewDim) then
                            Error(SameDimensionLbl, OldDim, NewDim);

                        DimValues.SetRange("Global Dimension No.", 2);
                        DimValues.SetRange(Code, OldDim);
                        DimValues.Find('-');

                        DimValues2.SetRange("Global Dimension No.", 2);
                        DimValues2.SetRange(Code, NewDim);
                        DimValues2.Find('-');

                        COCorrectionCU.UpdateCostObject(DimValues, DimValues2);

                    end;

                end;


            }

        }
    }

    var
        EmptyDimensionLbl: Label 'Select dimension values !';
        SameDimensionLbl: Label 'Dimension %1 must be <> than dimension %2 !';



}