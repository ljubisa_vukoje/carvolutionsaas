pageextension 50013 "DTA Setup" extends "DTA Setup"
{


    layout
    {
        modify("DTA/EZAG")
        {
            Visible = false;
        }
        addafter("Bank Code")
        {
            field("DTA/EZAG/ISO"; Rec."DTA/EZAG/ISO")
            {
                Caption = 'DTA/EZAG/ISO';
                ApplicationArea = All;

            }
        }
        addafter(EZAG)
        {
            group(ISO)
            {
                Caption = 'ISO';
                field("ISO Sender IBAN"; Rec."ISO Sender IBAN")
                {
                    ApplicationArea = all;
                }
                field("ISO SWIFT Code (BIC)"; Rec."ISO SWIFT Code (BIC)")
                {
                    ApplicationArea = all;
                }

                field("ISO File Folder"; Rec."ISO File Folder")
                {
                    ApplicationArea = all;
                }

                field("ISO Filename"; Rec."ISO Filename")
                {
                    ApplicationArea = all;
                }
                /*
                field("Backup Copy"; "Backup Copy")
                {
                    ApplicationArea = all;
                }
                field("Backup Folder"; "Backup Folder")
                {
                    ApplicationArea = all;
                }
                field("Last Backup No."; "Last Backup No.")
                {
                    ApplicationArea = all;
                }*/


            }
        }
    }

}