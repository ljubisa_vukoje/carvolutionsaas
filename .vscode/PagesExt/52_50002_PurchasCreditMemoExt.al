pageextension 50002 "52_50002_PurchasCreditMemoExt" extends "Purchase Credit Memo"
{
    layout
    {
        addafter("Ship-to")
        {
            field("Swiss QR-Bill Amount"; Rec."Swiss QR-Bill Amount")
            {
                ApplicationArea = All;
                Editable = true;
                Visible = true;
            }
            field("Swiss QR-Bill IBAN"; Rec."Swiss QR-Bill IBAN")
            {
                ApplicationArea = All;
                Editable = true;
                Visible = true;

            }
        }
    }
}
