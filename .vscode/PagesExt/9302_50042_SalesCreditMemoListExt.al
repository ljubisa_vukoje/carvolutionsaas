pageextension 50042 "Sales Credit Memo List Ext" extends "Sales Credit Memo"
{
    actions
    {
        addafter("P&osting")
        {

            action("Upload Sales Header CSV")
            {
                Image = ImportExcel;
                ApplicationArea = all;
                trigger OnAction()
                begin
                    CSVImport.Run();
                end;
            }

            action("Upload Sales Line CSV")
            {
                Image = ImportExcel;
                ApplicationArea = all;
                trigger OnAction()
                begin
                    SalesLineCSVImport.Run();
                end;
            }

        }
    }

    var
        CSVImport: Codeunit CSVImport;
        SalesLineCSVImport: Codeunit SalesLineCSVImport;
}