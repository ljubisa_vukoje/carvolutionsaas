pageextension 50032 PostedSalesInvoiceList extends "Posted Sales Invoices"
{

    actions
    {
        addafter(Dimensions)
        {
            action(CorrectDimension)
            {
                ApplicationArea = all;
                CaptionML = ENU = 'Change Dimension', DES = 'Maß ändern';
                Image = Dimensions;

                trigger OnAction()
                var
                    GlobalDimension2: Record "Dimension Value";
                    DimensionValuesListPage: Page "Dimension Value List";
                begin
                    Clear(DimensionValuesListPage);
                    DimensionValuesListPage.LookupMode := true;
                    GlobalDimension2.Reset();
                    GlobalDimension2.SetRange("Global Dimension No.", 2);
                    GlobalDimension2.SetRange("Dimension Value Type", GlobalDimension2."Dimension Value Type"::Standard);
                    GlobalDimension2.SetRange(Blocked, false);

                    DimensionValuesListPage.SetTableView(GlobalDimension2);
                    if DimensionValuesListPage.RunModal() = Action::LookupOK then begin
                        DimensionValuesListPage.GetRecord(GlobalDimension2);
                        CorrectDimension(GlobalDimension2);
                    end;

                end;

            }
        }
        addafter(Email)
        {
            action(SendEmailReplacementParts)
            {
                Image = Email;
                ApplicationArea = All;
                CaptionML = ENU = 'Send Replacement Parts email';

                trigger OnAction()
                var
                    TempIssuedReminder: Record "Issued Reminder Header" temporary;
                begin
                    if not Confirm(LabelConfirmation) then
                        exit;

                    CurrPage.SetSelectionFilter(Rec);
                    Rec.FindFirst();
                    repeat
                        CUSendReplacementParts.Sendemail(rec."No.");
                    until Rec.Next = 0;
                    Message(LabelReminderAreSend);
                end;

            }
        }
    }



    procedure CorrectDimension(var NewDimension: Record "Dimension Value")
    var
        GLEntry: Record "G/L Entry";
        DimensionCorrection: Record "Dimension Correction";
        DimensionCorrectionMgt: Codeunit "Dimension Correction Mgt";
        DimensionCorrectionChange: Record "Dim Correction Change";
        CostAccountingSetup: Record "Cost Accounting Setup";


    begin
        CostAccountingSetup.GET();
        CostAccountingSetup.TestField("Cost Journal Nos.");


        GLEntry.Reset();
        GLEntry.SetRange("Document No.", Rec."No.");
        // GLEntry.SetRange("Gen. Posting Type", GLEntry."Gen. Posting Type"::Sale);


        GLEntry.FindFirst();
        PostCostObjectAllocationJournal(GLEntry, NewDimension);
        GLEntry.FindFirst();

        DimensionCorrection.Init();

        DimensionCorrection.Insert();

        //CurrPage.SetSelectionFilter(GLEntry);
        DimensionCorrectionMgt.CreateCorrectionFromSelection(GLEntry, DimensionCorrection);
        //Page.Run(PAGE::"Dimension Correction Draft", DimensionCorrection);

        DimensionCorrectionMgt.VerifyCanValidateDimensionCorrection(DimensionCorrection);
        DimensionCorrectionMgt.VerifyCanStartJob(DimensionCorrection);

        DimensionCorrectionChange.SetRange("Dimension Correction Entry No.", DimensionCorrection."Entry No.");
        if DimensionCorrectionChange.Find('-') then begin
            DimensionCorrectionChange.Validate("Dimension Code", 'KOSTENTRÄGER');
            DimensionCorrectionChange.Validate("New Value", NewDimension.Code);
            DimensionCorrectionChange.Modify();
        end;
        DimensionCorrection."Update Analysis Views" := true;
        DimensionCorrection.Modify();
        DimensionCorrectionMgt.ScheduleRunJob(DimensionCorrection);
    end;

    procedure PostCostObjectAllocationJournal(var GLEntry: Record "G/L Entry"; var NewDimension: Record "Dimension Value")
    var
        CostJournalLine: Record "Cost Journal Line";
        LineNo: Integer;
        DocumentNo: Code[20];
        CostAccountingSetup: Record "Cost Accounting Setup";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        CostAccLedgerEntries: Record "Cost Entry";
    begin

        LineNo := 0;
        CostAccountingSetup.GET();
        CostAccountingSetup.TestField("Cost Journal Nos.");
        //DocumentNo := NoSeriesMgt.TryGetNextNo(CostAccountingSetup."Cost Journal Nos.", WorkDate());

        CLEAR(CostJournalLine);
        CostJournalLine.Reset();
        CostJournalLine.SetRange("Journal Template Name", 'STANDARD');
        CostJournalLine.SetRange("Journal Batch Name", 'STANDARD');
        if CostJournalLine.Find('-') then
            CostJournalLine.DeleteAll();

        repeat
            CostAccLedgerEntries.Reset();
            CostAccLedgerEntries.SetRange("G/L Entry No.", GLEntry."Entry No.");
            // CostAccLedgerEntries.SetRange("Cost Object Code", GLEntry."Global Dimension 2 Code");
            if CostAccLedgerEntries.Find('-') then begin


                LineNo := LineNo + 10000;
                CostJournalLine.Init();
                CostJournalLine.Validate("Journal Template Name", 'STANDARD');
                CostJournalLine.Validate("Journal Batch Name", 'STANDARD');
                CostJournalLine.validate("Posting Date", GLEntry."Posting Date");
                // IJL.SetUpNewLine(IJL);

                CostJournalLine."Line No." := LineNo;
                CostJournalLine.Validate("Document No.", GLEntry."Document No.");
                CostJournalLine.Validate("Cost Type No.", GLEntry."G/L Account No.");
                CostJournalLine.Validate("Cost Object Code", NewDimension.Code);
                CostJournalLine.Amount := CostAccLedgerEntries.Amount;
                CostJournalLine.Validate("Bal. Cost Type No.", GLEntry."G/L Account No.");
                CostJournalLine.Validate("Bal. Cost Object Code", GLEntry."Global Dimension 2 Code");

                CostJournalLine.Insert(TRUE);

            end;

        until GLEntry.Next() = 0;


    end;


    var
        NoSeriesMgt: Codeunit NoSeriesManagement;

        CUSendReplacementParts: Codeunit "Replacement Part Email send";
        LabelConfirmation: label 'Confirm sending email?';
        LabelReminderAreSend: Label 'Emails are sent.';

}