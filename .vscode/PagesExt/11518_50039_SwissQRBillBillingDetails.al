pageextension 50039 SwissQRBillBillingDetailsExt extends "Swiss QR-Bill Billing Details"
{
    procedure SetBuffer(var SourceSwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail")
    begin
        Rec.DeleteAll();
        if SourceSwissQRBillBillingDetail.FindSet() then
            repeat
                Rec := SourceSwissQRBillBillingDetail;
                Rec.Insert();
            until SourceSwissQRBillBillingDetail.Next() = 0;
        if Rec.FindFirst() then;
    end;
}