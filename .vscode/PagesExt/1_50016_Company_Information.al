pageextension 50016 ExtCompanyInformation extends "Company Information"
{

    layout
    {
        addafter("System Indicator")
        {
            group(ISO)
            {
                Caption = 'ISO';


                field("ISO active"; Rec."ISO active")
                {
                    ApplicationArea = all;
                }

                field("Credit Transfer Msg. Nos."; Rec."Credit Transfer Msg. Nos.")
                {
                    ApplicationArea = all;
                }
                field("Software Version"; Rec."Software Version")
                {
                    ApplicationArea = all;
                }
                field("Software Name"; Rec."Software Name")
                {
                    ApplicationArea = all;
                }
                field(Manufacturer; Rec.Manufacturer)
                {
                    ApplicationArea = all;
                }

            }
        }
    }


}