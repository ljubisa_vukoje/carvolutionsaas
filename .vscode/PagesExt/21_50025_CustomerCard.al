pageextension 50025 CustomerPageExtension extends "Customer Card"
{
    layout
    {
        modify("Language Code")
        {
            trigger OnAfterValidate()
            begin
                case Rec."Language Code" OF
                    'DE':
                        Rec.Validate("Reminder Terms Code", 'NORMAL');
                    'FR':
                        Rec.Validate("Reminder Terms Code", 'NORMAL F');
                    'EN':
                        Rec.Validate("Reminder Terms Code", 'NORMAL E');
                    ELSE
                        Rec.Validate("Reminder Terms Code", 'NORMAL');
                END;

            end;
        }


    }

    actions
    {
        addafter(SaveAsTemplate)
        {
            action("Populate Empty Payment Terms Code")
            {
                Image = Insert;
                CaptionML = ENU = 'Populate Empty Payment Terms Code', DES = 'Füllen Sie einen leeren Zahlungsbedingungencode aus';
                ApplicationArea = all;

                trigger OnAction()
                var
                    Customer: Record Customer;
                begin
                    Customer.Reset();
                    Customer.SetFilter("Payment Terms Code", '=%1', '');
                    if Customer.Find('-') then
                        Customer.ModifyAll("Payment Terms Code", '14TN');

                    Customer.Reset();
                    Customer.SetFilter("Payment Terms Code", '=%1', 'SOFORT');
                    if Customer.Find('-') then
                        Customer.ModifyAll("Payment Terms Code", '14TN');

                end;

            }
        }
    }

    ///
    ///

}