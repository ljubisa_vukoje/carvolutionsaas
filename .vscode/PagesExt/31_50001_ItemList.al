pageextension 50001 ItemList extends "Item List"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addlast(reporting)
        {
            action(ExportCembraReport)
            {
                Caption = 'Cembra Report';
                ApplicationArea = All;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = Export;

                trigger OnAction()
                begin
                end;
            }
        }
    }

}