pageextension 50035 CostEntries extends "Cost Entries"
{

    layout
    {
        addafter("Cost Type No.")
        {
            field(CostCenterName; CostCenterName)
            {
                ApplicationArea = all;
                Editable = false;
                Caption = 'Cost Center Name';

            }
            field(CostTypeName; CostTypeName)
            {
                ApplicationArea = all;
                Editable = false;
                Caption = 'Cost Type Name';
            }
            field("Customer No."; Rec."Customer No.")
            {
                ApplicationArea = all;
                Editable = false;
                Caption = 'Customer No.';
            }
            field("Customer Name"; Rec."Customer Name")
            {

                ApplicationArea = all;
                Editable = false;
                Caption = 'Customer Name';
            }
            field("Vendor No."; Rec."Vendor No.")
            {
                ApplicationArea = all;
                Editable = false;
                Caption = 'Vendor No.';
            }
            field("Vendor Noame"; Rec."Vendor Name")
            {
                ApplicationArea = all;
                Editable = false;
                Caption = 'Vendor Name';
            }
            field("Document Type"; Rec."Document Type New")
            {
                ApplicationArea = all;
                Editable = false;
                Caption = 'Document Type';

            }
            field(SystemCreatedAt; Rec.SystemCreatedAt)
            {
                ApplicationArea = all;
                Editable = false;
                Caption = 'System Created At';
            }
            field("Voided Comment"; Rec."Voided Comment")
            {
                ApplicationArea = all;
                Editable = false;
                Caption = 'Voided Comment';
            }
        }

    }

    actions
    {
        addfirst(processing)
        {
            action("Update Cost Entry")
            {
                ApplicationArea = All;
                Caption = 'Update Customer & Vendor on Cost Entry';
                Image = UpdateDescription;

                trigger OnAction()
                begin
                    UpdateEntry();
                end;
            }

        }
    }

    procedure UpdateEntry()
    var
        ConfigProgresBar: Codeunit "Config. Progress Bar";
        CostEntry: Record "Cost Entry";
        GLEntry: Record "G/L Entry";
    begin
        if CostEntry.FindSet() then begin
            ConfigProgresBar.Init(CostEntry.Count, 1, CostEntry.TableCaption);
            GLEntry.Reset();
            GLEntry.SetCurrentKey("Document No.", "Posting Date");
            repeat
                ConfigProgresBar.Update(Format(CostEntry."Entry No."));
                GLEntry.SetRange("Document No.", CostEntry."Document No.");
                GLEntry.SetRange("Source Type", GLEntry."Source Type"::Customer);
                if GLEntry.FindFirst() then begin
                    Customer.get(GLEntry."Source No.");
                    CostEntry."Customer No." := Customer."No.";
                    CostEntry."Customer Name" := Customer.Name;
                    CostEntry."Document Type New" := GLEntry."Document Type";
                    CostEntry.Modify();

                end
                else begin
                    GLEntry.SetRange("Source Type", GLEntry."Source Type"::Vendor);
                    if GLEntry.FindFirst() then begin
                        Vendor.get(GLEntry."Source No.");
                        CostEntry."Vendor No." := Vendor."No.";
                        CostEntry."Vendor Name" := Vendor.Name;
                        CostEntry."Document Type New" := GLEntry."Document Type";
                        CostEntry.Modify();
                    end
                end;
            until CostEntry.Next() = 0;
            ConfigProgresBar.Close();
        end;
    end;

    var
        GLEntry: Record "G/L Entry";
        Customer: Record Customer;
        Vendor: Record Vendor;
        Salesheader: Record "Sales Invoice Header";
        PurchasHeader: Record "Purch. Inv. Header";
        CostCenterName: Text;
        CostTypeName: Text;

}