pageextension 50015 "Issued Reminders Extensions" extends 440
{
    layout
    {
        addafter(Name)
        {
            field("Reminder Level"; Rec."Reminder Level")
            {
                ApplicationArea = all;
            }
        }

    }
    actions
    {
        addlast(processing)
        {

            action(SendEmailReminders)
            {
                Image = Email;
                ApplicationArea = All;
                CaptionML = ENU = 'Send Email Reminders In Batch';

                trigger OnAction()
                var
                    TempIssuedReminder: Record "Issued Reminder Header" temporary;
                begin
                    if not Confirm(LabelConfirmation) then
                        exit;

                    CurrPage.SetSelectionFilter(Rec);
                    Rec.FindFirst();
                    repeat
                        IF Rec."No. Printed" > 0 then begin
                            IF Confirm(StrSubstNo(LblConfirmationSend, Rec."No.", Rec."No. Printed")) then
                                CUSendReminders.SendReminder(Rec."No.");
                        end
                        else begin
                            CUSendReminders.SendReminder(Rec."No.");
                        end;
                    until Rec.Next = 0;
                    Message(LabelReminderAreSend);
                end;

            }
        }

        addlast("&Reminder")
        {

            action(ReminderEmailTemplates)
            {
                Image = NewRow;
                ApplicationArea = All;
                Caption = 'Reminder Email Templates';
                RunObject = page "Reminder Email Template List";

            }


        }


    }



    var
        CUSendReminders: Codeunit "Reminder Email Batch Send";
        LabelConfirmation: label 'Confirm sending reminders in batch ?';
        LabelReminderAreSend: Label 'Reminders are send.';
        LblConfirmationSend: Label 'Reminder %1 is already printed %2 times.\Are you sure you want to send reminder ?';


}