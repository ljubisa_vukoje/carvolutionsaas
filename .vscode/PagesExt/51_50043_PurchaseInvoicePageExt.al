pageextension 50043 PurchaseInvoicePageExt extends "Purchase Invoice"
{
    layout
    {
        modify("Swiss QR-Bill Amount")
        {
            Editable = true;
        }
    }
}