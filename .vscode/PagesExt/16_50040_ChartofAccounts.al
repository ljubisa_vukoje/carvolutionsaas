/// <summary>
/// PageExtension ChartOfAccounts (ID 50040) extends Record Chart of Accounts.
/// </summary>
pageextension 50040 ChartOfAccounts extends "Chart of Accounts"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addlast(reporting)
        {
            action(ExportBalanceSheetToExcelInternal)
            {
                Caption = 'Balance Sheet to Excel - Internal';
                ApplicationArea = All;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = Export;

                trigger OnAction()

                begin
                end;
            }

            action(ExportBalanceSheetToExcelExternal)
            {
                Caption = 'Balance Sheet to Excel - External';
                ApplicationArea = All;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = Export;

                trigger OnAction()

                begin
                end;
            }
        }
    }


    local procedure ExportInternal()
    var
        TempExcelBuffer: Record "Excel Buffer" temporary;
        ExcelFileName: Label 'tesst';
    begin
        TempExcelBuffer.Reset();
        TempExcelBuffer.DeleteAll();
    end;

    var

        SomeReport: Report "Balance Sheet Internal";

}