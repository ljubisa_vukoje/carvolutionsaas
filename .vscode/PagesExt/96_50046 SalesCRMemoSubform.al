pageextension 50046 SalesCRMemoSubform extends "Sales Cr. Memo Subform"
{
    layout
    {

        addafter("Shortcut Dimension 1 Code")
        {
            field("Kostenträger Code"; Rec."Kostenträger Code")
            {
                ApplicationArea = all;
                AssistEdit = true;
                Caption = 'Carvo Kostenträger Code';

                trigger OnValidate()
                var
                    GlobalDimension2: Record "Dimension Value";
                    DimensionValuesListPage: Page "Dimension Value List";
                    LicencePlateFormat: Code[30];
                    DummyInt: Integer;
                begin
                    Clear(DimensionValuesListPage);
                    DimensionValuesListPage.LookupMode := true;
                    //Empty
                    if (Rec."Kostenträger Code" = '') then begin
                        Rec.Validate("Shortcut Dimension 2 Code", '');
                        CurrPage.Update(TRUE);
                        exit;
                    end;
                    //carvolutionID
                    GlobalDimension2.Reset();
                    GlobalDimension2.SetRange("Global Dimension No.", 2);
                    GlobalDimension2.SetRange(Carvolution_ID, Rec."Kostenträger Code");
                    GlobalDimension2.SetRange("Dimension Value Type", GlobalDimension2."Dimension Value Type"::Standard);
                    if GlobalDimension2.Find('-') then begin
                        Rec.Validate("Shortcut Dimension 2 Code", GlobalDimension2.Code);
                        Rec."Kostenträger Code" := GlobalDimension2.Code;
                        CurrPage.Update(TRUE);
                        exit;
                    end;
                    //Licence Plate
                    GlobalDimension2.Reset();
                    GlobalDimension2.SetRange("Global Dimension No.", 2);
                    LicencePlateFormat := Rec."Kostenträger Code";
                    if StrLen(Rec."Kostenträger Code") > 3 then begin
                        DummyInt := StrPos(Rec."Kostenträger Code", ' ');
                        if DummyInt <> 3 then
                            LicencePlateFormat := InsStr(LicencePlateFormat, ' ', 3);

                    end;
                    GlobalDimension2.SetRange(GlobalDimension2."Licence plate", LicencePlateFormat);
                    GlobalDimension2.SetRange("Dimension Value Type", GlobalDimension2."Dimension Value Type"::Standard);
                    GlobalDimension2.SetRange(Blocked, false);
                    if GlobalDimension2.Find('-') then begin
                        Rec.Validate("Shortcut Dimension 2 Code", GlobalDimension2.Code);
                        Rec."Kostenträger Code" := GlobalDimension2.Code;
                        CurrPage.Update(TRUE);
                        exit;
                    end;

                    //VIN
                    GlobalDimension2.Reset();
                    GlobalDimension2.SetRange("Global Dimension No.", 2);
                    GlobalDimension2.SetFilter(GlobalDimension2."Vehicle VIN", '%1*', Rec."Kostenträger Code");
                    GlobalDimension2.SetRange("Dimension Value Type", GlobalDimension2."Dimension Value Type"::Standard);
                    GlobalDimension2.SetRange(Blocked, false);
                    if GlobalDimension2.Find('-') then begin
                        if GlobalDimension2.Count = 1 then begin
                            Rec.Validate("Shortcut Dimension 2 Code", GlobalDimension2.Code);
                            Rec."Kostenträger Code" := GlobalDimension2.Code;
                            CurrPage.Update(TRUE);
                            exit;
                        end
                        else begin
                            DimensionValuesListPage.SetTableView(GlobalDimension2);
                            if DimensionValuesListPage.RunModal() = Action::LookupOK then begin
                                DimensionValuesListPage.GetRecord(GlobalDimension2);
                                Rec.Validate("Shortcut Dimension 2 Code", GlobalDimension2.Code);
                                Rec."Kostenträger Code" := GlobalDimension2.Code;
                                CurrPage.Update(TRUE);
                            end;
                        end;
                    end
                    else begin//Not found
                        GlobalDimension2.Reset();
                        GlobalDimension2.SetRange("Global Dimension No.", 2);
                        GlobalDimension2.SetRange("Dimension Value Type", GlobalDimension2."Dimension Value Type"::Standard);
                        GlobalDimension2.SetRange(Blocked, false);
                        repeat
                            if FORMAT(GlobalDimension2."Vehicle VIN").Contains(Rec."Kostenträger Code") then
                                GlobalDimension2.Mark(TRUE);
                            if FORMAT(GlobalDimension2."Licence plate").Contains(Rec."Kostenträger Code") then
                                GlobalDimension2.Mark(TRUE);
                            if FORMAT(GlobalDimension2.Carvolution_ID).Contains(Rec."Kostenträger Code") then
                                GlobalDimension2.Mark(TRUE);
                        until GlobalDimension2.Next() = 0;

                        GlobalDimension2.MarkedOnly(TRUE);
                        DimensionValuesListPage.SetTableView(GlobalDimension2);
                        if DimensionValuesListPage.RunModal() = Action::LookupOK then begin
                            DimensionValuesListPage.GetRecord(GlobalDimension2);
                            Rec.Validate("Shortcut Dimension 2 Code", GlobalDimension2.Code);
                            Rec."Kostenträger Code" := GlobalDimension2.Code;
                            CurrPage.Update(TRUE);
                        end
                        else begin
                            Rec.Validate("Shortcut Dimension 2 Code", '');
                            Rec."Kostenträger Code" := '';
                            CurrPage.Update(TRUE);
                        end;

                    end;

                end;


                trigger OnAssistEdit()
                var
                    GlobalDimension2: Record "Dimension Value";
                    DimensionValuesListPage: Page "Dimension Value List";
                begin
                    CLEAR(DimensionValuesListPage);
                    DimensionValuesListPage.LookupMode := true;
                    GlobalDimension2.Reset();
                    GlobalDimension2.SetRange("Global Dimension No.", 2);
                    GlobalDimension2.SetRange(Blocked, false);
                    GlobalDimension2.SetRange(GlobalDimension2."Dimension Value Type", GlobalDimension2."Dimension Value Type"::Standard);
                    DimensionValuesListPage.SetTableView(GlobalDimension2);
                    if DimensionValuesListPage.RunModal() = Action::LookupOK then begin
                        DimensionValuesListPage.GetRecord(GlobalDimension2);
                        Rec.Validate("Shortcut Dimension 2 Code", GlobalDimension2.Code);
                        Rec."Kostenträger Code" := GlobalDimension2.Code;
                        CurrPage.Update(TRUE);
                    end
                    else begin
                        Rec."Kostenträger Code" := '';
                        Rec.Validate("Shortcut Dimension 2 Code", '');
                        CurrPage.Update(TRUE);
                    end;

                end;

            }
        }

    }
}