pageextension 50004 VendorLedgerEntries extends "Vendor Ledger Entries"
{
    layout
    {
        addafter("User ID")
        {
            field(SystemCreatedAt; SystemCreatedAt)
            {
                Caption = 'Created At';
                ApplicationArea = All;
            }
        }

        modify("User ID")
        {
            Caption = 'Created By';
            ApplicationArea = All;
            Visible = true;
        }

        modify("Source Code")
        {
            Visible = true;
        }
    }
}
