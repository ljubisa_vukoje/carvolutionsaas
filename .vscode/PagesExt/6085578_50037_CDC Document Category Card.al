pageextension 50037 "CDC Document Category Card" extends "CDC Document Category Card"
{
    layout
    {
        addafter(Codeunits)
        {
            group(Extensions)
            {
                Caption = 'Extensions';

                field(SwissQrCodeProccessing; Rec.SwissQrCodeProccessing)
                {
                    ApplicationArea = All;
                    Caption = 'Automatic Swiss QR code processing';
                    ToolTip = 'Automatic Swiss QR code processing';
                }
            }

        }
    }
}