pageextension 50019 SRSetup extends "Sales & Receivables Setup"
{

    layout
    {
        addafter("Number Series")
        {
            group("Credit Cards")
            {
                CaptionML = ENU = 'Credit Cards', DES = 'Kreditkarten';

                field("Creditcard Clearing New"; Rec."Creditcard Clearing New")
                {
                    ApplicationArea = all;
                }

                field("Auxiliary Account Creditcard"; Rec."Auxiliary Account Creditcard")
                {
                    ApplicationArea = all;

                }
                field("G/L Account VISA"; Rec."G/L Account VISA")
                {
                    ApplicationArea = all;
                }
                field("G/L Account Mastercard"; Rec."G/L Account Mastercard")
                {
                    ApplicationArea = all;
                }


            }
        }

    }

}