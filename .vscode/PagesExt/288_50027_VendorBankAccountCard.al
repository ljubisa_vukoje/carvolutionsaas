pageextension 50027 VendorBankAccountCard extends "Vendor Bank Account Card"
{
    layout
    {
        addafter(Code)
        {
            field("Payment Form Enum"; Rec."Payment Form Enum")
            {
                ApplicationArea = All;
            }
        }
        /*modify(IBAN)
        {
            trigger OnAfterValidate()
            var
                BankDirectory: Record "Bank Directory";
                clearingNoCOde: Code[5];
                ClearingNo: Integer;
                IbanNO: Text;
                DummyOK: Boolean;
                VendorLE: Record "Vendor Bank Account";

            begin

                if IBAN = '' then begin
                    "SWIFT Code" := '';
                    "Clearing No." := '';
                    Modify(true);
                    exit;
                end;

                if "Payment Form" <> "Payment Form"::"Bank Payment Domestic" then begin
                    "SWIFT Code" := '';
                    "Clearing No." := '';
                    Modify(true);
                    exit;
                end;

                IbanNo := IBAN;
                IbanNO := DelChr(IbanNO, '=', ' ');
                clearingNoCOde := CopyStr(IbanNO, 5, 5);
                DummyOK := EVALUATE(ClearingNo, clearingNoCOde);

                BankDirectory.Reset();
                BankDirectory.SetRange("Clearing No.", FORMAT(ClearingNo));
                IF BankDirectory.FindFirst() then begin
                    "SWIFT Code" := BankDirectory."SWIFT Address";
                    Validate("Clearing No.", BankDirectory."Clearing No.");
                    Modify(true);
                end
                else begin
                    "SWIFT Code" := '';
                    "Clearing No." := '';
                    Modify(true);
                end;


            end;
        }*/
    }




    actions
    {
        addfirst(Processing)
        {
            action("Update Bank Acc. QR Type 27 digit Ref.No.")
            {
                CaptionML = ENU = 'Update Bank Acc. QR Type 27 digit Ref.No.', DES = 'Bankkonto aktualisieren QR Typ 27-stellig Ref.Nr.';
                Promoted = true;
                PromotedCategory = Process;
                Image = Process;
                ApplicationArea = All;
                trigger OnAction()
                var
                    BankAcc: Record "Vendor Bank Account";

                begin

                    BankAcc.Reset();
                    BankAcc.SetRange(Code, 'QR1');
                    BankAcc.SetRange("ESR Type", Rec."ESR Type"::"9/27");
                    if BankAcc.Find('-') then begin
                        repeat
                            BankAcc."Payment Form Enum" := BankAcc."Payment Form Enum"::"QR with 27 digit Reference";
                            BankAcc."Clearing No." := '';
                            BankAcc.Modify();

                        until BankAcc.Next() = 0;
                    end;

                    BankAcc.Reset();
                    BankAcc.SetFilter(Code, '<>%1', 'QR1');

                    if BankAcc.Find('-') then begin

                        repeat
                            case BankAcc."Payment Form" of
                                BankAcc."Payment Form"::"Bank Payment Abroad":
                                    BankAcc."Payment Form Enum" := Rec."Payment Form Enum"::"Bank Payment Abroad";
                                BankAcc."Payment Form"::"Bank Payment Domestic":
                                    BankAcc."Payment Form Enum" := Rec."Payment Form Enum"::"Bank Payment Domestic";
                                BankAcc."Payment Form"::"Cash Outpayment Order Abroad":
                                    BankAcc."Payment Form Enum" := Rec."Payment Form Enum"::"Cash Outpayment Order Abroad";
                                BankAcc."Payment Form"::"Cash Outpayment Order Domestic":
                                    BankAcc."Payment Form Enum" := Rec."Payment Form Enum"::"Cash Outpayment Order Domestic";
                                BankAcc."Payment Form"::ESR:
                                    BankAcc."Payment Form Enum" := Rec."Payment Form Enum"::ESR;
                                BankAcc."Payment Form"::"ESR+":
                                    BankAcc."Payment Form Enum" := Rec."Payment Form Enum"::"ESR+";
                                BankAcc."Payment Form"::"Post Payment Abroad":
                                    BankAcc."Payment Form Enum" := Rec."Payment Form Enum"::"Post Payment Abroad";
                                BankAcc."Payment Form"::"Post Payment Domestic":
                                    BankAcc."Payment Form Enum" := Rec."Payment Form Enum"::"Post Payment Domestic";
                                BankAcc."Payment Form"::"SWIFT Payment Abroad":
                                    BankAcc."Payment Form Enum" := Rec."Payment Form Enum"::"SWIFT Payment Abroad";
                            end;
                            BankAcc.Modify();

                        until BankAcc.Next() = 0;

                    end;
                    Message('FINISHED !');

                end;
            }

        }
    }


}

