pageextension 50022 CustLedgEntry extends "Customer Ledger Entries"
{

    layout
    {
        addafter("On Hold")
        {
            field("Payment Reference"; Rec."Payment Reference")
            {
                ApplicationArea = all;

            }
            field("Debt Collection"; Rec."Debt Collection")
            {
                ApplicationArea = all;
                Editable = true;


            }
            field("Transfered to Debt Collection"; Rec."Transfered to Debt Collection")
            {
                ApplicationArea = all;
                Editable = true;
            }
            field("Last Issued Reminder Level"; Rec."Last Issued Reminder Level")
            {
                ApplicationArea = all;
                Editable = false;
            }
            field("Exclude From Reminder"; Rec."Exclude From Reminder")
            {
                ApplicationArea = all;
                Editable = true;
            }

            field("End Date of the Dunning Stop"; Rec."End Date of the Dunning Stop")
            {
                ApplicationArea = all;
                Editable = true;
            }

            field(UserRSId; Rec.UserRSId)
            {
                ApplicationArea = all;
                Editable = true;
            }

            field("Reminder Stop Notes"; Rec."Reminder Stop Notes")
            {
                ApplicationArea = all;
                Editable = true;
            }



        }

        addafter("User ID")
        {
            field(SystemCreatedAt; SystemCreatedAt)
            {
                Caption = 'Created At';
                ApplicationArea = All;
            }
        }

        modify("User ID")
        {
            Caption = 'Created By';
            ApplicationArea = All;
            Visible = true;
        }

        modify("Source Code")
        {
            Visible = true;
        }

    }

}