pageextension 50034 VatStatement extends "VAT Statement"
{

    actions
    {
        addafter(GLVATReconciliation)
        {
            action(PopulateVatEntryBlankGlAccNo)
            {
                Caption = 'Populate Blank GlAccount No.';
                Image = Accounts;
                ApplicationArea = all;

                trigger OnAction()
                var
                    CUVatEntry: Codeunit VatEntry;
                begin
                    CLEAR(CUVatEntry);
                    CUVatEntry.UpdateGlAccountyOnVatEntryBlankValues();
                end;


            }
        }
    }
}