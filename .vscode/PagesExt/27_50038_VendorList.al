pageextension 50038 "VendorListPageExt" extends "Vendor List"
{
    layout
    {
        addafter(Contact)
        {
            field("Preferred Bank Account Code"; Rec."Preferred Bank Account Code")
            {
                Visible = IsVisible;
                ApplicationArea = All;
            }
        }

    }
    actions
    {
        addafter(ApplyTemplate)
        {
            action("Update SWIFT")
            {
                Image = action;
                Caption = 'Temporary SWIFT update';
                ApplicationArea = all;

                trigger OnAction()
                var
                    BankDirectory: Record "Bank Directory";
                    clearingNoCOde: Code[5];
                    ClearingNo: Integer;
                    IbanNO: Text;
                    QRIbanNO: Text;
                    DummyOK: Boolean;
                    VendorLE: Record "Vendor Bank Account";
                    QRIbanSwiftBank: record "QR Iban Swift Bank List";
                    VendorBankRec: Record "Vendor Bank Account";
                begin
                    VendorBankRec.Reset();
                    VendorBankRec.SetFilter(IBAN, '<>%1', '');
                    VendorBankRec.SetRange("Payment Form", 3);
                    VendorBankRec.find('-');

                    repeat
                        IbanNo := VendorBankRec.IBAN;
                        IbanNO := DelChr(IbanNO, '=', ' ');
                        QRIbanNO := CopyStr(IbanNO, 5, 1);

                        clearingNoCOde := CopyStr(IbanNO, 5, 5);
                        DummyOK := EVALUATE(ClearingNo, clearingNoCOde);

                        if (QRIbanNO = '3')
                        then begin

                            QRIbanSwiftBank.Reset();
                            QRIbanSwiftBank.SetRange("Bank Code", clearingNoCOde);
                            If QRIbanSwiftBank.FindFirst() then begin
                                VendorBankRec."SWIFT Code" := QRIbanSwiftBank."Swift Code";
                                //Vukoje Ljubisa 11.08.2022
                                //if we populate ClearingNoCode 
                                //Suggested Swiss SEPA method is 2.2 and should be 3
                                /*
                                BankDirectory.Reset();
                                BankDirectory.SetRange("Swift address", QRIbanSwiftBank."Swift Code");
                                IF BankDirectory.FindFirst() then begin
                                    VendorBankRec.Validate(VendorBankRec."Clearing No.", BankDirectory."Clearing Main Office");

                                end;*/
                                VendorBankRec.Modify(true);
                            end

                        end
                        else begin
                            BankDirectory.Reset();
                            BankDirectory.SetRange("Clearing No.", FORMAT(ClearingNo));
                            IF BankDirectory.FindFirst() then begin
                                VendorBankRec."SWIFT Code" := BankDirectory."SWIFT Address";
                                //VendorBankRec.Validate(VendorBankRec."Clearing No.", BankDirectory."Clearing No.");
                                VendorBankRec.Modify(true);
                            end;
                        end;


                        VendorBankRec.Validate(VendorBankRec."Clearing No.", '');
                        VendorBankRec.Modify(true);
                    until VendorBankRec.Next() = 0;

                    Message('Update finished.');
                end;
            }

        }
    }
    trigger OnOpenPage()
    begin
        CompInformation.SetIsVisible(IsVisible);
    end;

    var
        CompInformation: Record "Company Information";
        IsVisible: Boolean;
}