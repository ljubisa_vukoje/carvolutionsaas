pageextension 50017 ExtESRSetup extends "ESR Setup"
{

    layout
    {

        addafter("BESR Customer ID")
        {
            /// <summary>
            /// ISO20022 BEGIN
            /// </summary>
            field("IBAN Recipient"; Rec."IBAN Recipient")
            {
                ApplicationArea = all;
            }
            /// <summary>
            /// ISO20022 END
            /// </summary>

        }


        addafter(Member)
        {
            group(ISO)
            {
                Caption = 'ISO';
                field("ESRFilename"; Rec."ESR Filename")
                {
                    ApplicationArea = all;
                }
                field("BackupCopy"; Rec."Backup Copy")
                {
                    ApplicationArea = all;
                }
                field(BF; Rec."Backup Folder")
                {
                    ApplicationArea = all;
                }
                field("LastBackupNo."; Rec."Last Backup No.")
                {
                    ApplicationArea = all;
                }

            }
        }

    }
}






