pageextension 50003 "20_50003_General Ledger Entry" extends "General Ledger Entries"
{
    layout
    {
        addafter("Source No.")
        {
            field("System-Created Entry"; "System-Created Entry")
            {
                Caption = 'System-Created Entry';
                ApplicationArea = All;
            }
            field(SystemCreatedAt; SystemCreatedAt)
            {
                Caption = 'Created At';
                ApplicationArea = All;
            }
        }

        modify("User ID")
        {
            Caption = 'Created By';
            ApplicationArea = All;
            Visible = true;
        }

        modify("Source Code")
        {
            Caption = 'Source Code';
            ApplicationArea = All;
            Visible = true;
        }

        modify("Source Type")
        {
            Caption = 'Source Type';
            ApplicationArea = All;
            Visible = true;
        }
        modify("Source No.")
        {
            Caption = 'Source No.';
            ApplicationArea = All;
            Visible = true;
        }


    }

}