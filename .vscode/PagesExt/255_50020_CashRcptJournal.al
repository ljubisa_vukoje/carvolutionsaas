pageextension 50200 CashRcptJournal extends "Cash Receipt Journal"
{
    CaptionML = ENU = 'Cash Receipt Journal', DES = 'Zahlungseingangs Erf.-Journal', ITS = 'Registrazioni incassi', FRS = 'Feuille rÕglement';

    actions
    {
        addafter("Read ESR File")
        {
            action("Credit Card Data Import")
            {
                CaptionML = ENU = 'Credit Card Data Import', DES = 'Kreditkartendatei Datatrans importieren';
                Promoted = true;
                PromotedCategory = Process;
                Image = Import;
                ApplicationArea = all;
                trigger OnAction()
                var
                    ImportCreditcardFile: Report "Import Carvolution CC File";
                begin
                    CLEAR(ImportCreditcardFile);
                    ImportCreditcardFile.DefJnlLine(Rec);
                    ImportCreditcardFile.RUNMODAL;
                    CurrPage.UPDATE(FALSE);
                end;
            }
        }
    }

}