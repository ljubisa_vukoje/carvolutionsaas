pageextension 50033 CostAccountingSetup extends "Cost Accounting Setup"
{
    layout
    {
        addafter("Last Allocation Doc. No.")
        {
            field("Cost Journal Nos."; Rec."Cost Journal Nos.")
            {
                ApplicationArea = all;
            }
        }
    }

}