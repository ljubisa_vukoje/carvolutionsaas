/// <summary>
/// Vukoje Ljubisa 20.01.2022
/// Importing dimension as Cost Object from Excel File
/// </summary>
pageextension 50029 DimensionValue extends "Dimension Values"
{
    layout
    {
        addafter(Blocked)
        {
            field("Vehicle Status"; Rec."Vehicle_Status")
            {
                ApplicationArea = all;
            }
            field("Vehicle VIN"; Rec."Vehicle VIN")
            {
                ApplicationArea = all;

            }
            field(Vehicle_Category; Rec.Vehicle_Category)
            {
                ApplicationArea = all;
            }
            field(CarvolutionModelID; Rec.CarvolutionModel_ID)
            {
                ApplicationArea = all;
            }
            field(CarvolutionID; Rec.Carvolution_ID)
            {
                ApplicationArea = all;
            }
            field("Licence plate"; Rec."Licence plate")
            {
                ApplicationArea = all;
            }
        }
    }
    actions
    {
        addafter("Where-Used List")
        {
            action(ImportDimensionValuesFromExcel)
            {
                CaptionML = ENU = 'Import Cost Objects From Excel File', DES = 'Importieren Sie Kostenobjekte aus einer Excel-Datei';
                ApplicationArea = All;
                Image = ImportExcel;

                trigger OnAction()
                begin
                    ReadExcelSheet();
                    ImportExcelData();
                end;

            }
        }
    }

    local procedure ReadExcelSheet()
    var
        FileMgt: Codeunit "File Management";
        IStream: InStream;
        FromFile: Text[100];
    begin
        UploadIntoStream(UploadExcelMsg, '', '', FromFile, IStream);
        if FromFile <> '' then begin
            FileName := FileMgt.GetFileName(FromFile);
            SheetName := TempExcelBuffer.SelectSheetsNameStream(IStream);
        end else
            Error(NoFileFoundMsg);
        TempExcelBuffer.Reset();
        TempExcelBuffer.DeleteAll();
        TempExcelBuffer.OpenBookStream(IStream, SheetName);
        TempExcelBuffer.ReadSheet();
    end;

    local procedure ImportExcelData()
    var

        RowNo: Integer;
        ColNo: Integer;
        LineNo: Integer;
        MaxRowNo: Integer;
        Name: Text;
        Blocked: Boolean;
        ModelName: Text;
        BrandName: Text;
        CarvolutionID: Code[10];
        ModelID: Code[10];
        VehicleCategory: Code[30];
        VIN: Code[25];
        VehicleStatus: Code[30];
        DimensionValues: Record "Dimension Value";
        DimensionValues2: Record "Dimension Value";
        ShortBrand: Code[3];
        ShortBrandModify: Code[3];
        ShortModelName: Code[12];
        CostObjectCode: Code[20];
        DummyName: Text;
        DummyObjectCode: Code[20];
        LicencePlate: Code[20];
    begin

        RowNo := 0;
        ColNo := 0;
        MaxRowNo := 0;


        TempExcelBuffer.Reset();
        if TempExcelBuffer.FindLast() then begin
            MaxRowNo := TempExcelBuffer."Row No.";
        end;

        for RowNo := 2 to MaxRowNo do begin
            LineNo := LineNo + 10000;
            Name := GetValueAtCell(RowNo, 1);
            Evaluate(Blocked, GetValueAtCell(RowNo, 2));
            ModelName := GetValueAtCell(RowNo, 3);
            BrandName := GetValueAtCell(RowNo, 4);
            Evaluate(CarvolutionID, GetValueAtCell(RowNo, 5));
            Evaluate(ModelID, GetValueAtCell(RowNo, 6));
            Evaluate(VehicleCategory, GetValueAtCell(RowNo, 7));
            VIN := GetValueAtCell(RowNo, 8);
            Evaluate(VehicleStatus, GetValueAtCell(RowNo, 9));
            Evaluate(LicencePlate, GetValueAtCell(RowNo, 10));

            if StrLen(BrandName) > 3 then
                ShortBrand := CopyStr(BrandName, 1, 3)
            else
                ShortBrand := BrandName;

            if StrLen(ModelName) > 8 then
                ShortModelName := CopyStr(ModelName, 1, 8)
            else
                ShortModelName := ModelName;

            CostObjectCode := ShortBrand + ' ' + ShortModelName;


            DimensionValues.RESET;
            DimensionValues.SetRange("Dimension Code", 'KOSTENTRÄGER');
            DimensionValues.SetRange(Code, ShortBrand);
            DimensionValues.SetRange("Dimension Value Type", DimensionValues."Dimension Value Type"::"Begin-Total");
            if not DimensionValues.FindFirst() then begin
                DimensionValues.Init();
                DimensionValues.Validate("Dimension Code", 'KOSTENTRÄGER');
                DimensionValues.Validate(Code, ShortBrand);
                DimensionValues."Brand Name" := BrandName;
                DimensionValues.Name := BrandName + ' ANFANG';
                DimensionValues.Validate("Dimension Value Type", DimensionValues."Dimension Value Type"::"Begin-Total");
                DimensionValues.Insert();

                DimensionValues.Init();
                DimensionValues.Validate("Dimension Code", 'KOSTENTRÄGER');
                DimensionValues.Validate(Code, ShortBrand + '-ZZ');
                DimensionValues."Brand Name" := BrandName;
                DimensionValues.Name := BrandName + ' ENDE';
                DimensionValues.Validate("Dimension Value Type", DimensionValues."Dimension Value Type"::"End-Total");
                DimensionValues.Validate("Global Dimension No.", 2);
                DimensionValues.Insert();


            end;


            DimensionValues.Reset();
            DimensionValues.SetRange(CarvolutionModel_ID, ModelID);
            if not DimensionValues.Find('-') then begin


                DimensionValues.Init();
                DimensionValues.Validate("Dimension Code", 'KOSTENTRÄGER');
                if StrLen(Format(ModelID)) >= 3 then
                    DimensionValues.Validate(Code, ShortBrand + '-' + Format(ModelID))
                else begin
                    if StrLen(Format(ModelID)) = 2 then
                        DimensionValues.Validate(Code, ShortBrand + '-' + '0' + Format(ModelID))
                    else
                        DimensionValues.Validate(Code, ShortBrand + '-' + '00' + Format(ModelID));
                end;

                DimensionValues.Validate("Dimension Value Type", DimensionValues."Dimension Value Type"::"Begin-Total");
                DimensionValues.CarvolutionModel_ID := ModelID;
                DimensionValues."Brand Name" := BrandName;
                DummyName := BrandName + ' ' + ModelName;
                if StrLen(DummyName) > 43 then
                    DummyName := CopyStr(DummyName, 1, 43);
                DimensionValues.Name := DummyName + ' ANFANG';
                DimensionValues.Insert();

                DimensionValues.Init();
                DimensionValues.Validate("Dimension Code", 'KOSTENTRÄGER');
                if StrLen(Format(ModelID)) >= 3 then
                    DimensionValues.Validate(Code, ShortBrand + '-' + Format(ModelID) + '-ZZ')
                else begin
                    if StrLen(Format(ModelID)) = 2 then
                        DimensionValues.Validate(Code, ShortBrand + '-' + '0' + Format(ModelID) + '-ZZ')
                    else
                        DimensionValues.Validate(Code, ShortBrand + '-' + '00' + Format(ModelID) + '-ZZ')
                end;

                DimensionValues.Validate("Dimension Value Type", DimensionValues."Dimension Value Type"::"End-Total");
                DimensionValues.CarvolutionModel_ID := ModelID;
                DimensionValues."Brand Name" := BrandName;
                DimensionValues.Name := DummyName + ' ENDE';
                DimensionValues.Validate("Global Dimension No.", 2);
                DimensionValues.Insert();

            end;

            DimensionValues.Reset();
            DimensionValues.SetRange(Carvolution_ID, CarvolutionID);
            if not DimensionValues.FindFirst() then begin

                DummyObjectCode := CostObjectCode + '-' + Format(CarvolutionID);

                DimensionValues.Init();
                DimensionValues.Validate("Dimension Code", 'KOSTENTRÄGER');
                if StrLen(Format(ModelID)) >= 3 then
                    DimensionValues.Validate(Code, ShortBrand + '-' + Format(ModelID) + '-' + Format(CarvolutionID))
                else begin
                    if StrLen(Format(ModelID)) = 2 then
                        DimensionValues.Validate(Code, ShortBrand + '-' + '0' + Format(ModelID) + '-' + Format(CarvolutionID))
                    else
                        DimensionValues.Validate(Code, ShortBrand + '-' + '00' + Format(ModelID) + '-' + Format(CarvolutionID))
                end;

                DimensionValues.Validate("Dimension Value Type", DimensionValues."Dimension Value Type"::Standard);
                DimensionValues.CarvolutionModel_ID := ModelID;
                DimensionValues.Carvolution_ID := CarvolutionID;
                DimensionValues."Brand Name" := BrandName;
                DimensionValues."Model Name" := ModelName;
                DimensionValues."Vehicle_Category" := VehicleCategory;
                DimensionValues."Vehicle_Status" := VehicleStatus;
                DimensionValues."Vehicle VIN" := VIN;
                DimensionValues."Licence plate" := LicencePlate;
                if StrLen(Name) > 50 then
                    Name := CopyStr(Name, 1, 50);
                DimensionValues.Name := Name;
                DimensionValues.Validate(Blocked, Blocked);
                DimensionValues.Validate("Global Dimension No.", 2);
                DimensionValues.Insert();

            end
            else begin

                ///IF Carvolution ModelID change
                if ModelID <> DimensionValues.CarvolutionModel_ID then begin
                    ShortBrandModify := ShortBrand;
                    DimensionValues2.Reset();
                    DimensionValues2.SetRange(CarvolutionModel_ID, ModelID);
                    DimensionValues2.SetRange(DimensionValues2."Dimension Value Type", DimensionValues2."Dimension Value Type"::Standard);
                    if DimensionValues2.FindFirst() then
                        ShortBrandModify := CopyStr(DimensionValues2.Code, 1, 3);

                    if StrLen(Format(ModelID)) >= 3 then
                        DimensionValues.Rename('KOSTENTRÄGER', ShortBrandModify + '-' + Format(ModelID) + '-' + Format(CarvolutionID))
                    else begin
                        if StrLen(Format(ModelID)) = 2 then
                            DimensionValues.Rename('KOSTENTRÄGER', ShortBrandModify + '-' + '0' + Format(ModelID) + '-' + Format(CarvolutionID))
                        else
                            DimensionValues.Rename('KOSTENTRÄGER', ShortBrandModify + '-' + '00' + Format(ModelID) + '-' + Format(CarvolutionID));
                    end;
                end;

                DimensionValues.SetRange(Carvolution_ID, CarvolutionID);
                DimensionValues.FindFirst();
                DimensionValues.CarvolutionModel_ID := ModelID;
                DimensionValues.Blocked := Blocked;
                DimensionValues."Vehicle VIN" := VIN;
                DimensionValues."Vehicle_Status" := VehicleStatus;
                DimensionValues."Vehicle_Category" := VehicleCategory;
                DimensionValues."Model Name" := ModelName;
                DimensionValues."Brand Name" := BrandName;
                DimensionValues."Licence plate" := LicencePlate;
                if StrLen(Name) > 50 then
                    Name := CopyStr(Name, 1, 50);
                DimensionValues.Name := Name;
                DimensionValues.Validate("Global Dimension No.", 2);
                DimensionValues.Modify();

                CostObjects.Reset();
                CostObjects.SetRange(Code, DimensionValues.Code);
                IF CostObjects.Find('-') THEN begin
                    CostObjects.Blocked := Blocked;
                    CostObjects.Modify();
                end;
            end;

        end;
        Message(ExcelImportSucess);
    end;

    local procedure GetValueAtCell(RowNo: Integer; ColNo: Integer): Text
    begin

        TempExcelBuffer.Reset();
        If TempExcelBuffer.Get(RowNo, ColNo) then
            exit(TempExcelBuffer."Cell Value as Text")
        else
            exit('');
    end;

    var
        BatchName: Code[10];
        FileName: Text[100];
        SheetName: Text[100];
        DimensionValues: Record "Dimension Value";
        CostObjects: Record "Cost Object";

        TempExcelBuffer: Record "Excel Buffer" temporary;
        UploadExcelMsg: Label 'Please Choose the Excel file.';
        NoFileFoundMsg: Label 'No Excel file found!';
        BatchISBlankMsg: Label 'Batch name is blank';
        ExcelImportSucess: Label 'Excel is successfully imported.';

}