pageextension 50030 CostObjectCard extends "Cost Object Card"
{
    layout
    {
        addafter(Blocked)
        {
            field(CarvolutionID; CarvolutionID)
            {
                Caption = 'Carvolution ID';
                ApplicationArea = all;

            }
            field(CarvolutionModelID; CarvolutionModelID)
            {
                Caption = 'Carvolution Model ID';
                ApplicationArea = all;

            }
            field(VehicleCategory; VehicleCategory)
            {
                CaptionML = ENU = 'Vehicle Category', DES = 'Fahrzeugkategorie';
                ApplicationArea = all;

            }
            field(VehicleStatus; VehicleStatus)
            {
                CaptionML = ENU = 'Vehicle Status', DES = 'Fahrzeugstatus';
                ApplicationArea = all;

            }
            field(VIN; Rec.VIN)
            {
                CaptionML = ENU = 'VIN', DES = 'VIN';
                ApplicationArea = all;

            }
            field("Licence plate"; LicencePlate)
            {
                CaptionML = ENU = 'Licence plate', DES = 'Nummernschild';
                ApplicationArea = all;

            }
        }
    }
    trigger OnAfterGetRecord()
    var
        DimensionValue: Record "Dimension Value";
    begin
        DimensionValue.Reset();
        DimensionValue.SetRange(Code, Rec.Code);
        if DimensionValue.FindFirst() then begin
            Rec.VIN := DimensionValue."Vehicle VIN";
            VehicleStatus := DimensionValue."Vehicle_Status";
            CarvolutionID := DimensionValue.Carvolution_ID;
            CarvolutionMOdelID := DimensionValue.CarvolutionModel_ID;
            VehicleCategory := DimensionValue."Vehicle_Category";
            LicencePlate := DimensionValue."Licence plate";
        end
        else begin
            Rec.VIN := '';
            VehicleStatus := '';
            VehicleCategory := '';
            CarvolutionID := '0';
            CarvolutionModelID := '0';
            LicencePlate := '';
        end;

    end;

    var
        VIN: COde[30];
        VehicleStatus: Text[30];
        CarvolutionID: Code[10];
        CarvolutionModelID: Code[10];
        VehicleCategory: Text[30];
        LicencePlate: Code[20];
}