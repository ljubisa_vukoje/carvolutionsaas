pageextension 50024 SalesPostedInvoice extends "Posted Sales Invoice"
{

    layout
    {
        addafter("Work Description")
        {
            field(Remark; Rec.Remark)
            {
                Editable = true;
                ApplicationArea = all;
                MultiLine = true;


            }
        }

    }

    actions
    {
        addafter(Print)
        {
            action("Print QR Invoice with Rem. Amt.")
            {
                ApplicationArea = Basic, Suite;
                Caption = '&Print Invoice Amount';
                Ellipsis = true;
                Image = Print;
                Promoted = true;
                PromotedCategory = Category6;
                ToolTip = 'Prepare to print the document. A report request window for the document opens where you can specify what to include on the print-out.';
                Visible = NOT IsOfficeAddin;

                trigger OnAction()

                begin
                    SalesInvHeader := Rec;
                    CurrPage.SetSelectionFilter(SalesInvHeader);
                    Report.Run(50040, true, true, SalesInvHeader);
                end;

            }
        }
    }
}