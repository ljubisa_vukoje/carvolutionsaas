pageextension 50023 ReminderList extends "Reminder List"
{

    layout
    {
        addafter("Remaining Amount")
        {
            field("Reminder Level"; Rec."Reminder Level")
            {
                ApplicationArea = all;
            }
            field(SalesLCY; SalesLCY)
            {
                CaptionML = ENU = 'Sales LCY', DES = 'Verkauf (MW)';
                ApplicationArea = all;
            }
        }

    }

    trigger OnAfterGetRecord()
    begin
        CLEAR(SalesLCY);
        CLEAR(BalanceLCY);
        IF Cust.GET(Rec."Customer No.") THEN BEGIN
            Cust.CALCFIELDS("Balance (LCY)", "Sales (LCY)");
            SalesLCY := Cust."Sales (LCY)";
            BalanceLCY := Cust."Balance (LCY)"
        END;
    end;

    var

        SalesLCY: Decimal;
        BalanceLCY: Decimal;
        Cust: Record Customer;

}