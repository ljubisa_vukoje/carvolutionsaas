pageextension 50047 CostRegister extends "Cost Registers"
{
    layout
    {
        addafter("Processed Date")
        {
            field("Created From COC"; Rec."Created From COC")
            {
                ApplicationArea = all;
            }
            field("Deleted From COC"; Rec."Deleted From COC")
            {
                ApplicationArea = all;
            }
        }
    }
}