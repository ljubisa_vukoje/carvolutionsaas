
pageextension 50036 DimensionValueList extends "Dimension Value List"
{
    layout
    {
        addafter(Blocked)
        {
            field("Vehicle Status"; Rec."Vehicle_Status")
            {
                ApplicationArea = all;
            }
            field("Vehicle VIN"; Rec."Vehicle VIN")
            {
                ApplicationArea = all;

            }
            field(Vehicle_Category; Rec.Vehicle_Category)
            {
                ApplicationArea = all;
            }
            field(CarvolutionModelID; Rec.CarvolutionModel_ID)
            {
                ApplicationArea = all;
            }
            field(CarvolutionID; Rec.Carvolution_ID)
            {
                ApplicationArea = all;
            }
            field("Licence plate"; Rec."Licence plate")
            {
                ApplicationArea = all;
            }
        }
    }



}