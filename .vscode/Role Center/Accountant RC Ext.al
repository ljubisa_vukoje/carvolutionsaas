pageextension 50021 AccountantRCExt extends "Accountant Role Center"
{


    actions
    {

        addafter("Reconcile Cus&t. and Vend. Accs")
        {
            action("Closed Initial Invoices CV")
            {
                CaptionML = ENU = 'Closed Initial Invoices CV', DEU = 'Bezahlte Anfangsrechnungen Carvolution';
                ApplicationArea = all;
                Image = Report;
                RunObject = Report "Closed Initial Invoices CV";

            }

        }

    }

}