/// <summary>
/// Report 50001_CembraReport (ID 50001).
/// </summary>
report 50001 "Cembra Report"
{
    ApplicationArea = All;
    Caption = 'Cembra Report';
    UsageCategory = ReportsAndAnalysis;
    ProcessingOnly = True;

    dataset
    {
        dataitem(Item; Item)
        {
            trigger OnAfterGetRecord()

            begin
                CalculateItem(Item);
            end;
        }

    }
    requestpage
    {

        SaveValues = true;
        layout
        {
            area(Content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(StartDate; StartDate)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Starting Date';
                        ToolTip = 'Specifies the date from which the report or batch job processes information.';
                    }
                    field(EndDate; EndDate)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Ending Date';
                        ToolTip = 'Specifies the date to which the report or batch job processes information.';
                    }

                }
            }
        }
        actions
        {
            area(processing)
            {
            }
        }
    }

    trigger OnPreReport()
    begin
        rowNo := 2;
        colNo3 := 3;
        colNo4 := 4;

        gla1 := Format(400060);
        gla2 := Format(210020);
        gla3 := Format(320000);

        TotalStartingInvoicedValue := 0;
        TotalStartingInvoicedQty := 0;
        TotalIncreaseInvoiceValue := 0;
        TotalIncreasedInvoicedQty := 0;
        TotalDecreaseInvoicedValue := 0;
        TotalDecreaseInvoicedQty := 0;

        ExcelTemplateList.LookupMode(true);
        ExcelTemplateList.Editable(false);
        if Action::LookupOK = ExcelTemplateList.RunModal() then begin
            ExcelTemplateList.GetRecord(ExcelTemplate);
        end else
            exit;

        if ExcelTemplate.DefaultFileName = '' then
            ExcelTemplate.DefaultFileName := 'Export';

        LastRowNo := ExportToExcel.InitExcel(ExcelTemplate.TemplateName, TempExcelBuffer);

        if (StartDate = 0D) and (EndDate = 0D) then
            EndDate := WorkDate();

        if StartDate in [0D, 00000101D] then
            StartDateText := ''
        else
            StartDateText := Format(StartDate - 1);

        ItemFilter := Item.GetFilters();

        PreviousMonthEndDate := CalcDate('<CM-1M>', EndDate);


    end;

    trigger OnPostReport()
    begin
        AddToHeader(rowNo, StartDate, EndDate);
        AddToBody();
        TempExcelBuffer.WriteAllToCurrentSheet(ExcelBufferSheet);
        TempExcelBuffer.CloseBook();
        TempExcelBuffer.SetFriendlyFilename(ExcelTemplate.DefaultFileName);
        TempExcelBuffer.OpenExcel();
    end;

    var
        StartDate, EndDate, PreviousMonthEndDate : Date;

        ItemFilter: Text;
        StartDateText: Text[10];

        TempExcelBuffer: Record "Excel Buffer" temporary;
        ExcelBufferSheet: record "Excel Buffer" temporary;

        ExportToExcel: Codeunit "Excel Management";
        ExcelTemplate: record "Excel Template Storage";
        ExcelTemplateList: page ExcelTemplateList;


        LastRowNo, rowNo, colNo3, colNo4 : Integer;
        gla1, gla2, gla3 : code[20];


        ValueEntry: Record "Value Entry";
        StartingInvoicedQty, StartingInvoicedValue : Decimal;
        StartingExpectedQty, StartingExpectedValue : Decimal;

        IncreaseInvoicedQty, IncreaseInvoicedValue : Decimal;
        IncreaseExpectedQty, IncreaseExpectedValue : Decimal;

        DecreaseInvoicedQty, DecreaseInvoicedValue : Decimal;
        DecreaseExpectedQty, DecreaseExpectedValue : Decimal;

        TotalStartingInvoicedQty, TotalStartingInvoicedValue : Decimal;
        TotalIncreasedInvoicedQty, TotalIncreaseInvoiceValue : Decimal;
        TotalDecreaseInvoicedQty, TotalDecreaseInvoicedValue : Decimal;


        InvCostPostedToGL: Decimal;
        CostPostedToGL: Decimal;
        ExpCostPostedToGL: Decimal;
        IsEmptyLine: Boolean;


    local procedure AddToHeader(rowNo: Integer; inputStartDate: Date; inputEndDate: Date)
    var
    begin
        ExcelBufferSheet.NumberFormat := 'Çustom';
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo - 1, colNo3, 'As per ' + formatDate(inputEndDate), true, false, false);
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo + 1, colNo4 + 2, formatDate(inputStartDate), true, false, false);
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo + 1, colNo4 + 3, formatDate(inputEndDate), true, false, false);
    end;

    local procedure AddToBody()
    var
        decValue: Decimal;
    begin
        rowNo += 1;
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo3, TotalStartingInvoicedQty + TotalIncreasedInvoicedQty - TotalDecreaseInvoicedQty, false, false, false);

        rowNo += 1;
        Evaluate(decValue, (FORMAT((TotalStartingInvoicedValue + TotalIncreaseInvoiceValue - TotalDecreaseInvoicedValue), 0, '<Precision,2:2><Standard Format,2>')));
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo3, decValue, false, false, false);

        rowNo += 1;
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo4, TotalIncreasedInvoicedQty, false, false, false);

        rowNo += 1;
        Evaluate(decValue, (FORMAT(TotalIncreaseInvoiceValue, 0, '<Precision,2:2><Standard Format,2>')));
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo4, decValue, false, false, false);

        rowNo += 1;
        Evaluate(decValue, (FORMAT(getTotalAmountGLA(gla3, StartDate, EndDate), 0, '<Precision,2:2><Standard Format,2>')));
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo4, decValue, false, false, false);

        rowNo += 1;
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo4, TotalDecreaseInvoicedQty, false, false, false);

        rowNo += 1;
        Evaluate(decValue, FORMAT(TotalDecreaseInvoicedValue, 0, '<Precision,2:2><Standard Format,2>'));
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo4, decValue, false, false, false);

        rowNo += 1;
        Evaluate(decValue, (FORMAT(getTotalAmountGLA(gla1, StartDate, EndDate), 0, '<Precision,2:2><Standard Format,2>')));
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo4, decValue, false, false, false);

        rowNo += 5;
        Evaluate(decValue, (FORMAT(getQtyNegativeAmounts(gla2, StartDate, EndDate), 0, '<Precision,2:2><Standard Format,2>')));
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo4, decValue, false, false, false);

        rowNo += 1;
        Evaluate(decValue, (FORMAT(getQtyPositiveAmounts(gla2, StartDate, EndDate), 0, '<Precision,2:2><Standard Format,2>')));
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo4, decValue, false, false, false);

        rowNo += 1;
        Evaluate(decValue, (FORMAT(GetTotAmntNegativeAmounts(gla2, StartDate, EndDate), 0, '<Precision,2:2><Standard Format,2>')));
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo4, decValue, false, false, false);


        rowNo += 1;
        Evaluate(decValue, (FORMAT(GetTotAmntPositiveAmounts(gla2, StartDate, EndDate), 0, '<Precision,2:2><Standard Format,2>')));
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNo4, decValue, false, false, false);
    end;

    local procedure GetTotalAmountGLA(gla: Code[20]; StartDate: Date; EndDate: Date): Decimal
    var
        gle: Record "G/L Entry";
        amount: Decimal;
    begin
        amount := 0;
        gle.SetFilter("G/L Account No.", gla);
        gle.SetRange("Posting Date", StartDate, EndDate);

        if gle.FindFirst() then
            repeat
                amount += gle.Amount;
            until gle.next = 0;
        exit(amount);
    end;

    local procedure GetQtyNegativeAmounts(gla: Code[20]; StartDate: Date; EndDate: Date): Integer
    var
        gle: Record "G/L Entry";

    begin
        gle.SetFilter("G/L Account No.", gla);
        gle.SetRange("Posting Date", StartDate, EndDate);
        gle.SetFilter(Amount, '<%1', 0);
        exit(gle.Count);
    end;

    local procedure GetQtyPositiveAmounts(gla: Code[20]; StartDate: Date; EndDate: Date): Integer
    var
        gle: Record "G/L Entry";

    begin
        gle.SetFilter("G/L Account No.", gla);
        gle.SetRange("Posting Date", StartDate, EndDate);
        gle.SetFilter(Amount, '>%1', 0);
        exit(gle.Count);
    end;

    local procedure GetTotAmntNegativeAmounts(gla: Code[20]; StartDate: Date; EndDate: Date): Decimal
    var
        gle: Record "G/L Entry";
        amount: Decimal;

    begin
        amount := 0;
        gle.SetFilter("G/L Account No.", gla);
        gle.SetRange("Posting Date", StartDate, EndDate);
        gle.SetFilter(Amount, '<%1', 0);
        if gle.FindFirst() then
            repeat
                amount += gle.Amount;
            until gle.next = 0;
        exit(abs(amount));
    end;

    local procedure GetTotAmntPositiveAmounts(gla: Code[20]; StartDate: Date; EndDate: Date): Decimal
    var
        gle: Record "G/L Entry";
        amount: Decimal;

    begin
        amount := 0;
        gle.SetFilter("G/L Account No.", gla);
        gle.SetRange("Posting Date", StartDate, EndDate);
        gle.SetFilter(Amount, '>%1', 0);
        if gle.FindFirst() then
            repeat
                amount += gle.Amount;
            until gle.next = 0;
        exit(amount);
    end;

    local procedure FormatDate(InputDate: Date): Text
    var
        newDate: Text;
        day, month, year : Integer;
    begin
        day := Date2DMY(InputDate, 1);
        month := Date2DMY(InputDate, 2);
        year := Date2DMY(InputDate, 3);
        newDate := Format(day) + '.' + Format(month) + '.' + Format(year);
        exit(newDate);
    end;

    local procedure AssignAmounts(ValueEntry: Record "Value Entry"; var InvoicedValue: Decimal; var InvoicedQty: Decimal; var ExpectedValue: Decimal; var ExpectedQty: Decimal; Sign: Decimal)
    begin
        InvoicedValue += ValueEntry."Cost Amount (Actual)" * Sign;
        InvoicedQty += ValueEntry."Invoiced Quantity" * Sign;
        ExpectedValue += ValueEntry."Cost Amount (Expected)" * Sign;
        ExpectedQty += ValueEntry."Item Ledger Entry Quantity" * Sign;
    end;

    /// <summary>
    /// CalculateItem.
    /// </summary>
    /// <param name="Item">VAR Record Item.</param>
    procedure CalculateItem(var Item: Record Item)
    begin
        Item.CalcFields("Assembly BOM");

        if EndDate = 0D then
            EndDate := DMY2Date(31, 12, 9999);

        StartingInvoicedValue := 0;
        StartingExpectedValue := 0;
        StartingInvoicedQty := 0;
        StartingExpectedQty := 0;
        IncreaseInvoicedValue := 0;
        IncreaseExpectedValue := 0;
        IncreaseInvoicedQty := 0;
        IncreaseExpectedQty := 0;
        DecreaseInvoicedValue := 0;
        DecreaseExpectedValue := 0;
        DecreaseInvoicedQty := 0;
        DecreaseExpectedQty := 0;

        InvCostPostedToGL := 0;
        CostPostedToGL := 0;
        ExpCostPostedToGL := 0;

        ValueEntry.Reset();
        ValueEntry.SetRange("Item No.", Item."No.");
        ValueEntry.SetFilter("Variant Code", Item.GetFilter("Variant Filter"));
        ValueEntry.SetFilter("Location Code", Item.GetFilter("Location Filter"));
        ValueEntry.SetFilter("Global Dimension 1 Code", Item.GetFilter("Global Dimension 1 Filter"));
        ValueEntry.SetFilter("Global Dimension 2 Code", Item.GetFilter("Global Dimension 2 Filter"));

        if StartDate > 0D then begin
            ValueEntry.SetRange("Posting Date", 0D, CalcDate('<-1D>', StartDate));
            ValueEntry.CalcSums("Item Ledger Entry Quantity", "Cost Amount (Actual)", "Cost Amount (Expected)", "Invoiced Quantity");
            AssignAmounts(ValueEntry, StartingInvoicedValue, StartingInvoicedQty, StartingExpectedValue, StartingExpectedQty, 1);
            IsEmptyLine := IsEmptyLine and ((StartingInvoicedValue = 0) and (StartingInvoicedQty = 0));
        end;

        ValueEntry.SetRange("Posting Date", StartDate, EndDate);
        ValueEntry.SetFilter(
            "Item Ledger Entry Type", '%1|%2|%3|%4',
            ValueEntry."Item Ledger Entry Type"::Purchase,
            ValueEntry."Item Ledger Entry Type"::"Positive Adjmt.",
            ValueEntry."Item Ledger Entry Type"::Output,
            ValueEntry."Item Ledger Entry Type"::"Assembly Output");
        ValueEntry.CalcSums("Item Ledger Entry Quantity", "Cost Amount (Actual)", "Cost Amount (Expected)", "Invoiced Quantity");
        AssignAmounts(ValueEntry, IncreaseInvoicedValue, IncreaseInvoicedQty, IncreaseExpectedValue, IncreaseExpectedQty, 1);

        ValueEntry.SetRange("Posting Date", StartDate, EndDate);
        ValueEntry.SetFilter(
            "Item Ledger Entry Type", '%1|%2|%3|%4',
            ValueEntry."Item Ledger Entry Type"::Sale,
            ValueEntry."Item Ledger Entry Type"::"Negative Adjmt.",
            ValueEntry."Item Ledger Entry Type"::Consumption,
            ValueEntry."Item Ledger Entry Type"::"Assembly Consumption");
        ValueEntry.CalcSums("Item Ledger Entry Quantity", "Cost Amount (Actual)", "Cost Amount (Expected)", "Invoiced Quantity");
        AssignAmounts(ValueEntry, DecreaseInvoicedValue, DecreaseInvoicedQty, DecreaseExpectedValue, DecreaseExpectedQty, -1);

        ValueEntry.SetRange("Posting Date", StartDate, EndDate);
        ValueEntry.SetRange("Item Ledger Entry Type", ValueEntry."Item Ledger Entry Type"::Transfer);
        if ValueEntry.FindSet() then
            repeat
                if true in [ValueEntry."Valued Quantity" < 0, not GetOutboundItemEntry(ValueEntry."Item Ledger Entry No.")] then
                    AssignAmounts(ValueEntry, DecreaseInvoicedValue, DecreaseInvoicedQty, DecreaseExpectedValue, DecreaseExpectedQty, -1)
                else
                    AssignAmounts(ValueEntry, IncreaseInvoicedValue, IncreaseInvoicedQty, IncreaseExpectedValue, IncreaseExpectedQty, 1);
            until ValueEntry.Next() = 0;


        ValueEntry.SetRange("Posting Date", 0D, EndDate);
        ValueEntry.SetRange("Item Ledger Entry Type");
        ValueEntry.CalcSums("Cost Posted to G/L", "Expected Cost Posted to G/L");
        ExpCostPostedToGL += ValueEntry."Expected Cost Posted to G/L";
        InvCostPostedToGL += ValueEntry."Cost Posted to G/L";

        StartingExpectedValue += StartingInvoicedValue;
        IncreaseExpectedValue += IncreaseInvoicedValue;
        DecreaseExpectedValue += DecreaseInvoicedValue;
        CostPostedToGL := ExpCostPostedToGL + InvCostPostedToGL;

        TotalStartingInvoicedValue += StartingInvoicedValue;
        TotalStartingInvoicedQty += StartingInvoicedQty;
        TotalIncreaseInvoiceValue += IncreaseInvoicedValue;
        TotalIncreasedInvoicedQty += IncreaseInvoicedQty;
        TotalDecreaseInvoicedValue += DecreaseInvoicedValue;
        TotalDecreaseInvoicedQty += DecreaseInvoicedQty;
    end;

    local procedure GetOutboundItemEntry(ItemLedgerEntryNo: Integer): Boolean
    var
        ItemApplnEntry: Record "Item Application Entry";
        ItemLedgEntry: Record "Item Ledger Entry";
    begin
        ItemApplnEntry.SetCurrentKey("Item Ledger Entry No.");
        ItemApplnEntry.SetRange("Item Ledger Entry No.", ItemLedgerEntryNo);
        if not ItemApplnEntry.FindFirst() then
            exit(true);

        ItemLedgEntry.SetRange("Item No.", Item."No.");
        ItemLedgEntry.SetFilter("Variant Code", Item.GetFilter("Variant Filter"));
        ItemLedgEntry.SetFilter("Location Code", Item.GetFilter("Location Filter"));
        ItemLedgEntry.SetFilter("Global Dimension 1 Code", Item.GetFilter("Global Dimension 1 Filter"));
        ItemLedgEntry.SetFilter("Global Dimension 2 Code", Item.GetFilter("Global Dimension 2 Filter"));
        ItemLedgEntry."Entry No." := ItemApplnEntry."Outbound Item Entry No.";
        exit(ItemLedgEntry.IsEmpty());
    end;

    /// <summary>
    /// SetStartDate.
    /// </summary>
    /// <param name="DateValue">Date.</param>
    procedure SetStartDate(DateValue: Date)
    begin
        StartDate := DateValue;
    end;

    /// <summary>
    /// SetEndDate.
    /// </summary>
    /// <param name="DateValue">Date.</param>
    procedure SetEndDate(DateValue: Date)
    begin
        EndDate := DateValue;
    end;

    /// <summary>
    /// InitializeRequest.
    /// </summary>
    /// <param name="NewStartDate">Date.</param>
    /// <param name="NewEndDate">Date.</param>
    procedure InitializeRequest(NewStartDate: Date; NewEndDate: Date)
    begin
        StartDate := NewStartDate;
        EndDate := NewEndDate;
    end;
}
