report 82800 ISOFIle
{

    ProcessingOnly = true;
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = all;
    Caption = 'ISO File';
    Permissions = TableData "Vendor Ledger Entry" = m, TableData "DTA Setup" = m;


    dataset
    {
        dataitem("Gen. Journal Line"; "Gen. Journal Line")
        {


            trigger OnPreDataItem()
            begin

                GlSetup.GET;
                ISOMgt.CheckISOSetup(FileBank);

                SETRANGE("Account Type", "Account Type"::Vendor);
                SETRANGE("Document Type", "Document Type"::Payment);

                SETFILTER(Amount, '<>0');
                SETFILTER("Account No.", '<>%1', '');

                ClearHeaderVAR;
                PaymentCounter := 0;
                ControlSum := ISOMgt.CalcFileTotAmount("Gen. Journal Line");

                NoOfTransactions := "Gen. Journal Line".COUNT;

                IF NoOfTransactions = 0 THEN
                    ERROR(Text001);

                //Vendor Bank Account for Payment
                IF "Gen. Journal Line".FINDFIRST THEN BEGIN
                    REPEAT
                        IF NOT VendBank.GET("Account No.", "Recipient Bank Account") THEN
                            ERROR(Text002, "Recipient Bank Account", "Account No.");
                    UNTIL "Gen. Journal Line".NEXT = 0;
                END;

                //SEPA Curreny exist?
                CLEAR(SEPACurr);
                CLEAR(SEPACurrCode);
                SEPACurrFound := FALSE;
                IF SEPACurr.FINDFIRST THEN BEGIN
                    REPEAT
                        IF SEPACurr."ISO Currency Code" = 'EUR' THEN BEGIN
                            SEPACurrFound := TRUE;
                            SEPACurrCode := SEPACurr.Code;
                        END;
                    UNTIL (SEPACurr.NEXT = 0) OR SEPACurrFound;
                END;

                //Split Journal Lines NoneSEPA and SEPA
                CLEAR(GenJourLine2);
                GenJourLine2.SETCURRENTKEY("Journal Template Name", "Journal Batch Name", "Posting Date", Clearing, "Debit Bank");
                GenJourLine2.COPYFILTERS("Gen. Journal Line");

                CLEAR(GenJourLineSEPA2);
                GenJourLineSEPA2.SETCURRENTKEY("Journal Template Name", "Journal Batch Name", "Posting Date", Clearing, "Debit Bank");
                GenJourLineSEPA2.COPYFILTERS("Gen. Journal Line");

                IF SEPACurrFound THEN BEGIN
                    IF GenJourLineSEPA2.FINDFIRST THEN BEGIN
                        REPEAT
                            IF GenJourLineSEPA2."Currency Code" = SEPACurrCode THEN BEGIN
                                IF VendBank.GET(GenJourLineSEPA2."Account No.", GenJourLineSEPA2."Recipient Bank Account") THEN BEGIN
                                    IF (VendBank."Payment Form" = VendBank."Payment Form"::"SWIFT Payment Abroad") AND
                                       (VendBank.IBAN <> '') AND
                                       (VendBank.SEPA = TRUE)
                                    THEN BEGIN
                                        GenJourLineSEPA2.MARK(TRUE);
                                    END;
                                END;
                            END;
                        UNTIL GenJourLineSEPA2.NEXT = 0;
                        GenJourLineSEPA2.MARKEDONLY(TRUE);
                        IF GenJourLineSEPA2.FINDFIRST THEN;
                    END;

                    IF GenJourLine2.FINDFIRST THEN BEGIN
                        REPEAT
                            IF GenJourLine2."Currency Code" = SEPACurrCode THEN BEGIN
                                IF VendBank.GET(GenJourLine2."Account No.", GenJourLine2."Recipient Bank Account") THEN BEGIN
                                    IF (VendBank."Payment Form" = VendBank."Payment Form"::"SWIFT Payment Abroad") AND
                                       (VendBank.IBAN <> '') AND
                                       (VendBank.SEPA = TRUE)
                                    THEN BEGIN
                                        GenJourLine2.MARK(FALSE);
                                    END ELSE BEGIN
                                        GenJourLine2.MARK(TRUE);
                                    END;
                                END;
                            END ELSE BEGIN
                                GenJourLine2.MARK(TRUE);
                            END;
                        UNTIL GenJourLine2.NEXT = 0;
                        GenJourLine2.MARKEDONLY(TRUE);
                        IF GenJourLine2.FINDFIRST THEN;
                    END;
                END;

                NoOfTransactions := 0;

                FirstPostingDate := ISOMgt.CheckPostingDate("Gen. Journal Line");

                CompanySetup.GET;
                NoSeriesMgt.InitSeries(CompanySetup."Credit Transfer Msg. Nos.", '', 0D, IDD, CompanySetup."Credit Transfer Msg. Nos.");

                MessageID := 'MSG' + IDD;

                xmlDocNodes := ISOMgt.CreateXMLHeader(xmlDoc, root, NoOfTransactions, ControlSum, MessageID);

                d.OPEN(Text003 + Text004);
            end;

        }
        dataitem("GenJourLine"; "Gen. Journal Line")
        {
            DataItemTableView = SORTING("Journal Template Name", "Journal Batch Name", "Posting Date", Clearing, "Debit Bank");
            trigger OnPreDataItem()
            begin
                GenJourLine.COPY(GenJourLine2);
                IF GenJourLine.FINDFIRST THEN BEGIN
                    PaymentInfoID := 'PMTID' + IDD + '1';
                    xmlDocNodes += ISOMgt.AddPaymentInfoToXML(xmlDoc, root, FileBank, PaymentInfoID, FirstPostingDate, FALSE);
                END;
            end;

            trigger OnAfterGetRecord()
            begin
                PaymentCounter := PaymentCounter + 1;

                d.UPDATE(1, PaymentCounter);
                d.UPDATE(2, "Account No.");

                //Vendor Bank Account for Payment
                IF NOT VendBank.GET("Account No.", "Recipient Bank Account") THEN
                    ERROR(Text002, "Recipient Bank Account", "Account No.");

                TotalLCY := TotalLCY + "Amount (LCY)";
                TotalAmt := TotalAmt + Amount;

                // Summary Pmt, if not ESR, same Account, Bank, Debit Bank, Currency
                SummaryPmtAmt := SummaryPmtAmt + Amount;
                PrepareSummaryPmt(GenJourLine);

                //Write ISO Payment - or add amt. for summary pmt.
                NextGlLine.COPY(GenJourLine);
                IF (NextGlLine.NEXT = 0) OR
                   (SummaryPerVendor = FALSE) OR
                   (NextGlLine."Account No." <> "Account No.") OR
                   (NextGlLine."Recipient Bank Account" <> "Recipient Bank Account") OR
                   (NextGlLine."Debit Bank" <> "Debit Bank") OR
                   (NextGlLine."Currency Code" <> "Currency Code") OR
                   (VendBank."Payment Form" IN [VendBank."Payment Form"::ESR, VendBank."Payment Form"::"ESR+"])
                THEN BEGIN
                    AdjustSummaryPmtText(GenJourLine);
                    NoOfTransactions := NoOfTransactions + 1;

                    ISOMgt.CreateIsoPayment(xmlDoc, root, GenJourLine, VendBank,
                                            FirstPostingDate, IDD, PaymentCounter, SummaryPmtAmt, SummaryPmtTxt, FALSE);
                    SummaryPmtAmt := 0;
                    SummaryPmtTxt := '';
                END;

                VendEntry.RESET;
                VendEntry.SETCURRENTKEY("Document No.");
                VendEntry.SETRANGE("Document Type", VendEntry."Document Type"::Invoice);
                VendEntry.SETRANGE("Document No.", "Applies-to Doc. No.");
                VendEntry.SETRANGE("Vendor No.", "Account No.");
                IF VendEntry.FIND('-') THEN BEGIN
                    IF VendEntry."On Hold" = '' THEN BEGIN
                        VendEntry."On Hold" := 'DTA';
                        VendEntry.MODIFY;
                    END;
                END;
            end;

        }
        dataitem("GenJourLineSEPA"; "Gen. Journal Line")
        {
            DataItemTableView = SORTING("Journal Template Name", "Journal Batch Name", "Posting Date", Clearing, "Debit Bank");
            trigger OnPreDataItem()
            begin
                GenJourLineSEPA.COPY(GenJourLineSEPA2);
                IF GenJourLineSEPA.FINDFIRST THEN BEGIN
                    IF SEPACurrFound THEN BEGIN
                        PaymentInfoID := 'PMTID' + IDD + FORMAT(PaymentCounter + 1);
                        ISOMgt.AddPaymentInfoSEPAToXML(xmlDoc, root, FileBank, PaymentInfoID, FirstPostingDate);
                    END;
                END;
            end;

            trigger OnAfterGetRecord()
            begin

                IF NOT SEPACurrFound THEN
                    CurrReport.SKIP;

                PaymentCounter := PaymentCounter + 1;

                d.UPDATE(1, PaymentCounter);
                d.UPDATE(2, "Account No.");

                //Vendor Bank Account for Payment
                IF NOT VendBank.GET("Account No.", "Recipient Bank Account") THEN
                    ERROR(Text002, "Recipient Bank Account", "Account No.");

                TotalLCY := TotalLCY + "Amount (LCY)";
                TotalAmt := TotalAmt + Amount;

                // Summary Pmt, if not ESR, same Account, Bank, Debit Bank, Currency
                SummaryPmtAmt := SummaryPmtAmt + Amount;
                PrepareSummaryPmt(GenJourLineSEPA);

                //Write ISO Payment - or add amt. for summary pmt.
                NextGlLine.COPY(GenJourLineSEPA);
                IF (NextGlLine.NEXT = 0) OR
                   (SummaryPerVendor = FALSE) OR
                   (NextGlLine."Account No." <> "Account No.") OR
                   (NextGlLine."Recipient Bank Account" <> "Recipient Bank Account") OR
                   (NextGlLine."Debit Bank" <> "Debit Bank") OR
                   (NextGlLine."Currency Code" <> "Currency Code") OR
                   (VendBank."Payment Form" IN [VendBank."Payment Form"::ESR, VendBank."Payment Form"::"ESR+"])
                THEN BEGIN
                    AdjustSummaryPmtText(GenJourLineSEPA);
                    NoOfTransactions := NoOfTransactions + 1;
                    ISOMgt.CreateIsoPayment(xmlDoc, root, GenJourLineSEPA, VendBank,
                                           FirstPostingDate, IDD, PaymentCounter, SummaryPmtAmt, SummaryPmtTxt, TRUE);
                    SummaryPmtAmt := 0;
                    SummaryPmtTxt := '';
                END;

                VendEntry.RESET;
                VendEntry.SETCURRENTKEY("Document No.");
                VendEntry.SETRANGE("Document Type", VendEntry."Document Type"::Invoice);
                VendEntry.SETRANGE("Document No.", "Applies-to Doc. No.");
                VendEntry.SETRANGE("Vendor No.", "Account No.");
                IF VendEntry.FIND('-') THEN BEGIN
                    IF VendEntry."On Hold" = '' THEN BEGIN
                        VendEntry."On Hold" := 'DTA';
                        VendEntry.MODIFY;
                    END;
                END;
            end;

        }
    }


    procedure ClearHeaderVAR()
    begin
        CLEAR(NoOfTransactions);
        CLEAR(ControlSum);
        CLEAR(MessageID);
        CLEAR(PaymentInfoID);
        CLEAR(FirstPostingDate);
        CLEAR(IDD);
        CLEAR(PaymentCounter);
        CLEAR(TotalLCY);
        CLEAR(TotalAmt);
    end;

    local procedure PrepareSummaryPmt(VAR GenJournalLine: Record "Gen. Journal Line")
    begin
        //Text and summary pmt text prepare
        VendEntry.SETCURRENTKEY("Document No.");
        VendEntry.SETRANGE("Document Type", VendEntry."Document Type"::Invoice);
        VendEntry.SETRANGE("Document No.", GenJournalLine."Applies-to Doc. No.");
        VendEntry.SETRANGE("Vendor No.", GenJournalLine."Account No.");
        IF VendEntry.FIND('-') THEN BEGIN
            IF SummaryPmtTxt = '' THEN
                SummaryPmtTxt := VendEntry."External Document No." // 1. Line: Ext. No.
            ELSE
                SummaryPmtTxt := COPYSTR(SummaryPmtTxt + ', ' + VendEntry."External Document No.", 1, MAXSTRLEN(SummaryPmtTxt));
        END;
    end;

    procedure AdjustSummaryPmtText(VAR GenJournalLine: Record "Gen. Journal Line")
    begin
        //Adjust text and summary text before ISO Payment is written
        IF SummaryPmtTxt = VendEntry."External Document No." THEN  // Only one Pmt.
            SummaryPmtTxt := ''
    end;

    trigger OnPreReport()
    begin


    end;

    trigger OnPostReport()
    begin
        d.CLOSE;

        ISOMgt.ModifyXMLHeader(xmlDoc, NoOfTransactions, TotalAmt);
        ISOMgt.SaveXMLDoc(xmlDOc);

        //IF STRPOS(UPPERCASE(FileBank."ISO Filename"), '.XML') = 0 THEN
        //    FileBank."ISO Filename" := FileBank."ISO Filename" + '.xml';

        MESSAGE(Text005 + Text006,
                FileBank."ISO File Folder", FileBank."ISO Filename",
                PaymentCounter, GlSetup."LCY Code", FORMAT(TotalLCY, 0, '<integer thousand><decimals,3>'));
    end;

    var
        FileBank: Record "DTA Setup";
        DtaSetup: Record "DTA Setup";
        GlSetup: Record "General Ledger Setup";
        BankDirectory: Record "Bank Directory";
        VendBank: Record "Vendor Bank Account";
        Vendor: Record Vendor;
        VendEntry: Record "Vendor Ledger Entry";
        CompanySetup: Record "Company Information";
        NextGlLine: Record "Gen. Journal Line";
        GenJourLine2: Record "Gen. Journal Line";
        GenJourLineSEPA2: Record "Gen. Journal Line";
        SEPACurr: Record "Currency";
        SEPACurrCode: Code[10];
        SEPACurrFound: Boolean;
        SummaryPerVendor: Boolean;
        SummaryPmtTxt: Text[140];
        SummaryPmtAmt: Decimal;
        NoSeriesMgt: Codeunit "NoSeriesManagement";
        PaymentCounter: Integer;
        TotalLCY: Decimal;
        TotalAmt: Decimal;
        d: Dialog;
        ISOMgt: Codeunit "ISO 20022 Management2";
        FirstPostingDate: Date;
        NoOfTransactions: Integer;
        ControlSum: Decimal;
        IDD: Code[40];
        MessageID: Code[40];
        PaymentInfoID: Code[40];
        SEPAPayments: Boolean;
        xmlDoc: XmlDocument;
        root: XmlElement;
        xmlDocNodes: text;
        Text001: label '<Es befinden Sie keine Zahlungen im Journal.>';
        Text002: label 'No bank connection found with bank code %1 for vendor %2.';
        Text003: label 'Processing Line:        #1###########\';
        Text004: label 'Vendor Number:          #2###########';
        Text005: label 'The DTA file was sucessfully written to file "%1%2".\';
        Text006: label 'Total of %3 records for %4 %5 processed. %6 payment records created.';

}