/// <summary>
/// Report BalanceSheetExternal (ID 50046).
/// </summary>
report 50046 "Balance Sheet External"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    ProcessingOnly = True;


    dataset
    {
        dataitem("G/L Account"; "G/L Account")
        {
            DataItemTableView = SORTING("No.");

            trigger OnAfterGetRecord()
            var
                gla1, gla2, gla3 : Decimal;
                found: Boolean;

            begin
                Evaluate(gKonto, "G/L Account"."No.");
                if (gKonto = 1) or (gKonto = 2) or (gKonto = 299999) or (gKonto = 899999) or ((gKonto > 10) and (gKonto < 100)) then begin

                    "G/L Account".SetRange("Date Filter", StartDate, PreviousMonthEndDate);
                    "G/L Account".CalcFields("Net Change");
                    gla1 := "G/L Account"."Net Change";

                    "G/L Account".SetRange("Date Filter", StartDate, EndDate);
                    "G/L Account".CalcFields("Net Change");
                    gla3 := "G/L Account"."Net Change";

                    gla2 := gla3 - gla1;

                    TempExcelBuffer.SetRange("Column No.", 1);
                    TempExcelBuffer.SetRange("Row No.", 1, LastRowNo);

                    if TempExcelBuffer.FindFirst() then
                        repeat
                            if (Evaluate(gExcKonto, TempExcelBuffer."Cell Value as Text")) then begin
                                if gKonto = gExcKonto then begin
                                    if (gKonto = 899999) or (gKonto = 299999) then
                                        AddDescriptionToBody(TempExcelBuffer."Row No.", gla1, gla2, gla3);
                                    AddToBody(TempExcelBuffer."Row No.", gla1, gla2, gla3);
                                    found := true;
                                    exit;
                                end
                                else
                                    found := false;
                            end;

                        until TempExcelBuffer.Next() < 1;
                    if not found then
                        Message("G/L Account"."No." + ' : could not be found in the excel template');

                end;

            end;

        }

    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(StartDate; StartDate)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Start Date';
                    }
                    field(EndDate; EndDate)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'End Date';
                    }

                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }

        }

    }


    trigger OnPreReport()
    begin
        rowNoHeader := 10;
        colNoHeader1 := 1;
        colNoHeader2 := 8;
        colNoHeader3 := 15;

        colNoPrevDateSoll := 3;
        colNoPrevDateHaben := 5;
        colNoMiddleDateSoll := 10;
        colNoMiddleDateHaben := 12;
        colNoEndDateSoll := 17;
        colNoEndDateHaben := 19;

        ExcelTemplateList.LookupMode(true);
        ExcelTemplateList.Editable(false);
        if Action::LookupOK = ExcelTemplateList.RunModal() then begin
            ExcelTemplateList.GetRecord(ExcelTemplate);
        end else
            exit;

        if ExcelTemplate.DefaultFileName = '' then
            ExcelTemplate.DefaultFileName := 'Export';

        LastRowNo := ExportToExcel.InitExcel(ExcelTemplate.TemplateName, TempExcelBuffer);

        if StartDate = 0D then
            ERROR('Start Date must not be blank');
        if EndDate = 0D then
            ERROR('End Date must not be blank');

        PreviousMonthEndDate := CalcDate('<CM-1M>', EndDate);
        AddToHeader(rowNoHeader, PreviousMonthEndDate, EndDate);

    end;

    trigger OnPostReport()
    begin
        TempExcelBuffer.WriteAllToCurrentSheet(ExcelBufferSheet);
        TempExcelBuffer.CloseBook();
        TempExcelBuffer.SetFriendlyFilename(ExcelTemplate.DefaultFileName);
        TempExcelBuffer.OpenExcel();
    end;

    var
        StartDate: Date;
        EndDate: Date;
        PreviousMonthEndDate: Date;
        TempExcelBuffer: Record "Excel Buffer" temporary;
        ExcelBufferSheet: record "Excel Buffer" temporary;

        ExportToExcel: Codeunit "Excel Management";
        ExcelTemplate: record "Excel Template Storage";
        ExcelTemplateList: page ExcelTemplateList;
        InStr: InStream;

        LastRowNo: Integer;

        rowNoHeader: Integer;
        colNoHeader1: Integer;
        colNoHeader2: Integer;
        colNoHeader3: Integer;

        colNoPrevDateSoll: Integer;
        colNoPrevDateHaben: Integer;
        colNoMiddleDateSoll: Integer;
        colNoMiddleDateHaben: Integer;
        colNoEndDateSoll: Integer;
        colNoEndDateHaben: Integer;

        gKonto: Integer;
        gKontoTxt: Text;
        gExcKonto: Integer;

    local procedure AddToHeader(rowNo: Integer; inputPrevDate: Date; inputEndDate: Date)
    var
    begin
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNoHeader1, 'Abschluss per ' + formatDate(inputPrevDate), true, false, false);
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNoHeader2, 'Monatsabschluss per ' + formatDate(inputEndDate), true, false, false);
        ExcelBufferSheet.EnterCell(ExcelBufferSheet, rowNo, colNoHeader3, 'Abschluss per ' + formatDate(inputEndDate), true, false, false);
    end;

    local procedure AddToBody(rwLnNo: Integer; totAmnt1: Decimal; totAmnt2: Decimal; totAmnt3: Decimal)
    begin
        if totAmnt1 > 0 then
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoPrevDateSoll, totAmnt1, false, false, false)
        else
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoPrevDateHaben, abs(totAmnt1), false, false, false);

        if totAmnt2 > 0 then
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoMiddleDateSoll, totAmnt2, false, false, false)
        else
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoMiddleDateHaben, abs(totAmnt2), false, false, false);

        if totAmnt3 > 0 then
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoEndDateSoll, totAmnt3, false, false, false)
        else
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoEndDateHaben, abs(totAmnt3), false, false, false);
    end;

    local procedure AddDescriptionToBody(rwLnNo: Integer; totAmnt1: Decimal; totAmnt2: Decimal; totAmnt3: Decimal)
    var
        myInt: Integer;
    begin
        if totAmnt1 > 0 then
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoHeader1 + 1, 'Verlust', true, false, false)
        else
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoHeader1 + 1, 'Gewinn', true, false, false);

        if totAmnt2 > 0 then
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoHeader2 + 1, 'Verlust', true, false, false)
        else
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoHeader2 + 1, 'Gewinn', true, false, false);

        if totAmnt3 > 0 then
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoHeader3 + 1, 'Verlust', true, false, false)
        else
            ExcelBufferSheet.EnterCell(ExcelBufferSheet, rwLnNo, colNoHeader3 + 1, 'Gewinn', true, false, false);
    end;

    local procedure formatDate(inputDate: Date): Text
    var
        newDate: Text;
        day, month, year : Integer;
    begin
        day := Date2DMY(inputDate, 1);
        month := Date2DMY(inputDate, 2);
        year := Date2DMY(inputDate, 3);
        newDate := Format(day) + '.' + Format(month) + '.' + Format(year);
        exit(newDate);
    end;
}
