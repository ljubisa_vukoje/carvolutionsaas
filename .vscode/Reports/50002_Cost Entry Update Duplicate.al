report 50002 "Cost Entry Update Duplicate"
{
    ApplicationArea = Basic, Suite;
    Caption = 'Cost Entries Update Duplicate Entries';
    ProcessingOnly = true;
    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem("Cost Entry"; "Cost Entry")
        {
            trigger OnPreDataItem()
            begin
                //"Cost Entry".SetFilter(Amount, '<>%1', 0);
                ConfigProgressBarG.Init("Cost Entry".Count, 1, "Cost Entry".TableCaption);
            end;

            trigger OnAfterGetRecord()
            var
                CostEntryL: Record "Cost Entry";
                CostEntry2L: Record "Cost Entry";
            begin
                ConfigProgressBarG.Update(Format("Cost Entry"."Entry No."));

                "Cost Entry".Amount := 0;
                "Cost Entry"."Debit Amount" := 0;
                "Cost Entry"."Credit Amount" := 0;
                "Cost Entry"."Additional-Currency Amount" := 0;
                "Cost Entry"."Add.-Currency Debit Amount" := 0;
                "Cost Entry"."Add.-Currency Credit Amount" := 0;
                "Cost Entry".Modify();

                /**
                //belezimo stavke glavne knjige koje smo obradili da se ne bi ponavljalo
                if not ProcessedGLEntryNoG.Get("Cost Entry"."G/L Entry No.") then begin
                    ProcessedGLEntryNoG.Init();
                    ProcessedGLEntryNoG."Entry No." := "Cost Entry"."G/L Entry No.";
                    ProcessedGLEntryNoG.Insert();

                    CostEntryL.Reset();
                    CostEntryL.SetCurrentKey("Entry No.");
                    CostEntryL.SetRange("G/L Entry No.", "Cost Entry"."G/L Entry No.");
                    CostEntryL.SetFilter(Amount, '<>%1', 0);
                    if CostEntryL.FindSet() then //ako postoje doplikati
                        repeat
                            CostEntry2L.Reset();
                            CostEntry2L.SetFilter("Entry No.", '>%1', CostEntryL."Entry No.");
                            CostEntry2L.SetRange("Cost Center Code", CostEntryL."Cost Center Code");
                            CostEntry2L.SetRange("Cost Object Code", CostEntryL."Cost Object Code");
                            CostEntry2L.SetRange("Posting Date", CostEntryL."Posting Date");
                            CostEntry2L.SetRange("Document No.", CostEntryL."Document No.");
                            CostEntry2L.SetRange(Amount, CostEntryL.Amount);
                            CostEntry2L.ModifyAll(Amount, 0);
                            CostEntry2L.ModifyAll("Debit Amount", 0);
                            CostEntry2L.ModifyAll("Credit Amount", 0);
                            CostEntry2L.ModifyAll("Additional-Currency Amount", 0);
                            CostEntry2L.ModifyAll("Add.-Currency Debit Amount", 0);
                            CostEntry2L.ModifyAll("Add.-Currency Credit Amount", 0);
                        until CostEntryL.Next() = 0;
                end;**/
            end;

            trigger OnPostDataItem()
            begin
                ConfigProgressBarG.Close();
            end;
        }
    }

    var
        ProcessedGLEntryNoG: Record "G/L Entry" temporary;
        ProcessedCostEntryCombinationG: Record "Cost Entry" temporary;
        ConfigProgressBarG: Codeunit "Config. Progress Bar";
}
