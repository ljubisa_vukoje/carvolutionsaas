reportextension 50000 "Customer - Detail Trial Bal." extends "Customer - Detail Trial Bal."
{

    RDLCLayout = 'Extended Customer - Detail Trial Bal.rdl';
    dataset
    {
        add("Cust. Ledger Entry")
        {
            column(DocTypeLbl; DocTypeLbl)
            { }
            column(CustomerLable; CustomerLable)
            { }
            column(DocNoLbl; DocNoLbl)
            { }
            column(PaymentsLbl; PaymentsLbl)
            { }
            column(AmountLbl; AmountLbl)
            { }
            column(TitleLable; TitleLable)
            { }
            column(TotalAllLbl; TotalAllLbl)
            { }
            column(TotalBeforeLbl; TotalBeforeLbl)
            { }
            column(PostLbl; PostLbl)
            { }
            column(ToDateLabel; ToDateLabel)
            { }
            column(DescriptionLbl; DescriptionLbl)
            { }
            column(FromDateLable; FromDateLable)
            { }
            column(PostingDateLbl; PostingDateLbl)
            { }
            column(ExtDocLbl; ExtDocLbl)
            { }
            column(SaldoLbl; SaldoLbl)
            { }
            column(DueDateLbl; DueDateLbl)
            { }
            column(Picture; CompanyInfo.Picture)
            { }
            column(PageLabel; PageLabel)
            { }
            column(Document_Type; "Document Type")
            {
                Caption = 'Document type';
            }
            column(External_Document_No_; "External Document No.")
            {
                Caption = 'External Document No.';
            }

            column(FromDate; format(FromDate, 0, '<day>.<month>.<year>'))
            { }
            column(ToDate; format(ToDate, 0, '<day>.<month>.<year>'))
            { }
            column(Email; CompanyInfo."E-Mail")
            {
            }
            column(Posting_Date; "Posting Date")
            { }
            column(Fax; CompanyInfo."Fax No.")
            {
            }
            column(Tel; CompanyInfo."Phone No.")
            {
            }
            column(Web; CompanyInfo."Home Page")
            {
            }
            column(Vat; CompanyInfo."VAT Registration No.")
            {

            }
            column(Yesterday; format(Yesterday, 0, '<day>.<month>.<year>'))
            { }
        }
        modify("Cust. Ledger Entry")
        {
            trigger OnAfterPreDataItem()
            begin
                if Customer."Language Code" <> '' then begin
                    Language.Get(Customer."Language Code");
                    CurrReport.Language := Language(Language."Windows Language ID");
                end else begin
                    CurrReport.Language := Language(2055);
                end;

                Yesterday := CalcDate('<-1D>', Today);
                FromDate := 0D;
                SetRange("Posting Date", FromDate, Yesterday);
            end;
        }

        add(Customer)
        {
            column(Country_Region_Code; "Country/Region Code")
            { }
            column(Currency_Code; "Currency Code")
            { }
            column(CustomerAdres; "Address 2")
            { }
            column(Address; Address)
            { }
            column(City; City)
            { }
            column(Post_Code; "Post Code")
            { }
        }
    }


    trigger OnPreReport()
    begin
        CompanyInfo.get();
        CompanyInfo.CalcFields(Picture);
    end;

    var
        CompanyInfo: Record "Company Information";
        FromDate: Date;

        ToDate: Date;

        FromDateLable: Label 'From date';
        ToDateLabel: Label 'To date';

        TitleLable: Label 'Statement of account';
        PostingDateLbl: Label 'Posting date';
        ExtDocLbl: Label 'External document no.';
        SaldoLbl: Label 'Saldo';
        DueDateLbl: Label 'Due date';

        DescriptionLbl: Label 'Description';

        PageLabel: Label 'Page';

        PostLbl: Label 'Entries in ';

        TotalBeforeLbl: Label 'Total Before Period';

        TotalAllLbl: Label 'Total';

        Language: Record Language;

        DocTypeLbl: Label 'Document type';
        DocNoLbl: Label 'Document No.';

        AmountLbl: Label 'Amount';

        CustomerLable: Label 'Customer No.:';

        PaymentsLbl: Label 'Paymants considered until';

        Yesterday: Date;
}
