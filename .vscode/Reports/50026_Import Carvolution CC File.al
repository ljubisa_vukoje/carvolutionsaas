Report 50026 "Import Carvolution CC File"
{
    // CV1
    // 08.06.20/ADVA/AG - Neuer Report

    CaptionML = ENU = 'Import Carvolution CC File', DES = 'Kreditkartendatei Carvolution importieren';
    Permissions = TableData "Cust. Ledger Entry" = rm;
    ProcessingOnly = true;

    dataset
    {
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field("GenJnlLine.""Journal Template Name"""; GenJnlLine."Journal Template Name")
                {
                    ApplicationArea = Basic;
                    Caption = 'General Journal Template';
                    Editable = false;
                }
                field("GenJnlLine.""Journal Batch Name"""; GenJnlLine."Journal Batch Name")
                {
                    ApplicationArea = Basic;
                    Caption = 'Batch Name';
                    Editable = false;
                }

                field("Dateiname Datatrans Datei"; CSVFileName)
                {
                    ApplicationArea = Basic;
                    Caption = 'Dateiname Datatrans Datei';

                    trigger OnAssistEdit()
                    var
                        TmpFileName: Text[1024];
                    begin
                        //SCM1
                        //TmpFileName := CustFileMgt.UploadFile(RequestOptionsPage.Caption, '.csv');
                        // CSVFileName := FileMgt.OpenFileDialog('', '', FileMgt.GetToFilterText('', '.csv'));
                        UploadIntoStream('', '', '', CSVFileName, CSVInSTream);
                        //UploadIntoStream('', '', '', CSVFileName, CSVInSTream);
                        //Run();
                        //SelFileName := FileMgt.UploadFile('', '', FileMgt.GetToFilterText('', '.csv'));
                        //SCM1/END
                    end;
                }
            }
        }

        actions
        {
        }
    }

    labels
    {
    }

    trigger OnInitReport()
    begin
        SalesSetup.Get;
    end;

    trigger OnPostReport()
    var
        GenJnlLine2: Record "Gen. Journal Line";
        GenJnlLine3: Record "Gen. Journal Line";
    begin
        if not SalesSetup."Creditcard Clearing New" then begin
            Clear(GenJnlLine2);
            GenJnlLine2.SetCurrentkey("Journal Template Name", "Journal Batch Name", "Account Type", "Document Type");
            GenJnlLine2.SetRange("Journal Template Name", GenJnlLine."Journal Template Name");
            GenJnlLine2.SetRange("Journal Batch Name", GenJnlLine."Journal Batch Name");
            GenJnlLine2.SetRange("Document Type", GenJnlLine2."document type"::Refund);
            if GenJnlLine2.FindFirst then begin
                repeat
                    Clear(GenJnlLine3);
                    GenJnlLine3.SetCurrentkey("Journal Template Name", "Journal Batch Name", "Account Type", "Document Type");
                    GenJnlLine3.SetRange("Journal Template Name", GenJnlLine2."Journal Template Name");
                    GenJnlLine3.SetRange("Journal Batch Name", GenJnlLine2."Journal Batch Name");
                    GenJnlLine3.SetRange("Document Type", GenJnlLine3."document type"::Payment);
                    GenJnlLine3.SetRange("Applies-to Doc. No.", GenJnlLine2."Applies-to Doc. No.");
                    if GenJnlLine3.FindFirst then begin
                        GenJnlLine2.Validate("Applies-to Doc. Type", GenJnlLine2."applies-to doc. type"::" ");
                        GenJnlLine2.Validate("Applies-to Doc. No.", '');
                        GenJnlLine2.Modify;
                    end;
                until GenJnlLine2.Next = 0;
            end;
        end;
    end;

    trigger OnPreReport()
    var
        SkipLine: Boolean;
        InPaymentTrxId: Text[50];
        InTrxTime: Text[250];
        InBookTyp: Text[30];
        InCustomerID: Code[20];
        InSubdocId: Code[20];
        InAmount: Text[50];
        InCardBrand: Code[50];
        InCost: Text[30];
        CustomerNo: Code[20];
        DocNo: Code[20];
        InCurrency: Code[10];
        Day: Integer;
        Month: Integer;
        Year: Integer;
        PostDate: Date;
        dAmount: Decimal;
        LineNo: Integer;
        ReasonCode: Code[10];
        SplitLine: Boolean;
        CustLedgerEntry: Record "Cust. Ledger Entry";
        dPaymCost: Decimal;
        dTotalPaymCost: Decimal;
        NewLine: Boolean;


    begin
        if CSVFileName = '' then
            Error(Text001);

        //UploadResult := UploadIntoStream('', '', '', CSVFileName, CSVInSTream);
        CSVBuffer.DeleteAll();
        CSVBuffer.LoadDataFromStream(CSVInSTream, ';');

        CSVBuffer.Find('-');


        SalesSetup.TestField(SalesSetup."G/L Account Mastercard");
        SalesSetup.TestField(SalesSetup."G/L Account VISA");

        FileName := CSVFileName;

        Clear(LineCounter);
        Clear(LineCounter2);
        LastBookDate := 0D;

        LineNo := 10000;

        d.Open(Text002 + '@1@@@@@@@@@@@@@@@@@@@@@@@@');
        LineCounter := 0;
        Clear(dTotalPaymCost);
        SkipLine := true;
        SplitLine := false;
        NewLine := false;

        repeat

            //FileLine := DelChr(FileLine, '=', '"');
            //FileLine := ConvertStr(FileLine, ';', ',');

            // Nicht relevate Zeilen aussortieren
            // if LineCounter = 1 then begin
            //     SkipLine := true;
            // end else begin

            case CSVBuffer."Field No." of
                1:
                    InPaymentTrxId := CSVBuffer.Value;
                2:
                    InTrxTime := CSVBuffer.Value;
                3:
                    InBookTyp := CSVBuffer.Value;
                4:
                    InCustomerID := CSVBuffer.Value;
                5:
                    InSubdocId := CSVBuffer.Value;
                6:
                    InAmount := CSVBuffer.Value;
                7:
                    begin
                        InCardBrand := CSVBuffer.Value;
                        SkipLine := false;
                        NewLine := true;
                        LineCounter += 1;
                    end;
            end;
            //end;
            // Relevante Zeile lesen
            if (newline) and (LineCounter > 1) then begin
                /*
                //Bei -Betrag Erstattung
                IF COPYSTR(InAmount, 1, 1) = '-' THEN BEGIN
                  InBookTyp := 'E';
                END ELSE BEGIN
                  InBookTyp := 'Z';
                END;
                */
                //Message(InPaymentTrxId + ' ' + InTrxTime + ' ' + InBookTyp + ' ' + InCustomerID + ' ' + InSubdocId +
                //  ' ' + InAmount + ' ' + InCardBrand);
                //Buchungsdatum
                Evaluate(Day, CopyStr(InTrxTime, 9, 2));
                Evaluate(Month, CopyStr(InTrxTime, 6, 2));
                Evaluate(Year, CopyStr(InTrxTime, 1, 4));
                PostDate := Dmy2date(Day, Month, Year);
                if PostDate > LastBookDate then
                    LastBookDate := PostDate;

                //Fibu Gegenkonto
                case InCardBrand of
                    'VISA':
                        GLAcc.Get(SalesSetup."G/L Account VISA");
                    'MASTERCARD':
                        GLAcc.Get(SalesSetup."G/L Account Mastercard");
                    else
                        Error(StrSubstNo('Katentyp %1 nicht vorhanden.', InCardBrand));
                end;

                //Ursache
                if not Reason.Get(InCardBrand) then begin
                    Reason.Init;
                    Reason.Code := InCardBrand;
                    Reason.Insert;
                end;

                //Debitor ermitteln
                CustomerNo := InCustomerID;
                if not Customer.Get(CustomerNo) then
                    Error(StrSubstNo('Debitor %1 ist nicht vorhanden.', InCustomerID));

                //Belegnr.
                if SalesSetup."Creditcard Clearing New" then begin
                    DocNo := InSubdocId;
                    CustLedgerEntry.Reset;
                    CustLedgerEntry.SetCurrentkey("Customer No.", Open, Positive, "Due Date", "Currency Code");
                    CustLedgerEntry.SetRange("Customer No.", CustomerNo);
                    CustLedgerEntry.SetRange(Open, true);
                    CustLedgerEntry.SetRange("Document Type", CustLedgerEntry."document type"::Invoice);
                    CustLedgerEntry.SetRange("Document No.", DocNo);
                    if not CustLedgerEntry.FindFirst then
                        Clear(CustLedgerEntry);
                end else begin
                    SalesSetup.TestField(SalesSetup."Creditcard Clearing New", true);
                end;

                //Betrag
                Evaluate(dAmount, InAmount);

                //Fibu Erfassungsjournalzeile anlegen;
                Clear(NewGenJnlLine);
                NewGenJnlLine.Init;
                NewGenJnlLine := LastGenJnlLine;
                NewGenJnlLine."Applies-to Doc. Type" := NewGenJnlLine."applies-to doc. type"::" ";
                NewGenJnlLine."Applies-to Doc. No." := '';
                NewGenJnlLine."Applies-to ID" := '';
                NewGenJnlLine."Line No." := LineNo;
                NewGenJnlLine.SetUpNewLine(LastGenJnlLine, 0, false);
                NewGenJnlLine.Insert(true);
                NewGenJnlLine.Validate("Posting Date", PostDate);
                NewGenJnlLine.Validate("Reason Code", Reason.Code);


                if InBookTyp = 'refund' then begin
                    NewGenJnlLine.Validate("Document Type", NewGenJnlLine."document type"::Refund);
                    NewGenJnlLine.Validate("Document No.", 'E-' + DocNo);
                end else begin
                    NewGenJnlLine.Validate("Document Type", NewGenJnlLine."document type"::Payment);
                    NewGenJnlLine.Validate("Document No.", 'Z-' + DocNo);
                end;
                NewGenJnlLine.Validate("Account Type", NewGenJnlLine."account type"::Customer);
                NewGenJnlLine.Validate("Account No.", CustomerNo);
                if InBookTyp = 'refund' then
                    NewGenJnlLine.Validate(Amount, dAmount)
                else
                    NewGenJnlLine.Validate(Amount, dAmount * -1);
                NewGenJnlLine.Validate("Bal. Account Type", NewGenJnlLine."bal. account type"::"G/L Account");
                NewGenJnlLine.Validate("Bal. Account No.", GLAcc."No.");
                if CustLedgerEntry."Entry No." <> 0 then begin
                    if (CustLedgerEntry."Posting Date" > PostDate) then begin
                        NewGenJnlLine.Validate("Posting Date", CustLedgerEntry."Posting Date");
                        NewGenJnlLine.Validate("Bal. Account No.", SalesSetup."Auxiliary Account Creditcard");
                        SplitLine := true;
                    end;
                    if NewGenJnlLine."Document Type" = NewGenJnlLine."document type"::Payment then begin
                        NewGenJnlLine.Validate("Applies-to Doc. Type", NewGenJnlLine."applies-to doc. type"::Invoice);
                        NewGenJnlLine.Validate("Applies-to Doc. No.", CustLedgerEntry."Document No.");
                    end;
                end;
                NewGenJnlLine.Description := CopyStr(StrSubstNo('%1 %2 ID %3', NewGenJnlLine.Description, InCardBrand, InPaymentTrxId), 1, MaxStrLen(NewGenJnlLine.Description));
                NewGenJnlLine."On Hold" := 'KK';
                NewGenJnlLine.Modify;
                LineNo := LineNo + 10000;
                LastGenJnlLine := NewGenJnlLine;

                if SplitLine then begin
                    Clear(NewGenJnlLine);
                    NewGenJnlLine.Init;
                    NewGenJnlLine := LastGenJnlLine;
                    NewGenJnlLine."Line No." := LineNo;
                    NewGenJnlLine.SetUpNewLine(LastGenJnlLine, 0, false);
                    NewGenJnlLine.Validate(NewGenJnlLine."Applies-to Doc. Type", 0);
                    NewGenJnlLine.Validate(NewGenJnlLine."Applies-to Doc. No.", '');
                    NewGenJnlLine.Validate("Applies-to ID", '');
                    NewGenJnlLine.Insert(true);
                    NewGenJnlLine.Validate("Posting Date", PostDate);
                    NewGenJnlLine.Validate("Document Type", 0);
                    NewGenJnlLine.Validate("Account Type", NewGenJnlLine."account type"::"G/L Account");
                    NewGenJnlLine.Validate("Account No.", GLAcc."No.");
                    if LastGenJnlLine."Document Type" = LastGenJnlLine."document type"::Refund then
                        NewGenJnlLine.Validate(Amount, dAmount * -1)
                    else
                        NewGenJnlLine.Validate(Amount, dAmount);
                    NewGenJnlLine.Validate("Bal. Account Type", NewGenJnlLine."bal. account type"::"G/L Account");
                    NewGenJnlLine.Validate("Bal. Account No.", SalesSetup."Auxiliary Account Creditcard");
                    NewGenJnlLine.Validate("Document No.", 'Z-' + DocNo);
                    NewGenJnlLine.Description :=
                      StrSubstNo(Text007,
                                 NewGenJnlLine."Account No.",
                                 NewGenJnlLine."Bal. Account No.",
                                 NewGenJnlLine."Posting Date");
                    NewGenJnlLine."On Hold" := 'KK';
                    NewGenJnlLine.Modify;
                    LineNo := LineNo + 10000;
                    LastGenJnlLine := NewGenJnlLine;
                end;

                if NewLine then begin
                    Clear(FileLine);
                    Clear(InPaymentTrxId);
                    Clear(InTrxTime);
                    Clear(InBookTyp);
                    Clear(InCustomerID);
                    Clear(InSubdocId);
                    Clear(InAmount);
                    Clear(InCardBrand);
                    Clear(InCurrency);
                    Clear(ReasonCode);
                    Clear(CustomerNo);
                    Clear(DocNo);
                    NewLine := false;
                    SkipLine := true;
                    d.Update(1, LineCounter);
                end;
            end;

        // LineCounter := LineCounter + 1;
        //SkipLine := false;
        until CSVBuffer.Next() = 0;

        d.Close;

        /*
        //Zahlungskosten
        IF dTotalPaymCost <> 0 THEN BEGIN
          CLEAR(NewGenJnlLine);
          NewGenJnlLine.INIT;
          NewGenJnlLine := LastGenJnlLine;
          NewGenJnlLine."Line No." := LineNo;
          NewGenJnlLine.SetUpNewLine(LastGenJnlLine, 0, FALSE);
          NewGenJnlLine."Document Type" := NewGenJnlLine."Document Type"::" ";
          NewGenJnlLine."Account Type" := NewGenJnlLine."Account Type"::"G/L Account";
          NewGenJnlLine.INSERT(TRUE);
          NewGenJnlLine.VALIDATE("Posting Date", LastBookDate);
          NewGenJnlLine.VALIDATE("Document No.", 'MFG-PAYMCOST');
          NewGenJnlLine.VALIDATE("Account No.", BalAcc."No." );
          NewGenJnlLine.VALIDATE("Bal. Account Type", NewGenJnlLine."Bal. Account Type"::"G/L Account");
          NewGenJnlLine.VALIDATE("Bal. Account No.", GLAcc."No.");
          NewGenJnlLine.Description := STRSUBSTNO(Text020, FileNameText);
          NewGenJnlLine.VALIDATE(Amount, dTotalPaymCost);
          NewGenJnlLine.VALIDATE("Reason Code", Reason.Code);
        
          NewGenJnlLine.MODIFY;
          LineNo := LineNo + 10000;
          LastGenJnlLine := NewGenJnlLine;
        END;
        */

    end;

    var
        SalesSetup: Record "Sales & Receivables Setup";
        GLAcc: Record "G/L Account";
        BalAcc: Record "G/L Account";
        PayMeth: Record "Payment Method";
        Reason: Record "Reason Code";
        Customer: Record Customer;
        Contact: Record Contact;
        GenJnlLine: Record "Gen. Journal Line";
        NewGenJnlLine: Record "Gen. Journal Line";
        LastGenJnlLine: Record "Gen. Journal Line";
        //TransactIDShop: Record UnknownRecord50000;
        //CommonDlgMgt: Codeunit "SMTP Test Mail";
        d: Dialog;
        FileLine: Text[1024];
        SelFileName: Text[1024];
        FileName: Text[1024];
        LineCounter: Integer;
        Text001: label 'Sie müssen Dateiname Kreditkartendatei angeben!';
        Text002: label 'Datei lesen...';
        Text003: label 'Debitor kann nicht ermittelt werden! Dateizeile: %1';
        LineCounter2: Integer;
        Text004: label '%1 ist keine gültiger Kreditkartentyp!';
        Text005: label 'Kreditkartenimport %1';
        Text006: label 'Das Erfassungsjournal %1 %2 muss leer sein!';
        Text007: label 'Umbuchung %1-%2 Valutadatum %3';
        Text008: label 'Zeilennummer: %1 Transactions ID %2 nicht vorhanden!';
        Text020: label 'Paymentfee MFGroup %1';
        LastBookDate: Date;
        FileNameText: Text[1024];
        CSVBuffer: REcord "CSV Buffer";
        CSVInSTream: InStream;
        CSVFileName: Text;
        FileMgt: Codeunit "File Management";


    local procedure GetCustomer(CustomerNoIn: Code[20])
    begin
        begin

            if not Customer.Get(CustomerNoIn) and not Contact.Get(CustomerNoIn) then begin
                Contact.Init;
                Contact.SetHideValidationDialog(true);
                Contact."No." := CustomerNoIn;
                Contact.Name := CopyStr(StrSubstNo(Text005, CurrentDatetime), 1, MaxStrLen(Contact.Name));
                Contact.Insert(true);
            end;
            /*
            if not Customer.Get(CustomerNoIn) then begin
                SalesSetup.TestField("Domestic Cust. Template");
                Contact.SetHideValidationDialog(true);
                Contact.CreateCustomer(SalesSetup."Domestic Cust. Template");
            end;*/
        end;
    end;


    procedure DefJnlLine(var GenJnlLine2: Record "Gen. Journal Line")
    begin
        GenJnlLine.Copy(GenJnlLine2);
        LastGenJnlLine.Copy(GenJnlLine);
        if GenJnlLine.FindFirst then
            Error(Text006, GenJnlLine."Journal Template Name", GenJnlLine."Journal Batch Name");
    end;
}
