/// <summary>
/// PageExtension "DefaultDimExt" (ID 50800) extends Record MyTargetPage.
/// </summary>
pageextension 50800 DefaultDimExt extends "Default Dimensions"
{
    layout
    {
        addbefore("Dimension Code")
        {
            field(ItemNo; Rec.ItemNo)
            {
                Visible = IsVisible;
                ApplicationArea = all;
            }
            field(ItemInventory; Rec.ItemInventory)
            {
                Visible = IsVisible;
                ApplicationArea = all;
            }
            field("Parent Type"; Rec."Parent Type")
            {
                Visible = IsVisible;
                ApplicationArea = all;
            }
            field(ParentId; Rec.ParentId)
            {
                Visible = IsVisible;
                ApplicationArea = all;
            }
        }
        addafter("Dimension Value Code")
        {
            field("Dimension Value Name"; Rec."Dimension Value Name")
            {
                Visible = IsVisible;
                ApplicationArea = all;
            }
        }

        addafter("Dimension Value Name")
        {
            field(ItemInventoryEndMonth; Rec.ItemInventoryEndMonth)
            {
                Visible = IsVisible;
                ApplicationArea = all;
            }
        }

        addafter(ItemInventoryEndMonth)
        {
            field(DateEndMonth; Rec.DateEndMonth)
            {
                Visible = IsVisible;
                ApplicationArea = all;
            }
        }
    }
    actions
    {
        addlast(Processing)
        {
            action(GetItemNo)
            {
                ApplicationArea = all;
                Visible = IsVisible;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = Item;
                Caption = 'Artikel und Lagerbestand zuweisen';

                trigger OnAction()
                var
                    Item: record item;
                    ParentTypeItem: Text;
                    ItemGUIDList: List of [Guid];
                    i: Integer;
                    ItemGuid: Guid;
                    itemInventoryL: Integer;
                begin
                    //Get Item List
                    if Item.FindFirst() then begin
                        repeat
                            ItemGUIDList.Add(Item.SystemId);
                        until Item.Next() = 0;
                    end;

                    for i := 1 to ItemGUIDList.Count() do begin
                        //Get GUID
                        ItemGuid := ItemGUIDList.Get(i);
                        rec.SetRange(ParentId, ItemGuid);
                        if rec.FindFirst() then begin
                            item.SetRange(SystemId, ItemGuid);
                            if item.FindFirst() then begin
                                item.CalcFields(Inventory);
                                itemInventoryL := item.Inventory;
                                repeat
                                    rec.ItemNo := item."No.";
                                    rec.ItemInventory := itemInventoryL;
                                    rec.Reset();
                                    rec.Modify();
                                until rec.next() = 0;
                            end;

                        end;
                    end;
                end;
            }
        }
    }

    trigger OnOpenPage()
    begin
        CompInformation.SetIsVisible(IsVisible);
    end;

    var
        CompInformation: Record "Company Information";
        IsVisible: Boolean;
}