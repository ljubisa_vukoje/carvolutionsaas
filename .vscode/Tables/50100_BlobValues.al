/// <summary>
/// Table "BlobValues" (ID 50502).
/// </summary>
table 50100 BlobValues
{
    Caption = 'Blob';
    ReplicateData = false;
    TableType = Temporary;

    fields
    {
        field(1; "File Name"; Text[250])
        {
            Caption = 'File Name';
            DataClassification = SystemMetadata;
        }
        field(2; Content; BLOB)
        {
            Caption = 'Content';
            DataClassification = SystemMetadata;
            SubType = UserDefined;
        }
        field(3; Type; Option)
        {
            Caption = 'Type';
            DataClassification = SystemMetadata;
            OptionCaption = ' ,Image,PDF,Word,Excel,PowerPoint,Email,XML,Other';
            OptionMembers = " ",Image,PDF,Word,Excel,PowerPoint,Email,XML,Other;
        }
        field(4; "Document Type"; Enum "Attachment Entity Buffer Document Type")
        {
            Caption = 'Document Type';
            DataClassification = CustomerContent;
        }
        field(5; "Byte Size"; Integer)
        {
            Caption = 'Byte Size';
            DataClassification = SystemMetadata;
        }
        field(6; RecordGuid; Guid)
        {
            Caption = 'System ID';
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "File Name")
        {
            Clustered = true;
        }
    }

    /// <summary>
    /// SetBinaryContent.
    /// </summary>
    /// <param name="BinaryContent">Text.</param>
    procedure SetBinaryContent(BinaryContent: Text)
    var
        OutStream: OutStream;
    begin
        Content.CreateOutStream(OutStream);
        OutStream.Write(BinaryContent, StrLen(BinaryContent));
    end;

    /// <summary>
    /// SetTextContent.
    /// </summary>
    /// <param name="TextContent">Text.</param>
    procedure SetTextContent(TextContent: Text)
    var
        OutStream: OutStream;
    begin
        Content.CreateOutStream(OutStream, GetContentTextEncoding);
        OutStream.Write(TextContent, StrLen(TextContent));
    end;

    trigger OnInsert()
    var
        myInt: Integer;
    begin
    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

    local procedure GetContentTextEncoding(): TextEncoding
    begin
        exit(TEXTENCODING::UTF8);
    end;


    var
        Base64String: Text;
        Base64Convert: Codeunit "Base64 Convert";
        FinalOStream: OutStream;
        attachment: Record "Document Attachment";
        FinalInstream: InStream;
        Filename: Text;
        BlobTable: record BlobValues;
        today: Date;
        DocumentType: Enum "Attachment Entity Buffer Document Type";
        SalesInv: Record "Sales Invoice Header";
        PurchInv: Record "Purch. Inv. Header";
        actionContext: WebServiceActionContext;
        Message: Text;

}