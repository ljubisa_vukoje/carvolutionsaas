table 50011 "Reminder Email Template"
{



    fields
    {
        field(1; "Language Code"; Code[20])
        {
            TableRelation = Language;
            CaptionML = ENU = 'Language';
        }
        field(2; "Email Subject"; Text[500])
        {

        }
        field(4; "Email Body Text"; Blob)
        {

        }
        field(5; "Email Body Text Lvl 4"; Blob)
        {

        }

    }



    keys

    {
        key(LanguageCode; "Language Code")
        {

        }
    }

    procedure SetEmailBody(EmailBodyHTML: Text[10000])
    begin
        Clear(outStr);
        Clear("Email Body Text");
        "Email Body Text".CreateOutStream(outStr, TextEncoding::UTF8);
        outStr.WriteText(EmailBodyHTML);
        Modify();
    end;

    procedure SetEmailBodyLvl4(EmailBodyHTML: Text[10000])
    begin
        Clear(outStrLvl4);
        Clear("Email Body Text Lvl 4");
        "Email Body Text Lvl 4".CreateOutStream(outStrLvl4, TextEncoding::UTF8);
        outStrLvl4.WriteText(EmailBodyHTML);
        Modify();
    end;

    procedure GetEmailBody(): Text
    var
        ReturnEmailBody: Text;
    begin

        Clear(inSTr);
        CalcFields("Email Body Text");
        IF NOT "Email Body Text".HasValue then
            EXIT('');
        "Email Body Text".CreateInStream(inSTr, TextEncoding::UTF8);
        inStr.Read(ReturnEmailBody);
        exit(ReturnEmailBody);


    end;

    procedure GetEmailBodyLvl4(): Text
    var
        ReturnEmailBody: Text;
    begin

        Clear(inSTrLvl4);
        CalcFields("Email Body Text Lvl 4");
        IF NOT "Email Body Text Lvl 4".HasValue then
            EXIT('');
        "Email Body Text Lvl 4".CreateInStream(inSTrLvl4, TextEncoding::UTF8);
        inStrLvl4.Read(ReturnEmailBody);
        exit(ReturnEmailBody);


    end;

    var

        outStr: OutStream;
        inSTr: InStream;
        outStrLvl4: OutStream;
        inSTrLvl4: InStream;


}



