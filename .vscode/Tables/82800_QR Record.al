/// <summary>
/// ISO20022QR
/// 28.092.2021 ljubisa.vukoje@holyerp.rs
/// </summary>
table 82800 "QR Record"
{


    fields
    {

        field(1; "Invoice No."; code[20])
        {
            DataClassification = ToBeClassified;

        }
        field(2; "1 QRType"; code[3])
        {
            DataClassification = ToBeClassified;

        }
        field(3; "2 Version"; code[4])
        {
            DataClassification = ToBeClassified;

        }
        field(4; "3 Coding Type"; code[1])
        {
            DataClassification = ToBeClassified;
        }
        field(5; "4 Konto"; code[21])
        {
            DataClassification = ToBeClassified;
        }
        field(6; "5 ZE-Adress-Typ"; code[1])
        {
            DataClassification = ToBeClassified;
        }
        field(7; "6 ZE-Name"; Text[70])
        {

        }
        field(8; "7 ZE-Adresszeile 1"; Text[70])
        {
            DataClassification = ToBeClassified;
        }
        field(9; "8 ZE-Adresszeile 2"; Text[70])
        {
            DataClassification = ToBeClassified;
        }
        field(10; "9 ZE-Postleitzahl"; Text[16])
        {
            DataClassification = ToBeClassified;
        }
        field(11; "10 ZE-Ort"; Text[35])
        {
            DataClassification = ToBeClassified;
        }
        field(12; "11 ZE-Land"; Code[2])
        {
            DataClassification = ToBeClassified;
        }
        field(13; "12 EZE-Adress-Typ"; Code[1])
        {
            DataClassification = ToBeClassified;
        }
        field(14; "13 EZE-Name"; Text[70])
        {
            DataClassification = ToBeClassified;
        }
        field(15; "14 EZE-Adresszeile 1"; Text[70])
        {
            DataClassification = ToBeClassified;
        }
        field(16; "15 EZE-Adresszeile 2"; Text[70])
        {
            DataClassification = ToBeClassified;
        }
        field(17; "16 EZE-Postleitzahl"; Text[16])
        {
            DataClassification = ToBeClassified;
        }
        field(18; "17 EZE-Ort"; Text[35])
        {
            DataClassification = ToBeClassified;
        }
        field(19; "18 EZE-Land"; Code[2])
        {
            DataClassification = ToBeClassified;
        }
        field(20; "19 Betrag"; decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(21; "20 Währung"; Code[3])
        {
            DataClassification = ToBeClassified;
        }
        field(22; "21 EZP-Adress-Typ"; Code[1])
        {
            DataClassification = ToBeClassified;
        }
        field(23; "22 EZP-Name"; Text[70])
        {
            DataClassification = ToBeClassified;
        }
        field(24; "23 EZP-Adresszeile 1"; Text[70])
        {
            DataClassification = ToBeClassified;
        }
        field(25; "24 EZP-Adresszeile 2"; Text[70])
        {
            DataClassification = ToBeClassified;
        }
        field(26; "25 EZP-Postleitzahl"; Text[16])
        {
            DataClassification = ToBeClassified;
        }
        field(27; "26 EZP-Ort"; Text[35])
        {
            DataClassification = ToBeClassified;
        }
        field(28; "27 EZP-Land"; Code[2])
        {
            DataClassification = ToBeClassified;
        }
        field(29; "28 Referenztyp"; Code[4])
        {
            DataClassification = ToBeClassified;
        }
        field(30; "29 Referenz"; Code[27])
        {
            DataClassification = ToBeClassified;
        }
    }

}