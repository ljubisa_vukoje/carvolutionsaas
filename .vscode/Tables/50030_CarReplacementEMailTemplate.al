table 50030 "Car Replacement template"
{



    fields
    {
        field(1; "Language Code"; Code[20])
        {
            TableRelation = Language;
            CaptionML = ENU = 'Language';
        }
        field(2; "Email Subject"; Text[500])
        {

        }
        field(4; "Email Body Text"; Blob)
        {

        }


    }



    keys

    {
        key(LanguageCode; "Language Code")
        {

        }
    }

    procedure SetEmailBody(EmailBodyHTML: Text[10000])
    begin
        Clear(outStr);
        Clear("Email Body Text");
        "Email Body Text".CreateOutStream(outStr, TextEncoding::UTF8);
        outStr.WriteText(EmailBodyHTML);
        Modify();
    end;

    procedure GetEmailBody(): Text
    var
        ReturnEmailBody: Text;
    begin

        Clear(inSTr);
        CalcFields("Email Body Text");
        IF NOT "Email Body Text".HasValue then
            EXIT('');
        "Email Body Text".CreateInStream(inSTr, TextEncoding::UTF8);
        inStr.Read(ReturnEmailBody);
        exit(ReturnEmailBody);


    end;

    var

        outStr: OutStream;
        inSTr: InStream;


}



