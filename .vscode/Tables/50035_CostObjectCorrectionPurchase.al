table 50035 CostObjectCorrectionPurchTable
{

    Caption = 'Cost Object Correction';
    fields
    {
        field(50000; "Entry No."; integer)
        {
            Caption = 'Entry No.';
            DataClassification = ToBeClassified;
            AutoIncrement = true;

        }
        field(50001; "Document No."; Code[20])
        {
            Caption = 'Document No.';
            DataClassification = ToBeClassified;
            trigger OnValidate()
            begin

                Rec.PurchInvAmt := Rec.CalcInvAmount();
                Rec.CalcFields(CostEntriesAmt);
                Rec.DiffAmount := Rec.PurchInvAmt - Rec.CostEntriesAmt;

            end;

        }
        field(50002; "CarvolutionID"; Integer)
        {
            //ObsoleteState = Removed;
            Caption = 'CarvolutionVehicleID';

            DataClassification = ToBeClassified;

            trigger OnValidate()
;
            begin
                VALIDATE(Carvolution_ID, FORMAT(CarvolutionID));
            end;

        }
        field(50008; "Carvolution_ID"; Code[30])
        {
            Caption = 'CarvolutionVehicleID';
            DataClassification = ToBeClassified;
            //TableRelation = "Dimension Value".Carvolution_ID where(Carvolution_ID = field(Carvolution_ID));

            trigger OnValidate()
            var
                DimensioValues: Record "Dimension Value";
            begin
                if Rec."Cost Object Status" <> Rec."Cost Object Status"::Open then
                    error(CostCorrStatusLbl);
                DimensioValues.Reset();
                DimensioValues.SetRange(Carvolution_ID, Carvolution_ID);
                if DimensioValues.Find('-') then
                    "New Dimension Value" := DimensioValues.Code;

                Rec.CalcFields(CostEntriesAmt);
                Rec.DiffAmount := PurchInvAmt - CostEntriesAmt;
                if Rec."Entry No." = 0 then
                    Rec.Insert()
                else
                    Rec.Modify();

            end;

        }
        field(50003; "Old Dimension Value"; Code[20])
        {
            Caption = 'Old Dimension Value';
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value".Code where("Dimension Value Type" = filter(Standard));
        }
        field(50004; "New Dimension Value"; Code[20])
        {
            Caption = 'New Dimension Value';
            DataClassification = ToBeClassified;
            TableRelation = "Dimension Value".Code where("Dimension Value Type" = filter(Standard));

        }
        field(50005; "Last Modification Date"; DateTime)
        {
            Caption = 'Last Modification Date';
            DataClassification = ToBeClassified;

        }
        field(50006; "Corrected"; Boolean)
        {
            Caption = 'DImension Corrected';
            DataClassification = ToBeClassified;

        }
        field(50007; "Error Text"; Text[1024])
        {
            Caption = 'Error Text';
            DataClassification = ToBeClassified;

        }
        field(50009; "DimensionCorrection ID"; Integer)
        {

            Caption = 'DimensionCorrection ID';

            DataClassification = ToBeClassified;

        }
        field(50010; "Cost Journal Posted"; Boolean)
        {
            Caption = 'Cost Journal Posted';
            DataClassification = ToBeClassified;

        }

        field(50011; "Cost Object Status"; Enum "Cost Correction Status")
        {
            Caption = 'Cost Object Correction Status';
            DataClassification = ToBeClassified;
            Editable = false;
            trigger OnValidate()
            begin
                if "Cost Object Status" = "Cost Object Status"::Open then begin
                    Corrected := false;
                    "Cost Journal Posted" := false;
                    "Error Text" := '';
                end;

                Rec.PurchInvAmt := Rec.CalcInvAmount();
                Rec.CalcFields(CostEntriesAmt);
                Rec.DiffAmount := Rec.PurchInvAmt - Rec.CostEntriesAmt;

            end;

        }
        field(50012; "Job Queue Entry ID"; Guid)
        {
            Caption = 'Job Queue Entry ID';
        }
        field(50013; USERID; Code[50])
        {
            Caption = 'User Id';
        }
        field(50014; PurchInvAmt; Decimal)
        {
            Caption = 'Purchase Invoice Amount';
            Editable = false;


        }

        field(50015; CostEntriesAmt; Decimal)
        {
            Caption = 'Cost Entries Amount';
            FieldClass = FlowField;
            Editable = false;

            CalcFormula = Sum("Cost Entry".Amount where("Document No." = FIELD("Document No."), "Cost Object Code"
            = FIELD("New Dimension Value")));
        }

        field(50016; DiffAmount; Decimal)
        {
            Caption = 'Diff. Amount';
            Editable = false;

        }

        field(50017; CostCorrCount; Integer)
        {
            Caption = 'Cost Correction Count';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = Count(CostObjectCorrectionPurchTable where("Document No." = FIELD("Document No.")));

        }
        /// <summary>
        /// Special case documents produces by Benno
        /// in terms of multiple cost object existing on origin
        /// purchase invoice are corrected to one cost object
        /// with cost correction list
        /// </summary>
        field(50018; "Special Case Document"; Boolean)
        {
            Caption = 'Special Case Document';
            Editable = false;

        }
    }

    keys
    {


        Key(Key1; "Entry No.", "Document No.", CarvolutionID)
        {


        }
        Key(Key2; "Entry No.", "Document No.", Carvolution_ID)
        {


        }

    }
    procedure CalcInvAmount(): Decimal
    var
        Amount: Decimal;
        TGllEntry: Record "G/L Entry";
        TGlAcc: Record "G/L Account";

    begin
        Amount := 0;
        TGllEntry.Reset();
        TGllEntry.SetRange("Document No.", "Document No.");
        if TGllEntry.Find('-') then begin
            repeat
                TGlAcc.Get(TGllEntry."G/L Account No.");
                if TGlAcc."Income/Balance" = TGlAcc."Income/Balance"::"Income Statement" then
                    Amount += TGllEntry.Amount;
            until TGllEntry.Next() = 0;
        end;
        exit(Amount);

    end;

    trigger OnInsert()
    begin
        "Cost Object Status" := "Cost Object Status"::Open;
    end;

    trigger OnModify()
    begin
        //if "Cost Object Status" <> "Cost Object Status"::Open then
        //    Error(CostCorrStatusLbl);
    end;


    trigger OnDelete()
    var
        JobQueueEntry: Record "Job Queue Entry";
    begin
        if NOT IsNullGuid("Job Queue Entry ID") then begin
            JobQueueEntry.Reset();
            JobQueueEntry.SetRange(ID, "Job Queue Entry ID");
            if JobQueueEntry.Find('-') then begin
                if JobQueueEntry.Status in
                [JobQueueEntry.Status::"On Hold", JobQueueEntry.Status::Error] then begin
                    if Confirm(ConfirmDeletionLbl) then begin
                        JobQueueEntry.Delete();
                    end
                    else begin
                        Error(OperationCancelLbl);
                    end;

                end;

                if JobQueueEntry.Status in [JobQueueEntry.Status::Ready, JobQueueEntry.Status::"In Process"] then
                    Error(StrSubstNo(DeleteErrorLbl, FORMAT(JobQueueEntry.Status)));


            end;
        end;
    end;


    var
        CostCorrStatusLbl: Label 'You are not allowed to change already processed data!';
        DoubleDocCheckLbl: Label 'Cost correction with document %1 , and status %2 already exists !';
        DeleteErrorLbl: Label 'There is related Job Queue Entry with status %1.\Set Job Queue Entry Status to "OnHold"!';
        ConfirmDeletionLbl: Label 'There is related Job Queue Entry which will be also deleted.\Confirm deletion ?';
        OperationCancelLbl: Label 'Operation canceled !';


}

