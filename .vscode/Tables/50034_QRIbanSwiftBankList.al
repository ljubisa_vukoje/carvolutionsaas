table 50034 "QR IBAN Swift bank list"
{

    fields
    {
        field(1; "Bank Code"; Code[20])
        {
            CaptionML = ENU = 'Bank code';
        }

        field(2; "Swift Code"; Text[20])
        {

        }
        field(3; "Description"; Text[100])
        {

        }
        field(4; "Country code"; Text[2])
        {

        }
        field(5; "Place"; Text[20])
        {

        }
    }
    keys
    {


        Key(Key1; "Bank code", "Swift code")
        {


        }

    }
}