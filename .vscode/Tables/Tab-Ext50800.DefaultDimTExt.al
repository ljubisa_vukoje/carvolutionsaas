/// <summary>
/// TableExtension "DefaultDimTExt" (ID 50800) extends Record Default Dimension.
/// </summary>
tableextension 50800 DefaultDimTExt extends "Default Dimension"
{
    fields
    {
        field(50800; ItemNo; Code[50])
        {
            Caption = 'Artikelnummer';
        }
        field(50801; ItemInventory; Integer)
        {
            Caption = 'Lagerbestand';
        }

        field(50802; ItemInventoryEndMonth; Integer)
        {
            Caption = 'Monthly Inventory';
        }

        field(50804; DateEndMonth; DateTime)
        {
            Caption = 'Running Date of Monthly Report';
        }

        field(50803; "Dimension Value Name"; Text[50])
        {
            Caption = 'Dimension Value Name';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup("Dimension Value"."Name" where("Dimension Code" = field("Dimension Code"), "Code" = field("Dimension Value Code")));
        }
    }

    var
        myInt: Integer;
}