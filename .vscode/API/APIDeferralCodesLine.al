page 50124 ApiDeferralCodesLine
{
    PageType = API;
    Caption = 'API Deferral Codes Line';
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'deferralCodesLines';
    EntitySetName = 'deferralCodesLines';
    ODataKeyFields = SystemId;
    SourceTable = "Deferral Line";
    PopulateAllFields = true;
    DelayedInsert = true;




    layout
    {
        area(Content)
        {
            repeater(General)
            {
                field(documentNo; Rec."Document No.")
                {

                }
                field(documentType; Rec."Document Type")
                {

                }
                field(sequence; Rec."Line No.")
                {

                }
                field(postingDate; Rec."Posting Date")
                {

                }


            }


        }
    }


}








