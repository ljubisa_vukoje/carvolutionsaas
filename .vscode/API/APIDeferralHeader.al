page 50123 ApiDeferralHeader
{
    PageType = API;
    Caption = 'API Deferral Header';
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'deferralHeader';
    EntitySetName = 'deferralHeader';
    ODataKeyFields = SystemId;
    SourceTable = "Deferral Header";
    PopulateAllFields = true;
    DelayedInsert = true;




    layout
    {
        area(Content)
        {
            repeater(General)
            {
                field(Id; Rec.SystemId)
                {

                }
                field(sequence; Rec."Line No.")
                {

                }
                field(documentType; Rec."Document Type")
                {

                }

                field(documentNumber; Rec."Document No.")
                {

                }

                field(deferralCode; Rec."Deferral Code")
                {


                }
                field(noOfPeriods; Rec."No. of Periods")
                {

                }
                field(startDate; Rec."Start Date")
                {


                }


            }


        }
    }


}








