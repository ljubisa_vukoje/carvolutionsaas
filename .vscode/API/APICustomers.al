page 50016 ApiCustomer
{
    PageType = API;
    Caption = 'API Customer';
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'customer';
    EntitySetName = 'customer';
    ODataKeyFields = SystemId;
    SourceTable = Customer;
    PopulateAllFields = true;
    DelayedInsert = true;
    ModifyAllowed = true;
    InsertAllowed = true;




    layout
    {
        area(Content)
        {
            repeater(General)
            {
                field(id; Rec.SystemId)
                {

                }
                field(number; Rec."No.")
                {

                }
                field(displayName; Rec.Name)
                {

                }

                field(displayName2; Rec."Name 2")
                {

                }
                field(type; Rec."Partner Type")
                {

                }
                field(addressLine1; Rec.Address)
                {

                }
                field(addressLine2; Rec."Address 2")
                {

                }
                field(city; Rec.City)
                {



                }
                field(state; Rec.County)
                {

                }
                field(country; Rec."Country/Region Code")
                {

                }
                field(postalCode; Rec."Post Code")
                {


                }
                field(phoneNumber; Rec."Phone No.")
                {

                }
                field(email; Rec."E-Mail")
                {

                }
                field(website; Rec."Home Page")
                {

                }
                field(taxLiable; Rec."Tax Liable")
                {

                }
                field(taxAreaCode; Rec."Tax Area Code")
                {

                }


                field(customerPostingGroup; Rec."Customer Posting Group")
                {
                    trigger OnValidate()
                    begin
                        case Rec."Customer Posting Group" of
                            'INLAND':
                                begin
                                    Rec.Validate("Gen. Bus. Posting Group", 'INLAND');
                                    Rec.validate("VAT Bus. Posting Group", 'INLAND');
                                end;
                            'AUSLAND':
                                begin
                                    Rec.Validate("Gen. Bus. Posting Group", 'AUSLAND');
                                    Rec.validate("VAT Bus. Posting Group", 'AUSLAND');
                                end;
                        end;
                    end;


                }
                field(taxRegistrationNumber; Rec."VAT Registration No.")
                {

                }
                field(currencyId; Rec."Currency Id")
                {

                }
                field(currencyCode; Rec."Currency Code")
                {

                }
                field(paymentTermsCode; Rec."Payment Terms Code")
                {

                }
                field(paymentTermsId; Rec."Payment Terms Id")
                {

                }
                field(shipmentMethodId; Rec."Shipment Method Id")
                {

                }
                field(paymentMethodId; Rec."Payment Method Id")
                {

                }
                field(blocked; Rec.Blocked)
                {

                }
                field(languageCode; Rec."Language Code")
                {
                    trigger OnValidate()
                    begin
                        case Rec."Language Code" OF
                            'DE':
                                Rec.Validate("Reminder Terms Code", 'NORMAL');
                            'FR':
                                Rec.Validate("Reminder Terms Code", 'NORMAL F');
                            'EN':
                                Rec.Validate("Reminder Terms Code", 'NORMAL E');
                            ELSE
                                Rec.Validate("Reminder Terms Code", 'NORMAL');
                        END;
                    end;


                }
                field(lastModifiedDateTime; Rec."Last Modified Date Time")
                {

                }
            }


        }
    }

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    var
        PostCode: Record "Post Code";
    begin
        Rec."Prices Including VAT" := true;
        if (Rec."Country/Region Code" = '') and (Rec."Post Code" <> '') then begin
            if PostCode.Get(Rec."Post Code", Rec.City) then begin
                Rec."Country/Region Code" := PostCode."Country/Region Code";
            end
            else begin
                PostCode.SetFilter(Code, Rec."Post Code");
                if PostCode.FindFirst() then
                    Rec."Country/Region Code" := PostCode."Country/Region Code";
            end;
        end;
    end;
}








