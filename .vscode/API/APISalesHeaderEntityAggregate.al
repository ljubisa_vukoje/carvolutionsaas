page 50122 ApiSalesHeaderEntityAgg
{
    PageType = API;
    Caption = 'API Sales Header Entity Agg';
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'mySalesHeaderEntityAgg';
    EntitySetName = 'mySalesHeaderEntityAgg';
    ODataKeyFields = SystemId;
    SourceTable = "Sales Invoice Entity Aggregate";
    DelayedInsert = true;


    layout
    {

        area(Content)
        {

            field(id; Rec.SystemId)
            {
                Caption = 'Id';
                Editable = false;
            }
            field(documentType; Rec."Document Type")
            {

            }
            field("number"; Rec."No.")
            {

            }
            field(status; Rec.Status)
            {

            }
            field(posted; Rec.Posted)
            {

            }
            field(postingNoSeries; Rec."Posting No. Series")
            {


            }
            field(sellToCustomerNo; Rec."Sell-to Customer No.")
            {

            }
            field(postingDate; Rec."Posting Date")
            {

            }

            field(Remark; Rec.Remark)
            {


            }
            field(carvolutionVehicleId; Rec.CarvolutionVehicleId)
            {

            }

        }

    }



    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        Rec."Document Type" := Rec."Document Type"::Invoice;

    end;



    trigger OnNewRecord(BelowxRec: Boolean)
    begin

        Rec."Document Type" := Rec."Document Type"::Invoice;
    end;


    procedure CheckSalesPersonCode()
    var
        SalesPerson: Record "Salesperson/Purchaser";
    begin
        if (Rec."Salesperson Code" <> '') then begin
            SalesPerson.SetRange(Code, Rec."Salesperson Code");
            if not SalesPerson.Find('-') then begin
                SalesPerson.Init();
                SalesPerson.Code := Rec."Salesperson Code";
                SalesPerson.Insert();
            end;

        end;

    end;






}