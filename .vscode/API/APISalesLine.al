page 50015 ApiSalesLine
{
    PageType = API;
    Caption = 'API Sales Line';
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'salesInvoiceLines';
    EntitySetName = 'salesInvoiceLines';
    ODataKeyFields = SystemId;
    SourceTable = "Sales Line";
    PopulateAllFields = true;
    DelayedInsert = true;




    layout
    {
        area(Content)
        {
            repeater(General)
            {
                field(SystemId; Rec.SystemId)
                {

                }
                field(sequence; Rec."Line No.")
                {

                }
                field(lineType; Rec.Type)
                {

                }
                field(lineObjectNumber; Rec."No.")
                {

                }
                field(documentType; Rec."Document Type")
                {

                }
                field(documentNumber; Rec."Document No.")
                {

                }
                field(postingGroup; Rec."Posting Group")
                {

                }
                field(description; Rec.Description)
                {



                }
                field(description2; Rec."Description 2")
                {

                }
                field(unitOfMeasureCode; Rec."Unit of Measure Code")
                {

                }
                field(deferralCode; Rec."Deferral Code")
                {


                }

                field(deferralStartDate; Rec."Deferral  Start Date")
                {


                }
                field(unitPrice; Rec."Unit Price")
                {

                }
                field(quantity; Rec.Quantity)
                {

                }
                field(discountPercent; Rec."Line Discount %")
                {

                }
                field(taxCode; Rec."VAT Prod. Posting Group")
                {

                }
                field(taxPercent; Rec."VAT %")
                {

                }
                field(amountExcludingTax; Rec.Amount)
                {

                }
                field(totalTaxAmount; Rec."Amount Including VAT" - Rec.Amount)
                {

                }
                field(amountIncludingTax; Rec."Amount Including VAT")
                {

                }
                part(myDeferralHeader; ApiDeferralHeader)
                {
                    Caption = 'Deferral Header';

                    SubPageLink = "Document No." = Field("Document No.");
                }

            }


        }
    }

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    var
        SalesHeader: Record "Sales Header";
        DimensionValues: Record "Dimension Value";
    begin

        SalesHeader.Reset();
        SalesHeader.SetRange("No.", Rec."Document No.");
        SalesHeader.Find('-');
        if SalesHeader.CarvolutionVehicleId <> '' then begin
            DimensionValues.Reset();
            DimensionValues.SetRange("Dimension Code", 'KOSTENTRÄGER');
            DimensionValues.SetRange(Carvolution_ID, SalesHeader.CarvolutionVehicleId);
            DimensionValues.SetRange("Dimension Value Type", DimensionValues."Dimension Value Type"::Standard);
            if not DimensionValues.Find('-') then
                Error(StrSubstNo('CarvolutionId %1 does not exist in database !', SalesHeader.CarvolutionVehicleId))
            else begin
                Rec.Validate("Kostenträger Code", DimensionValues.Code);
                Rec.Validate("Shortcut Dimension 2 Code", DimensionValues.Code);
            end;
        end;
    end;




}








