page 50121 ApiDeferralCodes
{
    PageType = API;
    Caption = 'API Deferral Codes';
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'myDeferralCodes';
    EntitySetName = 'myDeferralCodes';
    SourceTable = "Deferral Header";
    DelayedInsert = true;
    Editable = true;
    SourceTableView = where("Deferral Doc. Type" = const(Sales));



    layout
    {
        area(Content)
        {
            field(Id; Rec.SystemId)
            {

            }
            field(documentNo; Rec."Document No.")
            {

            }
            field(lineNo; Rec."Line No.")
            {

            }
            field(genJnlTemplateName; Rec."Gen. Jnl. Template Name")
            {

            }
            field(genJnlBatchName; Rec."Gen. Jnl. Batch Name")
            {

            }
            field(deferralCode; Rec."Deferral Code")
            {

            }
            field(description; Rec."Schedule Description")
            {

            }
            field(noOfPeriods; Rec."No. of Periods")
            {
            }
            field(startDate; Rec."Start Date")
            {

            }
            field(documentType; Rec."Document Type")
            {

            }
            field(deferralDocumentType; Rec."Deferral Doc. Type")
            {

            }

            part(myDeferralLines; ApiDeferralCodesLine)
            {
                Caption = 'Deferral Lines';

                SubPageLink = "Document No." = Field("Document No."), "Line No." = field("Line No.");
            }

        }

    }



}
