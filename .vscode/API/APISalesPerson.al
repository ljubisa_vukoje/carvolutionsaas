page 50125 ApiSalesPerson
{
    PageType = API;
    Caption = 'API SalesPerson';
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'mySalesPerson';
    EntitySetName = 'mySalesPerson';
    SourceTable = "Salesperson/Purchaser";
    DelayedInsert = true;
    Editable = true;



    layout
    {
        area(Content)
        {
            field(Id; Rec.SystemId)
            {

            }
            field(code; Rec.Code)
            {

            }



        }

    }



    procedure CheckSalesPersonCode()
    var
        SalesPerson: Record "Salesperson/Purchaser";
    begin
        if (Rec.Code <> '') then begin
            SalesPerson.SetRange(Code, Rec.Code);
            if not SalesPerson.Find('-') then begin
                SalesPerson.Init();
                SalesPerson.Code := Rec.Code;
                SalesPerson.Insert();
            end;

        end;

    end;


}
