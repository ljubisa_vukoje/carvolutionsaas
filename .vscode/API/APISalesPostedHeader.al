page 50126 ApiSalesInvoiceHeader
{
    PageType = API;
    Caption = 'API Sales Invoice Header';
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'mySalesInvoiceHeader';
    EntitySetName = 'mySalesInvoiceHeader';
    ODataKeyFields = Id;
    SourceTable = "Sales Invoice Entity Aggregate";
    DelayedInsert = true;


    layout
    {

        area(Content)
        {
            field(SystemId; Rec.SystemId)
            {
                Caption = 'SystemId';
                Editable = false;
            }
            field(Id; Rec.Id)
            {
                Caption = 'Id';
                Editable = false;
            }
            field(No; Rec."No.")
            {
                Caption = 'No.';
            }
            field(CustomerName; Rec."Sell-to Customer Name")
            {
                Caption = 'Customer name';
            }
            field(PostingDate; Rec."Posting Date")
            {
                Caption = 'Posting date';
            }
            field(Status; Rec.Status)
            {
                Caption = 'Status';
            }

        }


    }

    local procedure SetActionResponse(var ActionContext: WebServiceActionContext; InvoiceId: Guid)
    var
    begin
        SetActionResponse(ActionContext, Page::ApiSalesInvoiceHeader, InvoiceId);
    end;

    local procedure SetActionResponse(var ActionContext: WebServiceActionContext; PageId: Integer; DocumentId: Guid)
    var
    begin
        ActionContext.SetObjectType(ObjectType::Page);
        ActionContext.SetObjectId(PageId);
        ActionContext.AddEntityKey(Rec.FieldNo(SystemId), DocumentId);
        ActionContext.SetResultCode(WebServiceActionResultCode::Created);
    end;

    [ServiceEnabled]
    [Scope('Cloud')]
    procedure MakeCorrectiveCreditMemo(var ActionContext: WebServiceActionContext)
    var
        SalesHeader: Record "Sales Header";
        CorrectPostedSalesInvoice: Codeunit "Correct Posted Sales Invoice";
        CopyDocMgt: Codeunit "Copy Document Mgt.";
        NoSerieMgt: Codeunit NoSeriesManagement;
        SalesSetup: Record "Sales & Receivables Setup";
        CustLedgerEntry: Record "Cust. Ledger Entry";
        DayInPostingDate: Integer;
        MonthInPostingDate: Integer;
        YearInPostingDate: Integer;
        SalesLine: Record "Sales Line";
        PostingDateForCalc: Date;
        NewSalesHeaderPostingDate: Date;
        NewDate: Date;
        DefferalHeader: Record "Deferral Header";
        DefferalLine: Record "Deferral Line";

    begin
        SalesSetup.Reset();
        SalesSetup.FindFirst();
        Clear(SalesHeader);
        SalesHeader.Init();
        SalesHeader."No." := NoSerieMgt.GetNextNo(SalesSetup."Credit Memo Nos.", TODAY, true);
        SalesHeader."Document Type" := SalesHeader."Document Type"::"Credit Memo";
        SalesHeader.Insert(true);

        CopyDocMgt.SetPropertiesForCreditMemoCorrection();
        CopyDocMgt.CopySalesDocForInvoiceCancelling(Rec."No.", SalesHeader);
        SalesHeader.Validate("External Document No.", Rec."No.");
        SalesHeader.Modify();


        CustLedgerEntry.Reset();
        CustLedgerEntry.SetRange("Document No.", Rec."No.");
        CustLedgerEntry.SetRange("Document Type", Rec."Document Type"::Invoice);
        CustLedgerEntry.SetRange(Open, true);
        if CustLedgerEntry.Find('-') then begin
            SalesHeader.Validate("Applies-to Doc. Type", SalesHeader."Document Type"::Invoice);
            SalesHeader.Validate("Applies-to Doc. No.", Rec."No.");
            SalesHeader.Modify();
        end;


        // NewDate := WorkDate(20220820D);
        DayInPostingDate := DATE2DMY(WorkDate(), 1);
        MonthInPostingDate := DATE2DMY(WorkDate(), 2);
        YearInPostingDate := DATE2DMY(WorkDate(), 3);

        if DayInPostingDate < 10 then
            PostingDateForCalc := CALCDATE('<-1M-CM>', TODAY)
        else
            PostingDateForCalc := CALCDATE('<-CM>', TODAY);

        if SalesHeader."Posting Date" < PostingDateForCalc then
            SalesHeader.Validate("Posting Date", PostingDateForCalc);


        SalesHeader."Document Date" := TODAY;
        SalesHeader.Modify();



        DefferalHeader.Reset();
        DefferalHeader.SetRange("Document No.", SalesHeader."No.");

        IF DefferalHeader.Find('-') then begin
            repeat
                if DefferalHeader."Start Date" < SalesHeader."Posting Date" then begin
                    SalesLine.Reset();
                    SalesLine.SetRange("Document No.", DefferalHeader."Document No.");
                    SalesLine.SetRange("Line No.", DefferalHeader."Line No.");
                    if SalesLine.Find('-') then begin
                        SalesLine.Validate("Deferral Code", '');
                        SalesLine.Modify();
                    end;
                end;
            until DefferalHeader.Next() = 0;

        end;

        //bound action can not return any object or data
        SetActionResponse(ActionContext, Page::"ApiSalesInvoiceHeader", SalesHeader.SystemId);
    end;



}