page 50127 APICostEntriesPage
{
    PageType = API;
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    Caption = 'API CostEntries';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'costEntries';
    EntitySetName = 'costEntries';
    ODataKeyFields = SystemId;
    SourceTable = "Cost Entry";
    PopulateAllFields = true;
    DelayedInsert = true;
    Editable = false;



    layout
    {

        area(Content)
        {
            repeater(General)
            {
                field(id; Rec.SystemId)
                {

                }
                field(entryNo; Rec."Entry No.")
                {


                }

                field(documentNo; Rec."Document No.")
                {

                }
                field(description; Rec.Description)
                {

                }
                field(vendorNo; Rec."Vendor No.")
                {

                }
                field(vendorName; Rec."Vendor Name")
                {

                }
                field(customerNo; Rec."Customer No.")
                {

                }
                field(customerName; Rec."Customer Name")
                {

                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {

                }
                field(voidedComment; Rec."Voided Comment")
                {

                }
                field(batchName; rec."Batch Name")
                {

                }
                field(userID; Rec."User ID")
                {

                }
                field(costTypeNo; Rec."Cost Type No.")
                {

                }
                field(costCenterCode; Rec."Cost Center Code")
                {

                }
                field(costObjectCode; Rec."Cost Object Code")
                {

                }
                field(debitAmount; Rec."Debit Amount")
                {

                }

                field(creditAmount; Rec."Credit Amount")
                {

                }
                field(amount; Rec.Amount)
                {

                }
                field(postingDate; Rec."Posting Date")
                {

                }
                field(reasonCode; Rec."Reason Code")
                {

                }
                field(G_L_Account; Rec."G/L Account")
                {

                }
                field(G_L_EntryNo; Rec."G/L Entry No.")
                {

                }
                field(sourceCode; Rec."Source Code")
                {

                }
                field(systemCreatedEntry; Rec."System-Created Entry")
                {

                }
                field(allocated; Rec.Allocated)
                {

                }
                field(allocatedwithJournalNo; Rec."Allocated with Journal No.")
                {

                }
                field(allocationDescription; Rec."Allocation Description")
                {

                }
                field(allocationID; Rec."Allocation ID")
                {

                }
                field(additionalCurrencyAmount; Rec."Additional-Currency Amount")
                {

                }
                field(addCurrencyDebitAmount; Rec."Add.-Currency Debit Amount")
                {

                }
                field(addCurrencyCreditAmount; Rec."Add.-Currency Credit Amount")
                {

                }
                field(documentType; Rec."Document Type New")
                {

                }


            }

        }

    }


}