page 50120 ApiSalesHeader
{
    PageType = API;
    Caption = 'API Sales Header';
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'mySalesHeader';
    EntitySetName = 'mySalesHeader';
    ODataKeyFields = SystemId;
    SourceTable = "Sales Header";
    DelayedInsert = true;


    layout
    {

        area(Content)
        {

            field(id; Rec.SystemId)
            {
                Caption = 'Id';
                Editable = false;
            }
            field(documentType; Rec."Document Type")
            {
                trigger OnValidate()

                begin

                    Rec."Document Type" := Rec."Document Type"::Invoice;

                end;

            }

            field(postingNoSeries; Rec."Posting No. Series")
            {



            }
            field("number"; Rec."No.")
            {
            }

            field("externalDocumentNumber"; Rec."External Document No.")
            {

            }

            field("invoiceDate"; Rec."APISalesDocumentDate")
            {

            }

            field("postingDate"; Rec."Posting Date")
            {

            }

            field("dueDate"; Rec."Due Date")
            {

            }
            field(customerNumber; Rec."Sell-to Customer No.")
            {
                trigger OnValidate()
                begin
                    Rec."Posting No. Series" := '';
                end;
            }
            field(customerName; Rec."Sell-to Customer Name")
            {

            }
            field("shipToName"; Rec."Ship-to Name")
            {

            }
            field("shipToContact"; Rec."Ship-to Contact")
            {

            }
            field("sellToAddressLine1"; Rec."Sell-to Address")
            {

            }
            field("sellToAddressLine2"; Rec."Sell-to Address")
            {

            }
            field("sellToCity"; Rec."Sell-to City")
            {

            }
            field(sellToCountry; Rec."Sell-to Country/Region Code")
            {

            }
            field(sellToPostCode; Rec."Sell-to Post Code")
            {

            }
            field(currencyCode; Rec."Currency Code")
            {
                trigger OnValidate()
                begin
                    Rec."Posting No. Series" := '';
                end;
            }
            field(paymentTermsCode; Rec."Payment Terms Code")
            {

            }
            field(paymentMethodCode; Rec."Payment Method Code")
            {

            }
            field(salesPerson; Rec.APISalesPersonCode)
            {

            }
            field(priceIncludeTax; Rec."Prices Including VAT")
            {

            }
            field(status; Rec.Status)
            {

            }
            field(phoneNumber; Rec."Sell-to Phone No.")
            {

            }
            field(email; Rec."Sell-to E-Mail")
            {

            }
            field(remark; Rec.Remark)
            {

            }
            field(carvolutionVehicleId; Rec.CarvolutionVehicleId)
            {

            }


            part(mySalesLine; ApiSalesLine)
            {
                Caption = 'Sales Invoice Lines';

                SubPageLink = "Document No." = Field("No.");
            }
            part(myDeferralHeader; ApiDeferralHeader)
            {
                Caption = 'Deferral Header';

                SubPageLink = "Document No." = Field("No.");
            }


        }



    }

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        CLEAR(MyEventSUbscriber);
        Rec."Document Type" := Rec."Document Type"::Invoice;
        Rec.WebOrder := true;
        MyEventSUbscriber.SubcribeToOnAfterInitPostingNoSeries(Rec, xRec);

    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        CLEAR(MyEventSUbscriber);
        Rec."Document Type" := Rec."Document Type"::Invoice;
        Rec.WebOrder := true;
        MyEventSUbscriber.SubcribeToOnAfterInitPostingNoSeries(Rec, xRec);

    end;

    [ServiceEnabled]
    [Scope('Cloud')]
    procedure Test(var ActionContext: WebServiceActionContext): Text
    begin
        exit('Test');
    end;

    [ServiceEnabled]
    procedure Copy(var actionContext: WebServiceActionContext)
    var
        FromSalesHeader: Record "Sales Header";
        ToSalesHeader: Record "Sales Header";
        SalesSetup: Record "Sales & Receivables Setup";
        CopyDocMgt: Codeunit "Copy Document Mgt.";
        DocType: Option Quote,"Blanket Order",Order,Invoice,"Return Order","Credit Memo","Posted Shipment","Posted Invoice","Posted Return Receipt","Posted Credit Memo";
    begin
        SalesSetup.Get;
        CopyDocMgt.SetProperties(true, false, false, false, false, SalesSetup."Exact Cost Reversing Mandatory", false);

        FromSalesHeader.Get(Rec."Document Type", Rec."No.");
        ToSalesHeader."Document Type" := FromSalesHeader."Document Type";
        ToSalesHeader.Insert(true);

        CopyDocMgt.CopySalesDoc(DocType::Invoice, FromSalesHeader."No.", ToSalesHeader);

        actionContext.SetObjectType(ObjectType::Page);
        actionContext.SetObjectId(Page::ApiSalesHeader);
        actionContext.AddEntityKey(Rec.FIELDNO(Id), ToSalesHeader.Id);

        // Set the result code to inform the caller that an item was created.
        actionContext.SetResultCode(WebServiceActionResultCode::Created);

    end;

    var
        MyEventSUbscriber: Codeunit MyEventSubscriberCU;









}