page 50034 APICostObjectCorrectionsPage
{
    PageType = API;
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    Caption = 'API Cost Object Corrections';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'costObjectCorrections';
    EntitySetName = 'costObjectCorrections';
    //ODataKeyFields = SystemId;
    SourceTable = CostObjectCorrectionTable;
    //PopulateAllFields = true;
    DelayedInsert = true;



    layout
    {

        area(Content)
        {
            part(costObjectCorrection; APICostObjectCorrectionPage)
            {


            }
        }




    }

    trigger OnNewRecord(BelowxRec: Boolean)
    begin

        Rec.Insert(False);

    end;


    trigger OnAfterGetRecord()
    begin
        if (Rec."Document No." = '') then
            Rec.Delete();
    end;









}