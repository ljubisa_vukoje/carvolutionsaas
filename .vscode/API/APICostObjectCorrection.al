page 50033 APICostObjectCorrectionPage
{
    PageType = API;
    APIPublisher = 'HolyErp';
    APIGroup = 'app1';
    Caption = 'API Cost Object Correction';
    APIVersion = 'v2.0', 'v1.0';
    EntityName = 'costObjectCorrection';
    EntitySetName = 'costObjectCorrection';
    ODataKeyFields = SystemId;
    SourceTable = CostObjectCorrectionTable;
    PopulateAllFields = true;
    DelayedInsert = true;



    layout
    {

        area(Content)
        {
            repeater(General)
            {
                field(id; Rec.SystemId)
                {

                }
                field(entryNo; Rec."Entry No.")
                {


                }

                field(documentNo; Rec."Document No.")
                {

                }
                field(carvolutionVehicleID; Rec.Carvolution_ID)
                {

                }

            }

        }

    }



    procedure GetLastEntryNo(): Integer
    var
        CostCorrObject: Record CostObjectCorrectionTable;
    begin
        CostCorrObject.Reset();
        CostCorrObject.SetCurrentKey("Entry No.");
        if CostCorrObject.Find('+') then
            Rec."Entry No." := CostCorrObject."Entry No." + 1
        else
            Rec."Entry No." := 1;
    end;




}