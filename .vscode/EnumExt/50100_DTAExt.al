enum 50100 "DTA/EZAG/ISO"
{
    Extensible = true;
    AssignmentCompatibility = true;

    value(0; DTA) { Caption = 'DTA'; }
    value(1; EZAG) { Caption = 'EZAG'; }
    value(2; ISO) { Caption = 'ISO'; }

}
enum 50101 "Payment Form"
{
    Extensible = true;
    AssignmentCompatibility = true;

    value(0; ESR) { Caption = 'ESR'; }
    value(1; "ESR+") { Caption = 'ESR+'; }
    value(2; "Post Payment Domestic") { Caption = 'Post Payment Domestic'; }
    value(4; "Bank Payment Domestic") { Caption = 'Bank Payment Domestic'; }
    value(5; "Cash Outpayment Order Domestic") { Caption = 'Cash Outpayment Order Domestic'; }
    value(6; "Post Payment Abroad") { Caption = 'Post Payment Abroad'; }
    value(7; "Bank Payment Abroad") { Caption = 'Bank Payment Abroad'; }
    value(8; "SWIFT Payment Abroad") { Caption = 'SWIFT Payment Abroad'; }
    value(9; "Cash Outpayment Order Abroad") { Caption = 'Cash Outpayment Order Abroad'; }
    value(10; "QR with 27 digit Reference") { Caption = 'QR with 27 digit Reference'; }
    value(11; "QR") { Caption = 'QR'; }

}
/// <summary>
/// We don't use this anymore
/// we use Vehicle_Category and Vehicle_Status code type
/// </summary>
enum 50102 "Vehicle Category"
{
    CaptionML = ENU = 'Vehicle Category', DES = 'Fahrzeugkategorie';
    Extensible = true;
    AssignmentCompatibility = true;
    ObsoleteState = Pending;
    ObsoleteReason = 'Incorrect variable type';

    value(0; Cabriolet) { Caption = 'Cabriolet'; }
    value(1; Limousine) { Caption = 'Limousine'; }
    value(2; Small_Car) { CaptionML = ENU = 'Small Car', DES = 'Kleines Auto'; }
    value(3; SUV) { Caption = 'SUV'; }
    value(4; VAN) { Caption = 'VAN'; }
    value(5; Wagon) { Caption = 'Wagon'; }

}


enum 50103 "Vehicle Status"
{
    CaptionML = ENU = 'Vehicle Status', DES = 'Fahrzeugstatus';
    Extensible = true;
    AssignmentCompatibility = true;
    ObsoleteState = Pending;
    ObsoleteReason = 'Incorrect variable type';

    value(0; Ausgeflottet) { CaptionML = ENU = 'Fled out', DES = 'Ausgeflottet'; }
    value(1; Betrieb) { CaptionML = ENU = 'Operation', DES = 'Betrieb'; }
    value(2; Haendlerlager) { CaptionML = ENU = 'Dealer Warehouse', DES = 'Haendlerlager'; }
    value(3; Instandstellung) { CaptionML = ENU = 'Repair', DES = 'Instandstellung'; }
    value(4; Lager) { CaptionML = ENU = 'Warehouse', DES = 'Lager'; }
    value(5; Zum_Verkauf) { CaptionML = ENU = 'For Sale', DES = 'Zum Verkauf'; }
    value(6; In_Produktion) { CaptionML = ENU = 'In Production', DES = 'In Produktion'; }

}

enum 50104 "Cost Correction Status"
{
    Caption = 'Cost Correction Status';
    Extensible = true;
    AssignmentCompatibility = true;

    value(0; Open) { Caption = 'Open'; }
    value(1; Error) { Caption = 'Error'; }
    value(2; "In Process") { Caption = 'In Process'; }
    value(3; Complete) { Caption = 'Complete'; }
    value(4; "Dim.Corr. Finished") { Caption = 'Dim.Corr. Finished'; }
    value(5; "Completed Prev.") { Caption = 'Completed Prev.'; }

}
