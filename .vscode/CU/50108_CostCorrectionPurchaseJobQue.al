codeunit 50108 CostCorrectionPurchaseJobQueue
{

    trigger OnRun()
    begin
        Clear(CostCorrectionPurchase);
        if CostCorrectionPurchase.Run() then;
    end;

    var
        CostCorrectionPurchase: Codeunit CostObjectCorrectionPurchCU;

}

