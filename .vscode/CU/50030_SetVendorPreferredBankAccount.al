/// <summary>
/// Vukoje Ljubisa
/// 26/09/2022
/// Set preferred bank account on vendor card
/// </summary>
codeunit 50030 SetVendorPreferredBankAccount
{
    trigger OnRun()
    begin
        SetVendorBankAccount();
    end;


    procedure SetVendorBankAccount()
    var
        Vendor: Record Vendor;
        VendorBankAcc: Record "Vendor Bank Account";
    begin
        Vendor.Reset();
        Vendor.SetRange("Preferred Bank Account Code", '');
        if Vendor.Find('-') then begin
            repeat
                VendorBankAcc.Reset();
                VendorBankAcc.SetRange("Vendor No.", Vendor."No.");
                // VendorBankAcc.SetRange(Code, 'BANK');
                VendorBankAcc.SetFilter(Code, '<>%1', '');
                if VendorBankAcc.Find('-') then begin
                    // Vendor.Validate(Vendor."Preferred Bank Account Code", 'BANK');
                    Vendor.Validate(Vendor."Preferred Bank Account Code", VendorBankAcc.Code);
                    Vendor.Modify();
                end;
            until Vendor.Next() = 0;
        end;

    end;
}