
/// <summary>
/// Codeunit ImportMissingAttachments (ID 50002).
/// </summary>
codeunit 50002 ImportMissingAttachments
{
    trigger OnRun()
    begin
        UploadResult := UploadIntoStream(ImportZipTxt, '', '(*.zip*)|*.zip', FileName, ReadStream);
        ImportAttachSpecInExcelFile(ReadStream);
    end;

    var
        UploadResult: Boolean;
        ReadStream: InStream;
        ImportZipTxt: Label 'Import zip file';
        FileName: Text;
        RecNo: Text;

    local procedure ImportAttachSpecInExcelFile(ReadStream: Instream)
    var
        TempExcelBuffer: Record "Excel Buffer" temporary;
        Zip: Codeunit "Data Compression";
        FileManagement: Codeunit "File Management";
        TempBlob: Codeunit "Temp Blob";
        FileInStream: InStream;
        CurrRec: Integer;
        NoOfRecs: Integer;
        ImportExcelTxt: Label 'Import excel file';
        AttachmentName: Text;
        ErrorMessage, SheetName : Text;
        Window: Dialog;
    begin

        FileManagement.BLOBImportWithFilter(TempBlob, ImportExcelTxt, '', FileManagement.GetToFilterText('', '.xlsx'), 'xlsx');
        TempBlob.CreateInStream(FileInStream);
        SheetName := TempExcelBuffer.SelectSheetsNameStream(FileInStream);
        TempBlob.CreateInStream(FileInStream);
        ErrorMessage := TempExcelBuffer.OpenBookStream(FileInStream, SheetName);
        if ErrorMessage <> '' then
            Error(ErrorMessage);

        TempExcelBuffer.ReadSheet();
        if TempExcelBuffer.FindSet() then begin
            NoOfRecs := TempExcelBuffer.Count;
            Message('%1: %2', 'Number of missing attachments: ', NoOfRecs);
            Zip.OpenZipArchive(ReadStream, false);
            Window.OPEN('Processing data... @1@@@@@@@@@@');
            repeat
                CurrRec += 1;
                IF NoOfRecs <= 100 THEN
                    Window.UPDATE(1, (CurrRec / NoOfRecs * 10000) div 1)
                ELSE
                    IF CurrRec mod (NoOfRecs div 100) = 0 THEN
                        Window.UPDATE(1, (CurrRec / NoOfRecs * 10000) div 1);
                RecNo := TempExcelBuffer."Cell Value as Text";
                AttachmentName := RecNo + '.pdf';
                AddAttachmentToPostedPurchInv(Zip, AttachmentName);
            //Message('%1, %2: %3', TempExcelBuffer."Row No.", TempExcelBuffer."Column No.", TempExcelBuffer."Cell Value as Text");
            until TempExcelBuffer.Next() < 1;
            Zip.CloseZipArchive();
            Clear(ReadStream);
            Window.CLOSE;
            Message('Finished');
        end;
    end;

    local procedure AddAttachmentToPostedPurchInv(Zip: Codeunit "Data Compression"; FileName: Text[250])
    var
        TempFiles: Record BlobValues temporary;
        DocumentAttachment: Record "Document Attachment";
        PurchInvHeader: Record "Purch. Inv. Header";
        RecRef: RecordRef;
        DocInStreamL: InStream;
        FileLength: Integer;
        i: Integer;
        FileList: List of [Text];
        DocOutStreamL: OutStream;

    begin
        PurchInvHeader.Get(RecNo);
        RecRef.Get(PurchInvHeader.RecordId);
        DocumentAttachment.InitFieldsFromRecRef(RecRef);
        DocumentAttachment."Document Flow Sales" := RecRef.Number() = Database::"Sales Header";
        DocumentAttachment."Document Flow Purchase" := RecRef.Number() = Database::"Purchase Header";
        Zip.GetEntryList(FileList);
        if FileList.Contains(FileName) then begin
            i := FileList.IndexOf(FileName);
            TempFiles.INIT;
            TempFiles."File Name" := FileList.Get(i);
            TempFiles.Content.CreateOutStream(DocOutStreamL);
            Zip.ExtractEntry(FileList.Get(i), DocOutStreamL, FileLength);
            TempFiles.INSERT;
            TempFiles.Content.CreateInStream(DocInStreamL);
            DocumentAttachment.SaveAttachmentFromStream(DocInStreamL, RecRef, FileList.Get(i));
        end;
        Clear(DocInStreamL);
        Clear(DocOutStreamL);
    end;

}
