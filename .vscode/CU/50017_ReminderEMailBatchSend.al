
codeunit 50017 "Reminder Email Batch Send"
{



    procedure SendReminder(ReminderCode: code[20])
    var

        Customer: Record Customer;
        IssuedReminders: Record "Issued Reminder Header";
        IssuedReminderTemplate: Record "Reminder Email Template";
        RecordRef: RecordRef;
        FieldRef: FieldRef;
        RecordRefAttach: RecordRef;
        FieldRefAttach: FieldRef;
        EmailSubject: text;
        EmailBody: text;
        Recipients: List of [Text];
        RecipientsCC: List of [Text];
        RecipientsBCC: List of [Text];
        CurrencyCode: COde[10];
        LogoFile: Text;
        ExportFile: FIle;
        CustomerText: List of [Text];
        DummyBool: Boolean;
        IssuedReminderLines: Record "Issued Reminder Line";
        SalesInvHeader: Record "Sales Invoice Header";
        UserText: List of [Text];
        UserName: Text[30];
        UserNameFC: Text[1];
        UserNameFULL: Text[32];
    begin
        CLEAR(CUEnvInformation);
        Clear(OStream);
        Clear(IssueReminderReport);
        Clear(IStream);
        CLear(EmailCU);
        Clear(EmailMessageCU);
        Clear(EmailBody);
        CLear(IssueReminderReport);
        CLear(TempBlob);
        Clear(Recipients);
        Clear(RecipientsBCC);


        if not IssuedReminders.get(ReminderCode) then
            exit;


        DummyBool := IssuedReminders.CalcFields("Remaining Amount");
        if IssuedReminders."Remaining Amount" = 0 then
            exit;

        Customer.Reset;
        Customer.get(IssuedReminders."Customer No.");
        if Customer."E-Mail" = '' then
            exit;//if email address does not exist just skip
        // Error(Text001, Customer.Name);

        if CUEnvInformation.IsProduction() then begin
            Recipients.Add(Customer."E-Mail")
        end
        else begin
            Recipients.Add('robin.schmid@carvolution.com');
            //Recipients.Add('ljubisa.vukoje@holyerp.rs');
            Recipients.Add('maria.okolisan@holyerp.rs');
            Recipients.Add('lazar.jevtic@holyerp.rs');
            Recipients.Add('selina.ris@ximiq.ch');
        end;


        Lang := Customer."Language Code";
        if Lang = '' then
            Lang := 'ENU';


        RecordRef.Open(Database::"Issued Reminder Header");
        FieldRef := RecordRef.Field(1);
        FieldRef.SetRange(IssuedReminders."No.");
        if not RecordRef.Find('-') then
            exit;



        if not IssuedReminderTemplate.Get(Lang) then
            exit;

        if IssuedReminders."Currency Code" <> '' then
            CurrencyCode := IssuedReminders."Currency Code"
        else
            CurrencyCode := 'CHF';

        CustomerText := Customer.Name.Split(' ');
        if Customer."Name 2" <> '' then
            CustomerText := Customer."Name 2".Split(' ');

        UserText := UserId.Split('.');
        UserName := LowerCase(UserText.Get(1));
        UserNameFC := CopyStr(UserName, 1, 1);
        UserNameFC := UpperCase(UserNameFC);
        UserNameFULL := CopyStr(UserName, 2, StrLen(UserName) - 1);
        UserName := UserNameFC + UserNameFULL;

        DummyBool := IssuedReminders.CalcFields("Remaining Amount");
        //EmailSubject := IssuedReminderTemplate."Email Subject";
        if IssuedReminders."Reminder Level" = 4 then begin
            EmailBody := StrSubstNo(IssuedReminderTemplate.GetEmailBodyLvl4(), CustomerText.Get(1), IssuedReminders."Remaining Amount");
        end
        else begin
            EmailBody := StrSubstNo(IssuedReminderTemplate.GetEmailBody(), CustomerText.Get(1), IssuedReminders."Remaining Amount");
        end;

        case IssuedReminders."Reminder Level" of
            1:
                if (IssuedReminderTemplate."Language Code" = 'DE') then begin
                    EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '1.');
                end
                else
                    if (IssuedReminderTemplate."Language Code" = 'FR') then begin
                        EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '1.');
                    end
                    else begin
                        EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '1st');
                    end;
            2:
                if (IssuedReminderTemplate."Language Code" = 'DE') then begin
                    EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '2.');
                end
                else
                    if (IssuedReminderTemplate."Language Code" = 'FR') then begin
                        EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '2.');
                    end
                    else begin
                        EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '2nd');
                    end;
            3:
                if (IssuedReminderTemplate."Language Code" = 'DE') then begin
                    EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '3.');
                end
                else
                    if (IssuedReminderTemplate."Language Code" = 'FR') then begin
                        EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '3.');
                    end
                    else begin
                        EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '3rd');
                    end;
            4:
                if (IssuedReminderTemplate."Language Code" = 'DE') then begin
                    EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '4.');
                end
                else
                    if (IssuedReminderTemplate."Language Code" = 'FR') then begin
                        EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '4.');
                    end
                    else begin
                        EmailSubject := strSubstNo(IssuedReminderTemplate."Email Subject", '4th');
                    end;
        end;

        if CUEnvInformation.IsProduction() then begin
            RecipientsBCC.Add('5313716@bcc.hubspot.com');
        end;

        TempBlob.CreateOutStream(OStream);
        Language.Reset();
        Language.SetRange(Code, Lang);
        IF Language.FindFirst() then
            IssueReminderReport.Language := Language."Windows Language ID";

        IF IssueReminderReport.SaveAs('', ReportFormat::Pdf, OStream, RecordRef) THEN
            //Report.SaveAs(117, '', ReportFormat::Pdf, OStream, RecordRef);
            TempBlob.CreateInStream(IStream);


        EmailMessageCU.Create(Recipients, EmailSubject, EmailBody, true, RecipientsCC, RecipientsBCC);
        EmailMessageCU.AddAttachment(FORMAT(IssuedReminders."No.") + '.pdf', 'PDF', IStream);

        IssuedReminderLines.Reset();
        IssuedReminderLines.SetFilter(IssuedReminderLines."Reminder No.", IssuedReminders."No.");
        IssuedReminderLines.SetRange(IssuedReminderLines."Document Type", IssuedReminderLines."Document Type"::Invoice);
        IF IssuedReminderLines.FInd('-') then begin
            RecordRefAttach.Open(Database::"Sales Invoice Header");
            FieldRefAttach := RecordRefAttach.Field(3);
            repeat
                if SalesInvHeader.Get(IssuedReminderLines."Document No.") then begin
                    FieldRefAttach.SetRange(IssuedReminderLines."Document No.");
                    if not RecordRefAttach.Find('-') then
                        exit;

                    Clear(OStreamAttach);
                    Clear(InStreamAttach);
                    // Clear(SwissQRBillReport);
                    Clear(SwissQRBillRemainingReport);

                    TempBlob.CreateOutStream(OStreamAttach);
                    IF SwissQRBillRemainingReport.SaveAs('', ReportFormat::Pdf, OStreamAttach, RecordRefAttach) THEN begin
                        //Report.SaveAs(117, '', ReportFormat::Pdf, OStreamAttach, RecordRefAttach);
                        TempBlob.CreateInStream(InStreamAttach);
                        EmailMessageCU.AddAttachment(FORMAT(SalesInvHeader."No.") + '.pdf', 'PDF', InStreamAttach);
                    end;

                end;
            until IssuedReminderLines.Next() = 0;
        end;

        EmailCU.Send(EmailMessageCU, Enum::"Email Scenario"::Reminder);

    end;



    var
        EmailMessageCU: Codeunit "Email Message";
        EmailCU: Codeunit Email;
        EmailSubjectLbl: Label 'Carvolution - 1st reminder';
        EmailSubjectLbl2: Label 'Carvolution - 2nd reminder';
        EmailSubjectLbl3: Label 'Carvolution - 3rd reminder';
        EmailSubjectLbl4: Label 'Carvolution - 4th reminder';
        OStream: OutStream;
        OStreamAttach: OutStream;
        IStream: InStream;
        InStreamAttach: InStream;
        CUFileManagment: Codeunit "File Management";
        TempBlob: codeunit "Temp Blob";
        IssueReminderReport: Report 50038;
        Language: Record Language;
        DummyBool: Boolean;
        CompanyInfo: Record "Company Information";
        Lang: code[10];
        CUEnvInformation: Codeunit "Environment Information";
        Text001: Label 'For customer  %1 you need to populate email address !';
        CUReminder: record "Reminder Header";
        SwissQRBillRemainingReport: Report "Sales - InvoiceQRRemAmount";




    /// <summary>
    /// CompletlyOpen.
    /// </summary>
    /// <param name="SalesInvoiceHeader">Record Sales Invoice Header.</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure CompletlyOpen(SalesInvoiceHeader: Record "Sales Invoice Header"): Boolean
    begin
        if NOT (SalesInvoiceHeader.Closed) then begin
            SalesInvoiceHeader.CalcFields(SalesInvoiceHeader."Amount Including VAT");
            SalesInvoiceHeader.CalcFields(SalesInvoiceHeader."Remaining Amount");
            if SalesInvoiceHeader."Amount Including VAT" = SalesInvoiceHeader."Remaining Amount" then
                exit(true);
        end;
        exit(false);
    end;
}