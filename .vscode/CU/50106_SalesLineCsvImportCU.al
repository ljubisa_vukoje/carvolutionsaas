codeunit 50106 SalesLineCSVImport
{
    trigger OnRun()
    begin
        UploadCSV();
    end;

    local procedure UploadCSV()
    var
        CSVInStream: InStream;
        UploadResult: Boolean;
        DialogCaption: Text;
        CSVFileName: Text;
        CSVBUffer: Record "CSV Buffer";
        SalesLine: Record "Sales Line";
        SalesHeader: Record "Sales Header";
        Counter: Integer;
        Imported: Boolean;
        Customer: Record Customer;
        OrderNo: Code[50];
        SalesLineNo: Code[50];
        LineNo: Integer;
        quantity: decimal;
        unitPrice: decimal;

    begin

        UploadResult := UploadIntoStream(DialogCaption, '', '', CSVFileName, CSVInStream);
        CSVBUffer.DeleteAll();
        CSVBUffer.LoadDataFromStream(CSVInStream, ',');

        if CSVBUffer.FindSet() then
            repeat
                if CSVBUffer."Line No." > 3 then begin
                    case CSVBUffer."Field No." of
                        1:
                            if CSVBUffer.Value = 'Rechnung' then begin
                                SalesLine.Reset();
                                SalesLine.Init();
                                SalesLine."Document Type" := "Sales Document Type"::Order;
                            end else
                                if CSVBUffer.Value = 'Gutschrift' then begin
                                    SalesLine.Reset();
                                    SalesLine.Init();
                                    SalesLine."Document Type" := "Sales Document Type"::"Credit Memo";
                                end;
                        2:
                            begin
                                OrderNo := CSVBUffer.Value;
                            end;
                        3:
                            begin
                                Evaluate(SalesLine."Line No.", CSVBUffer.Value);
                                LineNo := SalesLine."Line No.";
                            end;
                        5:
                            begin
                                if CSVBUffer.Value = 'Fibukonto' then begin
                                    SalesLine.Type := "Sales Line Type"::"G/L Account";
                                end else
                                    if CSVBUffer.Value = 'Titel' then begin
                                        SalesLine."No." := 'Title';
                                        SalesLine.Type := "Sales Line Type"::Title;
                                    end
                                    else
                                        if CSVBUffer.Value = '' then begin
                                            SalesLine.Type := "Sales Line Type"::" ";
                                        end;
                            end;
                        6:
                            begin
                                SalesLine."No." := CSVBUffer.Value;
                                SalesLineNo := CSVBUffer.Value;
                            end;
                        7:
                            begin
                                SalesLine.Description := CSVBUffer.Value;
                            end;
                        8:
                            begin
                                SalesLine."Unit of Measure" := CSVBUffer.Value;
                            end;
                        9:
                            begin
                                if CSVBUffer.Value = '0' then begin
                                    quantity := 1;
                                end
                                else begin
                                    Evaluate(quantity, CSVBUffer.Value);
                                end;
                            end;
                        10:
                            begin
                                if CSVBUffer.Value = '0' then begin
                                    unitPrice := 1;
                                end
                                else begin
                                    Evaluate(unitPrice, CSVBUffer.Value);
                                end;
                            end;
                        11:
                            begin
                                SalesLine."Shortcut Dimension 1 Code" := CSVBUffer.Value;
                            end;
                        12:
                            begin
                                SalesLine."Shortcut Dimension 2 Code" := CSVBUffer.Value;
                            end;
                        13:
                            begin
                                SalesLine."Posting Group" := CSVBUffer.Value;
                            end;
                        14:
                            begin
                                SalesLine."Gen. Bus. Posting Group" := CSVBUffer.Value;
                            end;
                        15:
                            begin
                                SalesLine."VAT Bus. Posting Group" := CSVBUffer.Value;
                                Imported := true;
                            end;

                    end;
                    if Imported then begin
                        SalesHeader.SetRange("No.", OrderNo);
                        SalesHeader."Document Type" := SalesHeader."Document Type";
                        if SalesHeader.Find('-') then begin
                            SalesLine.SetRange("Document No.", SalesHeader."No.");
                            SalesLine.SetRange("No.", SalesLineNo);
                            SalesLine.SetRange("Line No.", LineNo);
                            if SalesLine.Find('-') then begin
                                if SalesHeader.Status = "Sales Document Status"::Open then begin
                                    SalesLine.Quantity := quantity;
                                    salesline."Unit Price" := unitPrice;
                                    SalesLine.Validate(Amount);
                                    SalesLine.Modify(true);
                                    Imported := false;
                                end;
                            end
                            else begin
                                if SalesLine.Type = SalesLine.Type::" " then
                                    SalesLine.InitType();
                                SalesLine."Document No." := OrderNo;
                                SalesLine.Quantity := quantity;
                                salesline."Unit Price" := unitPrice;
                                SalesLine."Bill-to Customer No." := SalesHeader."Bill-to Customer No.";
                                SalesLine.Validate("Amount");
                                SalesLine.Insert(true);
                                Imported := false;
                            end;
                        end;

                    end;
                end;
            until CSVBUffer.Next() = 0;
        Message('Upload finished.');
    end;
}
