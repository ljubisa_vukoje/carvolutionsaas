codeunit 50050 CVCDCValidateCostObject
{


    TableNo = 6085593;

    trigger OnRun()
    var
        DimensionValue: Record "Dimension Value";
        OriginString: Text;
        TruncateString: Text;
        LicPlateString: Text;

    begin


        if Rec."Value (Text)" = '' then
            exit;

        OriginString := rec."Value (Text)";

        IF DimensionValue.GET(Rec."Value (Text)") THEN;

        //search by VIN
        DimensionValue.SetRange("Global Dimension No.", 2);
        DimensionValue.SetRange("Vehicle VIN", Rec."Value (Text)");
        if DimensionValue.Find('-') then begin
            Rec."Value (Text)" := DimensionValue.Code;
            Rec.Modify;
            exit;
        end;

        TruncateString := DELCHR(OriginString, '=', DELCHR(OriginString, '=', '1234567890'));
        //search by CarvolutionID
        //Trim Letters from CD Value 
        //Only numeric chars are needed for CarvoluyionID
        DimensionValue.Reset();
        DimensionValue.SetRange(Carvolution_ID, TruncateString);
        if DimensionValue.Find('-') then begin
            Rec."Value (Text)" := DimensionValue.Code;
            rec.Modify;
            exit;
        end;

        //search by Licence Plate 
        //Trim Letters from CD Value 
        //Only numeric chars are needed for Licence PLate
        DimensionValue.Reset();
        DimensionValue.SetRange("Global Dimension No.", 2);
        DimensionValue.SetFilter("Licence plate", '<>%1', '');
        if DimensionValue.Find('-') then begin
            begin
                repeat
                    LicPlateString := DELCHR(DimensionValue."Licence plate", '=', DELCHR(DimensionValue."Licence plate", '=', '1234567890'));
                    if (LicPlateString = TruncateString) then begin
                        Rec."Value (Text)" := DimensionValue.Code;
                        Rec.Modify;
                        exit;
                    end;
                until DimensionValue.Next() = 0;
            end;
        end;


    end;



}