/// <summary>
/// Codeunit MyCodeunit (ID 50100).
/// </summary>
codeunit 50100 "AddAttachment"
{
    /// <summary>
    /// Attach.
    /// </summary>
    /// <param name="SystemID">Guid.</param>
    /// <param name="DocumentType">Enum "Attachment Entity Buffer Document Type".</param>
    /// <param name="Base64String">Text.</param>
    /// <param name="Filename">Text.</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure Attach(SystemID: Guid; DocumentType: Enum "Attachment Entity Buffer Document Type"; Base64String: Text; Filename: Text): Text
    var
        RecRef: RecordRef;
    begin

        //Set the result code to inform the caller that the result is retrieved
        // ---- DOCUMENT TYPES ----
        //  value(5; "Sales Invoice") { Caption = 'Sales Invoice'; }
        //  value(6; "Purchase Invoice") { Caption = 'Purchase Invoice'; }

        today := WorkDate();

        //Check for posted sales invoices
        if DocumentType = DocumentType::"Sales Invoice" then begin
            SalesInv.SetRange(SystemId, SystemID);
            if SalesInv.FindFirst() then begin
                clear(RecRef);
                RecRef.GetTable(SalesInv);
                TableID := RecRef.Number;
            end
            else begin
                //Check for open sales invoices
                SalesHea.SetRange(SystemId, SystemID);
                if SalesHea.FindFirst() then begin
                    clear(RecRef);
                    RecRef.GetTable(SalesHea);
                    TableID := RecRef.Number;
                end
            end
        end else
            //Check for posted purchase invoices
            if DocumentType = DocumentType::"Purchase Invoice" then begin
                PurchInv.SetRange(SystemId, SystemID);
                if PurchInv.FindFirst() then begin
                    clear(RecRef);
                    RecRef.GetTable(PurchInv);
                    TableID := RecRef.Number;
                end
                else begin
                    //Check for open purchase invoices
                    PurchHea.SetRange(SystemId, SystemID);
                    if PurchHea.FindFirst() then begin
                        clear(RecRef);
                        RecRef.GetTable(PurchHea);
                        TableID := RecRef.Number;
                    end
                end;
            end
            else begin
                Message := 'could not find document type';
                exit(Message);
            end;

        //Create empty OutStream
        BlobTable.Content.CreateOutStream(FinalOStream);
        //Directly convert the incoming base64 string into the OutStream
        Base64Convert.FromBase64(Base64String, FinalOStream);
        //Get InStream of now existing Blob Content
        BlobTable.Content.CreateInStream(FinalInstream);
        //Add UNIQUE FileName
        DateTimetext := FORMAT(CurrentDateTime, 0, '<Day,2><Month,2><Year4><Hours24,2><Minutes,2><Seconds,2><Second dec>');
        txtCharsToKeep := 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        NewString := DELCHR(DateTimetext, '=', DELCHR(DateTimetext, '=', txtCharsToKeep));
        FilenameID := filename + '_' + NewString + '.pdf';
        //download pdf file
        //--- DownloadFromStream(FinalInstream, '', '', '', FileName);
        //upload to document attachment
        DocumentAttachment.SaveAttachmentFromStream(FinalInstream, RecRef, FilenameID);
        //exit(true);
        exit(Message);
    end;


    var
        txtCharsToKeep: Text;
        NewString: Text;
        TableID: Integer;
        FilenameID: Text;
        Base64Convert: Codeunit "Base64 Convert";
        FinalOStream: OutStream;
        DocumentAttachment: record "Document Attachment";
        FinalInstream: InStream;
        BlobTable: record BlobValues;
        Today: Date;
        DateTimetext: Text;
        SalesInv: Record "Sales Invoice Header";
        SalesHea: Record "Sales Header";
        PurchInv: Record "Purch. Inv. Header";
        PurchHea: Record "Purchase Header";
        Message: Text;

}