codeunit 50107 CostCorrectionSalesJobQueue
{

    trigger OnRun()
    begin
        Clear(CostCorrectionSales);
        if CostCorrectionSales.Run() then;
    end;

    var
        CostCorrectionSales: Codeunit CostObjectCorrectionCU;

}

