codeunit 50049 COCErrorHandling
{
    trigger OnRun()
    begin
        GetErrorJobQueEntries();
    end;


    local procedure GetErrorJobQueEntries()
    var
        JQE: Record "Job Queue Entry";
    begin

        JQE.Reset();
        JQE.SetRange("Object Type to Run", JQE."Object Type to Run"::Codeunit);
        JQE.SetRange("Object ID to Run", 2581);
        JQE.SetRange(Status, JQE.Status::Error);

        if JQE.find('-') then begin
            SendErrorEmail(JQE);
        end;
    end;

    procedure SendErrorEmail(JQE: Record "Job Queue Entry")
    var
        EmailSubject: text;
        EmailBody: text;
        Recipients: List of [Text];
        RecipientsCC: List of [Text];
        RecipientsBCC: List of [Text];
    begin
        CLear(EmailCU);
        Clear(EmailMessageCU);
        Clear(Recipients);
        Clear(RecipientsCC);
        Clear(RecipientsBCC);

        //Recipients.Add('robin.schmid@carvolution.com');
        Recipients.Add('ljubisa.vukoje@holycode.com');


        EmailSubject := 'Cost Correction Error';
        EmailBody := JQE."Error Message";


        EmailMessageCU.Create(Recipients, EmailSubject, EmailBody, true, RecipientsCC, RecipientsBCC);
        EmailCU.Send(EmailMessageCU, Enum::"Email Scenario"::Reminder);

    end;



    var
        EmailMessageCU: Codeunit "Email Message";
        EmailCU: Codeunit Email;



}