/// <summary>
/// Maria Okolisan
/// 13.04.2022
/// maria.okolisan@holyerp.rs
/// CU for handling filling Country/Region Code where it is empty and City is already filled
/// </summary>
codeunit 50019 CustCardCountryRegionCodeCU
{
    trigger OnRun()
    begin
        FillCustCardCRCodeBatchJob();
    end;

    procedure FillCustCardCRCodeBatchJob()
    var
        Cust: Record Customer;
        PostCode: Record "Post Code";
    begin
        Cust.Reset();
        Cust.SetFilter("Country/Region Code", '=%1', '');
        Cust.SetFilter("Post Code", '<>%1', '');

        if Cust.FindFirst() then begin
            repeat
                PostCode.Reset();
                if PostCode.Get(Cust."Post Code", Cust.City) then begin
                    Cust."Country/Region Code" := PostCode."Country/Region Code";
                    Cust.Modify();
                end
                else begin
                    PostCode.SetFilter(Code, Cust."Post Code");
                    if PostCode.FindFirst() then
                        Cust."Country/Region Code" := PostCode."Country/Region Code";
                    Cust.Modify();
                end;
            until Cust.Next() = 0;
        end;
    end;
}