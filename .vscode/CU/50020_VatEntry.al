codeunit 50020 VatEntry
{
    Permissions = tabledata "VAT Entry" = rim;


    procedure UpdateGlAccountyOnVatEntry(var VatEntry: Record "VAT Entry"; GlAccNo: Code[20])
    var
        GLAccL: Record "G/L Account";
    begin
        GLAccL.Reset();
        if GLAccL.Get(GlAccNo) then begin
            VatEntry.Validate(VatEntry."G/L Acc. No.", GlAccNo);
            VatEntry.Modify();
        end;

    end;

    procedure UpdateGlAccountyOnVatEntryBlankValues()
    begin

        VATEntry.Reset();
        VATEntry.SetCurrentKey("Entry No.");
        VATEntry.SetRange("G/L Acc. No.", '');
        if VATEntry.Find('-') then begin
            repeat
                GlEntryVatEntryLink.Reset();
                GlEntryVatEntryLink.SetRange("VAT Entry No.", VATEntry."Entry No.");
                if GlEntryVatEntryLink.Find('-') then begin
                    GlEntries.Reset();
                    GlEntries.SetRange("Entry No.", GlEntryVatEntryLink."G/L Entry No.");
                    GlEntries.Find('-');
                    VATEntry.Validate("G/L Acc. No.", GlEntries."G/L Account No.");
                    VATEntry.Modify();
                end;
            until VATEntry.Next() = 0;
        end;

        Message(FinishLbl);

    end;

    var
        VatEntry: Record "VAT Entry";
        GlEntryVatEntryLink: Record "G/L Entry - VAT Entry Link";
        GlEntries: Record "G/L Entry";

        FinishLbl: label 'Finished !';


}