codeunit 50051 CVCDCSelectCostObject
{


    TableNo = 6085593;

    trigger OnRun()
    var
        DimensionValue: Record "Dimension Value";
    begin


        if Rec."Value (Text)" = '' then
            exit;


        IF DimensionValue.GET(Rec."Value (Text)") THEN;

        DimensionValue.Reset();
        DimensionValue.SetRange("Global Dimension No.", 2);
        DimensionValue.Find('-');

        IF PAGE.RUNMODAL(0, DimensionValue) = ACTION::LookupOK THEN
            Rec."Value (Text)" := DimensionValue.Code;
    end;


}