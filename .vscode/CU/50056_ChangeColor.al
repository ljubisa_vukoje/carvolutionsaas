codeunit 50056 ChangeColor
{
    procedure ChangeCostCorrColor(CostObjCorr: Record CostObjectCorrectionTable): Text[50]
    begin

        with CostObjCorr do
            case CostObjCorr."Cost Object Status" of
                CostObjCorr."Cost Object Status"::Complete:
                    exit('favorable');
                CostObjCorr."Cost Object Status"::Error:
                    exit('Unfavorable');
                CostObjCorr."Cost Object Status"::"Completed Prev.":
                    exit('favorable');

            end;
    end;

    procedure ChangeCostCorrColorPurch(CostObjCorr: Record CostObjectCorrectionPurchTable): Text[50]
    begin

        with CostObjCorr do
            case CostObjCorr."Cost Object Status" of
                CostObjCorr."Cost Object Status"::Complete:
                    exit('favorable');
                CostObjCorr."Cost Object Status"::Error:
                    exit('Unfavorable');
                CostObjCorr."Cost Object Status"::"Completed Prev.":
                    exit('favorable');

            end;
    end;

}