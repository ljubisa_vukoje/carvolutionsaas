codeunit 50105 CSVImport
{
    trigger OnRun()
    begin
        UploadCSV();
    end;

    local procedure UploadCSV()
    var
        CSVInStream: InStream;
        UploadResult: Boolean;
        DialogCaption: Text;
        CSVFileName: Text;
        CSVBUffer: Record "CSV Buffer";
        SalesHeader: Record "Sales Header";
        Counter: Integer;
        Imported: Boolean;
        Customer: Record Customer;
        OrderNo: Code[50];

    begin
        UploadResult := UploadIntoStream(DialogCaption, '', '', CSVFileName, CSVInStream);
        CSVBUffer.DeleteAll();
        CSVBUffer.LoadDataFromStream(CSVInStream, ',');

        if CSVBUffer.FindSet() then
            repeat
                if CSVBUffer."Line No." > 3 then begin
                    case CSVBUffer."Field No." of
                        1:
                            if CSVBUffer.Value = 'Rechnung' then begin
                                SalesHeader.Reset();
                                SalesHeader.Init();
                                SalesHeader."Document Type" := SalesHeader."Document Type"::Order;
                                SalesHeader."No." := '';
                            end else
                                if CSVBUffer.Value = 'Gutschrift' then begin
                                    SalesHeader.Reset();
                                    SalesHeader.Init();
                                    SalesHeader."Document Type" := SalesHeader."Document Type"::"Credit Memo";
                                    SalesHeader."No." := '';
                                end;
                        2:
                            begin
                                OrderNo := CSVBUffer.Value;
                            end;
                        3:
                            begin
                                Customer.Reset();
                                Customer.SetRange("No.", CSVBUffer.Value);
                                Customer.Find('-');
                                Imported := true;
                            end;

                    end;
                    if Imported then begin
                        SalesHeader.SetRange("No.", OrderNo);
                        if SalesHeader.Find('-') then begin
                            if SalesHeader.Status = "Sales Document Status"::Open then begin
                                SalesHeader."Bill-to Customer No." := Customer."No.";
                                SalesHeader."Sell-to Customer No." := Customer."No.";
                                SalesHeader."Sell-to Customer Name" := Customer.Name;
                                SalesHeader."Your Reference" := OrderNo;
                                SalesHeader."Sell-to Address" := Customer.Address;
                                SalesHeader."Sell-to City" := Customer.City;
                                SalesHeader."Sell-to E-Mail" := Customer."E-Mail";
                                SalesHeader."Sell-to Post Code" := Customer."Post Code";
                                SalesHeader."Ship-to Address" := Customer.Address;
                                SalesHeader."Sell-to Phone No." := Customer."Phone No.";
                                SalesHeader."Customer Posting Group" := Customer."Customer Posting Group";
                                SalesHeader.Modify(true);
                                Imported := false;
                            end;

                        end
                        else begin
                            SalesHeader."No." := OrderNo;
                            SalesHeader."Bill-to Customer No." := Customer."No.";
                            SalesHeader."Sell-to Customer No." := Customer."No.";
                            SalesHeader."Sell-to Customer Name" := Customer.Name;
                            SalesHeader."Your Reference" := OrderNo;
                            SalesHeader."Sell-to Address" := Customer.Address;
                            SalesHeader."Sell-to City" := Customer.City;
                            SalesHeader."Sell-to E-Mail" := Customer."E-Mail";
                            SalesHeader."Sell-to Post Code" := Customer."Post Code";
                            SalesHeader."Ship-to Address" := Customer.Address;
                            SalesHeader."Sell-to Phone No." := Customer."Phone No.";
                            SalesHeader."Customer Posting Group" := Customer."Customer Posting Group";
                            SalesHeader.Insert(true);
                            Imported := false;
                        end;

                    end;
                end;
            until CSVBUffer.Next() = 0;

        Message('Upload finished.');
    end;
}
