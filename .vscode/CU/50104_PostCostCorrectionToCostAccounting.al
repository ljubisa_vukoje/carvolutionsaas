/// <summary>
/// Vukoje Ljubisa
/// 12.10.2022
/// Create and post Cost Journal
/// </summary>
codeunit 50104 "Post Cost Corr. To Cost Acc."
{

    TableNo = "Dimension Correction";
    trigger OnRun()
    begin
        DimensionCorrection := Rec;
        StartCostPostingToCA();
    end;

    procedure StartCostPostingToCA()

    begin
        CreateCostObjectAllocationJournal(DimensionCorrection);
    end;

    procedure CreateCostObjectAllocationJournal(var DimensionCorrection: Record "Dimension Correction"): Boolean
    var
        GLEntry: Record "G/L Entry";
        CostJournalLine: Record "Cost Journal Line";
        LineNo: Integer;
        DocumentNo: Code[20];
        CostAccountingSetup: Record "Cost Accounting Setup";
        CorrectionObject: Record CostObjectCorrectionTable;
        CorrectionObjectPurchase: Record CostObjectCorrectionPurchTable;
        PurchaseInvoice: Record "Purch. Inv. Header";
        SalesInvoice: Record "Sales Invoice Header";

    begin
        //sales correction
        CorrectionObject.Reset();
        CorrectionObject.SetRange("Entry No.", DimensionCorrection.CostCorrectionID);
        if CorrectionObject.Find('-') then begin


            CostJournalLine.Reset();
            CostJournalLine.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");

            CostJournalLine.SetRange("Journal Template Name", 'COC');
            CostJournalLine.SetRange("Journal Batch Name", 'COC');

            if CostJournalLine.Find('-') then
                CostJournalLine.DeleteAll();

            LineNo := 0;

            DimensionValue.Reset();
            DimensionValue.SetRange("Dimension Value Type", DimensionValue."Dimension Value Type"::Standard);
            DimensionValue.SetRange(Carvolution_ID, CorrectionObject.Carvolution_ID);
            DimensionValue.Find('-');

            GLEntry.Reset();
            GLEntry.SetRange("Document No.", CorrectionObject."Document No.");
            if GLEntry.Find('-') then
                CreateCostJournal(GLEntry, CorrectionObject);

        end;


        //purchase correction
        CorrectionObjectPurchase.Reset();
        CorrectionObjectPurchase.SetRange("Entry No.", DimensionCorrection.CostCorrectionID);
        if CorrectionObjectPurchase.Find('-') then begin



            CostJournalLine.Reset();
            CostJournalLine.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");

            CostJournalLine.SetRange("Journal Template Name", 'COC');
            CostJournalLine.SetRange("Journal Batch Name", 'COC');

            if CostJournalLine.Find('-') then
                CostJournalLine.DeleteAll();

            LineNo := 0;

            DimensionValue.Reset();
            DimensionValue.SetRange("Dimension Value Type", DimensionValue."Dimension Value Type"::Standard);
            DimensionValue.SetRange(Carvolution_ID, CorrectionObjectPurchase.Carvolution_ID);
            DimensionValue.Find('-');

            GLEntry.Reset();
            GLEntry.SetRange("Document No.", CorrectionObjectPurchase."Document No.");
            if GLEntry.Find('-') then
                CreateCostJournalPurchase(GLEntry, CorrectionObjectPurchase);

        end;

        exit(true);

    end;

    procedure CreateCostJournal(var GlEntry: Record "G/L Entry"; var CorrectionObject: Record CostObjectCorrectionTable): Boolean
    var
        DimValue: Record "Dimension Value";
    begin

        GlEntry.Find('-');
        DimValue.Reset();
        DimValue.SetRange("Dimension Value Type", DimensionValue."Dimension Value Type"::Standard);
        DimValue.SetRange(Carvolution_ID, CorrectionObject.Carvolution_ID);
        DimValue.Find('-');
        repeat
            if GLEntry.Amount <> 0 then begin
                GlAcc.Reset();
                GlAcc.Get(GLEntry."G/L Account No.");
                if GlAcc."Income/Balance" = GlAcc."Income/Balance"::"Income Statement" then begin
                    //check if posting date is in allowed range
                    if NOt CheckIfPostingDateInRange(GLEntry."Posting Date") then begin
                        CorrectionObject."Error Text" := StrSubstNo(PostingDateLbl, CorrectionObject."Document No.");
                        CorrectionObject."Cost Object Status" := "Cost Correction Status"::Error;
                        CorrectionObject.Modify();
                        EXIT(FALSE);
                    end;

                    if (CorrectionObject."Old Dimension Value" <> '')
                    AND (CorrectionObject."Old Dimension Value" <> DimValue.Code) then begin
                        LineNo := GetCostJournalLineNo();
                        CostJournalLine.Init();
                        CostJournalLine.Validate("Journal Template Name", 'COC');
                        CostJournalLine.Validate("Journal Batch Name", 'COC');
                        CostJournalLine.validate("Posting Date", GLEntry."Posting Date");
                        CostJournalLine."G/L Entry No." := GLEntry."Entry No.";
                        CostJournalLine."Line No." := LineNo;
                        CostJournalLine.Validate("Document No.", GLEntry."Document No.");
                        CostJournalLine."Source Code" := 'COC';
                        CostJournalLine.Validate("Cost Type No.", GLEntry."G/L Account No.");
                        CostJournalLine.Validate("Cost Object Code", CorrectionObject."Old Dimension Value");
                        CostJournalLine.Amount := -GLEntry.Amount;
                        CostJournalLine."Created From COC" := true;
                        CostJournalLine.Insert(TRUE);
                    end;

                    LineNo := GetCostJournalLineNo();
                    CostJournalLine.Init();
                    CostJournalLine.Validate("Journal Template Name", 'COC');
                    CostJournalLine.Validate("Journal Batch Name", 'COC');
                    CostJournalLine.validate("Posting Date", GLEntry."Posting Date");
                    CostJournalLine."G/L Entry No." := GLEntry."Entry No.";
                    CostJournalLine."Line No." := LineNo;
                    CostJournalLine.Validate("Document No.", GLEntry."Document No.");
                    CostJournalLine."Source Code" := 'COC';
                    CostJournalLine.Validate("Cost Type No.", GLEntry."G/L Account No.");
                    CostJournalLine.Validate("Cost Object Code", DimValue.Code);
                    CostJournalLine.Amount := GLEntry.Amount;
                    CostJournalLine."Created From COC" := true;
                    CostJournalLine.Insert(TRUE);

                end;
            end;


        until GLEntry.Next() = 0;

        PostCostJournal(DimensionCorrection, CorrectionObject);
        exit(true);
    end;

    procedure CreateCostJournalPurchase(var GlEntry: Record "G/L Entry"; var CorrectionObjectPurchase: Record CostObjectCorrectionPurchTable): Boolean
    var
        DimValue: Record "Dimension Value";
    begin

        GlEntry.Find('-');
        DimValue.Reset();
        DimValue.SetRange("Dimension Value Type", DimensionValue."Dimension Value Type"::Standard);
        DimValue.SetRange(Carvolution_ID, CorrectionObjectPurchase.Carvolution_ID);
        DimValue.Find('-');
        repeat
            if GLEntry.Amount <> 0 then begin
                GlAcc.Reset();
                GlAcc.Get(GLEntry."G/L Account No.");
                if GlAcc."Income/Balance" = GlAcc."Income/Balance"::"Income Statement" then begin
                    //check if posting date is in allowed range
                    if NOt CheckIfPostingDateInRange(GLEntry."Posting Date") then begin
                        CorrectionObjectPurchase."Error Text" := StrSubstNo(PostingDateLbl, CorrectionObjectPurchase."Document No.");
                        CorrectionObjectPurchase."Cost Object Status" := "Cost Correction Status"::Error;
                        CorrectionObjectPurchase.Modify();
                        EXIT(FALSE);
                    end;
                    if (CorrectionObjectPurchase."Old Dimension Value" <> '')
                    AND (CorrectionObjectPurchase."Old Dimension Value" <> DimValue.Code) then begin
                        LineNo := GetCostJournalLineNo();
                        CostJournalLine.Init();
                        CostJournalLine.Validate("Journal Template Name", 'COC');
                        CostJournalLine.Validate("Journal Batch Name", 'COC');
                        CostJournalLine.validate("Posting Date", GLEntry."Posting Date");
                        CostJournalLine."G/L Entry No." := GLEntry."Entry No.";
                        CostJournalLine."Line No." := LineNo;
                        CostJournalLine.Validate("Document No.", GLEntry."Document No.");
                        CostJournalLine."Source Code" := 'COC';
                        CostJournalLine.Validate("Cost Type No.", GLEntry."G/L Account No.");
                        CostJournalLine.Validate("Cost Object Code", CorrectionObjectPurchase."Old Dimension Value");
                        CostJournalLine.Amount := -GLEntry.Amount;
                        CostJournalLine."Created From COC" := true;
                        CostJournalLine.Insert(TRUE);
                    end;

                    LineNo := GetCostJournalLineNo();
                    CostJournalLine.Init();
                    CostJournalLine.Validate("Journal Template Name", 'COC');
                    CostJournalLine.Validate("Journal Batch Name", 'COC');
                    CostJournalLine.validate("Posting Date", GLEntry."Posting Date");
                    CostJournalLine."G/L Entry No." := GLEntry."Entry No.";
                    CostJournalLine."Line No." := LineNo;
                    CostJournalLine.Validate("Document No.", GLEntry."Document No.");
                    CostJournalLine."Source Code" := 'COC';
                    CostJournalLine.Validate("Cost Type No.", GLEntry."G/L Account No.");
                    CostJournalLine.Validate("Cost Object Code", DimValue.Code);
                    CostJournalLine.Amount := GLEntry.Amount;
                    CostJournalLine."Created From COC" := true;
                    CostJournalLine.Insert(TRUE);
                end;
            end;

        until GLEntry.Next() = 0;

        PostCostJournalPurchase(DimensionCorrection, CorrectionObjectPurchase);

    end;


    //post journal for sales dimension correction
    procedure PostCostJournal(var DimensionCorrection: Record "Dimension Correction"; CorrectionObject: Record CostObjectCorrectionTable)
    var
        CostJournalLine: Record "Cost Journal Line";
    begin
        CostJournalLine.Reset();
        CostJournalLine.SetRange("Journal Template Name", 'COC');
        CostJournalLine.SetRange("Journal Batch Name", 'COC');
        CostJournalLine.SetFilter(CostJournalLine."Document No.", '<>%1', '');
        if CostJournalLine.Find('-') then begin
            CODEUNIT.Run(CODEUNIT::"CA Jnl.-Post Batch", CostJournalLine);
        end;

        CorrectionObject."Error Text" := '';
        CorrectionObject."Cost Journal Posted" := true;
        CorrectionObject."Cost Object Status" := "Cost Correction Status"::Complete;
        CorrectionObject.CalcFields(CostEntriesAmt);
        CorrectionObject.DiffAmount := CorrectionObject.SalesInvAmt + CorrectionObject.CostEntriesAmt;
        CorrectionObject.Modify();
    end;

    //post journal for purchase dimension correction
    procedure PostCostJournalPurchase(var DimensionCorrection: Record "Dimension Correction"; CorrectionObject: Record CostObjectCorrectionPurchTable)
    var
        CostJournalLine: Record "Cost Journal Line";
    begin
        CostJournalLine.Reset();
        CostJournalLine.SetRange("Journal Template Name", 'COC');
        CostJournalLine.SetRange("Journal Batch Name", 'COC');
        CostJournalLine.SetFilter(CostJournalLine."Document No.", '<>%1', '');
        if CostJournalLine.Find('-') then begin
            CODEUNIT.Run(CODEUNIT::"CA Jnl.-Post Batch", CostJournalLine);
        end;

        CorrectionObject."Error Text" := '';
        CorrectionObject."Cost Journal Posted" := true;
        CorrectionObject."Cost Object Status" := "Cost Correction Status"::Complete;
        CorrectionObject.CalcFields(CostEntriesAmt);
        CorrectionObject.DiffAmount := CorrectionObject.PurchInvAmt - CorrectionObject.CostEntriesAmt;
        CorrectionObject.Modify();
    end;

    procedure GetCostJournalLineNo(): Integer
    var
        CostJournalLine: Record "Cost Journal Line";
        LineNo: Integer;
    begin
        CostJournalLine.Reset();
        CostJournalLine.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");

        CostJournalLine.SetRange("Journal Template Name", 'COC');
        CostJournalLine.SetRange("Journal Batch Name", 'COC');

        if CostJournalLine.Find('+') then
            exit(CostJournalLine."Line No." + 10000)
        else
            exit(10000);

    end;

    procedure CheckIfPostingDateInRange(var PostingDate: Date): Boolean
    begin
        GLSetup.Get();

        if (PostingDate < GLSetup."Allow Posting From") OR (PostingDate > GLSetup."Allow Posting To") then
            EXIT(false);

        EXIT(true);

    end;

    var
        GLSetup: Record "General Ledger Setup";
        DimensionCorrection: Record "Dimension Correction";
        DimensionValue: Record "Dimension Value";
        DimensionValueOld: Record "Dimension Value";
        CostJournalLine: Record "Cost Journal Line";
        GlAcc: Record "G/L Account";
        LineNo: Integer;
        GLEntryLbl: label 'Document No. %1 does not exist in database !';
        SwitchedDocLbl: label 'Document No. %1 is not %2.If you want to make a Cost Object Correction for %3, please use the respective action  !';
        PostingDateLbl: Label 'Posting Date for Document No. %1 is not in allowed posting range in G/L !';

}