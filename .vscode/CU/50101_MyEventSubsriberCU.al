codeunit 50101 MyEventSubscriberCU
{

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnAfterInitPostingNoSeries', '', true, true)]
    procedure SubcribeToOnAfterInitPostingNoSeries(var SalesHeader: Record "Sales Header"; xSalesHeader: Record "Sales Header")
    begin
        SalesHeader.Validate("Posting No. Series", '');
    end;

    [EventSubscriber(ObjectType::Codeunit, 392, 'OnAfterMakeReminder', '', true, true)]
    procedure SubscribeOnAfterMakeReminder(var ReminderHeader: Record "Reminder Header"; var ReminderLine: Record "Reminder Line")
    var
        CustLedgerEntries: Record "Cust. Ledger Entry";
        EmptyHeader: boolean;
        CU392: Record "Reminder Line";

        Customer: Record Customer;
        CustomerText: List of [Text];
        UserText: List of [Text];
        ReminderTerms: Record "Reminder Terms";
        ReminderLevel: Record "Reminder Level";
        UserName: Text[30];
        UserNameFC: Text[1];
        UserNameFULL: Text[32];
        HelperCU: Codeunit HelperCU;

    begin


        ReminderLine.GetReminderHeader();
        ReminderLine.SetRange(Type, ReminderLine.Type::"Customer Ledger Entry");
        IF ReminderLine.Find('-') then begin
            repeat
                CustLedgerEntries.Reset();
                CustLedgerEntries.get(ReminderLine."Entry No.");
                if (CustLedgerEntries."Debt Collection")
                   OR ((ReminderLine."Remaining Amount" <= 0) AND (ReminderLine.Type <> ReminderLine.Type::" ")) then
                    ReminderLine.Delete();

            until ReminderLine.Next() = 0;
        end;




        EmptyHeader := true;
        IF ReminderLine.Find('-') then begin
            repeat

                if ReminderLine.Type <> ReminderLine.Type::" " then
                    EmptyHeader := false;

            until ReminderLine.Next() = 0;

        end;

        if EmptyHeader then begin
            if ReminderHeader."No." <> '' then begin
                ReminderLine.DeleteAll();
                ReminderHeader.Delete();
            end;
        end;

        ReminderLine.Reset();
        ReminderLine.SetRange("Reminder No.", ReminderHeader."No.");
        ReminderLine.SetFilter(Type, '<>%1', ReminderLine.Type::"Customer Ledger Entry");
        IF ReminderLine.Find('-') then begin
            repeat

                if ReminderHeader."Reminder Level" = 4 then begin
                    UserText := UserId.Split('.');
                    UserName := LowerCase(UserText.Get(1));
                    UserNameFC := CopyStr(UserName, 1, 1);
                    UserNameFC := UpperCase(UserNameFC);
                    UserNameFULL := CopyStr(UserName, 2, StrLen(UserName) - 1);
                    UserName := UserNameFC + UserNameFULL;
                    Customer.Reset;
                    Customer.Get(ReminderHeader."Customer No.");

                    CustomerText := Customer.Name.Split(' ');
                    if Customer."Name 2" <> '' then
                        CustomerText := Customer."Name 2".Split(' ');
                    ReminderLine.Description := HelperCU.ReplaceString(ReminderLine.Description, '%CUSTOMERNAME%', CustomerText.Get(1));
                    ReminderLine.Description := HelperCU.ReplaceString(ReminderLine.Description, '%AMOUNT%', FORMAT(ReminderHeader."Remaining Amount") + ' ' + ReminderHeader."Currency Code");
                    ReminderLine.Description := HelperCU.ReplaceString(ReminderLine.Description, '%USERNAME%', UserName);
                    ReminderLine.Modify();
                end;

            until ReminderLine.Next() = 0;
        end;

    end;


    [EventSubscriber(ObjectType::Table, DataBase::"Sales Line", 'OnBeforeValidateDescription', '', true, true)]
    local procedure SubscribeOnBeforeValidateDescription(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; CurrentFieldNo: Integer; var InHandled: Boolean);

    var
        SalesHeader: Record "Sales Header";

    begin
        if SalesLine.Description <> '' then begin
            if STRLEN(SalesLine.Description) > 60 then begin
                SalesHeader.Reset();
                SalesHeader.SetRange("No.", SalesLine."Document No.");
                SalesHeader.FindFirst();
                if NOT SalesHeader.WebOrder then begin
                    if Confirm(Text000, false) then
                        SalesLine.Description := CopyStr(SalesLine.Description, 1, 59);
                end
                else begin
                    SalesLine.Description := CopyStr(SalesLine.Description, 1, 59);
                end;

            end;
        end;
    end;

    [EventSubscriber(ObjectType::Table, DataBase::Contact, 'OnBeforeVendorInsert', '', true, true)]
    procedure SubscribeOnBeforeVendorInsert(var Vend: Record Vendor; var Contact: Record Contact)
    begin
        Vend."No." := Contact."No.";
    end;

    [EventSubscriber(ObjectType::Report, Report::"Issue Reminders", 'OnBeforePrintIssuedReminderHeader', '', true, true)]
    local procedure SUbscribeOnBeforePrintIssuedReminderHeader(var IssuedReminderHeader: Record "Issued Reminder Header"; var IsHandled: Boolean)
    var
        CustomerLedgerEntries: Record "Cust. Ledger Entry";
        IssuedReminderLine: Record "Issued Reminder Line";
    begin

        IssuedReminderLine.Reset();
        IssuedReminderLine.SetRange("Document No.", IssuedReminderHeader."No.");
        IssuedReminderLine.SetRange(Type, IssuedReminderLine.type::"Customer Ledger Entry");
        if IssuedReminderLine.FindFirst() then begin
            repeat
                CustomerLedgerEntries.Reset();
                CustomerLedgerEntries.SetRange("Customer No.", IssuedReminderHeader."Customer No.");
                CustomerLedgerEntries.SetRange("Document No.", IssuedReminderLine."Document No.");
                CustomerLedgerEntries.SetRange("Document Type", IssuedReminderLine."Document Type");
                if CustomerLedgerEntries.FindFirst() then begin
                    CustomerLedgerEntries."Last Issued Reminder Level" := IssuedReminderHeader."Reminder Level";
                    CustomerLedgerEntries.Modify();
                end;
            until IssuedReminderLine.Next() = 0;
        end;
    end;


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Document-Mailing", 'OnBeforeSendEmail', '', true, true)]

    local procedure SubscribeOnBeforeSendEmail(var TempEmailItem: Record "Email Item" temporary; var IsFromPostedDoc: Boolean; var PostedDocNo: Code[20]; var HideDialog: Boolean; var ReportUsage: Integer; var EmailSentSuccesfully: Boolean; var IsHandled: Boolean; EmailDocName: Text[250]; SenderUserID: Code[50]; EmailScenario: Enum "Email Scenario")
    var
        CUEnvInformation: Codeunit "Environment Information";
    begin
        if (CUEnvInformation.IsProduction()) AND (ReportUsage = 15) then
            TempEmailItem."Send BCC" := '5313716@bcc.hubspot.com';
    end;

    [EventSubscriber(ObjectType::Table, Database::Customer, 'OnAfterOnInsert', '', true, true)]
    local procedure SubscribeOnAfterOnInsert(var Customer: Record Customer; xCustomer: Record Customer)
    begin

        case Customer."Language Code" OF
            'DE':
                Customer.Validate("Reminder Terms Code", 'NORMAL');
            'FR':
                Customer.Validate("Reminder Terms Code", 'NORMAL F');
            'EN':
                Customer.Validate("Reminder Terms Code", 'NORMAL E');
            ELSE
                Customer.Validate("Reminder Terms Code", 'NORMAL');
        END;

        Customer."Prices Including VAT" := true;
        if Customer."Payment Terms Code" = '' then
            customer.Validate("Payment Terms Code", '14TN');

        if Customer."Payment Terms Code" = 'SOFORT' then
            customer.Validate("Payment Terms Code", '14TN');

    end;


    [EventSubscriber(ObjectType::Table, Database::"Post Code", 'OnBeforeValidatePostCode', '', true, true)]
    local procedure SubscribeOnBeforeValidatePostCode(var CityTxt: Text[30]; var PostCode: Code[20]; var CountyTxt: Text[30]; var CountryCode: Code[10]; UseDialog: Boolean; var IsHandled: Boolean)
    begin
        IsHandled := true;
    end;


    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnAfterInitRecord', '', true, true)]
    procedure SubscribeOnAfterInitRecord(var SalesHeader: Record "Sales Header");
    begin
        IF SalesHeader.APISalesDocumentDate <> 0D then
            SalesHeader.SetNewDocumentDate(SalesHeader, SalesHeader.APISalesDocumentDate);
    end;



    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Gen. Jnl.-Post Line", 'OnAfterInsertVATEntry', '', true, true)]
    local procedure SubsribeOnAfterInsertVATEntry(GenJnlLine: Record "Gen. Journal Line"; VATEntry: Record "VAT Entry"; GLEntryNo: Integer; var NextEntryNo: Integer)
    var
        CUVatEntry: Codeunit VatEntry;
    begin
        CLEAR(CUVatEntry);
        CUVatEntry.UpdateGlAccountyOnVatEntry(VATEntry, GenJnlLine."Account No.");

    end;


    [EventSubscriber(ObjectType::COdeunit, COdeunit::"CDC Purch. - Register", 'OnAfterTransferPurchHeader', '', true, true)]
    procedure SubscribeOnAfterTransferPurchHeader(var PurchHeader: Record "Purchase Header"; var Document: Record "CDC Document")
    var
        Template: Record "CDC Template";
        PurchDocMgt: Codeunit "CDC Purch. Doc. - Management";
        DueDate: Date;

    begin
        Clear(PurchDocMgt);
        Template.Reset();
        Template.GET(Document."Template No.");

        PurchHeader."Document Date" := PurchDocMgt.GetDocumentDate(Document);
        DueDate := PurchDocMgt.GetDueDate(Document);
        PurchHeader."Due Date" := DueDate;
    end;

    [EventSubscriber(ObjectType::COdeunit, COdeunit::"Match Bank Pmt. Appl.", 'OnAfterMatchBankPayments', '', true, true)]
    local procedure OnAfterMatchBankPayments(var BankAccReconciliation: Record "Bank Acc. Reconciliation")
    var
        BankAccReconciliationLine: Record "Bank Acc. Reconciliation Line";
        BankAccReconciliationLine2: Record "Bank Acc. Reconciliation Line";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        DocumentNo: code[20];
        PaymentRefTemp: text[27];
        Customer: Record Customer;
        i: Integer;
        TempGenJournalLine: Record "Gen. Journal Line" temporary;
        MatchBankPaymentsHelperCU: codeunit "Match Bank Payments Helper";
        CustLedgerEntries: Record "Cust. Ledger Entry";
        AppToDocNo: Text[20];
        AppToAccNo: Text[20];

    begin
        BankAccReconciliationLine.SetRange("Statement Type", BankAccReconciliation."Statement Type");
        BankAccReconciliationLine.SetRange("Statement No.", BankAccReconciliation."Statement No.");
        BankAccReconciliationLine.SetRange("Bank Account No.", BankAccReconciliation."Bank Account No.");
        //BankAccReconciliationLine.SetFilter("Account Type", '<>%1', BankAccReconciliationLine."Account Type"::Customer);
        if BankAccReconciliationLine.Find('-') then begin
            repeat


            /* CustLedgerEntries.Reset();
            CustLedgerEntries.SetRange("Payment Reference", BankAccReconciliationLine."Payment Reference No.");
            if CustLedgerEntries.Find('-') then begin//QR Refrence

                if (NOT CustLedgerEntries.Open) OR
                    (CustLedgerEntries."Remaining Amount" <> BankAccReconciliationLine."Statement Amount") then begin
                    //  BankAccReconciliationLine.RejectAppliedPaymentEntriesSelectedLines;
                    BankAccReconciliationLine."Document No." := '';
                    BankAccReconciliationLine.Validate("Applied Amount", CustLedgerEntries."Remaining Amount");
                    BankAccReconciliationLine."Account Type" := BankAccReconciliationLine."Account Type"::Customer;
                    BankAccReconciliationLine.validate("Account No.", CustLedgerEntries."Customer No.");
                end
                else begin

                    BankAccReconciliationLine."Account Type" := BankAccReconciliationLine."Account Type"::Customer;
                    BankAccReconciliationLine.validate("Account No.", CustLedgerEntries."Customer No.");
                    BankAccReconciliationLine."Match Confidence" := BankAccReconciliationLine."Match Confidence"::Manual;
                    BankAccReconciliationLine.Validate("Document No.", CustLedgerEntries."Document No.");
                    BankAccReconciliationLine.Validate("Applied Amount", BankAccReconciliationLine."Statement Amount");

                end;
                BankAccReconciliationLine.Modify();


            end;*/

            /*else begin//ESR Reference

                PaymentRefTemp := CopyStr(BankAccReconciliationLine."Payment Reference No.", 7, 20);
                PaymentRefTemp := DELCHR(PaymentRefTemp, '<', '0');
                PaymentRefTemp := DelStr(PaymentRefTemp, StrLen(PaymentRefTemp) - 1, 1);
                SalesInvoiceHeader.Reset();
                SalesInvoiceHeader.SetRange("No.", PaymentRefTemp);
                if SalesInvoiceHeader.Find('-') then begin//match by sales invoice header

                    BankAccReconciliationLine.RejectAppliedPaymentEntriesSelectedLines;
                    BankAccReconciliationLine."Match Confidence" := BankAccReconciliationLine."Match Confidence"::Manual;
                    BankAccReconciliationLine."Account Type" := BankAccReconciliationLine."Account Type"::Customer;
                    BankAccReconciliationLine."Account No." := SalesInvoiceHeader."Sell-to Customer No.";

                    //BankAccReconciliationLine."Document No." := '';
                    //BankAccReconciliationLine.Validate("Document No.", SalesInvoiceHeader."No.");
                    //BankAccReconciliationLine.Modify();
                    if BankAccReconciliationLine."Document No." <> '' then begin
                        CustLedgerEntries.Reset();
                        CustLedgerEntries.SetRange("Customer No.", BankAccReconciliationLine."Account No.");
                        CustLedgerEntries.SetRange("Document No.", BankAccReconciliationLine."Document No.");
                        // if not CustLedgerEntries.Find('-') then
                        //     BankAccReconciliationLine.RejectAppliedPaymentEntriesSelectedLines;

                    end;


                end;*/
            //comment recognition by name.Robin said , because there is potential mistake with customer with same names
            //else begin//match by name
            //    Customer.Reset();
            //    Customer.SetFilter(Name, '=%1', BankAccReconciliationLine."Related-Party Name");
            //    if Customer.Find('-') then begin
            //        BankAccReconciliationLine."Match Confidence" := BankAccReconciliationLine."Match Confidence"::Low;
            //        BankAccReconciliationLine."Account Type" := BankAccReconciliationLine."Account Type"::Customer;
            //        BankAccReconciliationLine."Account No." := Customer."No.";
            //        BankAccReconciliationLine.Modify();
            //    end;
            //end;

            // end;
            //end;

            until BankAccReconciliationLine.Next() = 0;


            BankAccReconciliationLine.Reset();
            BankAccReconciliationLine.SetRange("Statement Type", BankAccReconciliation."Statement Type");
            BankAccReconciliationLine.SetRange("Statement No.", BankAccReconciliation."Statement No.");
            BankAccReconciliationLine.SetRange("Bank Account No.", BankAccReconciliation."Bank Account No.");
            BankAccReconciliationLine.SetFilter("Account Type", '=%1', BankAccReconciliationLine."Account Type"::Customer);
            BankAccReconciliationLine.SetFilter("Account No.", '<>%1', '');
            if BankAccReconciliationLine.Find('-') then begin
                repeat
                    Clear(TempGenJournalLine);
                    MatchBankPaymentsHelperCU.TransferDiffToAccount(BankAccReconciliationLine, TempGenJournalLine);
                until BankAccReconciliationLine.Next() = 0;

            end;


            BankAccReconciliationLine.Reset();
            BankAccReconciliationLine.SetRange("Statement Type", BankAccReconciliation."Statement Type");
            BankAccReconciliationLine.SetRange("Statement No.", BankAccReconciliation."Statement No.");
            BankAccReconciliationLine.SetRange("Bank Account No.", BankAccReconciliation."Bank Account No.");
            BankAccReconciliationLine.SetFilter("Account Type", '=%1', BankAccReconciliationLine."Account Type"::"G/L Account");
            if BankAccReconciliationLine.Find('-') then begin
                repeat
                    CustLedgerEntries.Reset();
                    CustLedgerEntries.SetRange("Payment Reference", BankAccReconciliationLine."Payment Reference No.");
                    if CustLedgerEntries.Find('-') then begin
                        BankAccReconciliationLine."Account Type" := BankAccReconciliationLine."Account Type"::Customer;
                        BankAccReconciliationLine.validate("Account No.", CustLedgerEntries."Customer No.");
                        BankAccReconciliationLine.Modify();
                    end;
                until BankAccReconciliationLine.Next() = 0;

            end;

            //transfer difference to account
            BankAccReconciliationLine.Reset();
            BankAccReconciliationLine.SetRange("Statement Type", BankAccReconciliation."Statement Type");
            BankAccReconciliationLine.SetRange("Statement No.", BankAccReconciliation."Statement No.");
            BankAccReconciliationLine.SetRange("Bank Account No.", BankAccReconciliation."Bank Account No.");
            BankAccReconciliationLine.SetFilter("Account Type", '=%1', BankAccReconciliationLine."Account Type"::Customer);
            BankAccReconciliationLine.SetFilter(BankAccReconciliationLine.Difference, '<>%1', 0);
            if BankAccReconciliationLine.Find('-') then begin
                repeat
                    BankAccReconciliationLine.TransferRemainingAmountToAccount();
                until BankAccReconciliationLine.Next() = 0;

            end;
        end;

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Reminder-Make", 'OnBeforeCustLedgerEntryFind', '', true, true)]
    local procedure SubscribeOnBeforeCustLedgerEntryFind(var CustLedgerEntry: Record "Cust. Ledger Entry"; ReminderHeader: Record "Reminder Header"; Customer: Record Customer)
    begin
        CustLedgerEntry.SetRange(CustLedgerEntry."Exclude From Reminder", false);
    end;


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Reminder-Make", 'OnAfterFilterCustLedgEntryReminderLevel', '', true, true)]
    local procedure SubscribeOnAfterFilterCustLedgEntryReminderLevel(var CustLedgerEntry: Record "Cust. Ledger Entry"; var ReminderLevel: Record "Reminder Level"; ReminderTerms: Record "Reminder Terms"; Customer: Record Customer)
    begin
        CustLedgerEntry.SetRange(CustLedgerEntry."Exclude From Reminder", false);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Deferral Utilities", 'OnBeforeDeferralCodeOnValidate', '', true, true)]
    local procedure SubscribeOnBeforeDeferralCodeOnValidate(DeferralCode: Code[10]; DeferralDocType: Integer; GenJnlTemplateName: Code[10]; GenJnlBatchName: Code[10]; DocumentType: Integer; DocumentNo: Code[20]; LineNo: Integer; Amount: Decimal; PostingDate: Date; Description: Text[100]; CurrencyCode: Code[10]; var IsHandled: Boolean)
    begin


        if not GuiAllowed then
            IsHandled := true;

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Deferral Utilities", 'OnBeforeIsDateNotAllowed', '', true, true)]
    local procedure SubscribeOnBeforeIsDateNotAllowed(PostingDate: Date; var Result: Boolean; var IsHandled: Boolean)
    begin

        if not GuiAllowed then
            IsHandled := true;

    end;

    /// <summary>
    /// populating global dimension 2 code on sales credit memo document if global
    /// dimension does not exist on scm lines
    /// </summary>
    /// <param name="FromDocumentType"></param>
    /// <param name="FromDocumentNo"></param>
    /// <param name="ToSalesHeader"></param>
    /// <param name="FromDocOccurenceNo"></param>
    /// <param name="FromDocVersionNo"></param>
    /// <param name="IncludeHeader"></param>
    /// <param name="RecalculateLines"></param>
    /// <param name="MoveNegLines"></param>
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnAfterCopySalesDocument', '', true, true)]
    local procedure SubscribeOnAfterCopySalesDocument(FromDocumentType: Option; FromDocumentNo: Code[20]; var ToSalesHeader: Record "Sales Header"; FromDocOccurenceNo: Integer; FromDocVersionNo: Integer; IncludeHeader: Boolean; RecalculateLines: Boolean; MoveNegLines: Boolean)
    var

        GLEntries: Record "G/L Entry";
        InsertCostObject: boolean;
        SalesLine: Record "Sales Line";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        SalesInvoiceLIne: Record "Sales Invoice Line";

    begin

        if ToSalesHeader."Document Type" <> ToSalesHeader."Document Type"::"Credit Memo" then
            exit;

        SalesInvoiceHeader.Reset();
        SalesInvoiceHeader.SetRange("No.", FromDocumentNo);
        if not SalesInvoiceHeader.Find('-') then
            exit;

        InsertCostObject := TRUE;
        SalesInvoiceLIne.Reset();
        SalesInvoiceLIne.SetRange("Document No.", SalesInvoiceHeader."No.");
        if SalesInvoiceLIne.Find('-') then begin

            repeat
                if SalesInvoiceLIne."Shortcut Dimension 2 Code" <> '' then
                    InsertCostObject := FALSE;
            until SalesInvoiceLIne.Next() = 0;
        end;

        IF NOT InsertCostObject then
            exit;

        GLEntries.Reset();
        GLEntries.SetRange("Document No.", SalesInvoiceHeader."No.");
        GLEntries.SetFilter("Global Dimension 2 Code", '<>%1', '');
        if GLEntries.Find('-') then begin
            ToSalesHeader.SetHideValidationDialog(true);
            ToSalesHeader.ValidateShortcutDimCode(2, GLEntries."Global Dimension 2 Code");
            ToSalesHeader."Shortcut Dimension 2 Code" := GLEntries."Global Dimension 2 Code";
            ToSalesHeader.Modify();

            SalesLine.Reset();
            SalesLine.SetRange("Document Type", ToSalesHeader."Document Type");
            SalesLine.SetRange("Document No.", ToSalesHeader."No.");
            if SalesLine.Find('-') then begin
                repeat
                    SalesLine."Kostenträger Code" := GLEntries."Global Dimension 2 Code";
                    SalesLine.Validate("Shortcut Dimension 2 Code", GLEntries."Global Dimension 2 Code");
                    SalesLine.Modify();
                until SalesLine.Next() = 0;
            end;

        end;


    end;

    /// <summary>
    /// populating global dimension 2 code on purchase credit memo document if global
    /// dimension does not exist on pcm lines
    /// </summary>
    /// <param name="FromDocumentType"></param>
    /// <param name="FromDocumentNo"></param>
    /// <param name="ToPurchaseHeader"></param>
    /// <param name="FromDocOccurenceNo"></param>
    /// <param name="FromDocVersionNo"></param>
    /// <param name="IncludeHeader"></param>
    /// <param name="RecalculateLines"></param>
    /// <param name="MoveNegLines"></param>
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnAfterCopyPurchaseDocument', '', true, true)]
    local procedure SubscribeOnAfterCopyPurchaseDocument(FromDocumentType: Option; FromDocumentNo: Code[20]; var ToPurchaseHeader: Record "Purchase Header"; FromDocOccurenceNo: Integer; FromDocVersionNo: Integer; IncludeHeader: Boolean; RecalculateLines: Boolean; MoveNegLines: Boolean)
    var

        GLEntries: Record "G/L Entry";
        InsertCostObject: boolean;
        PurchaseLine: Record "Purchase Line";
        PurchaseInvoiceHeader: Record "Purch. Inv. Header";
        PurchaseInvoiceLIne: Record "Purch. Inv. Line";

    begin

        if ToPurchaseHeader."Document Type" <> ToPurchaseHeader."Document Type"::"Credit Memo" then
            exit;

        PurchaseInvoiceHeader.Reset();
        PurchaseInvoiceHeader.SetRange("No.", FromDocumentNo);
        if not PurchaseInvoiceHeader.Find('-') then
            exit;

        InsertCostObject := TRUE;
        PurchaseInvoiceLIne.Reset();
        PurchaseInvoiceLIne.SetRange("Document No.", PurchaseInvoiceHeader."No.");
        if PurchaseInvoiceLIne.Find('-') then begin

            repeat
                if PurchaseInvoiceLIne."Shortcut Dimension 2 Code" <> '' then
                    InsertCostObject := FALSE;
            until PurchaseInvoiceLIne.Next() = 0;
        end;

        IF NOT InsertCostObject then
            exit;

        GLEntries.Reset();
        GLEntries.SetRange("Document No.", PurchaseInvoiceHeader."No.");
        GLEntries.SetFilter("Global Dimension 2 Code", '<>%1', '');
        if GLEntries.Find('-') then begin
            ToPurchaseHeader.SetHideValidationDialog(true);
            ToPurchaseHeader.ValidateShortcutDimCode(2, GLEntries."Global Dimension 2 Code");
            ToPurchaseHeader."Shortcut Dimension 2 Code" := GLEntries."Global Dimension 2 Code";
            ToPurchaseHeader.Modify();
            PurchaseLine.Reset();
            PurchaseLine.SetRange("Document Type", ToPurchaseHeader."Document Type");
            PurchaseLine.SetRange("Document No.", ToPurchaseHeader."No.");
            if PurchaseLine.Find('-') then begin
                repeat
                    PurchaseLine."Kostenträger Code" := PurchaseLine."Shortcut Dimension 2 Code";
                    PurchaseLine.Validate("Shortcut Dimension 2 Code", GLEntries."Global Dimension 2 Code");
                    PurchaseLine.Modify();
                until PurchaseLine.Next() = 0;
            end;

        end;


    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Transfer GL Entries to CA", 'OnBeforeRun', '', true, true)]
    local procedure OnBeforeRun(var IsHandled: Boolean)
    var
        CUEnvInformation: Codeunit "Environment Information";
        ErrorLbl: label 'Function temporarily disabled!\\Reason the Task might lead to unwanted behaviour and due to the fact that Cost Accounting is updated automatically from G/L based on CA Setup.\For further Question reach out to %1 !';
    begin
        if CUEnvInformation.IsProduction() then
            Error(StrSubstNo(ErrorLbl, 'robin.schmid@carvolution.ch'));
    end;


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"CA Jnl.-Post Line", 'OnCreateCostRegisterOnBeforeInsert', '', true, true)]
    local procedure SubscribeOnCreateCostRegisterOnBeforeInsert(var CostRegister: Record "Cost Register"; CostJournalLine: Record "Cost Journal Line"; SourceCodeSetup: Record "Source Code Setup")
    begin
        CostRegister."Created From COC" := CostJournalLine."Created From COC";
        if CostRegister."Created From COC" then
            CostRegister.Source := CostRegister.Source::"Cost Journal";
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', true, true)]
    local procedure SubscribeOnBeforePostSalesDoc(var SalesHeader: Record "Sales Header"; CommitIsSuppressed: Boolean; PreviewMode: Boolean; var HideProgressWindow: Boolean)
    begin
        if (SalesHeader."Document Type" = SalesHeader."Document Type"::"Credit Memo")
        AND (SalesHeader."Posting No. Series" = '') then begin
            SalesHeader."Posting No. Series" := SalesHeader."No. Series";
        end;

    end;


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Approvals Mgmt.", 'OnBeforePrePostApprovalCheckPurch', '', true, true)]
    local procedure "Approvals Mgmt._OnBeforePrePostApprovalCheckPurch"
    (
        var PurchaseHeader: Record "Purchase Header";
        var Result: Boolean;
        var IsHandled: Boolean
    )
    var
        CompanyInfo: Record "Company Information";
        UserSecStatus: Record "User Security Status";
    begin
        if CompanyInfo.CheckCompanyNameIsFarieOrFarieFleet() then begin
            UserSecStatus.Reset();
            UserSecStatus.SetRange(UserSecStatus."User Name", 'CARVOLUTION API');
            If UserSecStatus.FindFirst() then begin
                if (PurchaseHeader.SystemCreatedBy = UserSecStatus."User Security ID") then begin
                    IsHandled := true;
                    Result := true;
                end;
                exit;
            end;
        end;
    end;


    [EventSubscriber(ObjectType::Table, Database::"Job Queue Entry", 'OnBeforeModifyLogEntry', '', true, true)]
    local procedure SubscribeOnBeforeModifyLogEntry(var JobQueueLogEntry: Record "Job Queue Log Entry"; var JobQueueEntry: Record "Job Queue Entry")
    var
        CostCorrPurch: Record CostObjectCorrectionPurchTable;
        CostCorrSales: Record CostObjectCorrectionTable;
        DimmCorrection: Record "Dimension Correction";

    begin

        //purchase
        if JobQueueEntry."Job Queue Category Code" = 'COC-P' then begin
            if JobQueueLogEntry.Status = JobQueueLogEntry.Status::Success then begin
                CostCorrPurch.Reset();
                CostCorrPurch.SetRange("Entry No.", JobQueueEntry."Cost Record ID");
                if CostCorrPurch.Find('-') then begin
                    CostCorrPurch."Error Text" += JobQueueLogEntry."Error Message";
                    CostCorrPurch.Corrected := true;
                    CostCorrPurch."Cost Object Status" := CostCorrPurch."Cost Object Status"::"Dim.Corr. Finished";
                    CostCorrPurch.Modify();
                    DimmCorrection.Reset();
                    DimmCorrection.SetRange(CostCorrectionID, CostCorrPurch."Entry No.");
                    if DimmCorrection.Find('-') then
                        Codeunit.Run(Codeunit::"Post Cost Corr. To Cost Acc.", DimmCorrection);
                end;
            end;
        end;



        //sales
        if JobQueueEntry."Job Queue Category Code" = 'COC-S' then begin
            if JobQueueLogEntry.Status = JobQueueLogEntry.Status::Success then begin
                CostCorrSales.Reset();
                CostCorrSales.SetRange("Entry No.", JobQueueEntry."Cost Record ID");
                if CostCorrSales.Find('-') then begin
                    CostCorrSales."Error Text" += JobQueueLogEntry."Error Message";
                    CostCorrSales.Corrected := true;
                    CostCorrSales."Cost Object Status" := CostCorrSales."Cost Object Status"::"Dim.Corr. Finished";
                    CostCorrSales.Modify();
                    DimmCorrection.Reset();
                    DimmCorrection.SetRange(CostCorrectionID, CostCorrSales."Entry No.");
                    if DimmCorrection.Find('-') then
                        Codeunit.Run(Codeunit::"Post Cost Corr. To Cost Acc.", DimmCorrection);
                end;
            end;
        end;

    end;



    [EventSubscriber(ObjectType::Table, Database::"Job Queue Entry", 'OnBeforeSetStatusValue', '', true, true)]
    local procedure OnBeforeSetStatusValue(var JobQueueEntry: Record "Job Queue Entry"; var xJobQueueEntry: Record "Job Queue Entry"; var NewStatus: Option)
    var
        CostCorrPurch: Record CostObjectCorrectionPurchTable;
        CostCorrSales: Record CostObjectCorrectionTable;
        JobQueueLogEntry: Record "Job Queue Log Entry";

    begin

        //purchase
        if JobQueueEntry."Job Queue Category Code" = 'COC-P' then begin
            if NewStatus = JobQueueEntry.Status::Error then begin
                CostCorrPurch.Reset();
                CostCorrPurch.SetRange("Entry No.", JobQueueEntry."Cost Record ID");
                if CostCorrPurch.Find('-') then begin

                    CostCorrPurch."Error Text" := JobQueueEntry."Error Message";
                    /*if CostCorrPurch."Error Text" = '' then begin
                        JobQueueLogEntry.Reset();
                        JobQueueLogEntry.SetRange(ID, JobQueueEntry.ID);
                        JobQueueLogEntry.SetCurrentKey("Entry No.");
                        if JobQueueLogEntry.Find('-') then
                            CostCorrPurch."Error Text" := JobQueueLogEntry."Error Message";
                    end;*/
                    CostCorrPurch."Cost Object Status" := CostCorrPurch."Cost Object Status"::Error;
                    CostCorrPurch.Modify();
                end;
            end;
        end;


        //sales
        if JobQueueEntry."Job Queue Category Code" = 'COC-S' then begin
            if NewStatus = JobQueueEntry.Status::Error then begin
                CostCorrSales.Reset();
                CostCorrSales.SetRange("Entry No.", JobQueueEntry."Cost Record ID");
                if CostCorrSales.Find('-') then begin
                    CostCorrSales."Error Text" := JobQueueEntry."Error Message";
                    /*if CostCorrSales."Error Text" = '' then begin
                        JobQueueLogEntry.Reset();
                        JobQueueLogEntry.SetRange(ID, JobQueueEntry.ID);
                        JobQueueLogEntry.SetCurrentKey("Entry No.");
                        if JobQueueLogEntry.Find('-') then
                            CostCorrSales."Error Text" := JobQueueLogEntry."Error Message";
                    end;*/
                    CostCorrSales."Cost Object Status" := CostCorrSales."Cost Object Status"::Error;
                    CostCorrSales.Modify();
                end;
            end;
        end;
        ;
    end;

    var
        Text000: Label 'You have reached max number of characters!\Max number of characacters in description field is 59! \Should the description be shortened to the maximum number of characters?';


}
