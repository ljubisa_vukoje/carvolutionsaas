/// <summary>
/// Codeunit Excel Management (ID 50048).
/// </summary>
codeunit 50048 "Excel Management"
{
    trigger OnRun()
    begin

    end;

    var
        ExcelTemplate: Record "Excel Template Storage";
        InStr: InStream;
        FileMgt: codeunit "File Management";
        OutStr: OutStream;
        Filename: text;
        TempBlob: Codeunit "Temp Blob";
        ConfirmSelectSheet: Label 'Do you want to predefine sheetname?';
        SelectSheetMsg: label 'Select Excel sheet:';
        DlgCaptionImport: Label 'Select File';
        DlgCaptionExport: Label 'Export File';
        FileNameFilter: Label 'Excel Files (*.xlsx)|*.xlsx';
        ReferenceNotCoveredErr: LAbel 'Reference not covered!';



    /// <summary>
    /// ExportToExcelInternal.
    /// </summary>
    procedure ExportToExcelInternal()
    var

        folderPath: Text;
    begin

    end;

    /// <summary>
    /// ImportExcelTemplate.
    /// </summary>
    procedure ImportExcelTemplate()
    var
        SheetName: text;

    begin
        TempBlob.CreateInStream(InStr);
        Filename := FileMgt.BLOBImportWithFilter(TempBlob, DlgCaptionImport, '', FileNameFilter, 'xlsx');
        if Filename = '' then
            exit;
        ExcelTemplate.TemplateFile.CreateOutStream(OutStr);
        CopyStream(OutStr, InStr);
        if ExcelTemplate.TemplateName = '' then
            ExcelTemplate.TemplateName := Filename;
        ExcelTemplate.TemplateFileName := Filename;

        If not ExcelTemplate.Modify() then
            ExcelTemplate.Insert();

        SheetName := SelectSheetName(ExcelTemplate);
        if SheetName <> '' then begin
            ExcelTemplate.SheetName := SheetName;
            ExcelTemplate.modify;
        end;


    end;

    Procedure SelectSheetName(var ExcelTemplate: record "Excel Template Storage"): Text
    var
        ExcelBuffer: record "Excel Buffer" temporary;
        NameValueBuffer: record "Name/Value Buffer" temporary;
        SheetNameString: text;
        SelectedSheetIndex: integer;
    begin
        ExcelTemplate.TemplateFile.CreateInStream(InStr);
        ExcelTemplate.CalcFields(TemplateFile);
        if ExcelTemplate.TemplateFile.HasValue then begin
            if ExcelBuffer.GetSheetsNameListFromStream(InStr, NameValueBuffer) then begin
                NameValueBuffer.FindSet();
                repeat
                    if SheetNameString <> '' then
                        SheetNameString += ',';
                    SheetNameString += NameValueBuffer.Value;
                until NameValueBuffer.Next() = 0;

            end;
            if NameValueBuffer.Count = 1 then
                ExcelTemplate.SheetName := NameValueBuffer.Value
            else begin
                if confirm(ConfirmSelectSheet, true) then begin
                    SelectedSheetIndex := StrMenu(SheetNameString, 1, SelectSheetMsg);
                    if SelectedSheetIndex <> 0 then begin
                        NameValueBuffer.FindFirst();
                        NameValueBuffer.next(SelectedSheetIndex - 1);
                        ExcelTemplate.SheetName := NameValueBuffer.Value;
                    end;
                end;
            end;
        end;
    end;

    /// <summary>
    /// ExportExcelTemplate.
    /// </summary>
    /// <param name="ExcelTemplate">record "Excel Template Storage".</param>
    procedure ExportExcelTemplate(ExcelTemplate: record "Excel Template Storage")

    begin
        ExcelTemplate.CalcFields(TemplateFile);
        if ExcelTemplate.TemplateFile.HasValue then begin
            ExcelTemplate.TemplateFile.CreateInStream(InStr);
            FileName := FileMgt.CreateFileNameWithExtension(ExcelTemplate.TemplateFileName, 'xlsx');
            DownloadFromStream(InStr, DlgCaptionExport, '', FileNameFilter, Filename);
        end;
    end;

    /// <summary>
    /// InitExcel.
    /// </summary>
    /// <param name="TemplateName">Text.</param>
    /// <param name="ExcelBuffer">VAR record "Excel Buffer".</param>
    /// <returns>Return value of type integer.</returns>
    procedure InitExcel(TemplateName: Text; var ExcelBuffer: record "Excel Buffer"): integer
    var
        noOfRows: integer;
        SheetName: text;
    begin
        ExcelTemplate.Get(TemplateName);
        ExcelTemplate.CalcFields(TemplateFile);
        ExcelTemplate.TemplateFile.CreateInStream(InStr);
        SheetName := ExcelTemplate.SheetName;
        if SheetName = '' then
            SheetName := ExcelBuffer.SelectSheetsNameStream(InStr);
        ExcelBuffer.OpenBookStream(InStr, SheetName);
        ExcelBuffer.ReadSheet();
        ExcelBuffer.SetCurrentKey("Row No.");
        if ExcelBuffer.findlast then
            noOfRows := ExcelBuffer."Row No.";
        clear(ExcelBuffer);
        ExcelTemplate.CalcFields(TemplateFile);
        ExcelTemplate.TemplateFile.CreateInStream(InStr);
        ExcelBuffer.UpdateBookStream(InStr, SheetName, false);
        exit(noOfRows);

    end;


    /// <summary>
    /// ExportToExcel.
    /// </summary>
    /// <param name="recVar">VAR Variant.</param>
    /// <param name="ExcelTemplatName">Text[250].</param>
    /// <param name="FriendlyName">text.</param>
    procedure ExportToExcel(var recVar: Variant; ExcelTemplatName: Text[250]; FriendlyName: text)
    var
        recRef: RecordRef;
        fldRef: FieldRef;
        TypeHelper: Codeunit "Type Helper";
        DataExchDef: record "Data Exch. Def";
        DataExchLineMapping: record "Data Exch. Field Mapping";
        ExcelTemplate: record "Excel Template Storage";
        ExcelBuffer: record "Excel Buffer" temporary;
        ExcelBufferSheet: record "Excel Buffer" temporary;
        LastRowNo: integer;
    begin
        if not ExcelTemplate.get(ExcelTemplatName) then
            exit;

        case true of
            recvar.IsRecordRef:
                recRef := recVar;
            recvar.IsRecord:
                recref.GETTABLE(recVar);
            else
                error(ReferenceNotCoveredErr);
        end;


        DataExchLineMapping.setrange("Data Exch. Def Code", ExcelTemplate.DataExchangeCode);
        DataExchLineMapping.SetRange("Table ID", recRef.Number);
        if DataExchLineMapping.FindSet() then begin
            LastRowNo := InitExcel(ExcelTemplatName, ExcelBuffer);
            if Recref.findset() then
                repeat
                    LastRowNo += 1;
                    DataExchLineMapping.FindSet();
                    repeat
                        fldRef := recRef.Field(DataExchLineMapping."Field ID");
                        if fldRef.Class = fldref.class::FlowField then
                            fldRef.CalcField();
                        ExcelBufferSheet.EnterCell(ExcelBufferSheet, LastRowNo, DataExchLineMapping."Column No.", fldRef.Value, false, false, false);


                    until DataExchLineMapping.Next() = 0;
                until recref.Next() = 0;
            ExcelBuffer.WriteAllToCurrentSheet(ExcelBufferSheet);
            ExcelBuffer.CloseBook();
            ExcelBuffer.SetFriendlyFilename(FriendlyName);
            ExcelBuffer.OpenExcel();
        end;
    end;

    [EventSubscriber(ObjectType::page, Page::"Chart of Accounts", 'OnAfterActionEvent', 'ExportBalanceSheetToExcelInternal', false, false)]
    local procedure ExportFromChartofAccounts();
    begin
        Report.Run(Report::"Balance Sheet Internal", TRUE, true);
    end;

    [EventSubscriber(ObjectType::page, Page::"Chart of Accounts", 'OnAfterActionEvent', 'ExportBalanceSheetToExcelExternal', false, false)]
    local procedure ExportFromExtChartofAccounts();
    begin
        Report.Run(Report::"Balance Sheet External", TRUE, true);
    end;

    [EventSubscriber(ObjectType::page, Page::"Item List", 'OnAfterActionEvent', 'ExportCembraReport', false, false)]
    local procedure ExportCembraReport();
    begin
        Report.Run(Report::"Cembra Report", TRUE, true);
    end;

}

