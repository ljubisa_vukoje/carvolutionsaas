/// <summary>
/// CU for SEPA (ISO20022) export vendor payment bank file
/// vukoje Ljubisa 12.09.2021
/// Vukoje
/// </summary>
codeunit 82799 "ISO 20022 Management2"
{


    /*
    procedure BrowseNetworkFolder2(Ttitle: Text[250]): Text[260]
    begin
        EXIT(BrowseForFolder(Ttitle, 18));
    end;

    procedure BrowseForFolder(Ttitle: Text[250]; Root: Variant): Text[260]
    var
        SelectedFolder: Text;
    begin
        SelectedFolder := FileMgt.BrowseForFolderDialog('', '', TRUE);
        EXIT(SelectedFolder);

    end;*/
    //pain001()

    procedure CheckISOSetup(VAR ISOBank: Record "DTA Setup")
    var

    begin
        CompanyInfo.GET;
        ISOBank.Reset();
        ISOBank.SetRange("DTA/EZAG/ISO", "DTA/EZAG/ISO"::ISO);
        ISOBank.FindFirst();

        CompanyInfo.TESTFIELD("ISO active");
        CompanyInfo.TESTFIELD("Credit Transfer Msg. Nos.");
        CompanyInfo.TESTFIELD("Software Version");
        CompanyInfo.TESTFIELD("Software Name");
        CompanyInfo.TESTFIELD(Manufacturer);

        ISOBank.TESTFIELD("ISO Sender IBAN");
        ISOBank.TESTFIELD("ISO SWIFT Code (BIC)");
        //TESTFIELD("ISO File Folder");
        //TESTFIELD("ISO Filename");
        ISOBank.TESTFIELD("DTA Sender Name");
        ISOBank.TESTFIELD("DTA/EZAG/ISO", 2);


    end;

    procedure CalcFileTotAmount(VAR GenJourLine: Record "Gen. Journal Line"): Decimal
    var
        Amt: Decimal;
    begin
        CLEAR(Amt);
        IF GenJourLine.FINDFIRST THEN BEGIN
            REPEAT
                Amt := Amt + GenJourLine.Amount;
            UNTIL GenJourLine.NEXT = 0;
        END;

        EXIT(Amt);
    end;

    procedure CheckPostingDate(VAR GenJourLine: Record "Gen. Journal Line"): Date
    var
        PostingDate: Date;
    begin
        CLEAR(PostingDate);
        IF GenJourLine.FINDFIRST THEN BEGIN
            PostingDate := GenJourLine."Posting Date";
            IF PostingDate = 0D THEN
                ERROR(Text002, GenJourLine.FIELDCAPTION("Posting Date"))
            ELSE
                IF PostingDate < TODAY THEN
                    ERROR(Text003, GenJourLine.FIELDCAPTION("Posting Date"));
            EXIT(PostingDate);
        END;
    end;


    procedure CreateIsoPayment(var xmlDoc: XmlDocument; var root: XmlElement; GenJourLine: Record "Gen. Journal Line";
    VendBank: Record "Vendor Bank Account"; PostingDate: Date; _ID: Code[40]; PaymentCounter: Integer; SummaryPmtAmt: decimal;
    _SummaryPmtTxt: Text[140]; _SEPA: Boolean)

    var
        Currency: Record Currency;
    begin

        ClearPaymentVAR;

        VendorBank := VendBank;
        SEPA := _SEPA;

        ID := _ID;
        vInstrId := 'INSTRID' + _ID + FORMAT(PaymentCounter);
        vEndToEndId := 'ENDTOENDID' + _ID + 'A' + FORMAT(PaymentCounter);

        IF PostingDate <> GenJourLine."Posting Date" THEN
            ERROR(Text005 + Text004, PostingDate, GenJourLine."Posting Date");

        LocalInstrument := FALSE;
        IF Currency.GET(GenJourLine."Currency Code") THEN BEGIN
            Currency.TESTFIELD("ISO Currency Code");
            CurrCode := Currency."ISO Currency Code";
            IF (Currency."ISO Currency Code" = 'EUR') OR (Currency."ISO Currency Code" = 'CHF') THEN
                LocalInstrument := TRUE;
        END ELSE BEGIN
            CurrCode := 'CHF';
            LocalInstrument := TRUE;
        END;

        Amount := SummaryPmtAmt;
        SummaryPmtTxt := _SummaryPmtTxt;
        Vendor.GET(VendorBank."Vendor No.");
        Vendor.TESTFIELD(Name);

        IF Vendor.Address <> '' THEN
            StreetName := Vendor.Address
        ELSE BEGIN
            IF Vendor."Address 2" <> '' THEN
                StreetName := Vendor."Address 2"
            ELSE
                IF Vendor."Name 2" <> '' THEN
                    StreetName := Vendor."Name 2";
        END;
        IF StreetName <> '' THEN
            FindStreetNumber(StreetName, StreetNumber);

        IF Vendor."Country/Region Code" = '' THEN
            CountryCode := 'CH'
        ELSE
            CountryCode := Vendor."Country/Region Code";

        IF VendorBank."Country/Region Code" = '' THEN
            CreditAgentCountryCode := 'CH'
        ELSE
            CreditAgentCountryCode := VendorBank."Country/Region Code";

        IF VendorBank.Address <> '' THEN
            CreditAgentStreetName := VendorBank.Address
        ELSE BEGIN
            IF VendorBank."Address 2" <> '' THEN
                CreditAgentStreetName := VendorBank."Address 2"
            ELSE
                IF VendorBank."Name 2" <> '' THEN
                    CreditAgentStreetName := VendorBank."Name 2";
        END;
        IF CreditAgentStreetName <> '' THEN
            FindStreetNumber(CreditAgentStreetName, CreditAgentStreetNumber);

        AddressLine1 := CreateAddrLine1(Vendor.Address, Vendor."Address 2");
        AddressLine2 := CreateAddrLine1(Vendor."Post Code", Vendor.City);

        BankAddressLine1 := CreateAddrLine1(VendorBank.Address, VendorBank."Address 2");
        BankAddressLine2 := CreateAddrLine1(VendorBank."Post Code", VendorBank.City);

        CASE VendorBank."Payment Form Enum" OF
            //Zahlungsart CH01 1
            VendorBank."Payment Form Enum"::ESR, VendorBank."Payment Form Enum"::"ESR+":
                BEGIN
                    VendorBank.TESTFIELD("ESR Account No.");
                    VendorBank.TESTFIELD("ESR Type");
                    VendLedgerEntry.RESET;
                    VendLedgerEntry.SETCURRENTKEY("Document No.");
                    VendLedgerEntry.SETRANGE("Document Type", VendLedgerEntry."Document Type"::Invoice);
                    VendLedgerEntry.SETRANGE("Document No.", GenJourLine."Applies-to Doc. No.");
                    VendLedgerEntry.SETRANGE("Vendor No.", VendorBank."Vendor No.");
                    IF NOT VendLedgerEntry.FIND('-') THEN
                        ERROR(Text007, GenJourLine."Applies-to Doc. No.", GenJourLine."Account No.");
                    // Reference number 5/15
                    IF VendorBank."ESR Type" = VendorBank."ESR Type"::"5/15" THEN BEGIN
                        BankAccountNo := '0000' + VendorBank."ESR Account No.";
                        RefNo := COPYSTR(VendLedgerEntry."Reference No.", 1, 15);
                        ChkDig := COPYSTR(VendLedgerEntry."Reference No.", 17, 2);
                        IF STRLEN(RefNo) <> 15 THEN
                            ERROR(Text008, GenJourLine."Applies-to Doc. No.", GenJourLine."Account No.", 15);
                    END;
                    // 9/27
                    IF VendorBank."ESR Type" = VendorBank."ESR Type"::"9/27" THEN BEGIN
                        BankAccountNo := DELCHR(VendorBank."ESR Account No.", '=', '-');  // remove '-'
                        RefNo := VendLedgerEntry."Reference No.";
                        IF STRLEN(RefNo) <> 27 THEN
                            ERROR(Text008, GenJourLine."Applies-to Doc. No.", GenJourLine."Account No.", 27);
                    END;
                    // 9/16
                    IF VendorBank."ESR Type" = VendorBank."ESR Type"::"9/16" THEN BEGIN
                        BankAccountNo := DELCHR(VendorBank."ESR Account No.", '=', '-');  // remove '-'
                        RefNo := VendLedgerEntry."Reference No.";
                        IF STRLEN(RefNo) <> 16 THEN
                            ERROR(Text008, GenJourLine."Applies-to Doc. No.", GenJourLine."Account No.", 16);
                        RefNo := '00000000000' + RefNo;
                    END;
                    AddESRPaymentToXML(xmlDoc, root);
                END;

            //Zahlungsart CH02 2.1 CH03 2.2
            VendorBank."Payment Form Enum"::"Post Payment Domestic":
                BEGIN
                    VendorBank.TESTFIELD("Giro Account No.");
                    IF NOT LocalInstrument THEN
                        ERROR(Text012, VendorBank."Payment Form Enum", 'CHF/EUR', Vendor."No.", Amount);
                    VendLedgerEntry.RESET;
                    VendLedgerEntry.SETCURRENTKEY("Document No.");
                    VendLedgerEntry.SETRANGE("Document Type", VendLedgerEntry."Document Type"::Invoice);
                    VendLedgerEntry.SETRANGE("Document No.", GenJourLine."Applies-to Doc. No.");
                    VendLedgerEntry.SETRANGE("Vendor No.", VendorBank."Vendor No.");
                    IF NOT VendLedgerEntry.FIND('-') THEN
                        VendLedgerEntry.INIT;
                    BankAccountNo := DELCHR(VendorBank."Giro Account No.", '=', '-');
                    IF SummaryPmtTxt = '' THEN
                        SummaryPmtTxt := VendLedgerEntry."External Document No.";
                    IBAN := IBANDELCHR(VendorBank.IBAN);
                    IF IBAN <> '' THEN BEGIN
                        ClearingNo := GetIBAN_IID(IBAN);
                        AddBankDomesticPaymentToXML(xmlDoc, root);
                    END ELSE BEGIN
                        AddPostDomesticPaymentToXML(xmlDoc, root);
                    END;
                END;

            //Zahlungsarten CH03 2.2 und 3(V1)
            VendorBank."Payment Form Enum"::"Bank Payment Domestic":
                BEGIN
                    IF (VendorBank."Bank Account No." = '') AND (VendorBank.IBAN = '') THEN
                        ERROR(Text009, VendorBank.Code, Vendor."No.", VendorBank.FIELDCAPTION("Bank Account No."), VendorBank.FIELDCAPTION(IBAN));
                    VendLedgerEntry.RESET;
                    VendLedgerEntry.SETCURRENTKEY("Document No.");
                    VendLedgerEntry.SETRANGE("Document Type", VendLedgerEntry."Document Type"::Invoice);
                    VendLedgerEntry.SETRANGE("Document No.", GenJourLine."Applies-to Doc. No.");
                    VendLedgerEntry.SETRANGE("Vendor No.", VendorBank."Vendor No.");
                    IF NOT VendLedgerEntry.FIND('-') THEN
                        VendLedgerEntry.INIT;
                    BankAccountNo := VendorBank."Bank Account No.";
                    IBAN := IBANDELCHR(VendorBank.IBAN);
                    IF IBAN <> '' THEN BEGIN
                        ClearingNo := GetIBAN_IID(IBAN);
                    END ELSE BEGIN
                        VendorBank.TESTFIELD("Clearing No.");
                        ClearingNo := VendorBank."Clearing No.";
                    END;
                    IF NOT LocalInstrument THEN BEGIN
                        VendorBank.TESTFIELD(Name);
                        VendorBank.TESTFIELD("Post Code");
                        VendorBank.TESTFIELD(City);
                    END;
                    IF SummaryPmtTxt = '' THEN
                        SummaryPmtTxt := VendLedgerEntry."External Document No.";
                    AddBankDomesticPaymentToXML(xmlDoc, root);
                END;

            VendorBank."Payment Form Enum"::"Cash Outpayment Order Domestic":
                BEGIN
                    ERROR(Text011, VendorBank."Payment Form Enum", Vendor."No.", VendorBank.Code);
                END;

            VendorBank."Payment Form Enum"::"Post Payment Abroad":
                BEGIN
                    ERROR(Text011, VendorBank."Payment Form Enum", Vendor."No.", VendorBank.Code);
                END;

            //Zahlungsart 6(V3)
            VendorBank."Payment Form Enum"::"Bank Payment Abroad":
                BEGIN
                    IF (VendorBank."Bank Account No." = '') AND (VendorBank.IBAN = '') THEN
                        ERROR(Text009, VendorBank.Code, Vendor."No.", VendorBank.FIELDCAPTION("Bank Account No."), VendorBank.FIELDCAPTION(IBAN));
                    Vendor.TESTFIELD("Country/Region Code");
                    CountryCode := Vendor."Country/Region Code";
                    VendLedgerEntry.RESET;
                    VendLedgerEntry.SETCURRENTKEY("Document No.");
                    VendLedgerEntry.SETRANGE("Document Type", VendLedgerEntry."Document Type"::Invoice);
                    VendLedgerEntry.SETRANGE("Document No.", GenJourLine."Applies-to Doc. No.");
                    VendLedgerEntry.SETRANGE("Vendor No.", VendorBank."Vendor No.");
                    IF NOT VendLedgerEntry.FIND('-') THEN
                        VendLedgerEntry.INIT;
                    VendorBank.TESTFIELD(Name);

                    BankAccountNo := DELCHR(VendorBank."Bank Account No.", '=', ' ');
                    IBAN := IBANDELCHR(VendorBank.IBAN);
                    IF IBAN <> '' THEN BEGIN
                        IF VendorBank."Country/Region Code" = '' THEN
                            CreditAgentCountryCode := COPYSTR(IBAN, 1, 2)
                    END ELSE BEGIN
                        VendorBank.TESTFIELD("Country/Region Code");
                    END;
                    IF (CreditAgentCountryCode = 'CH') THEN
                        ERROR(Text020, Vendor."No.", VendorBank."Payment Form Enum");
                    IF SummaryPmtTxt = '' THEN
                        SummaryPmtTxt := VendLedgerEntry."External Document No.";
                    AddBankAbroadPaymentToXML(xmlDoc, root);
                END;

            //Zahlungsarten 5(SEPA) und 6(V1)
            VendorBank."Payment Form Enum"::"SWIFT Payment Abroad":
                BEGIN
                    IF NOT SEPA THEN
                        VendorBank.TESTFIELD("SWIFT Code");
                    IF (VendorBank."Bank Account No." = '') AND (VendorBank.IBAN = '') THEN
                        ERROR(Text009, VendorBank.Code, Vendor."No.", VendorBank.FIELDCAPTION("Bank Account No."), VendorBank.FIELDCAPTION(IBAN));
                    Vendor.TESTFIELD("Country/Region Code");
                    CountryCode := Vendor."Country/Region Code";
                    VendLedgerEntry.RESET;
                    VendLedgerEntry.SETCURRENTKEY("Document No.");
                    VendLedgerEntry.SETRANGE("Document Type", VendLedgerEntry."Document Type"::Invoice);
                    VendLedgerEntry.SETRANGE("Document No.", GenJourLine."Applies-to Doc. No.");
                    VendLedgerEntry.SETRANGE("Vendor No.", VendorBank."Vendor No.");
                    IF NOT VendLedgerEntry.FIND('-') THEN
                        VendLedgerEntry.INIT;
                    SwiftCode := VendorBank."SWIFT Code";
                    BankAccountNo := VendorBank."Bank Account No.";
                    IBAN := IBANDELCHR(VendorBank.IBAN);
                    DocumentNo := VendLedgerEntry."External Document No.";
                    IF SummaryPmtTxt = '' THEN
                        SummaryPmtTxt := VendLedgerEntry."External Document No.";
                    AddSwiftPaymentToXML(xmlDoc, root);
                END;

            VendorBank."Payment Form Enum"::"Cash Outpayment Order Abroad":
                BEGIN
                    ERROR(Text011, VendorBank."Payment Form Enum", Vendor."No.", VendorBank.Code);
                END;

            //ISO20022QR
            //Zahlungsarten CH03 3(QRR)
            VendorBank."Payment Form Enum"::"QR with 27 digit Reference", VendorBank."Payment Form Enum"::QR:
                BEGIN
                    IF VendorBank.IBAN = '' THEN
                        ERROR(Text006, VendorBank.Code, Vendor."No.", VendorBank.FIELDCAPTION(IBAN));
                    VendLedgerEntry.RESET;
                    VendLedgerEntry.SETCURRENTKEY("Document No.");
                    VendLedgerEntry.SETRANGE("Document Type", VendLedgerEntry."Document Type"::Invoice);
                    VendLedgerEntry.SETRANGE("Document No.", GenJourLine."Applies-to Doc. No.");
                    VendLedgerEntry.SETRANGE("Vendor No.", VendorBank."Vendor No.");
                    IF NOT VendLedgerEntry.FIND('-') THEN
                        VendLedgerEntry.INIT;
                    IBAN := IBANDELCHR(VendorBank.IBAN);
                    ClearingNo := GetIBAN_IID(IBAN);
                    RefNo := VendLedgerEntry."Reference No.";
                    if RefNo = '' then
                        ERROR(Text214, GenJourLine."Account No.", GenJourLine."Applies-to Doc. No.", GenJourLine."Recipient Bank Account");
                    IF SummaryPmtTxt = '' THEN
                        SummaryPmtTxt := VendLedgerEntry."External Document No.";


                    AddBankQRRPaymentToXML(xmlDoc, root);
                END;
        //ISO20022QR
        END;
    END;


    procedure ClearPaymentVAR()
    begin
        CLEAR(LocalInstrument);
        CLEAR(BankAccountNo);
        CLEAR(IBAN);
        CLEAR(DocumentNo);
        CLEAR(RefNo);
        CLEAR(ChkDig);
        CLEAR(vInstrId);
        CLEAR(vEndToEndId);
        CLEAR(ID);
        CLEAR(CurrCode);
        CLEAR(Amount);
        CLEAR(CountryCode);
        CLEAR(ClearingNo);
        CLEAR(SwiftCode);
        CLEAR(BankIdentifier);
        CLEAR(StreetName);
        CLEAR(StreetNumber);
        CLEAR(AddressLine1);
        CLEAR(AddressLine2);
        CLEAR(CreditAgentCountryCode);
        CLEAR(CreditAgentName);
        CLEAR(CreditAgentStreetName);
        CLEAR(CreditAgentStreetNumber);
        CLEAR(CreditAgentPostCode);
        CLEAR(CreditAgentCity);
        CLEAR(BeneficiaryName);
        CLEAR(BeneficiaryAddress);
        CLEAR(BankAddressLine1);
        CLEAR(BankAddressLine2);
        CLEAR(SummaryPmtTxt);
        CLEAR(SEPA);
    end;

    procedure IBANDELCHR(VAR _IBAN: Code[37]): Code[37]
    begin
        // Delete all spaces and ':'
        _IBAN := DELCHR(_IBAN);
        _IBAN := DELCHR(_IBAN, '=', ':');
        EXIT(_IBAN);
    end;

    procedure GetIBAN_IID(IBAN: Code[50]) IID: Code[5]
    begin
        IID := COPYSTR(IBAN, 5, 5);
        EXIT(IID);
    end;

    procedure FindStreetNumber(VAR StreetName: Text[50]; VAR StreetNumber: Text[50])
    var
        WordArray: array[50] of Text[50];
        ArrayCounter: Integer;
        _StreetName: Text;
    begin

        ArrayCounter := 1;
        _StreetName := StreetName;
        IF STRPOS(_StreetName, ' ') > 0 THEN BEGIN
            REPEAT
                IF STRPOS(_StreetName, ' ') > 0 THEN BEGIN
                    WordArray[ArrayCounter] := COPYSTR(_StreetName, 1, STRPOS(_StreetName, ' ') - 1);
                    _StreetName := COPYSTR(_StreetName, STRPOS(_StreetName, ' ') + 1, STRLEN(_StreetName));
                END ELSE BEGIN
                    WordArray[ArrayCounter] := COPYSTR(_StreetName, 1, STRLEN(_StreetName));
                    _StreetName := '';
                END;
                ArrayCounter := ArrayCounter + 1;
            UNTIL STRLEN(_StreetName) = 0;

            ArrayCounter := COMPRESSARRAY(WordArray);
            REPEAT
                IF COPYSTR(WordArray[ArrayCounter], 1, 1) IN ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'] THEN BEGIN
                    StreetNumber := WordArray[ArrayCounter];
                    EXIT;
                END;
                ArrayCounter := ArrayCounter - 1;
            UNTIL ArrayCounter = 0;
        END;
    end;

    procedure CreateAddrLine1(Address: Text[50]; Address2: Text[50]) AddrLine: Text[110]
    begin
        IF Address <> '' THEN
            AddrLine := Address;
        IF Address2 <> '' THEN
            IF AddrLine <> '' THEN
                AddrLine := AddrLine + ' ' + Address2
            ELSE
                AddrLine := Address2;

        AddrLine := COPYSTR(AddrLine, 1, 70);
    end;

    procedure CreateAddrLine2(PostCode: Text[20]; City: Text[30]) AddrLine2: Text[60]
    begin
        IF PostCode <> '' THEN
            AddrLine2 := PostCode;
        IF City <> '' THEN
            IF AddrLine2 <> '' THEN
                AddrLine2 := AddrLine2 + ' ' + City
            ELSE
                AddrLine2 := City;
    end;

    //XML()


    procedure CreateXMLHeader(var xmlDoc: XmlDocument; var root: XmlElement; var NoOfTransactions: integer; var ControlSum: Decimal; var MessageID: Code[40]): Text
    var

        ChildNode: XmlNode;
        Counter: integer;
        BankAccount: Record "DTA Setup";
        PaymentInfoID: Code[40];
        PostingDate: Date;
        Salary: Boolean;
        xmlAtt: XmlAttribute;
        xmlDec: XmlDeclaration;

        xmlElem2: xmlElement;
        xmlElem3: xmlElement;
        xmlElem4: xmlElement;
        xmlElem5: xmlElement;
        xmlElem6: xmlElement;
        xmlElem7: xmlElement;
        xmlElem8: xmlElement;
        xmlElem9: xmlElement;
        xmlElem10: xmlElement;
        xmlElem11: xmlElement;
        xmlElem12: xmlElement;
        xmlElem13: xmlElement;
        xmlElem14: xmlElement;
        xmlElem15: xmlElement;
        xmlElem16: xmlElement;
        xmlElem17: xmlElement;



    begin

        CompanyInfo.GET;
        fileName := 'ISO2022.xml';
        ns := 'http://www.six-interbank-clearing.com/de/pain.001.001.03.ch.02.xsd';
        xmlDoc := XmlDocument.Create();
        xmlDec := xmlDeclaration.Create('', '', '');
        xmlDec.Encoding := 'UTF-8';
        xmlDec.Standalone := 'yes';
        xmlDec.Version := '1.0';
        xmlDoc.SetDeclaration(xmlDec);


        root := xmlElement.Create('Document', ns);


        xmlAtt := XmlAttribute.CreateNamespaceDeclaration('xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        root.Add(xmlAtt);

        root.SetAttribute('schemaLocation', 'http://www.w3.org/2001/XMLSchema-instance', 'http://www.six-interbank-clearing.com/de/pain.001.001.03.ch.02.xsd  pain.001.001.03.ch.02.xsd');

        xmlDoc.Add(root);




        xmlElem2 := XmlElement.Create('CstmrCdtTrfInitn', ns);
        root.Add(xmlElem2);


        xmlElem3 := XmlElement.Create('GrpHdr', ns);
        xmlElem2.Add(xmlElem3);


        xmlElem4 := XmlElement.Create('MsgId', ns);
        xmlElem4.Add(MessageID);
        xmlElem3.Add(xmlElem4);

        xmlElem5 := XmlElement.Create('CreDtTm', ns);
        xmlElem5.Add(DT_NAV2XML(CURRENTDATETIME));
        xmlElem3.Add(xmlElem5);

        xmlElem6 := XmlElement.Create('NbOfTxs', ns);
        xmlElem6.Add(FORMAT(NoOfTransactions));
        xmlElem3.Add(xmlElem6);

        xmlElem7 := XmlElement.Create('CtrlSum', ns);
        xmlElem7.Add(FormXMLDecimal(ControlSum, 20, 2, TRUE));
        xmlElem3.Add(xmlElem7);

        xmlElem8 := XmlElement.Create('InitgPty', ns);
        xmlElem3.Add(xmlElem8);

        xmlElem9 := XmlElement.Create('Nm', ns);
        xmlElem9.Add(CompanyName);
        xmlElem8.Add(xmlElem9);

        IF CompanyInfo."VAT Registration No." <> '' THEN BEGIN
            xmlElem10 := XmlElement.Create('Id', ns);
            xmlElem8.Add(xmlElem10);
            xmlElem11 := XmlElement.Create('OrgId', ns);
            xmlElem10.Add(xmlElem11);
            xmlElem12 := XmlElement.Create('Othr', ns);
            xmlElem11.Add(xmlElem12);
            xmlElem13 := XmlElement.Create('Id', ns);
            xmlElem13.Add(CompanyInfo."VAT Registration No.");
            xmlElem12.Add(xmlElem13);
        end;


        xmlElem14 := XmlElement.Create('CtctDtls', ns);
        xmlElem8.Add(xmlElem14);

        xmlElem15 := XmlElement.Create('Nm', ns);
        xmlElem15.Add(CompanyInfo.Manufacturer + ' // ' + CompanyInfo."Software Name");
        xmlElem14.Add(xmlElem15);

        xmlElem16 := XmlElement.Create('Othr', ns);
        xmlElem16.Add(CompanyInfo."Software Version");
        xmlElem14.Add(xmlElem16);


    end;


    procedure SaveXMLDoc(var xmlDoc: XMlDocument)
    begin


        DtaISOSetup.get('BEKB CHF');
        // aCreate an outStream from the Blob, notice the encoding.
        TempBlob.CreateOutStream(outStr);

        // Write the contents of the docxmlDoc.SelectSingleNode('Document', CstmrCdtTrfInitn2); to the stream
        xmlDoc.WriteTo(outStr);

        // From the same Blob, that now contains the XML document, create an inStr
        TempBlob.CreateInStream(inStr);
        fileName := DtaISOSetup."ISO Filename";

        // Save the data of the InStream as a file.
        File.DownloadFromStream(inStr, 'Export', '', '', fileName);

    end;

    procedure AssignElementValue(ParentNodeName: text; NodeName: text; NodeValue: text; var root: XmlNode): Boolean
    var
        Counter: integer;
        list: XmlNodeList;
        ChildNode: XmlNode;
        ParentNode: XMlElement;

        innerText: XmlText;
        OK: Boolean;

    begin
        Clear(ParentNode);
        Clear(ChildNode);

        if root.IsXmlText then
            exit(false);

        list := root.AsXmlElement().GetChildNodes();
        if (list.Count = 0) then
            exit(false);
        innerText := XmlText.Create(NodeValue);

        FOR Counter := 1 TO list.Count DO BEGIN

            list.Get(counter, ChildNode);
            if NOT ChildNode.IsXmlText then begin
                if ParentNodeName <> '' then begin
                    OK := ChildNode.GetParent(ParentNode);
                    if (OK) AND (ParentNode.LocalName = ParentNodeName) then begin
                        if ChildNode.AsXmlElement().LocalName = NodeName then begin
                            ChildNode.AsXmlElement().Add(innerText);
                            EXIT(TRUE);
                        end
                        else begin
                            AssignElementValue(ParentNodeName, NodeName, NodeValue, ChildNode);
                        end;

                    end;
                end;

                if ParentNodeName = '' then begin
                    if ChildNode.AsXmlElement().LocalName = NodeName then begin
                        ChildNode.AsXmlElement().Add(innerText);
                        EXIT(TRUE);
                    end;
                end;
            end;

            AssignElementValue(ParentNodeName, NodeName, NodeValue, ChildNode);
        end;

        //Message('%1', ChildNode.AsXmlElement().LocalName);
        EXIT(FALSE);

    end;

    procedure GetNode(ParentNodeName: text; NodeName: Text; root: XmlNode): XmlNode
    var
        Counter: integer;
        ChildNode: XmlNode;
        ParentNode: XMlElement;
        list: XmlNodeList;
        OK: Boolean;
    begin

        Clear(ParentNode);
        Clear(ChildNode);

        if root.IsXmlText then
            exit;

        list := root.AsXmlElement().GetChildNodes();
        if (list.Count = 0) then
            exit;

        FOR Counter := 1 TO list.Count DO BEGIN

            list.Get(counter, ChildNode);
            if NOT ChildNode.IsXmlText then begin
                if ParentNodeName <> '' then begin
                    OK := ChildNode.GetParent(ParentNode);
                    if (OK) AND (ParentNode.LocalName = ParentNodeName) then begin
                        if ChildNode.AsXmlElement().LocalName = NodeName then begin
                            EXIT(ChildNode);
                        end
                        else begin
                            GetNode(ParentNodeName, NodeName, ChildNode);
                        end;

                    end;
                end;

                if ParentNodeName = '' then begin
                    if ChildNode.AsXmlElement().LocalName = NodeName then begin
                        EXIT(ChildNode);
                    end;
                end;
            end;

            GetNode(ParentNodeName, NodeName, ChildNode);
        end;




    end;

    procedure AddPaymentInfoToXML(var xmlDoc: XMLDocument; var root: XMlElement; VAR BankAccount: Record "DTA Setup"; VAR PaymentInfoID: Code[40]; VAR PostingDate: Date; Salary: Boolean): Text
    var
        parentNode: XmlNode;
        PmtInf: XmlElement;
        PmtInfId: XmlElement;
        PmtMtd: XmlNode;
        BtchBookg: XmlNode;
        PmtTpInf: XmlNode;
        CtgyPurp: XmlNode;
        Cd: XmlNode;
        ReqdExctnDt: XmlNode;
        Dbtr: XmlNode;
        Nm: XmlNode;
        DbtrAcct: XmlNode;
        Id: XmlNode;
        IBAN: XmlNode;
        Tp: XmlNode;
        Prtry: XMlNOde;
        DbtrAgt: XmlNode;
        FinInstnId: XmlNode;
        BIC: XmlNode;
        nodes: Text;
        txt1: Text;
        created: XmlNodeList;
        xmltxt: XmlText;
        Document: XmlNode;
        CstmrCdtTrfInitn: XmlNode;

        xmlElem2: xmlElement;
        xmlElem3: xmlElement;
        xmlElem4: xmlElement;
        xmlElem5: xmlElement;
        xmlElem6: xmlElement;
        xmlElem7: xmlElement;
        xmlElem8: xmlElement;
        xmlElem9: xmlElement;
        xmlElem10: xmlElement;
        xmlElem11: xmlElement;
        xmlElem12: xmlElement;
        xmlElem13: xmlElement;
        xmlElem14: xmlElement;
        xmlElem15: xmlElement;
        xmlElem16: xmlElement;
        xmlElem17: xmlElement;
        xmlElem18: xmlElement;
        xmlElem19: xmlElement;

        xmlList: XmlNodeList;
        xmlList2: XmlNodeList;


    begin

        CompanyInfo.GET;

        xmlList := xmlDoc.GetChildElements();
        xmlList.Get(1, Document);

        xmlList2 := Document.AsXmlElement().GetChildNodes();
        xmlList2.Get(1, CstmrCdtTrfInitn);


        xmlElem2 := XmlElement.Create('PmtInf', ns);
        CstmrCdtTrfInitn.AsXmlElement().Add(xmlElem2);
        root := xmlElem2;

        xmlElem3 := XmlElement.Create('PmtInfId', ns);
        xmlElem3.Add(PaymentInfoID);
        xmlElem2.Add(xmlElem3);

        xmlElem4 := XmlElement.Create('PmtMtd', ns);
        xmlElem4.Add('TRF');
        xmlElem2.Add(xmlElem4);

        xmlElem5 := XmlElement.Create('BtchBookg', ns);
        xmlElem5.Add('true');
        xmlElem2.Add(xmlElem5);



        IF Salary THEN BEGIN
            xmlElem6 := XmlElement.Create('PmtTpInf', ns);
            xmlElem2.Add(xmlElem6);

            xmlElem7 := XmlElement.Create('CtgyPurp', ns);
            xmlElem6.Add(xmlElem7);


            xmlElem8 := XmlElement.Create('Cd', ns);
            xmlElem8.Add('SALA');
            xmlElem7.Add(xmlElem8);

        END;

        xmlElem9 := XmlElement.Create('ReqdExctnDt', ns);
        xmlElem9.Add(D_NAV2XML(PostingDate));
        xmlElem2.Add(xmlElem9);

        xmlElem10 := XmlElement.Create('Dbtr', ns);
        xmlElem2.Add(xmlElem10);

        xmlElem11 := XmlElement.Create('Nm', ns);
        xmlElem11.Add(BankAccount."DTA Sender Name");
        xmlElem10.Add(xmlElem11);

        xmlElem12 := XmlElement.Create('DbtrAcct', ns);
        xmlElem2.Add(xmlElem12);

        xmlElem13 := XmlElement.Create('Id', ns);
        xmlElem12.Add(xmlElem13);

        xmlElem14 := XmlElement.Create('IBAN', ns);
        xmlElem14.Add(IBANDELCHR(BankAccount."ISO Sender IBAN"));
        xmlElem13.Add(xmlElem14);



        IF Salary THEN BEGIN
            xmlElem15 := XmlElement.Create('Tp', ns);
            xmlElem12.Add(xmlElem15);

            xmlElem16 := XmlElement.Create('Prtry', ns);
            xmlElem16.Add('CND');
            xmlElem15.Add(xmlElem16);

        END;

        xmlElem17 := XmlElement.Create('DbtrAgt', ns);
        xmlElem2.Add(xmlElem17);

        xmlElem18 := XmlElement.Create('FinInstnId', ns);
        xmlElem17.Add(xmlElem18);

        xmlElem19 := XmlElement.Create('BIC', ns);
        xmlElem19.Add(BankAccount."ISO SWIFT Code (BIC)");
        xmlElem18.Add(xmlElem19);


    end;


    procedure AddPaymentInfoSEPAToXML(var xmlDoc: XmlDOcument; var root: XmlElement; VAR BankAccount: Record "DTA Setup"; VAR PaymentInfoID: Code[40]; VAR PostingDate: Date): Text
    var
        CstmrCdtTrfInitn: XmlNode;
        Document: XmlNode;

        xmlElem2: xmlElement;
        xmlElem3: xmlElement;
        xmlElem4: xmlElement;
        xmlElem5: xmlElement;
        xmlElem6: xmlElement;
        xmlElem7: xmlElement;
        xmlElem8: xmlElement;
        xmlElem9: xmlElement;
        xmlElem10: xmlElement;
        xmlElem11: xmlElement;
        xmlElem12: xmlElement;
        xmlElem13: xmlElement;
        xmlElem14: xmlElement;
        xmlElem15: xmlElement;
        xmlElem16: xmlElement;
        xmlElem17: xmlElement;
        xmlElem18: xmlElement;
        xmlElem19: xmlElement;

        xmlList: XmlNodeList;
        xmlList2: XmlNodeList;
    begin
        CompanyInfo.GET;

        xmlList := xmlDoc.GetChildElements();
        xmlList.Get(1, Document);

        xmlList2 := Document.AsXmlElement().GetChildNodes();
        xmlList2.Get(1, CstmrCdtTrfInitn);



        xmlElem2 := XmlElement.Create('PmtInf', ns);
        CstmrCdtTrfInitn.AsXmlElement().Add(xmlElem2);
        root := xmlElem2;

        xmlElem3 := XmlElement.Create('PmtInfId', ns);
        xmlElem3.Add(PaymentInfoID);
        xmlElem2.Add(xmlElem3);

        xmlElem4 := XmlElement.Create('PmtMtd', ns);
        xmlElem4.Add('TRF');
        xmlElem2.Add(xmlElem4);

        xmlElem5 := XmlElement.Create('BtchBookg', ns);
        xmlElem5.Add('true');
        xmlElem2.Add(xmlElem5);



        //SEPA

        xmlElem6 := XmlElement.Create('PmtTpInf', ns);
        xmlElem2.Add(xmlElem6);

        xmlElem7 := XmlElement.Create('SvcLvl', ns);
        xmlElem6.Add(xmlElem7);

        xmlElem8 := XmlElement.Create('Cd', ns);
        xmlElem8.Add('SEPA');
        xmlElem7.Add(xmlElem8);


        xmlElem9 := XmlElement.Create('ReqdExctnDt', ns);
        xmlElem9.Add(D_NAV2XML(PostingDate));
        xmlElem2.Add(xmlElem9);

        xmlElem10 := XmlElement.Create('Dbtr', ns);
        xmlElem2.Add(xmlElem10);


        xmlElem11 := XmlElement.Create('Nm', ns);
        xmlElem11.Add(BankAccount."DTA Sender Name");
        xmlElem10.Add(xmlElem11);

        xmlElem12 := XmlElement.Create('DbtrAcct', ns);
        xmlElem2.Add(xmlElem12);

        xmlElem13 := XmlElement.Create('Id', ns);
        xmlElem12.Add(xmlElem13);

        xmlElem14 := XmlElement.Create('IBAN', ns);
        xmlElem14.Add(IBANDELCHR(BankAccount."ISO Sender IBAN"));
        xmlElem13.Add(xmlElem14);

        xmlElem15 := XmlElement.Create('DbtrAgt', ns);
        xmlElem2.Add(xmlElem15);

        xmlElem16 := XmlElement.Create('FinInstnId', ns);
        xmlElem15.Add(xmlElem16);

        xmlElem17 := XmlElement.Create('BIC', ns);
        xmlElem17.Add(BankAccount."ISO SWIFT Code (BIC)");
        xmlElem16.Add(xmlElem17);


    end;

    procedure AddESRPaymentToXML(var xmlDoc: XmlDOcument; var root: XmlElement)
    var
        PmtInf: XmlNode;
        CdtTrfTxInf: XmlNode;
        PmtId: XmlNode;
        InstrId: XmlNode;
        EndToEndId: XmlNode;
        PmtTpInf: XmlNode;
        LclInstrm: XmlNode;
        Prtry: XmlNode;
        Amt: XmlNode;
        InstdAmt: XmlNode;
        Cdtr: XmlNode;
        Nm: XmlNode;
        CdtrAcct: XmlNode;
        Id: XmlNode;
        Othr: XmlNode;
        IdAccount: XmlNode;
        RmtInf: XmlNode;
        Strd: XmlNode;
        CdtrRefInf: XmlNode;
        Ref: XmlNode;


        CstmrCdtTrfInitn: XmlNode;
        Document: xmlElement;

        xmlElem2: xmlElement;
        xmlElem3: xmlElement;
        xmlElem4: xmlElement;
        xmlElem5: xmlElement;
        xmlElem6: xmlElement;
        xmlElem7: xmlElement;
        xmlElem8: xmlElement;
        xmlElem9: xmlElement;
        xmlElem10: xmlElement;
        xmlElem11: xmlElement;
        xmlElem12: xmlElement;
        xmlElem13: xmlElement;
        xmlElem14: xmlElement;
        xmlElem15: xmlElement;
        xmlElem16: xmlElement;
        xmlElem17: xmlElement;
        xmlElem18: xmlElement;
        xmlElem19: xmlElement;
        xmlElem20: xmlElement;

    begin
        CompanyInfo.GET;
        xmlElem2 := XmlElement.Create('CdtTrfTxInf', ns);
        root.Add(xmlElem2);

        xmlElem3 := XmlElement.Create('PmtId', ns);
        xmlElem2.Add(xmlElem3);

        xmlElem4 := XmlElement.Create('InstrId', ns);
        xmlElem4.Add(vInstrId);
        xmlElem3.Add(xmlElem4);

        xmlElem5 := XmlElement.Create('EndToEndId', ns);
        xmlElem5.Add(vEndToEndId);
        xmlElem3.Add(xmlElem5);


        xmlElem6 := XmlElement.Create('PmtTpInf', ns);
        xmlElem2.Add(xmlElem6);

        xmlElem7 := XmlElement.Create('LclInstrm', ns);
        xmlElem6.Add(xmlElem7);

        xmlElem8 := XmlElement.Create('Prtry', ns);
        xmlElem8.Add('CH01');
        xmlElem7.Add(xmlElem8);

        xmlElem9 := XmlElement.Create('Amt', ns);
        xmlElem2.Add(xmlElem9);

        xmlElem10 := XmlElement.Create('InstdAmt', ns);
        xmlElem10.Add(FormXMLDecimal(Amount, 20, 2, TRUE));
        xmlElem10.SetAttribute('Ccy', CurrCode);
        xmlElem9.Add(xmlElem10);

        xmlElem11 := XmlElement.Create('Cdtr', ns);
        xmlElem2.Add(xmlElem11);

        xmlElem12 := XmlElement.Create('Nm', ns);
        xmlElem12.Add(RemoveBadChar(Vendor.Name));
        xmlElem11.Add(xmlElem12);

        xmlElem13 := XmlElement.Create('CdtrAcct', ns);
        xmlElem2.Add(xmlElem13);


        xmlElem14 := XmlElement.Create('Id', ns);
        xmlElem13.Add(xmlElem14);

        xmlElem15 := XmlElement.Create('Othr', ns);
        xmlElem14.Add(xmlElem15);


        xmlElem16 := XmlElement.Create('Id', ns);
        xmlElem16.Add(BankAccountNo);
        xmlElem15.Add(xmlElem16);


        xmlElem17 := XmlElement.Create('RmtInf', ns);
        xmlElem2.Add(xmlElem17);


        xmlElem18 := XmlElement.Create('Strd', ns);
        xmlElem17.Add(xmlElem18);

        xmlElem19 := XmlElement.Create('CdtrRefInf', ns);
        xmlElem18.Add(xmlElem19);


        xmlElem20 := XmlElement.Create('Ref', ns);
        xmlElem20.Add(RefNo);
        xmlElem19.Add(xmlElem20);



    end;


    procedure AddPostDomesticPaymentToXML(var xmlDoc: XmlDOcument; var root: XmlElement)

    var
        PmtInf: XmlNode;
        CdtTrfTxInf: XmlNode;
        PmtId: XmlNode;
        InstrId: XmlNode;
        EndToEndId: XmlNode;
        PmtTpInf: XmlNode;
        LclInstrm: XmlNode;
        Prtry: XmlNode;
        Amt: XmlNode;
        InstdAmt: XmlNode;
        Cdtr: XmlNode;
        Nm: XmlNode;
        PstlAdr: XmlNode;
        StrtNm: XmlNode;
        BldgNb: XmlNode;
        PstCd: XmlNode;
        TwnNm: XmlNode;
        Ctry: XmlNode;
        CdtrAcct: XmlNode;
        Id: XmlNode;
        Othr: XmlNode;
        IdAccount: XmlNode;
        RmtInf: XmlNode;
        Ustrd: XmlNode;

        CstmrCdtTrfInitn: XmlNode;
        Document: xmlElement;

        xmlElem2: xmlElement;
        xmlElem3: xmlElement;
        xmlElem4: xmlElement;
        xmlElem5: xmlElement;
        xmlElem6: xmlElement;
        xmlElem7: xmlElement;
        xmlElem8: xmlElement;
        xmlElem9: xmlElement;
        xmlElem10: xmlElement;
        xmlElem11: xmlElement;
        xmlElem12: xmlElement;
        xmlElem13: xmlElement;
        xmlElem14: xmlElement;
        xmlElem15: xmlElement;
        xmlElem16: xmlElement;
        xmlElem17: xmlElement;
        xmlElem18: xmlElement;
        xmlElem19: xmlElement;
        xmlElem20: xmlElement;
        xmlElem21: xmlElement;
        xmlElem22: xmlElement;
        xmlElem23: xmlElement;
        xmlElem24: xmlElement;

    begin
        CompanyInfo.GET;

        xmlElem2 := XmlElement.Create('CdtTrfTxInf', ns);
        root.Add(xmlElem2);

        xmlElem3 := XmlElement.Create('PmtId', ns);
        xmlElem2.Add(xmlElem3);


        xmlElem4 := XmlElement.Create('InstrId', ns);
        xmlElem4.Add(vInstrId);
        xmlElem3.Add(xmlElem4);

        xmlElem5 := XmlElement.Create('EndToEndId', ns);
        xmlElem5.Add(vEndToEndId);
        xmlElem3.Add(xmlElem5);

        xmlElem6 := XmlElement.Create('PmtTpInf', ns);
        xmlElem2.Add(xmlElem6);


        xmlElem7 := XmlElement.Create('LclInstrm', ns);
        xmlElem6.Add(xmlElem7);


        xmlElem8 := XmlElement.Create('Prtry', ns);
        xmlElem8.Add('CH02');
        xmlElem7.Add(xmlElem8);


        xmlElem9 := XmlElement.Create('Amt', ns);
        xmlElem2.Add(xmlElem9);

        xmlElem10 := XmlElement.Create('InstdAmt', ns);
        xmlElem10.Add(FormXMLDecimal(Amount, 20, 2, TRUE));
        xmlElem10.SetAttribute('Ccy', CurrCode);
        xmlElem9.Add(xmlElem10);

        xmlElem11 := XmlElement.Create('Cdtr', ns);
        xmlElem2.Add(xmlElem11);

        xmlElem12 := XmlElement.Create('Nm', ns);
        xmlElem12.Add(RemoveBadChar(Vendor.Name));
        xmlElem11.Add(xmlElem12);

        xmlElem13 := XmlElement.Create('PstlAdr', ns);
        xmlElem11.Add(xmlElem13);

        IF StreetName <> '' THEN BEGIN
            xmlElem14 := XmlElement.Create('StrtNm', ns);
            xmlElem14.Add(RemoveBadChar(StreetName));
            xmlElem13.Add(xmlElem14);
        END;

        IF StreetNumber <> '' THEN BEGIN
            xmlElem15 := XmlElement.Create('BldgNb', ns);
            xmlElem15.Add(RemoveBadChar(StreetNumber));
            xmlElem13.Add(xmlElem15);
        END;

        IF Vendor."Post Code" <> '' THEN BEGIN
            xmlElem16 := XmlElement.Create('PstCd', ns);
            xmlElem16.Add(Vendor."Post Code");
            xmlElem13.Add(xmlElem16);
        END;

        IF Vendor.City <> '' THEN BEGIN
            xmlElem17 := XmlElement.Create('TwnNm', ns);
            xmlElem17.Add(RemoveBadChar(Vendor.City));
            xmlElem13.Add(xmlElem17);
        END;

        xmlElem18 := XmlElement.Create('Ctry', ns);
        xmlElem18.Add(CountryCode);
        xmlElem13.Add(xmlElem18);

        xmlElem19 := XmlElement.Create('CdtrAcct', ns);
        xmlElem2.Add(xmlElem19);

        xmlElem20 := XmlElement.Create('Id', ns);
        xmlElem19.Add(xmlElem20);

        xmlElem21 := XmlElement.Create('Othr', ns);
        xmlElem20.Add(xmlElem21);

        xmlElem22 := XmlElement.Create('Id', ns);
        xmlElem22.Add(BankAccountNo);
        xmlElem21.Add(xmlElem22);



        IF SummaryPmtTxt <> '' THEN BEGIN
            xmlElem23 := XmlElement.Create('RmtInf', ns);
            xmlElem2.Add(xmlElem23);


            xmlElem24 := XmlElement.Create('Ustrd', ns);
            xmlElem24.Add(SummaryPmtTxt);
            xmlElem23.Add(xmlElem24);
        END;

    end;

    procedure AddBankDomesticPaymentToXML(var xmlDoc: XmlDOcument; var root: XmlElement)
    var
        PmtInf: XmlNode;
        CdtTrfTxInf: XmlNode;
        PmtId: XmlNode;
        InstrId: XmlNode;
        EndToEndId: XmlNode;
        PmtTpInf: XmlNode;
        LclInstrm: XmlNode;
        Prtry: XmlNode;
        Amt: XmlNode;
        InstdAmt: XmlNode;
        Cdtr: XmlNode;
        Nm: XmlNode;
        PstlAdr: XmlNode;
        StrtNm: XmlNode;
        BldgNb: XmlNode;
        PstCd: XmlNode;
        TwnNm: XmlNode;
        Ctry: XmlNode;
        CdtrAcct: XmlNode;
        Id: XmlNode;
        Othr: XmlNode;
        IdAccount: XmlNode;
        RmtInf: XmlNode;
        Ustrd: XmlNode;
        CdtrAgt: XmlNode;
        FinInstnId: XmlNode;
        ClrSysMmbId: XmlNode;
        ClrSysId: XmlNode;
        Cd: XmlNode;
        MmbId: XmlNode;
        AgtNm: XmlNode;
        AgtPstlAdr: XmlNode;
        aIBAN: XmlNode;


        CstmrCdtTrfInitn: XmlNode;
        Document: xmlElement;

        xmlElem2: xmlElement;
        xmlElem3: xmlElement;
        xmlElem4: xmlElement;
        xmlElem5: xmlElement;
        xmlElem6: xmlElement;
        xmlElem7: xmlElement;
        xmlElem8: xmlElement;
        xmlElem9: xmlElement;
        xmlElem10: xmlElement;
        xmlElem11: xmlElement;
        xmlElem12: xmlElement;
        xmlElem13: xmlElement;
        xmlElem14: xmlElement;
        xmlElem15: xmlElement;
        xmlElem16: xmlElement;
        xmlElem17: xmlElement;
        xmlElem18: xmlElement;
        xmlElem19: xmlElement;
        xmlElem20: xmlElement;
        xmlElem21: xmlElement;
        xmlElem22: xmlElement;
        xmlElem23: xmlElement;
        xmlElem24: xmlElement;
        xmlElem25: xmlElement;
        xmlElem26: xmlElement;
        xmlElem27: xmlElement;
        xmlElem28: xmlElement;
        xmlElem29: xmlElement;
        xmlElem30: xmlElement;
        xmlElem31: xmlElement;
        xmlElem32: xmlElement;
        xmlElem33: xmlElement;
        xmlElem34: xmlElement;
        xmlElem35: xmlElement;
        xmlElem36: xmlElement;
        xmlElem37: xmlElement;
        xmlElem38: xmlElement;
    begin

        CompanyInfo.GET;
        xmlElem2 := XmlElement.Create('CdtTrfTxInf', ns);
        root.Add(xmlElem2);

        xmlElem3 := XmlElement.Create('PmtId', ns);
        xmlElem2.Add(xmlElem3);


        xmlElem4 := XmlElement.Create('InstrId', ns);
        xmlElem4.Add(vInstrId);
        xmlElem3.Add(xmlElem4);

        xmlElem5 := XmlElement.Create('EndToEndId', ns);
        xmlElem5.Add(vEndToEndId);
        xmlElem3.Add(xmlElem5);



        IF LocalInstrument THEN BEGIN
            xmlElem6 := XmlElement.Create('PmtTpInf', ns);
            xmlElem2.Add(xmlElem6);

            xmlElem7 := XmlElement.Create('LclInstrm', ns);
            xmlElem6.Add(xmlElem7);

            xmlElem8 := XmlElement.Create('Prtry', ns);
            xmlElem8.Add('CH03');
            xmlElem7.Add(xmlElem8);
        END;

        xmlElem9 := XmlElement.Create('Amt', ns);
        xmlElem2.Add(xmlElem9);

        xmlElem10 := XmlElement.Create('InstdAmt', ns);
        xmlElem10.Add(FormXMLDecimal(Amount, 20, 2, TRUE));
        xmlElem10.SetAttribute('Ccy', CurrCode);
        xmlElem9.Add(xmlElem10);

        xmlElem11 := XmlElement.Create('CdtrAgt', ns);
        xmlElem2.Add(xmlElem11);


        xmlElem12 := XmlElement.Create('FinInstnId', ns);
        xmlElem11.Add(xmlElem12);


        xmlElem13 := XmlElement.Create('ClrSysMmbId', ns);
        xmlElem12.Add(xmlElem13);

        xmlElem14 := XmlElement.Create('ClrSysId', ns);
        xmlElem13.Add(xmlElem14);


        xmlElem15 := XmlElement.Create('Cd', ns);
        xmlElem15.Add('CHBCC');
        xmlElem14.Add(xmlElem15);

        xmlElem16 := XmlElement.Create('MmbId', ns);
        xmlElem16.Add(ClearingNo);
        xmlElem13.Add(xmlElem16);


        IF NOT LocalInstrument THEN BEGIN

            xmlElem17 := XmlElement.Create('Nm', ns);
            xmlElem17.Add(RemoveBadChar(VendorBank.Name));
            xmlElem12.Add(xmlElem17);

            xmlElem18 := XmlElement.Create('PstlAdr', ns);
            xmlElem12.Add(xmlElem18);


            IF CreditAgentStreetName <> '' THEN BEGIN
                xmlElem19 := XmlElement.Create('StrtNm', ns);
                xmlElem19.Add(RemoveBadChar(CreditAgentStreetName));
                xmlElem18.Add(xmlElem19);

            end;

            IF CreditAgentStreetNumber <> '' THEN BEGIN
                xmlElem20 := XmlElement.Create('BldgNb', ns);
                xmlElem20.Add(RemoveBadChar(CreditAgentStreetNumber));
                xmlElem18.Add(xmlElem20);
            END;

            IF VendorBank."Post Code" <> '' THEN BEGIN
                xmlElem21 := XmlElement.Create('PstCd', ns);
                xmlElem21.Add(Vendor."Post Code");
                xmlElem18.Add(xmlElem21);
            END;

            IF VendorBank.City <> '' THEN begin
                xmlElem22 := XmlElement.Create('TwnNm', ns);
                xmlElem22.Add(RemoveBadChar(Vendor.City));
                xmlElem18.Add(xmlElem22);
            end;
            xmlElem23 := XmlElement.Create('Ctry', ns);
            xmlElem23.Add(CreditAgentCountryCode);
            xmlElem18.Add(xmlElem23);
        END;

        xmlElem24 := XmlElement.Create('Cdtr', ns);
        xmlElem2.Add(xmlElem24);

        xmlElem25 := XmlElement.Create('Nm', ns);
        xmlElem25.Add(RemoveBadChar(Vendor.Name));
        xmlElem24.Add(xmlElem25);

        xmlElem26 := XmlElement.Create('PstlAdr', ns);
        xmlElem24.Add(xmlElem26);


        IF StreetName <> '' THEN begin
            xmlElem27 := XmlElement.Create('StrtNm', ns);
            xmlElem27.Add(RemoveBadChar(StreetName));
            xmlElem26.Add(xmlElem27);
        end;
        IF StreetNumber <> '' THEN begin
            xmlElem28 := XmlElement.Create('BldgNb', ns);
            xmlElem28.Add(RemoveBadChar(StreetNumber));
            xmlElem26.Add(xmlElem28);
        end;
        IF Vendor."Post Code" <> '' THEN begin
            xmlElem29 := XmlElement.Create('PstCd', ns);
            xmlElem29.Add(Vendor."Post Code");
            xmlElem26.Add(xmlElem29);
        end;
        IF Vendor.City <> '' THEN begin
            xmlElem30 := XmlElement.Create('TwnNm', ns);
            xmlElem30.Add(RemoveBadChar(Vendor.City));
            xmlElem26.Add(xmlElem30);
        end;

        xmlElem31 := XmlElement.Create('Ctry', ns);
        xmlElem31.Add(CountryCode);
        xmlElem26.Add(xmlElem31);


        xmlElem32 := XmlElement.Create('CdtrAcct', ns);
        xmlElem2.Add(xmlElem32);

        xmlElem33 := XmlElement.Create('Id', ns);
        xmlElem32.Add(xmlElem33);

        IF IBAN <> '' THEN BEGIN

            xmlElem34 := XmlElement.Create('IBAN', ns);
            xmlElem34.Add(IBAN);
            xmlElem33.Add(xmlElem34);

        END ELSE BEGIN
            xmlElem35 := XmlElement.Create('Othr', ns);
            xmlElem33.Add(xmlElem35);

            xmlElem36 := XmlElement.Create('Id', ns);
            xmlElem36.Add(BankAccountNo);
            xmlElem35.Add(xmlElem36);

        END;

        IF SummaryPmtTxt <> '' THEN BEGIN
            xmlElem37 := XmlElement.Create('RmtInf', ns);
            xmlElem2.Add(xmlElem37);


            xmlElem38 := XmlElement.Create('Ustrd', ns);
            xmlElem38.Add(SummaryPmtTxt);
            xmlElem37.Add(xmlElem38);
        END;

    end;

    procedure AddBankAbroadPaymentToXML(var xmlDoc: XmlDOcument; var root: XmlElement)
    var
        PmtInf: XmlNode;
        CdtTrfTxInf: XmlNode;
        PmtId: XmlNode;
        InstrId: XmlNode;
        EndToEndId: XmlNode;
        PmtTpInf: XmlNode;
        LclInstrm: XmlNode;
        Prtry: XmlNode;
        Amt: XmlNode;
        InstdAmt: XmlNode;
        Cdtr: XmlNode;
        Nm: XmlNode;
        PstlAdr: XmlNode;
        StrtNm: XmlNode;
        BldgNb: XmlNode;
        PstCd: XmlNode;
        TwnNm: XmlNode;
        Ctry: XmlNode;
        CdtrAcct: XmlNode;
        Id: XmlNode;
        Othr: XmlNode;
        IdAccount: XmlNode;
        RmtInf: XmlNode;
        Ustrd: XmlNode;
        CdtrAgt: XmlNode;
        FinInstnId: XmlNode;
        ClrSysMmbId: XmlNode;
        ClrSysId: XmlNode;
        Cd: XmlNode;
        MmbId: XmlNode;
        AgtNm: XmlNode;
        AgtPstlAdr: XmlNode;
        aIBAN: XmlNode;
        AdrLine: XmlNode;
        ChrgBr: XmlNode;


        CstmrCdtTrfInitn: XmlNode;
        Document: xmlElement;

        xmlElem2: xmlElement;
        xmlElem3: xmlElement;
        xmlElem4: xmlElement;
        xmlElem5: xmlElement;
        xmlElem6: xmlElement;
        xmlElem7: xmlElement;
        xmlElem8: xmlElement;
        xmlElem9: xmlElement;
        xmlElem10: xmlElement;
        xmlElem11: xmlElement;
        xmlElem12: xmlElement;
        xmlElem13: xmlElement;
        xmlElem14: xmlElement;
        xmlElem15: xmlElement;
        xmlElem16: xmlElement;
        xmlElem17: xmlElement;
        xmlElem18: xmlElement;
        xmlElem19: xmlElement;
        xmlElem20: xmlElement;
        xmlElem21: xmlElement;
        xmlElem22: xmlElement;
        xmlElem23: xmlElement;
        xmlElem24: xmlElement;
        xmlElem25: xmlElement;
        xmlElem26: xmlElement;
        xmlElem27: xmlElement;
        xmlElem28: xmlElement;
        xmlElem29: xmlElement;
        xmlElem30: xmlElement;
        xmlElem31: xmlElement;
        xmlElem32: xmlElement;
        xmlElem33: xmlElement;
        xmlElem34: xmlElement;
        xmlElem35: xmlElement;
        xmlElem36: xmlElement;
        xmlElem37: xmlElement;
        xmlElem38: xmlElement;

    begin
        CompanyInfo.GET;
        xmlElem2 := XmlElement.Create('CdtTrfTxInf', ns);
        root.Add(xmlElem2);

        xmlElem3 := XmlElement.Create('PmtId', ns);
        xmlElem2.Add(xmlElem3);


        xmlElem4 := XmlElement.Create('InstrId', ns);
        xmlElem4.Add(vInstrId);
        xmlElem3.Add(xmlElem4);

        xmlElem5 := XmlElement.Create('EndToEndId', ns);
        xmlElem5.Add(vEndToEndId);
        xmlElem3.Add(xmlElem5);

        xmlElem6 := XmlElement.Create('Amt', ns);
        xmlElem2.Add(xmlElem6);

        xmlElem7 := XmlElement.Create('InstdAmt', ns);
        xmlElem7.Add(FormXMLDecimal(Amount, 20, 2, TRUE));
        xmlElem7.SetAttribute('Ccy', CurrCode);
        xmlElem6.Add(xmlElem7);

        xmlElem8 := XmlElement.Create('ChrgBr', ns);
        xmlElem8.Add('SHAR');
        xmlElem2.Add(xmlElem8);

        xmlElem9 := XmlElement.Create('CdtrAgt', ns);
        xmlElem2.Add(xmlElem9);

        xmlElem10 := XmlElement.Create('FinInstnId', ns);
        xmlElem9.Add(xmlElem10);

        xmlElem11 := XmlElement.Create('Nm', ns);
        xmlElem11.Add(RemoveBadChar(VendorBank.Name));
        xmlElem10.Add(xmlElem11);


        xmlElem12 := XmlElement.Create('PstlAdr', ns);
        xmlElem10.Add(xmlElem12);

        xmlElem13 := XmlElement.Create('Ctry', ns);
        xmlElem13.Add(CreditAgentCountryCode);
        xmlElem12.Add(xmlElem13);


        IF BankAddressLine1 <> '' THEN begin
            xmlElem14 := XmlElement.Create('AdrLine', ns);
            xmlElem14.Add(RemoveBadChar(BankAddressLine1));
            xmlElem12.Add(xmlElem14);
        end;
        IF BankAddressLine2 <> '' THEN begin
            xmlElem15 := XmlElement.Create('AdrLine', ns);
            xmlElem15.Add(RemoveBadChar(BankAddressLine2));
            xmlElem12.Add(xmlElem15);
        end;

        xmlElem16 := XmlElement.Create('Cdtr', ns);
        xmlElem2.Add(xmlElem16);

        xmlElem17 := XmlElement.Create('Nm', ns);
        xmlElem17.Add(RemoveBadChar(Vendor.Name));
        xmlElem16.Add(xmlElem17);

        xmlElem18 := XmlElement.Create('PstlAdr', ns);
        xmlElem16.Add(xmlElem18);

        xmlElem19 := XmlElement.Create('Ctry', ns);
        xmlElem18.Add(xmlElem19);



        IF AddressLine1 <> '' THEN begin
            xmlElem19 := XmlElement.Create('AdrLine', ns);
            xmlElem19.Add(RemoveBadChar(AddressLine1));
            xmlElem18.Add(xmlElem19);
        end;
        IF AddressLine2 <> '' THEN begin
            xmlElem20 := XmlElement.Create('AdrLine', ns);
            xmlElem20.Add(RemoveBadChar(AddressLine2));
            xmlElem18.Add(xmlElem20);
        end;

        xmlElem21 := XmlElement.Create('CdtrAcct', ns);
        xmlElem2.Add(xmlElem21);

        xmlElem22 := XmlElement.Create('Id', ns);
        xmlElem21.Add(xmlElem22);


        IF IBAN <> '' THEN BEGIN
            xmlElem23 := XmlElement.Create('IBAN', ns);
            xmlElem23.Add(IBAN);
            xmlElem22.Add(xmlElem23);
        END ELSE BEGIN
            xmlElem24 := XmlElement.Create('Othr', ns);
            xmlElem22.Add(xmlElem24);

            xmlElem25 := XmlElement.Create('Id', ns);
            xmlElem25.Add(BankAccountNo);
            xmlElem24.Add(xmlElem25);

        END;
        IF SummaryPmtTxt <> '' THEN BEGIN
            xmlElem26 := XmlElement.Create('RmtInf', ns);
            xmlElem2.Add(xmlElem26);


            xmlElem27 := XmlElement.Create('Ustrd', ns);
            xmlElem27.Add(SummaryPmtTxt);
            xmlElem26.Add(xmlElem27);
        END;


    end;


    procedure AddSwiftPaymentToXML(var xmlDoc: XmlDOcument; var root: XmlElement)
    var
        PmtInf: XmlNode;
        CdtTrfTxInf: XmlNode;
        PmtId: XmlNode;
        InstrId: XmlNode;
        EndToEndId: XmlNode;
        PmtTpInf: XmlNode;
        LclInstrm: XmlNode;
        Prtry: XmlNode;
        Amt: XmlNode;
        InstdAmt: XmlNode;
        Cdtr: XmlNode;
        Nm: XmlNode;
        PstlAdr: XmlNode;
        StrtNm: XmlNode;
        BldgNb: XmlNode;
        PstCd: XmlNode;
        TwnNm: XmlNode;
        Ctry: XmlNode;
        CdtrAcct: XmlNode;
        Id: XmlNode;
        Othr: XmlNode;
        IdAccount: XmlNode;
        RmtInf: XmlNode;
        Ustrd: XmlNode;
        CdtrAgt: XmlNode;
        FinInstnId: XmlNode;
        ClrSysMmbId: XmlNode;
        ClrSysId: XmlNode;
        Cd: XmlNode;
        MmbId: XmlNode;
        AgtNm: XmlNode;
        AgtPstlAdr: XmlNode;
        aIBAN: XmlNode;
        AdrLine: XmlNode;
        ChrgBr: XmlNode;
        BIC: XmlNode;

        CstmrCdtTrfInitn: XmlNode;
        Document: xmlElement;

        xmlElem2: xmlElement;
        xmlElem3: xmlElement;
        xmlElem4: xmlElement;
        xmlElem5: xmlElement;
        xmlElem6: xmlElement;
        xmlElem7: xmlElement;
        xmlElem8: xmlElement;
        xmlElem9: xmlElement;
        xmlElem10: xmlElement;
        xmlElem11: xmlElement;
        xmlElem12: xmlElement;
        xmlElem13: xmlElement;
        xmlElem14: xmlElement;
        xmlElem15: xmlElement;
        xmlElem16: xmlElement;
        xmlElem17: xmlElement;
        xmlElem18: xmlElement;
        xmlElem19: xmlElement;
        xmlElem20: xmlElement;
        xmlElem21: xmlElement;
        xmlElem22: xmlElement;
        xmlElem23: xmlElement;
        xmlElem24: xmlElement;
        xmlElem25: xmlElement;
        xmlElem26: xmlElement;
        xmlElem27: xmlElement;
        xmlElem28: xmlElement;
        xmlElem29: xmlElement;
        xmlElem30: xmlElement;
        xmlElem31: xmlElement;
        xmlElem32: xmlElement;
        xmlElem33: xmlElement;
        xmlElem34: xmlElement;
        xmlElem35: xmlElement;
        xmlElem36: xmlElement;
        xmlElem37: xmlElement;
        xmlElem38: xmlElement;

    begin
        CompanyInfo.GET;
        xmlElem2 := XmlElement.Create('CdtTrfTxInf', ns);
        root.Add(xmlElem2);

        xmlElem3 := XmlElement.Create('PmtId', ns);
        xmlElem2.Add(xmlElem3);


        xmlElem4 := XmlElement.Create('InstrId', ns);
        xmlElem4.Add(vInstrId);
        xmlElem3.Add(xmlElem4);

        xmlElem5 := XmlElement.Create('EndToEndId', ns);
        xmlElem5.Add(vEndToEndId);
        xmlElem3.Add(xmlElem5);

        xmlElem6 := XmlElement.Create('Amt', ns);
        xmlElem2.Add(xmlElem6);

        xmlElem7 := XmlElement.Create('InstdAmt', ns);
        xmlElem7.Add(FormXMLDecimal(Amount, 20, 2, TRUE));
        xmlElem7.SetAttribute('Ccy', CurrCode);
        xmlElem6.Add(xmlElem7);


        //SEPA or NonSEPA
        IF SEPA THEN BEGIN
            xmlElem8 := XmlElement.Create('ChrgBr', ns);
            xmlElem8.Add('SLEV');
            xmlElem2.Add(xmlElem8);
        END
        ELSE BEGIN
            xmlElem9 := XmlElement.Create('ChrgBr', ns);
            xmlElem9.Add('SHAR');
            xmlElem2.Add(xmlElem9);
        END;

        IF NOT SEPA THEN BEGIN
            xmlElem10 := XmlElement.Create('CdtrAgt', ns);
            xmlElem2.Add(xmlElem10);

            xmlElem11 := XmlElement.Create('FinInstnId', ns);
            xmlElem10.Add(xmlElem11);

            xmlElem12 := XmlElement.Create('BIC', ns);
            xmlElem12.Add(SwiftCode);
            xmlElem11.Add(xmlElem12);
        END;

        xmlElem13 := XmlElement.Create('Cdtr', ns);
        xmlElem2.Add(xmlElem13);

        xmlElem14 := XmlElement.Create('Nm', ns);
        xmlElem14.Add(RemoveBadChar(Vendor.Name));
        xmlElem13.Add(xmlElem14);

        xmlElem15 := XmlElement.Create('PstlAdr', ns);
        xmlElem13.Add(xmlElem15);

        xmlElem16 := XmlElement.Create('Ctry', ns);
        xmlElem16.Add(CountryCode);
        xmlElem15.Add(xmlElem16);


        IF AddressLine1 <> '' THEN begin
            xmlElem17 := XmlElement.Create('AdrLine', ns);
            xmlElem17.Add(RemoveBadChar(AddressLine1));
            xmlElem15.Add(xmlElem17);
        end;
        IF AddressLine2 <> '' THEN begin
            xmlElem18 := XmlElement.Create('AdrLine', ns);
            xmlElem18.Add(RemoveBadChar(AddressLine2));
            xmlElem15.Add(xmlElem18);
        end;

        xmlElem19 := XmlElement.Create('CdtrAcct', ns);
        xmlElem2.Add(xmlElem19);

        xmlElem20 := XmlElement.Create('Id', ns);
        xmlElem19.Add(xmlElem20);



        IF IBAN <> '' THEN BEGIN
            xmlElem21 := XmlElement.Create('IBAN', ns);
            xmlElem21.Add(IBAN);
            xmlElem20.Add(xmlElem21);
        END ELSE BEGIN
            xmlElem22 := XmlElement.Create('Othr', ns);
            xmlElem20.Add(xmlElem22);

            xmlElem23 := XmlElement.Create('Id', ns);
            xmlElem23.Add(BankAccountNo);
            xmlElem22.Add(xmlElem23);

        END;

        IF SummaryPmtTxt <> '' THEN BEGIN
            xmlElem24 := XmlElement.Create('RmtInf', ns);
            xmlElem2.Add(xmlElem24);


            xmlElem25 := XmlElement.Create('Ustrd', ns);
            xmlElem25.Add(SummaryPmtTxt);
            xmlElem24.Add(xmlElem25);
        END;

    End;

    procedure AddBankQRRPaymentToXML(var xmlDoc: XmlDOcument; var root: XmlElement)
    var
        PmtInf: XmlNode;
        CdtTrfTxInf: XmlNode;
        PmtId: XmlNode;
        InstrId: XmlNode;
        EndToEndId: XmlNode;
        PmtTpInf: XmlNode;
        LclInstrm: XmlNode;
        Prtry: XmlNode;
        Amt: XmlNode;
        InstdAmt: XmlNode;
        Cdtr: XmlNode;
        Nm: XmlNode;
        PstlAdr: XmlNode;
        StrtNm: XmlNode;
        BldgNb: XmlNode;
        PstCd: XmlNode;
        TwnNm: XmlNode;
        Ctry: XmlNode;
        CdtrAcct: XmlNode;
        Id: XmlNode;
        Othr: XmlNode;
        IdAccount: XmlNode;
        RmtInf: XmlNode;
        Ustrd: XmlNode;
        CdtrAgt: XmlNode;
        FinInstnId: XmlNode;
        ClrSysMmbId: XmlNode;
        ClrSysId: XmlNode;
        Cd: XmlNode;
        MmbId: XmlNode;
        AgtNm: XmlNode;
        AgtPstlAdr: XmlNode;
        aIBAN: XmlNode;
        AdrLine: XmlNode;
        ChrgBr: XmlNode;
        BIC: XmlNode;
        Strd: XmlNode;
        CdtrRefInf: XmlNode;
        Tp: XmlNode;
        CdOrPrtry: XmlNode;
        Ref: XmlNode;

        CstmrCdtTrfInitn: XmlNode;
        Document: xmlElement;

        xmlElem2: xmlElement;
        xmlElem3: xmlElement;
        xmlElem4: xmlElement;
        xmlElem5: xmlElement;
        xmlElem6: xmlElement;
        xmlElem7: xmlElement;
        xmlElem8: xmlElement;
        xmlElem9: xmlElement;
        xmlElem10: xmlElement;
        xmlElem11: xmlElement;
        xmlElem12: xmlElement;
        xmlElem13: xmlElement;
        xmlElem14: xmlElement;
        xmlElem15: xmlElement;
        xmlElem16: xmlElement;
        xmlElem17: xmlElement;
        xmlElem18: xmlElement;
        xmlElem19: xmlElement;
        xmlElem20: xmlElement;
        xmlElem21: xmlElement;
        xmlElem22: xmlElement;
        xmlElem23: xmlElement;
        xmlElem24: xmlElement;
        xmlElem25: xmlElement;
        xmlElem26: xmlElement;
        xmlElem27: xmlElement;
        xmlElem28: xmlElement;
        xmlElem29: xmlElement;
        xmlElem30: xmlElement;
        xmlElem31: xmlElement;
        xmlElem32: xmlElement;
        xmlElem33: xmlElement;
        xmlElem34: xmlElement;
        xmlElem35: xmlElement;
        xmlElem36: xmlElement;
        xmlElem37: xmlElement;
        xmlElem38: xmlElement;
        xmlElem39: xmlElement;
        xmlElem40: xmlElement;
    begin
        //ISO20022QR
        CompanyInfo.GET;

        xmlElem2 := XmlElement.Create('CdtTrfTxInf', ns);
        root.Add(xmlElem2);

        xmlElem3 := XmlElement.Create('PmtId', ns);
        xmlElem2.Add(xmlElem3);


        xmlElem4 := XmlElement.Create('InstrId', ns);
        xmlElem4.Add(vInstrId);
        xmlElem3.Add(xmlElem4);

        xmlElem5 := XmlElement.Create('EndToEndId', ns);
        xmlElem5.Add(vEndToEndId);
        xmlElem3.Add(xmlElem5);

        xmlElem6 := XmlElement.Create('Amt', ns);
        xmlElem2.Add(xmlElem6);

        xmlElem7 := XmlElement.Create('InstdAmt', ns);
        xmlElem7.Add(FormXMLDecimal(Amount, 20, 2, TRUE));
        xmlElem7.SetAttribute('Ccy', CurrCode);
        xmlElem6.Add(xmlElem7);


        IF NOT LocalInstrument THEN BEGIN/*
            xmlElem8 := XmlElement.Create('Nm');
            xmlElem8.Add(RemoveBadChar(VendorBank.Name));
            xmlElem12.Add(xmlElem17);

            xmlElem18 := XmlElement.Create('PstlAdr');
            xmlElem12.Add(xmlElem18);

            AddNode(FinInstnId, 'Nm', RemoveBadChar(VendorBank.Name), AgtNm);
            AddNode(FinInstnId, 'PstlAdr', '', AgtPstlAdr);


            IF CreditAgentStreetName <> '' THEN
                AddNode(AgtPstlAdr, 'StrtNm', RemoveBadChar(CreditAgentStreetName), StrtNm);
            IF CreditAgentStreetNumber <> '' THEN
                AddNode(AgtPstlAdr, 'BldgNb', RemoveBadChar(CreditAgentStreetNumber), BldgNb);
            IF VendorBank."Post Code" <> '' THEN
                AddNode(AgtPstlAdr, 'PstCd', VendorBank."Post Code", PstCd);
            IF VendorBank.City <> '' THEN
                AddNode(AgtPstlAdr, 'TwnNm', RemoveBadChar(VendorBank.City), TwnNm);
            AddNode(AgtPstlAdr, 'Ctry', CreditAgentCountryCode, Ctry);*/
        END;

        xmlElem20 := XmlElement.Create('Cdtr', ns);
        xmlElem2.Add(xmlElem20);

        xmlElem21 := XmlElement.Create('Nm', ns);
        xmlElem21.Add(RemoveBadChar(Vendor.Name));
        xmlElem20.Add(xmlElem21);

        xmlElem22 := XmlElement.Create('PstlAdr', ns);
        xmlElem20.Add(xmlElem22);


        IF StreetName <> '' THEN begin
            xmlElem23 := XmlElement.Create('StrtNm', ns);
            xmlElem23.Add(RemoveBadChar(StreetName));
            xmlElem22.Add(xmlElem23);
        end;
        IF StreetNumber <> '' THEN begin
            xmlElem24 := XmlElement.Create('BldgNb', ns);
            xmlElem24.Add(RemoveBadChar(StreetNumber));
            xmlElem22.Add(xmlElem24);
        end;
        IF Vendor."Post Code" <> '' THEN begin
            xmlElem25 := XmlElement.Create('PstCd', ns);
            xmlElem25.Add(Vendor."Post Code");
            xmlElem22.Add(xmlElem25);
        end;
        IF Vendor.City <> '' THEN begin
            xmlElem26 := XmlElement.Create('TwnNm', ns);
            xmlElem26.Add(RemoveBadChar(Vendor.City));
            xmlElem22.Add(xmlElem26);
        end;

        xmlElem27 := XmlElement.Create('Ctry', ns);
        xmlElem27.Add(CountryCode);
        xmlElem22.Add(xmlElem27);

        xmlElem28 := XmlElement.Create('CdtrAcct', ns);
        xmlElem2.Add(xmlElem28);

        xmlElem29 := XmlElement.Create('Id', ns);
        xmlElem28.Add(xmlElem29);

        xmlElem30 := XmlElement.Create('IBAN', ns);
        xmlElem30.Add(IBAN);
        xmlElem29.Add(xmlElem30);


        IF RefNo <> '' THEN BEGIN

            xmlElem31 := XmlElement.Create('RmtInf', ns);
            xmlElem2.Add(xmlElem31);

            xmlElem32 := XmlElement.Create('Strd', ns);
            xmlElem31.Add(xmlElem32);

            xmlElem33 := XmlElement.Create('CdtrRefInf', ns);
            xmlElem32.Add(xmlElem33);

            xmlElem34 := XmlElement.Create('Tp', ns);
            xmlElem33.Add(xmlElem34);


            xmlElem35 := XmlElement.Create('CdOrPrtry', ns);
            xmlElem34.Add(xmlElem35);


            IF COPYSTR(RefNo, 1, 2) = 'RF' THEN BEGIN
                xmlElem36 := XmlElement.Create('Cd', ns);
                xmlElem36.Add('SCOR');
                xmlElem35.Add(xmlElem36);
            END ELSE BEGIN
                xmlElem37 := XmlElement.Create('Prtry', ns);
                xmlElem37.Add('QRR');
                xmlElem35.Add(xmlElem37);
            END;
            xmlElem38 := XmlElement.Create('Ref', ns);
            xmlElem38.Add(RefNo);
            xmlElem33.Add(xmlElem38);
        END ELSE BEGIN
            IF SummaryPmtTxt <> '' THEN BEGIN
                xmlElem39 := XmlElement.Create('RmtInf', ns);
                xmlElem2.Add(xmlElem39);


                xmlElem40 := XmlElement.Create('Ustrd', ns);
                xmlElem40.Add(SummaryPmtTxt);
                xmlElem39.Add(xmlElem40);
            END;
        END;



        //ISO20022QR/END
    end;

    procedure ModifyXMLHeader(VAR xmlDoc: XmlDocument; VAR NoOfTransactions: Integer; VAR TotalAmt: Decimal)
    var
        NbOfTxs: XmlNode;
        CtrlSum: XMlNode;
        Document: XMlNode;
        GrpHdr: XMlNode;
        CstmrCdtTrfInitn: XmlNode;

        xmlList: XmlNodeList;
        xmlList2: XmlNodeList;
    begin


        CompanyInfo.GET;
        xmlList := xmlDoc.GetChildElements();
        xmlList.Get(1, Document);

        xmlList2 := Document.AsXmlElement().GetChildNodes();
        xmlList2.Get(1, CstmrCdtTrfInitn);

        xmlList := CstmrCdtTrfInitn.AsXmlElement().GetChildNodes();
        xmlList.Get(1, GrpHdr);

        xmlList2 := GrpHdr.AsXmlElement().GetChildNodes();
        xmlList2.Get(3, NbOfTxs);
        xmlList2.Get(2, CtrlSum);



        NbOfTxs.AsXmlElement().Add(FORMAT(NoOfTransactions));


    end;

    /// <summary>
    /// We don't use this method fo XML save file
    /// This method is for  OnPrem scope and does not work on SaaS
    /// vukoje Ljubisa 12.09.2021
    /// </summary>
    /// <param name="ActESRSetup"></param>
    /* 
    procedure SaveXMLDoc(VAR xmlDoc: XmlDocument; VAR FileBank: Record "DTA Setup")
    var

        FileName: Text[250];
        BackupFilename: Text[250];
        f: File;
    //FileRec: Record File;
    begin
        IF STRPOS(UPPERCASE(FileBank."ISO Filename"), '.XML') = 0 THEN
            FileName := FileBank."ISO Filename" + '.xml'
        ELSE
            FileName := FileBank."ISO Filename";
        FileName := FileBank."ISO File Folder" + FileName;

        // xmlDoc.save(ClientXMLTempFileName);
        //FileMgt.CopyClientFile(ClientXMLTempFileName, FileName, TRUE);

        // Backup file
        IF FileBank."Backup Copy" THEN BEGIN
            FileBank.LOCKTABLE;
            FileBank."Last Backup No." := INCSTR(FileBank."Last Backup No.");
            FileBank.MODIFY;
            // FileMgt.CopyClientFile(FileName, FileBank."Backup Folder" + 'ISO' + FileBank."Last Backup No." + '.xml', FALSE);
        END;
    end;
 */
    //camt054 esr()

    procedure CheckESRSetup(VAR ActESRSetup: Record "ESR Setup")
    begin
        // Check ESR Setup
        ActESRSetup.TESTFIELD("Bal. Account No.");
        ActESRSetup.TESTFIELD("ESR Filename");
        ActESRSetup.TESTFIELD("BESR Customer ID");
        ActESRSetup.TESTFIELD("ESR Account No.");
        ActESRSetup.TESTFIELD("IBAN Recipient");
        IF ActESRSetup."Backup Copy" THEN BEGIN
            ActESRSetup.TESTFIELD("Backup Folder");
            ActESRSetup.TESTFIELD("Last Backup No.");
        END;

        //IF NOT FileMgt.ClientFileExists(ActESRSetup."ESR Filename") THEN
        //    ERROR(Text125, ActESRSetup."ESR Filename");
    end;


    /// <summary>
    /// Carvolution does not use this for now
    /// IMportin camt054 (ISO 20022 payment from customer)
    /// </summary>
    /// <param name="InvoiceNo"></param>
    /*
    procedure ImportCamt054EsrFile(VAR ActGenJnlLine: Record "Gen. Journal Line")
    // ImportCamp054ESRFile from source to GLL

    // *** Select GL journal. Check if empty

    var

        GenJournalLine: Record "Gen. Journal Line";
        GlBatchName: Record "Gen. Journal Batch";
        CustLedgerEntry: Record "Cust. Ledger Entry";
        GeneralLedgerSetup: Record "General Ledger Setup";
        ESRBank: Record "ESR Setup";
        LSVSetup: Record "LSV Setup";
        BackupFilename: Code[250];
        XMLMainCurrNode: XmlNode;
        XMLNodeB: XmlNode;
        XMLNodeC: XmlNode;
        XMLNodeD: XmlNode;
        XMLNode2: XmlNode;
        XMLNodeListBLevel: XmlNodeList;
        XMLNodeListCLevel: XmlNodeList;
        XMLNodeListDLevel: XmlNodeList;
        XMLParseError: text;
        ESRAccNo: Code[20];
        ESRType: Code[10];
        ESRName: Text[140];
        PaymCharges: Decimal;
        PaymChargesText: Text[30];
        PostDate: Date;
        CurrencyCode: Code[10];
        d: Dialog;
        NoSeriesMgt: Codeunit NoSeriesManagement;
        LsvMgt: Codeunit LSVMgt;
        SaveAmount: Decimal;
        SaveDescription: Text[50];
        AmountC: Decimal;
        AmountD: Decimal;
        TotalAmtCLevel: Decimal;
        TotalAmount: Decimal;
        NoOfPayments: Integer;
        NoOfPaymCLevel: Integer;
        TotalNoOfPaymCLevel: Integer;
        PaymChargesCLevel: Decimal;
        TotalPaymChargesCLevel: Decimal;
        CdtDbtInd: Code[20];
        FirstDate: Date;
        MultiplePostingDates: Boolean;
        LastLineNo: Integer;
        NextDocNo: Code[20];
    begin
        GenJournalLine.SETRANGE("Journal Template Name", ActGenJnlLine."Journal Template Name");
        GenJournalLine.SETRANGE("Journal Batch Name", ActGenJnlLine."Journal Batch Name");
        GenJournalLine.SETFILTER("Account No.", '<>%1', '');
        IF GenJournalLine.FIND('-') THEN
            ERROR(Text105, GenJournalLine."Journal Batch Name");

        // One or multiple ESR banks
        ESRBank.RESET;
        IF ESRBank.COUNT = 1 THEN BEGIN
            ESRBank.FINDFIRST;
            IF NOT CONFIRM(Text101, FALSE) THEN
                EXIT;
        END ELSE BEGIN
            IF NOT CONFIRM(Text102 + Text101, FALSE) THEN
                EXIT;
            IF PAGE.RUNMODAL(3010531, ESRBank) = ACTION::LookupCancel THEN
                ERROR(Text103);
        END;

        CheckESRSetup(ESRBank);

        //IF ISCLEAR(XML054Doc) THEN
        //      CREATE(XML054Doc, FALSE, TRUE);

        XML054Doc := XmlDocument.Create(FALSE, TRUE);

       // XML054Doc.async(FALSE);
       // XML054Doc.validateOnParse(FALSE);
       // XML054Doc.resolveExternals(FALSE);
        IF NOT XmlDocument.load(ESRBank."ESR Filename") THEN
            ERROR(Text126, ESRBank."ESR Filename");

        XMLMainCurrNode := XML054Doc.documentElement;

        //Init. totalisation etc.
        CLEAR(TotalAmount);
        CLEAR(NoOfPayments);
        FirstDate := 0D;
        MultiplePostingDates := FALSE;
        CLEAR(TotalAmtCLevel);
        CLEAR(TotalNoOfPaymCLevel);

        //B-Level
        IF FindNodes(XMLMainCurrNode, '/Document/BkToCstmrDbtCdtNtfctn/Ntfctn', XMLNodeListBLevel) THEN BEGIN
            XMLNodeListBLevel.reset;
            XMLNodeB := XMLNodeListBLevel.nextNode;
            WHILE NOT ISCLEAR(XMLNodeB) DO BEGIN
                FindNode(XMLNodeB, 'Acct/Id/IBAN', XMLNode2);
                IF DELCHR(ESRBank."IBAN Recipient", '=', ' ') <> XMLNode2.text THEN
                    ERROR(Text127, ESRBank.FIELDCAPTION(ESRBank."IBAN Recipient"),
                                   DELCHR(ESRBank."IBAN Recipient", '=', ' '),
                                   ESRBank.TABLECAPTION,
                                   XMLNode2.text);

                //C-Level
                IF FindNodes(XMLNodeB, 'Ntry', XMLNodeListCLevel) THEN BEGIN
                    XMLNodeListCLevel.reset;
                    XMLNodeC := XMLNodeListCLevel.nextNode;
                    d.OPEN(
                      Text106 + // Import ISO ESR file
                      Text107 + // Payments #1
                      Text108); // Total amount #2

                    // Journal name for no serie
                    GlBatchName.GET(ActGenJnlLine."Journal Template Name", ActGenJnlLine."Journal Batch Name");
                    NextDocNo := NoSeriesMgt.GetNextNo(GlBatchName."No. Series", PostDate, FALSE);

                    WHILE NOT ISCLEAR(XMLNodeC) DO BEGIN
                        CLEAR(NoOfPaymCLevel);
                        CLEAR(PaymChargesCLevel);
                        CLEAR(ESRAccNo);
                        //momentan inaktiv da manchmal nicht bekannte Teilnehmernummern(Postcheckkonto) im XML
                        
                        // IF FindNode(XMLNodeC, 'NtryRef', XMLNode2) THEN BEGIN
                        //                     IF XMLNode2.text <> '' THEN BEGIN
                        //                         ESRAccNo := BankMgt.CheckPostAccountNo(ESRBank."ESR Account No.");
                        //                         ESRAccNo := DELCHR(ESRAccNo, '=', '-');
                        //                         IF ESRAccNo <> XMLNode2.text THEN
                        //                             ERROR(Text127, ESRBank.FIELDCAPTION("ESR Account No."),
                        //                                            ESRBank."ESR Account No.",
                        //                                            ESRBank.TABLECAPTION,
                        //                                            ESRBank.FIELDCAPTION("ESR Account No."));
                        //                     END;
                        //                 END;
                        
                        FindNode(XMLNodeC, 'Amt', XMLNode2);
                        AmountC := XMLDecText2NAVDec(XMLNode2.text);
                        TotalAmtCLevel := TotalAmtCLevel + AmountC;
                        IF FindNode(XMLNodeC, 'NtryDtls/Btch/NbOfTxs', XMLNode2) THEN
                            EVALUATE(NoOfPaymCLevel, XMLNode2.text)
                        ELSE
                            NoOfPaymCLevel := 1;

                        TotalNoOfPaymCLevel := TotalNoOfPaymCLevel + NoOfPaymCLevel;

                        FindNode(XMLNodeC, 'BookgDt/Dt', XMLNode2);
                        PostDate := DT_XML2NAVDate(XMLNode2.text);
                        //Multiple Posting Dates
                        IF FirstDate = 0D THEN
                            FirstDate := PostDate;
                        IF FirstDate <> PostDate THEN
                            MultiplePostingDates := TRUE;
                        IF FindNode(XMLNodeC, 'Chrgs/TtlChrgsAndTaxAmt', XMLNode2) THEN
                            PaymChargesCLevel := XMLDecText2NAVDec(XMLNode2.text);
                        TotalPaymChargesCLevel := TotalPaymChargesCLevel + PaymChargesCLevel;

                        //D-Level
                        IF FindNodes(XMLNodeC, 'NtryDtls/TxDtls', XMLNodeListDLevel) THEN BEGIN
                            XMLNodeListDLevel.reset;
                            XMLNodeD := XMLNodeListDLevel.nextNode;
                            WHILE NOT ISCLEAR(XMLNodeD) DO BEGIN
                                CLEAR(InInvoiceNo);
                                CLEAR(CurrencyCode);
                                CLEAR(CdtDbtInd);
                                CLEAR(ESRRefNo);
                                CLEAR(PaymCharges);
                                CLEAR(PaymChargesText);
                                CLEAR(ESRType);
                                CLEAR(ESRName);
                                IF FindNode(XMLNodeD, 'Refs/Prtry/Tp', XMLNode2) THEN
                                    ESRType := XMLNode2.text;
                                IF FindNode(XMLNodeD, 'Chrgs/TtlChrgsAndTaxAmt', XMLNode2) THEN
                                    PaymCharges := XMLDecText2NAVDec(XMLNode2.text);
                                IF FindNode(XMLNodeD, 'RltdPties/Dbtr/Nm', XMLNode2) THEN
                                    ESRName := XMLNode2.text;
                                FindNode(XMLNodeD, 'Amt', XMLNode2);
                                AmountD := XMLDecText2NAVDec(XMLNode2.text);
                                CurrencyCode := GetXMLAttributValue(XMLNode2, 'Ccy');
                                FindNode(XMLNodeD, 'CdtDbtInd', XMLNode2);
                                CdtDbtInd := XMLNode2.text;
                                FindNode(XMLNodeD, 'RmtInf/Strd/CdtrRefInf/Ref', XMLNode2);
                                ESRRefNo := XMLNode2.text;
                                InInvoiceNo := COPYSTR(ESRRefNo, 19, 8);
                                TrimInvoiceNo(InInvoiceNo);

                                //LSV
                                CASE ESRType OF
                                    '50':  // LSV
                                        BEGIN
                                            IF LSVSetup.READPERMISSION THEN
                                                LsvMgt.ClosedByESR(InInvoiceNo);
                                        END;
                                END;

                                // Insert GL line
                                GenJournalLine.INIT;
                                GenJournalLine."Journal Template Name" := ActGenJnlLine."Journal Template Name";
                                GenJournalLine."Journal Batch Name" := ActGenJnlLine."Journal Batch Name";
                                LastLineNo := LastLineNo + 10000;
                                GenJournalLine."Line No." := LastLineNo;
                                GenJournalLine."Document No." := NextDocNo;
                                GenJournalLine."Posting Date" := PostDate;
                                GenJournalLine."Account Type" := GenJournalLine."Account Type"::Customer;
                                GenJournalLine."Document Type" := GenJournalLine."Document Type"::Payment;

                                GeneralLedgerSetup.GET;
                                IF GeneralLedgerSetup."LCY Code" <> CurrencyCode THEN
                                    GenJournalLine."Currency Code" := CurrencyCode;
                                CustLedgerEntry.SETCURRENTKEY("Document No.");
                                CustLedgerEntry.SETRANGE("Document Type", CustLedgerEntry."Document Type"::Invoice);
                                CustLedgerEntry.SETRANGE("Document No.", InInvoiceNo);
                                IF CustLedgerEntry.FINDFIRST THEN BEGIN
                                    GenJournalLine.VALIDATE("Account No.", CustLedgerEntry."Customer No.");
                                    IF GenJournalLine."Currency Code" <> CustLedgerEntry."Currency Code" THEN
                                        GenJournalLine.VALIDATE("Currency Code", CustLedgerEntry."Currency Code");
                                END ELSE BEGIN
                                    GenJournalLine.VALIDATE("Currency Code");
                                END;
                                GenJournalLine."Applies-to Doc. Type" := GenJournalLine."Applies-to Doc. Type"::Invoice;
                                GenJournalLine."Applies-to Doc. No." := InInvoiceNo;

                                // Process transaction of credit record
                                CASE CdtDbtInd OF
                                    'CRDT':
                                        BEGIN
                                            GenJournalLine.Description := Text109 + ' ' + InInvoiceNo;
                                            GenJournalLine.Amount := -AmountD;
                                        END;
                                    'DBIT':
                                        BEGIN
                                            GenJournalLine."Document Type" := GenJournalLine."Document Type"::Invoice;
                                            GenJournalLine."Applies-to Doc. Type" := GenJournalLine."Applies-to Doc. Type"::" ";
                                            GenJournalLine."Applies-to Doc. No." := '';
                                            GenJournalLine.Description := FORMAT(Text110 + ' ' + InInvoiceNo, -MAXSTRLEN(GenJournalLine.Description));
                                            GenJournalLine.Amount := AmountD;
                                        END;
                                END;

                                GenJournalLine."Source Code" := 'ESR';
                                GenJournalLine."Reason Code" := GlBatchName."Reason Code";
                                IF PaymCharges <> 0 THEN BEGIN
                                    PaymChargesText := FORMAT(PaymCharges, 0, '<Integer,2><Filler Character,0><Decimals,3>');
                                    PaymChargesText := DELCHR(PaymChargesText, '=', '.');
                                    PaymChargesText := DELCHR(PaymChargesText, '=', ',');
                                END ELSE BEGIN
                                    PaymChargesText := '0000';
                                END;
                                GenJournalLine."ESR Information" := 'ESR ' + ESRRefNo + '/' + COPYSTR(PaymChargesText, 1, 4) + '/' +
                                                                    FORMAT(PostDate) + '/' + ESRType;
                                GenJournalLine.INSERT;
                                IF GenJournalLine."Account No." = '' THEN BEGIN
                                    IF (ESRName = '') OR (ESRName = 'NOTPROVIDED') THEN BEGIN
                                        GenJournalLine.Description := COPYSTR(STRSUBSTNO(Text111, InInvoiceNo, 'Ref ' + ESRRefNo),
                                                                              1, MAXSTRLEN(GenJournalLine.Description));
                                    END ELSE BEGIN
                                        GenJournalLine.Description := COPYSTR(STRSUBSTNO(Text111, InInvoiceNo, ESRName),
                                                                              1, MAXSTRLEN(GenJournalLine.Description));
                                    END;
                                    GenJournalLine.MODIFY;
                                END;
                                TotalAmount := TotalAmount + AmountD;
                                NoOfPayments := NoOfPayments + 1;
                                d.UPDATE(1, NoOfPayments);
                                d.UPDATE(2, TotalAmount);
                                //D-Level end
                                XMLNodeD := XMLNodeListDLevel.nextNode;
                            END;
                        END;
                        //C-Level end
                        XMLNodeC := XMLNodeListCLevel.nextNode;
                    END;
                END;
                //B-Level end
                XMLNodeB := XMLNodeListBLevel.nextNode;
            END;
        END;

        GenJournalLine.RESET;
        GenJournalLine.SETRANGE("Journal Template Name", ActGenJnlLine."Journal Template Name");
        GenJournalLine.SETRANGE("Journal Batch Name", ActGenJnlLine."Journal Batch Name");
        GenJournalLine.SETRANGE("Source Code", 'ESR');
        IF GenJournalLine.FINDSET THEN
            REPEAT
                SaveAmount := GenJournalLine.Amount;
                GenJournalLine.VALIDATE(Amount, 0);
                GenJournalLine.VALIDATE(Amount, SaveAmount);
                GenJournalLine.MODIFY;
            UNTIL GenJournalLine.NEXT = 0;
        GenJournalLine.SETRANGE("Journal Template Name");
        GenJournalLine.SETRANGE("Journal Batch Name");
        GenJournalLine.SETRANGE("Source Code");
        GenJournalLine.SETRANGE("Journal Template Name", ActGenJnlLine."Journal Template Name");
        GenJournalLine.SETRANGE("Journal Batch Name", ActGenJnlLine."Journal Batch Name");
        GenJournalLine.SETRANGE("Source Code", 'ESR');

        // *** Bal account per line or as combined entry
        IF MultiplePostingDates THEN BEGIN
            // Bal Account per line
            IF GenJournalLine.FIND('-') THEN
                REPEAT
                    CLEAR(SaveDescription);
                    IF GenJournalLine."Account No." = '' THEN
                        SaveDescription := GenJournalLine.Description;
                    IF ESRBank."Bal. Account Type" = ESRBank."Bal. Account Type"::"Bank Account" THEN
                        GenJournalLine."Bal. Account Type" := GenJournalLine."Bal. Account Type"::"Bank Account"
                    ELSE
                        GenJournalLine."Bal. Account Type" := GenJournalLine."Bal. Account Type"::"G/L Account";

                    GenJournalLine.VALIDATE("Bal. Account No.", ESRBank."Bal. Account No.");
                    IF SaveDescription <> '' THEN
                        GenJournalLine.Description := SaveDescription;
                    GenJournalLine.MODIFY;
                UNTIL GenJournalLine.NEXT = 0;
        END ELSE BEGIN
            // Add bal. acc. line at end
            GenJournalLine.INIT;
            GenJournalLine."Journal Template Name" := ActGenJnlLine."Journal Template Name";
            GenJournalLine."Journal Batch Name" := ActGenJnlLine."Journal Batch Name";
            LastLineNo := LastLineNo + 10000;
            GenJournalLine."Line No." := LastLineNo;
            GenJournalLine."Document No." := NextDocNo;
            GenJournalLine."Account Type" := ESRBank."Bal. Account Type";

            GeneralLedgerSetup.GET;
            IF GeneralLedgerSetup."LCY Code" <> CurrencyCode THEN
                GenJournalLine."Currency Code" := CurrencyCode;

            IF ESRBank."Bal. Account Type" = ESRBank."Bal. Account Type"::"Bank Account" THEN
                GenJournalLine."Account Type" := GenJournalLine."Bal. Account Type"::"Bank Account"
            ELSE
                GenJournalLine."Account Type" := GenJournalLine."Bal. Account Type"::"G/L Account";

            GenJournalLine."Posting Date" := FirstDate;
            GenJournalLine."Source Code" := 'ESR';

            GenJournalLine.VALIDATE("Account No.", ESRBank."Bal. Account No.");
            GenJournalLine.Description := Text112 + ' ' + ESRBank."Bank Code";
            GenJournalLine.VALIDATE("Document Type", GenJournalLine."Document Type"::Payment);
            GenJournalLine.VALIDATE(Amount, TotalAmount);
            GenJournalLine.INSERT;
        END;

        d.CLOSE;

        // Save sourcefile
        IF ESRBank."Backup Copy" THEN BEGIN
            ESRBank.LOCKTABLE;
            ESRBank."Last Backup No." := INCSTR(ESRBank."Last Backup No.");
            ESRBank.MODIFY;
            BackupFilename := ESRBank."Backup Folder" + 'ISOESR' + ESRBank."Last Backup No." + '.XML';
            IF FileMgt.ClientFileExists(ESRBank."ESR Filename") AND (NOT FileMgt.ClientFileExists(BackupFilename)) THEN BEGIN
                FileMgt.CopyClientFile(ESRBank."ESR Filename", BackupFilename, TRUE);
                IF NOT FileMgt.ClientFileExists(BackupFilename) THEN
                    MESSAGE(Text014, ESRBank."Bank Code");
            END ELSE
                MESSAGE(Text014, ESRBank."Bank Code");
        END;
        // Checksum error
        IF (NoOfPayments <> TotalNoOfPaymCLevel) OR (TotalAmount <> TotalAmtCLevel) THEN
            MESSAGE(
              Text113 +
              Text114 +
              Text115 +
              Text116 +
              Text117 +
              Text118 +
              Text119,
              NoOfPayments,
              TotalNoOfPaymCLevel,
              FORMAT(TotalAmount, 0, '<Sign><Integer Thousand><Decimals,3>'),
              FORMAT(TotalAmtCLevel, 0, '<Sign><Integer Thousand><Decimals,3>'))
        ELSE
            MESSAGE(Text120, NoOfPayments, TotalAmount, CurrencyCode);
    end;
    */


    procedure TrimInvoiceNo(InvoiceNo: Code[10])
    var

        CustLedgEntry: Record "Cust. Ledger Entry";
        CustLedgEntry2: Record "Cust. Ledger Entry";
        InvCount: Integer;
        TmpInvNo: Code[10];
        ReferenceNo: Code[10];
    begin
        ReferenceNo := InvoiceNo;
        TmpInvNo := InvoiceNo;
        CustLedgEntry.SETCURRENTKEY("Document No.");
        CustLedgEntry.SETRANGE("Document Type", CustLedgEntry."Document Type"::Invoice);
        CustLedgEntry.SETRANGE(Open, TRUE);
        WHILE (TmpInvNo[1] = '0') DO BEGIN
            CustLedgEntry.SETRANGE("Document No.", TmpInvNo);
            IF CustLedgEntry.FINDFIRST THEN BEGIN
                InvCount := InvCount + 1;
                IF (InvCount > 1) THEN
                    ERROR(Text124, ReferenceNo);
                InInvoiceNo := TmpInvNo;
            END;
            TmpInvNo := COPYSTR(TmpInvNo, 2);
        END;
        IF InvCount = 0 THEN
            InInvoiceNo := TmpInvNo;

        IF (TmpInvNo[1] <> '0') AND (InvCount = 1) THEN BEGIN
            CustLedgEntry2.SETCURRENTKEY("Document No.");
            CustLedgEntry2.SETRANGE("Document Type", CustLedgEntry2."Document Type"::Invoice);
            CustLedgEntry2.SETRANGE(Open, TRUE);
            CustLedgEntry2.SETRANGE("Document No.", TmpInvNo);
            IF CustLedgEntry2.FINDFIRST THEN
                ERROR(Text124, ReferenceNo);
        END;
    end;

    //Format()

    procedure FormXMLDate(_Date: Date): Text[30]
    begin
        // Hilfsmethode für Datumsformatierung inkl. Tag --> Für Ausgabe in XML als Text
        EXIT(FORMAT(_Date, 0, '<Year4>-<Month,2>-<Day,2>'));
    end;

    procedure FormXMLDateS(_Date: Date): Text[30]
    begin
        // Hilfsmethode für Datumsformatierung ohne Tag --> Für Ausgabe in XML als Text
        EXIT(FORMAT(_Date, 0, '<Year4>-<Month,2>'));
    end;

    procedure FormXMLDecimal(_Decimal: Decimal; _NumberAll: Integer; _NumberDec: Integer; _pflicht: Boolean): Text[30]
    // Hilfsmethode für Decimal-Formatierung --> Für Ausgabe in XML als Text
    begin
        IF (_Decimal = 0) AND (_pflicht = FALSE) THEN
            EXIT;
        IF (_Decimal = 0) AND _pflicht THEN
            EXIT('0');

        IF _NumberDec < 1 THEN
            _Decimal := ROUND(_Decimal, 1);
        IF _NumberDec = 1 THEN
            _Decimal := ROUND(_Decimal, 0.1);
        IF _NumberDec = 2 THEN
            _Decimal := ROUND(_Decimal, 0.01);
        IF _NumberDec = 3 THEN
            _Decimal := ROUND(_Decimal, 0.001);
        IF _NumberDec = 4 THEN
            _Decimal := ROUND(_Decimal, 0.0001);
        IF _NumberDec > 4 THEN
            _Decimal := ROUND(_Decimal, 0.00001);

        // Mit Absolutbetrag Länge der Zeichen testen (vor Komma) - Wenn zu lange bspw. 5000.39 (5,2)
        IF STRLEN(FORMAT(ABS(_Decimal), 0, '<Sign><Integer>')) > (_NumberAll - _NumberDec) THEN
            EXIT('0');

        IF _NumberDec < 1 THEN
            EXIT(CONVERTSTR(FORMAT(_Decimal, 0, '<Sign><Integer>'), ',', '.'));
        IF _NumberDec = 1 THEN
            EXIT(CONVERTSTR(FORMAT(_Decimal, 0, '<Sign><Integer><Decimals,2>'), ',', '.'));
        IF _NumberDec = 2 THEN
            EXIT(CONVERTSTR(FORMAT(_Decimal, 0, '<Sign><Integer><Decimals,3>'), ',', '.'));
        IF _NumberDec = 3 THEN
            EXIT(CONVERTSTR(FORMAT(_Decimal, 0, '<Sign><Integer><Decimals,4>'), ',', '.'));
        IF _NumberDec = 4 THEN
            EXIT(CONVERTSTR(FORMAT(_Decimal, 0, '<Sign><Integer><Decimals,5>'), ',', '.'));
        IF _NumberDec > 4 THEN
            EXIT(CONVERTSTR(FORMAT(_Decimal, 0, '<Sign><Integer><Decimals,6>'), ',', '.'));
    end;

    procedure FormXMLCurTime() _Output: Text[30]
    begin
        // Hilfsmethode für (aktuelle) Zeit-Formatierung --> Für Ausgabe in XML als Text
        _Output := COPYSTR(FORMAT(TIME), 1, 8);
        IF COPYSTR(_Output, 1, 1) = ' ' THEN
            _Output := '0' + COPYSTR(_Output, 2, 7);

        EXIT(_Output);
    end;

    procedure FormXMLTime(_Time: Time) _Output: Text[30]
    begin
        // Hilfsmethode für Zeit-Formatierung --> Für Ausgabe in XML als Text
        _Output := COPYSTR(FORMAT(_Time), 1, 8);
        IF COPYSTR(_Output, 1, 1) = ' ' THEN
            _Output := '0' + COPYSTR(_Output, 2, 7);

        EXIT(_Output);
    end;

    procedure FormNavDate(_Date: Text[30]): Text[30]
    begin
        // Hilfsmethode für Datumsformatierung inkl. Tag --> Für Ausgabe in Navision als Text
        EXIT(COPYSTR(_Date, 9, 2) + '.' + COPYSTR(_Date, 6, 2) + '.' + COPYSTR(_Date, 1, 4));
    end;

    procedure TI_XML2Text(_XMLDT: Text[50]) _TimeText: Text[20]
    var
        _NavDT: DateTime;
    begin

        // Konvertiert ein Java Datum Zeit mit UTC nach Time Text

        // Beispiel-Eingabe: Java (Text):     2014-09-05T11:04:53+02:00
        // Beispiel-Ausgabe: Time Text:       11:04:53

        _XMLDT := COPYSTR(_XMLDT, 1, 19);  // Zeitzone abschneiden, da diese nicht berücksichtigt werden soll

        EVALUATE(_NavDT, _XMLDT, 9); // Konvertieren vom XML Format mit Wert 9 (UTC wird anhand Z (ZULU) beachtet)
        EXIT(FORMAT(DT2TIME(ROUNDDATETIME(_NavDT))));
    end;

    procedure DT_XML2Nav(_XMLDT: Text[50]) _NavDT: DateTime
    begin
        // Konvertiert ein Java Datum mit UTC nach Navision DateTime

        // Beispiel-Eingabe: Java (Text):     2014-09-05T11:04:53+02:00
        // Beispiel-Ausgabe: Nav (DateTime):  05.09.14 13:04

        EVALUATE(_NavDT, _XMLDT, 9); // Konvertieren vom XML Format mit Wert 9 (UTC wird anhand Z (ZULU) beachtet)
        EXIT(ROUNDDATETIME(_NavDT));
    end;

    procedure DT_NAV2XMLGMT(_NavDT: DateTime) _XMLDT: Text[50]
    begin
        // Beispiel-Eingabe: 07.09.14 12:00:00
        // Beispiel-Ausgabe: 2014-09-07T10:00:00Z  --> Zulu Zeit wird automatisch berechnet (hier -2 Stunden)

        EXIT(FORMAT(_NavDT, 0, 9));  // Konvertieren nach XML Format mit Wert 9 (UTC wird anhand Z (ZULU) beachtet)
    end;

    procedure DT_NAV2XML(_NavDT: DateTime) _XMLDT: Text[50]
    begin
        // Beispiel-Eingabe: 07.09.14 12:00:00
        // Beispiel-Ausgabe: 2014-09-07T12:00:00

        _NavDT := ROUNDDATETIME(_NavDT);
        IF DT2DATE(_NavDT) = 0D THEN
            EXIT('');

        EXIT(D_NAV2XML(DT2DATE(_NavDT)) + 'T' + T_NAV2XML(DT2TIME(_NavDT)));
    end;

    procedure T_NAV2XML(_Time: Time): Text[50]
    var
        Hours: Text[30];
        Minuts: Text[30];
        Seconds: Text[30];
    begin
        // Beispiel-Eingabe: 12:00:00
        // Beispiel-Ausgabe: 12:00:00

        IF _Time = 0T THEN
            EXIT('00.00.00');

        Hours := FORMAT(_Time, 0, '<Hours24,2>');
        IF COPYSTR(Hours, 1, 1) = ' ' THEN
            Hours := '0' + COPYSTR(Hours, 2, 1);
        Minuts := FORMAT(_Time, 0, '<Minutes,2>');
        Seconds := FORMAT(_Time, 0, '<Seconds,2>');

        EXIT(Hours + ':' + Minuts + ':' + Seconds);
    end;

    procedure D_NAV2XML(_Date: Date): Text[30]
    begin
        // Beispiel-Eingabe: 07.09.14
        // Beispiel-Ausgabe: 2014-09-07

        IF _Date = 0D THEN
            EXIT('');
        EXIT(FORMAT(_Date, 0, '<Year4>-<Month,2>-<Day,2>'));
    end;

    procedure DT_XML2NAVDate(_DateTimeText: Text[30]): Date
    var
        Day: Integer;
        Month: Integer;
        Year: Integer;
    begin
        //Beispiel-Eingabe: 2017-06-16+02:00
        //Beispiel Ausgabe: 16.06.17
        EVALUATE(Day, COPYSTR(_DateTimeText, 9, 2));
        EVALUATE(Month, COPYSTR(_DateTimeText, 6, 2));
        EVALUATE(Year, COPYSTR(_DateTimeText, 1, 4));
        EXIT(DMY2DATE(Day, Month, Year));
    end;

    procedure RemoveBadChar(VAR Text: Text[140]) _Text: Text[140]
    var
        Ch: array[4] of Char;
    begin
        _Text := Text;

        Ch[1] := 9;   // TAB
        Ch[2] := 10;  // LF
        Ch[3] := 13;  // CR
        Ch[4] := 255; // leer undefiniert

        _Text := DELCHR(_Text, '=', FORMAT(Ch[1]));
        _Text := DELCHR(_Text, '=', FORMAT(Ch[2]));
        _Text := DELCHR(_Text, '=', FORMAT(Ch[3]));
        _Text := DELCHR(_Text, '=', FORMAT(Ch[4]));

        //SIX Implmentation Guidelines Appendix C conversion
        _Text := CONVERTSTR(_Text, 'şŻ¤żŢ§¨ŞŽ«¬­ęţ°˝řŘ´Š˘˙¸űž»ČźˇĘăĆĺĐň×ŁÝŔĂćĹđńŤšś˛‘©',
                                   '..............................AAADOxOY.aaaooo.yzZsS');
    end;

    procedure XMLDecText2NAVDec(XMLDecText: Text[100]): Decimal
    var

        Dec: Decimal;
        TestDecText: Text[100];
    begin
        CLEAR(Dec);
        TestDecText := FORMAT(1000.5, 0);

        IF STRPOS(TestDecText, ',') > 0 THEN
            EVALUATE(Dec, CONVERTSTR(XMLDecText, '.', ','))
        ELSE
            EVALUATE(Dec, XMLDecText);

        EXIT(Dec);
    end;

    //Convert Check ISO20022QR()

    procedure CheckQRRFRefNo(QRRFRefNo: Code[35])
    var

        Modulus97: Integer;
        I: Integer;
    begin
        //ISO20022QR
        IF QRRFRefNo = '' THEN
            EXIT;
        IF COPYSTR(QRRFRefNo, 1, 2) <> 'RF' THEN
            QRRFRefNoError;

        QRRFRefNo := DELCHR(QRRFRefNo);
        Modulus97 := 97;
        IF (STRLEN(QRRFRefNo) <= 5) OR (STRLEN(QRRFRefNo) > 25) THEN
            QRRFRefNoError;
        ConvertQRRFRefNo(QRRFRefNo);
        WHILE STRLEN(QRRFRefNo) > 6 DO
            QRRFRefNo := CalcModulus(COPYSTR(QRRFRefNo, 1, 6), Modulus97) + COPYSTR(QRRFRefNo, 7);
        EVALUATE(I, QRRFRefNo);
        IF (I MOD Modulus97) <> 1 THEN
            QRRFRefNoError;
        //ISO20022QR/END
    end;

    procedure ConvertQRRFRefNo(VAR QRRFRefNo: Code[35])
    var
        I: Integer;
    begin
        //ISO20022QR
        QRRFRefNo := COPYSTR(QRRFRefNo, 5) + COPYSTR(QRRFRefNo, 1, 4);
        I := 0;
        WHILE I < STRLEN(QRRFRefNo) DO BEGIN
            I := I + 1;
            IF ConvertLetter(QRRFRefNo, COPYSTR(QRRFRefNo, I, 1), I) THEN
                I := 0;
        END;
        //ISO20022QR/END
    end;

    procedure CalcModulus(Number: Code[10]; Modulus97: Integer): Code[10]
    var

        I: Integer;
    begin
        //ISO20022QR
        EVALUATE(I, Number);
        I := I MOD Modulus97;
        IF I = 0 THEN
            EXIT('');
        EXIT(FORMAT(I));
        //ISO20022QR/END
    end;


    procedure ConvertLetter(VAR QRRFRefNo: Code[35]; Letter: Code[1]; LetterPlace: Integer): Boolean
    var

        Letter2: Code[2];
    begin
        //ISO20022QR
        IF (Letter >= 'A') AND (Letter <= 'Z') THEN BEGIN
            CASE Letter OF
                'A':
                    Letter2 := '10';
                'B':
                    Letter2 := '11';
                'C':
                    Letter2 := '12';
                'D':
                    Letter2 := '13';
                'E':
                    Letter2 := '14';
                'F':
                    Letter2 := '15';
                'G':
                    Letter2 := '16';
                'H':
                    Letter2 := '17';
                'I':
                    Letter2 := '18';
                'J':
                    Letter2 := '19';
                'K':
                    Letter2 := '20';
                'L':
                    Letter2 := '21';
                'M':
                    Letter2 := '22';
                'N':
                    Letter2 := '23';
                'O':
                    Letter2 := '24';
                'P':
                    Letter2 := '25';
                'Q':
                    Letter2 := '26';
                'R':
                    Letter2 := '27';
                'S':
                    Letter2 := '28';
                'T':
                    Letter2 := '29';
                'U':
                    Letter2 := '30';
                'V':
                    Letter2 := '31';
                'W':
                    Letter2 := '32';
                'X':
                    Letter2 := '33';
                'Y':
                    Letter2 := '34';
                'Z':
                    Letter2 := '35';
            END;
            IF LetterPlace = 1 THEN
                QRRFRefNo := Letter2 + COPYSTR(QRRFRefNo, 2)
            ELSE BEGIN
                IF LetterPlace = STRLEN(QRRFRefNo) THEN
                    QRRFRefNo := COPYSTR(QRRFRefNo, 1, LetterPlace - 1) + Letter2
                ELSE
                    QRRFRefNo :=
                      COPYSTR(QRRFRefNo, 1, LetterPlace - 1) + Letter2 + COPYSTR(QRRFRefNo, LetterPlace + 1);
            END;
            EXIT(TRUE);
        END;
        IF (Letter >= '0') AND (Letter <= '9') THEN
            EXIT(FALSE);

        QRRFRefNoError;
        //ISO20022QR/END
    end;

    procedure QRRFRefNoError()
    begin
        //ISO20022QR
        ERROR(Text200);
        //ISO20022QR/END
    end;

    procedure CheckQRIID_IBAN(VendBank: Record "Vendor Bank Account")
    var

        QR_IID: Integer;
    begin
        //ISO20022QR
        //QR-IDD in IBAN 5. bis 9. Stelle Werte zwischen 30000 bis 31999
        CASE VendBank."Payment Form Enum" OF
            VendBank."Payment Form Enum"::"QR with 27 digit Reference":
                BEGIN
                    VendBank.IBAN := IBANDELCHR(VendBank.IBAN);
                    IF EVALUATE(QR_IID, COPYSTR(VendBank.IBAN, 5, 5)) THEN BEGIN
                        IF NOT (QR_IID IN [30000 .. 31999]) THEN
                            ERROR(Text201);
                    END;
                END;
            VendBank."Payment Form Enum"::QR, VendBank."Payment Form Enum"::"Post Payment Domestic", VendBank."Payment Form Enum"::"Bank Payment Domestic":
                BEGIN
                    VendBank.IBAN := IBANDELCHR(VendBank.IBAN);
                    IF EVALUATE(QR_IID, COPYSTR(VendBank.IBAN, 5, 5)) THEN BEGIN
                        IF QR_IID IN [30000 .. 31999] THEN
                            ERROR(Text202, VendBank.FIELDCAPTION("Payment Form Enum"), VendBank."Payment Form Enum");
                    END;
                END;
        END;
        //ISO20022QR/END
    end;

    /*
        procedure ProcessPurchHeaderQR(VAR _PurchHeader: Record "Purchase Header"; _CodingLine: Text[1024])
        var

            CurrExch: Record "Currency Exchange Rate";
            CurrencyCode: Code[10];
        begin
            //ISO20022QR
            IF (_CodingLine = '') OR (NOT (UPPERCASE(COPYSTR(_CodingLine, 1, 2)) IN ['CH', 'LI', 'SP'])) THEN
                EXIT;

            WITH _PurchHeader DO BEGIN
                "DTA Coding Line" := '';
                CLEAR(F);
                F.TEXTMODE := TRUE;
                F.WRITEMODE := TRUE;
                FileName := FileMgt.ServerTempFileName('');
                F.CREATE(FileName);

                F.WRITE(_CodingLine);
                F.CLOSE;

                CASE UPPERCASE(COPYSTR(_CodingLine, 1, 2)) OF
                    //ADVA Scanner Zebra DS9908
                    'SP':
                        BEGIN
                            F.OPEN(FileName);
                            F.READ(FileText);
                            FileText := DELCHR(FileText, '=', ',');
                            FileText := CONVERTSTR(FileText, '°', ',');
                            CLEAR(QRRecord);
                            IF QRRecord.GET("No.") THEN
                                QRRecord.DELETE;
                            QRRecord.INIT;
                            QRRecord.VALIDATE("Invoice No.", "No.");
                            QRRecord.INSERT;
                            QRRecord."1 QRType" := SELECTSTR(1, FileText);
                            QRRecord."4 Konto" := SELECTSTR(4, FileText);
                            QRRecord."28 Referenztyp" := SELECTSTR(28, FileText);
                            QRRecord."20 Währung" := SELECTSTR(20, FileText);
                            IF QRRecord."28 Referenztyp" <> 'NON' THEN
                                IF EVALUATE(QRRecord."19 Betrag", (SELECTSTR(19, FileText))) THEN;
                            QRRecord."29 Referenz" := SELECTSTR(29, FileText);
                            QRRecord.MODIFY;
                        END;
                    //CREALOGIX Scanner "NAV bossPayment"
                    'LI', 'CH':
                        BEGIN
                            F.OPEN(FileName);
                            F.READ(FileText);
                            FileText := DELCHR(FileText, '=', ',');
                            FileText := CONVERTSTR(FileText, ';', ',');
                            CLEAR(QRRecord);
                            IF QRRecord.GET("No.") THEN
                                QRRecord.DELETE;
                            QRRecord.INIT;
                            QRRecord.VALIDATE("Invoice No.", "No.");
                            QRRecord.INSERT;
                            QRRecord."4 Konto" := SELECTSTR(1, FileText);
                            QRRecord."28 Referenztyp" := SELECTSTR(2, FileText);
                            QRRecord."20 Währung" := SELECTSTR(4, FileText);
                            IF QRRecord."28 Referenztyp" <> 'NON' THEN
                                IF EVALUATE(QRRecord."19 Betrag", (SELECTSTR(5, FileText))) THEN;
                            QRRecord."29 Referenz" := SELECTSTR(3, FileText);
                            QRRecord.MODIFY;
                        END;
                    ELSE
                        EXIT;
                END;

                ProcessQRRecord(QRRecord);

                //QR Currency
                CLEAR(CurrencyCode);
                IF _PurchHeader."Currency Code" = '' THEN BEGIN
                    CurrencyCode := 'CHF'
                END ELSE BEGIN
                    CurrencyCode := _PurchHeader."Currency Code";
                END;
                IF NOT (QRRecord."20 Währung" IN ['CHF', 'EUR']) THEN
                    ERROR(Text213, 'EUR/CHF', QRRecord."20 Währung");
                IF (CurrencyCode <> QRRecord."20 Währung") THEN
                    ERROR(Text213, QRRecord."20 Währung", _PurchHeader."Currency Code");

                CASE VB2."Payment Form Enum" OF
                    VB2."Payment Form Enum"::QR, VB2."Payment Form Enum"::"QR with 27 digit Reference":
                        GetVendorBankQR(VB2, VB2.IBAN, "Pay-to Vendor No.");
                END;

                // Fill in purch header based on codingline and vendor bank
                IF "Buy-from Vendor No." = '' THEN BEGIN
                    VALIDATE("Buy-from Vendor No.", QRBank."Vendor No.");
                    COMMIT;
                END;
                VALIDATE("Bank Code", QRBank.Code);
                VALIDATE("Reference No.", RefNo);
                VALIDATE("ESR Amount", QRRecord."19 Betrag");

                IF QRRecord."20 Währung" = 'EUR' THEN BEGIN
                    "Currency Code" := 'EUR';
                    "Currency Factor" := CurrExch.ExchangeRate("Posting Date", "Currency Code");
                    VALIDATE(Amount);
                END;
                CLEAR(_CodingLine);
                F.CLOSE;
                IF EXISTS(FileName) THEN
                    ERASE(FileName);
            END;
            //ISO20022QR/END
        end;*/

    /*
   procedure ProcessGlLineQR(VAR _GenJnlLine: Record "Gen. Journal Line"; _CodingLine: Text[1024])
   var

       CurrExch: Record "Currency Exchange Rate";
       CurrencyCode: Code[10];
   begin
       //ISO20022QR
       IF (_CodingLine = '') OR (NOT (UPPERCASE(COPYSTR(_CodingLine, 1, 2)) IN ['CH', 'LI', 'SP'])) THEN
           EXIT;

       WITH _GenJnlLine DO BEGIN
           "DTA Coding Line" := '';
           CLEAR(F);
           F.TEXTMODE := TRUE;
           F.WRITEMODE := TRUE;
           FileName := FileMgt.ServerTempFileName('');
           F.CREATE(FileName);

           F.WRITE(_CodingLine);
           F.CLOSE;

           CASE UPPERCASE(COPYSTR(_CodingLine, 1, 2)) OF
               //ADVA Scanner Zebra DS9908
               'SP':
                   BEGIN
                       F.OPEN(FileName);
                       F.READ(FileText);
                       FileText := DELCHR(FileText, '=', ',');
                       FileText := CONVERTSTR(FileText, '°', ',');
                       IF QRRecord.GET('1') THEN
                           QRRecord.DELETE;
                       CLEAR(QRRecord);
                       QRRecord.INIT;
                       QRRecord.VALIDATE("Invoice No.", '1');
                       QRRecord.INSERT;
                       QRRecord."1 QRType" := SELECTSTR(1, FileText);
                       QRRecord."4 Konto" := SELECTSTR(4, FileText);
                       QRRecord."28 Referenztyp" := SELECTSTR(28, FileText);
                       QRRecord."20 Währung" := SELECTSTR(20, FileText);
                       IF QRRecord."28 Referenztyp" <> 'NON' THEN
                           IF EVALUATE(QRRecord."19 Betrag", (SELECTSTR(19, FileText))) THEN;
                       QRRecord."29 Referenz" := SELECTSTR(29, FileText);
                       QRRecord.MODIFY;
                   END;
               //CREALOGIX Scanner "NAV bossPayment"
               'LI', 'CH':
                   BEGIN
                       F.OPEN(FileName);
                       F.READ(FileText);
                       FileText := DELCHR(FileText, '=', ',');
                       FileText := CONVERTSTR(FileText, ';', ',');
                       CLEAR(QRRecord);
                       IF QRRecord.GET('1') THEN
                           QRRecord.DELETE;
                       QRRecord.INIT;
                       QRRecord.VALIDATE("Invoice No.", '1');
                       QRRecord.INSERT;
                       QRRecord."4 Konto" := SELECTSTR(1, FileText);
                       QRRecord."28 Referenztyp" := SELECTSTR(2, FileText);
                       QRRecord."20 Währung" := SELECTSTR(4, FileText);
                       IF QRRecord."28 Referenztyp" <> 'NON' THEN
                           IF EVALUATE(QRRecord."19 Betrag", (SELECTSTR(5, FileText))) THEN;
                       QRRecord."29 Referenz" := SELECTSTR(3, FileText);
                       QRRecord.MODIFY;
                   END;
               ELSE
                   EXIT;
           END;

           ProcessQRRecord(QRRecord);

           //QR Currency
           CLEAR(CurrencyCode);
           IF "Currency Code" = '' THEN BEGIN
               CurrencyCode := 'CHF'
           END ELSE BEGIN
               CurrencyCode := "Currency Code";
           END;
           IF NOT (QRRecord."20 Währung" IN ['CHF', 'EUR']) THEN
               ERROR(Text213, 'EUR/CHF', QRRecord."20 Währung");
           IF (CurrencyCode <> QRRecord."20 Währung") THEN
               ERROR(Text213, QRRecord."20 Währung", "Currency Code");

           CASE VB2."Payment Form Enum" OF
               VB2."Payment Form Enum"::QR, VB2."Payment Form Enum"::"QR with 27 digit Reference":
                   GetVendorBankQR(VB2, VB2.IBAN, "Account No.");
           END;

           // Fill in GLL according to coding line and vendor bank
           VALIDATE("Account Type", "Account Type"::Vendor);
           VALIDATE("Account No.", QRBank."Vendor No.");
           VALIDATE("Document Type", "Document Type"::Invoice);
           VALIDATE("Recipient Bank Account", QRBank.Code);
           VALIDATE("Reference No.", RefNo);
           VALIDATE(Amount, -QRRecord."19 Betrag");
           VALIDATE("Bal. Account Type", "Bal. Account Type"::"G/L Account");
           VALIDATE("Bal. Account No.", QRBank."Balance Account No.");

           IF QRRecord."20 Währung" = 'EUR' THEN BEGIN
               "Currency Code" := 'EUR';
               "Currency Factor" := CurrExch.ExchangeRate("Posting Date", "Currency Code");
               VALIDATE(Amount);
           END;
           CLEAR(_CodingLine);
           F.CLOSE;
           IF EXISTS(FileName) THEN
               ERASE(FileName);
       END;
       //ISO20022QR/END
   end;
*/
    procedure ProcessQRRecord(VAR _QRRecord: Record "QR Record")
    begin
        //ISO20022QR/END
        CLEAR(VB2);
        CLEAR(IBAN);
        CLEAR(RefNo);

        CASE _QRRecord."28 Referenztyp" OF
            'QRR':
                BEGIN
                    VB2."Payment Form Enum" := VB2."Payment Form Enum"::"QR with 27 digit Reference";
                    VB2.IBAN := QRRecord."4 Konto";
                    CheckQRIID_IBAN(VB2);
                    IBAN := VB2.IBAN;
                    RefNo := QRRecord."29 Referenz";
                END;
            'NON':
                BEGIN
                    VB2."Payment Form Enum" := VB2."Payment Form Enum"::QR;
                    VB2.IBAN := QRRecord."4 Konto";
                    CheckQRIID_IBAN(VB2);
                    IBAN := VB2.IBAN;
                END;
            'SCOR':
                BEGIN
                    VB2."Payment Form Enum" := VB2."Payment Form Enum"::QR;
                    VB2.IBAN := QRRecord."4 Konto";
                    CheckQRIID_IBAN(VB2);
                    IBAN := VB2.IBAN;
                    RefNo := QRRecord."29 Referenz";
                    CheckQRRFRefNo(RefNo);
                END;
            ELSE
                ERROR(Text203, _QRRecord."28 Referenztyp");
        END;
        //ISO20022QR/END
    end;
    /// <summary>
    /// Task broj
    /// vukoje Ljubisa 12.09.2021
    /// </summary>
    /// <param name="_VendorBankAccount"></param>
    /// <param name="_IBAN"></param>
    /// <param name="_VendorNo"></param>

    procedure GetVendorBankQR(_VendorBankAccount: Record "Vendor Bank Account"; _IBAN: Text[50]; _VendorNo: Code[20])
    begin
        //ISO20022QR/END
        // Get Vendor and Bankcode based on IBAN
        CLEAR(QRBank);

        IF NOT (_VendorBankAccount."Payment Form Enum" IN [_VendorBankAccount."Payment Form Enum"::"QR with 27 digit Reference",
                                                      _VendorBankAccount."Payment Form Enum"::QR])
        THEN
            EXIT;

        IF _VendorNo <> '' THEN
            QRBank.SETRANGE("Vendor No.", _VendorNo)
        ELSE
            QRBank.SETCURRENTKEY(IBAN);

        QRBank.SETRANGE("Payment Form Enum", _VendorBankAccount."Payment Form Enum");

        CASE _VendorBankAccount."Payment Form Enum" OF
            _VendorBankAccount."Payment Form Enum"::QR, _VendorBankAccount."Payment Form Enum"::"QR with 27 digit Reference":
                QRBank.SETRANGE(IBAN, _IBAN);
        END;

        CASE QRBank.COUNT OF
            // Only one entry with this account
            1:
                QRBank.FIND('-');
            // No bank and no vendor No.
            0:
                BEGIN
                    IF _VendorNo = '' THEN
                        ERROR(Text204 + Text205 + Text206, _VendorBankAccount."Payment Form Enum", IBAN);
                    // Define Bank
                    IF NOT CONFIRM(Text207 + Text208, FALSE, _VendorNo, FORMAT(_VendorBankAccount."Payment Form Enum")) THEN
                        EXIT;

                    CLEAR(QRBank);
                    QRBank."Vendor No." := _VendorNo;
                    IF _VendorBankAccount."Payment Form Enum" = _VendorBankAccount."Payment Form Enum"::"QR with 27 digit Reference" THEN
                        QRBank.Code := 'QR REF-27'
                    ELSE
                        QRBank.Code := FORMAT(_VendorBankAccount."Payment Form Enum", -MAXSTRLEN(QRBank.Code));
                    QRBank."Payment Form Enum" := _VendorBankAccount."Payment Form Enum";

                    IF VB2.IBAN <> '' THEN
                        QRBank.VALIDATE(IBAN, _IBAN);
                    QRBank.INSERT;
                    IF CONFIRM(Text209 + Text210, TRUE, _VendorNo, FORMAT(_VendorBankAccount."Payment Form Enum")) THEN BEGIN
                        COMMIT;
                        IF PAGE.RUNMODAL(PAGE::"Vendor Bank Account Card", QRBank) = ACTION::LookupOK THEN;
                    END;
                END;
            ELSE BEGIN
                MESSAGE(Text211 + Text212, _VendorBankAccount."Payment Form Enum", IBAN);
                IF PAGE.RUNMODAL(PAGE::"Vendor Bank Account List", QRBank) = ACTION::LookupOK THEN;
            END;
        END;
        //ISO20022QR/END
    end;

    var
        ns: Text;
        FileMgt: Codeunit "File Management";
        CompanyInfo: Record "Company Information";
        VendLedgerEntry: Record "Vendor Ledger Entry";
        Vendor: Record Vendor;
        VendorBank: Record "Vendor Bank Account";
        LocalInstrument: Boolean;
        BankAccountNo: code[50];
        IBAN: COde[50];
        ClearingNo: code[10];
        BankIdentifier: COde[21];
        SwiftCode: Code[20];
        RefNo: COde[30];
        DocumentNo: COde[35];
        ChkDig: Code[2];
        vInstrId: Text[50];
        vEndToEndId: Code[50];
        ID: Code[40];
        CurrCode: Text[10];
        Amount: Decimal;
        CountryCode: Code[10];
        StreetName: text;
        StreetNumber: text;
        AddressLine1: Text;
        AddressLine2: Text;
        CreditAgentCountryCode: Code[10];
        CreditAgentName: text;
        CreditAgentStreetName: text;
        CreditAgentStreetNumber: text;
        CreditAgentPostCode: code[20];
        CreditAgentCity: text;
        BeneficiaryName: text;
        BeneficiaryAddress: text;
        BankAddressLine1: text;
        BankAddressLine2: text;
        SummaryPmtTxt: text;
        SEPA: Boolean;
        BankMgt: codeunit BankMgt;
        ClientXMLTempFileName: text;
        GenJournalLine: record "Gen. Journal Line";
        XML054Doc: XmlDocument;
        InInvoiceNo: Code[10];
        ESRRefNo: Code[27];
        QRBank: Record "Vendor Bank Account";
        F: File;
        FileText: Text;
        QRRecord: Record "QR Record";
        VB2: Record "Vendor Bank Account";
        TempBlob: codeunit "Temp Blob";
        DtaISOSetup: Record "DTA Setup";
        outStr: OutStream;
        inStr: InStream;
        TempFile: File;
        fileName: Text;
        xmlDoc: XmlDocument;
        root: XmlNode;

        Text001: Label '<Microsoft Shell Controls And Automation.Shell kann nicht als Automationserver gestartet werden!>';

        Text002: Label '<%1 darf nicht leer sein.>';
        Text003: Label '<%1 darf nicht vor heute sein.>';
        Text004: Label 'If necessary, generate multiple files for your payments.';
        Text005: Label 'The payments have different posting dates. (%1 and %2)\';
        Text006: Label 'No bank connection found with bank code %1 for vendor %2.';
        Text007: Label 'Vendor entry for invoice %1 of vendor %2 not found.';
        Text008: Label 'Reference no. for invoice %1 of vendor %2 must have %3 digits according to the vendor bank definition.';
        Text009: Label 'No bank connection found with bank code %1 for vendor %2.';
        Text010: Label '<Rechnung Nr. %1>';
        Text011: Label 'Payment type %1 is not allowed for DTA.\Vendor: %2, Bank Code: %3.';
        Text012: Label 'Payment type %1 is only allowed in %2. Vendor %3, Amount %4.';
        Text013: Label 'The backup copy of the ISO file could not be written.\';
        Text014: Label 'Please check filename and folder in the ISO setup for bank %1.';
        Text015: Label 'No bank connection found with bank code %1 for vendor %2.';
        Text016: Label 'Salary payment';
        Text017: Label '<Bei Person %1 %2 %3 ist eine ungültige %4 hinterlegt.>';
        Text018: Label '<Bei Bankverbindung mit Bankcode %1 für Kreditor %2 muss für %3 der 11-stellige BIC vorhanden sein.>';
        Text019: Label '<Das System kann den angegebenen Pfad %1 nicht finden.>';
        Text020: Label '<Kreditor %1\Bei Ländercode CH darf nicht Zahlungsart %2 verwendet werden.>';
        Text101: Label 'Do you want to import the ISO file?';
        Text102: Label 'You have defined more than one ESR bank in the setup. Choose a bank in the following window.\';
        Text103: Label 'Import cancelled.';
        Text104: Label 'Backup copy of ESR file could not be written. Please check ESR setup.';
        Text105: Label 'Journal "%1" contains entries. Please process these first.';
        Text106: Label 'Import ISO ESR file\';
        Text107: Label 'No of payments   #1############\';
        Text108: Label 'Total amount     #2############';
        Text109: Label 'ESR payment, inv';
        Text110: Label 'ESR correction debit ';
        Text111: Label '? Inv %1? Ref %2';
        Text112: Label 'ESR payment ';
        Text113: Label 'Checksum error on import.\\';
        Text114: Label 'The number of payments or the sum of amounts does not match the checksum record.\\';
        Text115: Label 'Total records read    : #1############\';
        Text116: Label 'Total of checksum     : #2############\';
        Text117: Label 'Total amounts read    : #3############\';
        Text118: Label 'Total of checksum     : #4############\\';
        Text119: Label 'Check carefully the payments in the journal. If necessary, request a new ESR file.';
        Text120: Label '%1 payments for %3 %2 sucessfully imported.';
        Text121: Label 'Transaction not identified.\';
        Text123: Label 'The %1 as defined in %2 %3 cannot be detected at the expected position in the file. The system is not able to determine the ESR record length.';
        Text124: Label 'More than one open invoice were found for the Reference No. %1';
        Text125: Label '<Die Datei %1 ist nicht vorhanden.>';
        Text126: Label '<%1  ist keine gültige XML Datei.>';
        Text127: Label '<%1 %2 in %3 entspricht nicht %4 in der Datei.>';
        //ISO20022QR_	
        Text200: Label '<Die Referenz Nr. ist keine gültige RF Referenz Nr.>';
        Text201: Label '<Die IBAN enthält keine gültige QR-IID.>';
        Text202: Label '<Die IBAN enthält eine QR-IID.\Für %1 %2 darf in IBAN keine QR-IID enthalten sein.>';
        Text203: Label '<Referenztyp %1 wird nicht unterstützt.>';
        Text204: Label 'No vendor bank was found for payment type %1 and vendor %2. \';
        Text205: Label 'Select the vendor no. on the GL line and read the document again. \';
        Text206: Label 'Navision will then try to find a bank based on the coding line.';
        Text207: Label 'For vendor %1, no bank is defined with payment type %2.';
        Text208: Label '\\Should Navision insert a vendor bank with bank code %2 and payment type %2?';
        Text209: Label 'For vendor %1, bank %2 has been created.\\';
        Text210: Label 'Do you want to see the bank card to check the entry or to add balance account, bank acount no. or position of invoice number?';
        Text211: Label 'There are multiple vendor banks with payment type %1 and account %2.\\';
        Text212: Label 'Select an entry from the list.';
        Text213: Label 'You have entered a %1 ESR, the expected currency is %2.';
        Text214: Label 'Für Kreditoren Nr. %1, Rechnung Nr. %2, Kreditoren Bank Konto Nr. %3 muss die Referenznummer ausgefüllt werden! ';

}