codeunit 50003 PostCARVOAPIPurchaseInvoices
{
    trigger OnRun()
    var
        PurchaseHeader: Record "Purchase Header";
        PurchaseHeaderOpen: Record "Purchase Header";
        PurchaseHeaderForRelease: Record "Purchase Header";
        VendorBankAcc: Record "Vendor Bank Account";

        PurchaseInvoice: Page "Purchase Invoice";
        UserSecStatus: Record "User Security Status";

        ReleasePurchDoc: Codeunit "Release Purchase Document";

    begin
        UserSecStatus.Reset();
        UserSecStatus.SetRange(UserSecStatus."User Name", 'CARVOLUTION API');
        If UserSecStatus.FindFirst() then begin
            PurchaseHeader.Reset();
            PurchaseHeaderOpen.Reset();
            PurchaseHeaderForRelease.Reset();

            /*         PurchaseHeader.SetRange(PurchaseHeaderCU.SystemCreatedBy, '{3553735F-6A52-43C1-B539-91CF9F3ACC56}'); */
            PurchaseHeader.SetRange(PurchaseHeader.SystemCreatedBy, UserSecStatus."User Security ID");
            PurchaseHeader.SetFilter(PurchaseHeader.Status, '%1|%2', PurchaseHeader.Status::Open, PurchaseHeader.Status::Released);

            PurchaseHeaderOpen.CopyFilters(PurchaseHeader);
            PurchaseHeaderOpen.SetFilter(PurchaseHeaderOpen.Status, '%1', PurchaseHeaderOpen.Status::Open);

            PurchaseHeaderForRelease.CopyFilters(PurchaseHeaderOpen);

            PurchaseHeaderOpen.SetRange(PurchaseHeaderOpen."Bank Code", '');
            if (PurchaseHeaderOpen.Find('-')) then begin
                if PurchaseHeaderOpen."Bank Code" = '' then
                    repeat
                        VendorBankAcc.Reset();
                        VendorBankAcc.SetRange("Vendor No.", PurchaseHeaderOpen."Buy-from Vendor No.");
                        VendorBankAcc.SetFilter(Code, '<>%1', '');
                        if VendorBankAcc.Find('-') then begin
                            PurchaseHeaderOpen.Validate(PurchaseHeaderOpen."Bank Code", VendorBankAcc.Code);
                            PurchaseHeaderOpen.Modify();
                        end;
                    until PurchaseHeaderOpen.Next() = 0;
            end;

            if (PurchaseHeaderForRelease.Find('-')) then begin
                repeat
                    Codeunit.Run(Codeunit::"Release Purchase Document", PurchaseHeaderForRelease);
                until PurchaseHeaderForRelease.Next() = 0;
            end;

            if (PurchaseHeader.Find('-')) then
                repeat
                    Codeunit.Run(Codeunit::"Purch.-Post", PurchaseHeader);
                until PurchaseHeader.Next() = 0;
        end;
    end;
}