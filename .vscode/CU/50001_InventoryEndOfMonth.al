codeunit 50001 InventoryEndOfMonth
{
    trigger OnRun()
    begin
        SetInventoryOfMonthEnd()
    end;

    procedure SetInventoryOfMonthEnd()
    var
        DefaultDim: record "Default Dimension";
        Item: record item;
        ParentTypeItem: Text;
        ItemGUIDList: List of [Guid];
        i: Integer;
        ItemGuid: Guid;
    begin
        //Get Item List
        if Item.FindFirst() then begin
            repeat
                ItemGUIDList.Add(Item.SystemId);
            until Item.Next() = 0;
        end;

        for i := 1 to ItemGUIDList.Count() do begin
            //Get GUID
            ItemGuid := ItemGUIDList.Get(i);
            DefaultDim.SetRange(ParentId, ItemGuid);
            if DefaultDim.FindFirst() then begin
                repeat
                    item.SetRange(SystemId, ItemGuid);
                    if item.FindFirst() then begin
                        DefaultDim.ItemNo := item."No.";
                        item.CalcFields(Inventory);
                        DefaultDim.ItemInventoryEndMonth := Item.Inventory;
                        DefaultDim.DateEndMonth := CurrentDateTime();
                        DefaultDim.Reset();
                        DefaultDim.Modify();
                    end;
                until DefaultDim.next() = 0;
            end;
        end;
    end;

}
