codeunit 50054 "Cost Correction Log Entries"
{
    TableNo = CostObjectCorrectionTable;
    trigger OnRun()
    begin
        CostCorrRec := Rec;
        ReadLogEntries();
    end;

    procedure ReadLogEntries()
    var
        CostObjCorr: record CostObjectCorrectionTable;
        JobQueueLogEntry: Record "Job Queue Log Entry";
    begin

        JobQueueLogEntry.Reset();
        JobQueueLogEntry.SetRange(ID, CostCorrRec."Job Queue Entry ID");
        JobQueueLogEntry.SetFilter(JobQueueLogEntry.Status, '<>%1', JobQueueLogEntry.Status::"In Process");

        if JobQueueLogEntry.Find('-') then begin

            if JobQueueLogEntry.Status = JobQueueLogEntry.Status::Success then begin
                CostCorrRec."Error Text" := JobQueueLogEntry."Error Message";
                CostCorrRec."Cost Object Status" := "Cost Correction Status"::"Dim.Corr. Finished";
                CostCorrRec.Corrected := true;
                CostCorrRec.Modify();


                DimensionCorrection.Reset();
                DimensionCorrection.SetRange("Entry No.", CostCorrRec."DimensionCorrection ID");
                if DimensionCorrection.Find('-') then
                    Codeunit.Run(Codeunit::"Post Cost Corr. To Cost Acc.", DimensionCorrection);


            end
            else begin
                CostCorrRec."Error Text" := JobQueueLogEntry."Error Message";
                CostCorrRec."Cost Object Status" := "Cost Correction Status"::Error;
                CostCorrRec.Modify();
            end;
        end;

    end;

    var
        CostCorrRec: record CostObjectCorrectionTable;
        DimensionCorrection: Record "Dimension Correction";
        temp: Report "Transfer GL Entries to CA";
        tttt: record "Cost Register";
        ttr: page "Cost Journal";
}