
codeunit 50029 "Replacement Part Email send"
{

    procedure Sendemail(SalesCode: code[20])
    var

        Customer: Record Customer;
        CarReplacementTemplate: Record "Car Replacement template";
        RecordRef: RecordRef;
        FieldRef: FieldRef;
        EmailSubject: text;
        EmailBody: text;
        Recipients: List of [Text];
        RecipientsCC: List of [Text];
        RecipientsBCC: List of [Text];
        CurrencyCode: COde[10];
        LogoFile: Text;
        ExportFile: FIle;
        CustomerText: List of [Text];
        DummyBool: Boolean;

    begin
        CLEAR(CUEnvInformation);
        Clear(OStream);
        Clear(SalesInvoiceReport);
        Clear(IStream);
        CLear(EmailCU);
        Clear(EmailMessageCU);
        Clear(EmailBody);
        CLear(TempBlob);
        Clear(Recipients);
        Clear(RecipientsBCC);
        Clear(SalesInvoice);

        SalesInvoice.Reset();
        SalesInvoice.SetRange("No.", SalesCode);
        SalesInvoice.FindFirst();

        Customer.Reset;
        Customer.get(SalesInvoice."Sell-to Customer No.");
        if Customer."E-Mail" = '' then
            exit;//if email address does not exist just skip

        if CUEnvInformation.IsProduction() then begin
            Recipients.Add(Customer."E-Mail")
        end
        else begin
            //Recipients.Add('ljubisa.vukoje@holyerp.rs');
            Recipients.Add('lazar.jevtic@holyerp.rs');
            Recipients.Add('selena.ris@carvolution.ch');
            Recipients.Add('robin.schmid@carvolution.ch');
        end;

        Lang := Customer."Language Code";
        if Lang = '' then
            Lang := 'ENU';

        RecordRef.Open(Database::"Sales Invoice Header");
        FieldRef := RecordRef.Field(3);
        FieldRef.SetRange(SalesInvoice."No.");
        if not RecordRef.Find('-') then
            exit;

        if not CarReplacementTemplate.Get(Lang) then
            exit;

        CustomerText := Customer.Name.Split(' ');
        if Customer."Name 2" <> '' then
            CustomerText := Customer."Name 2".Split(' ');

        EmailSubject := CarReplacementTemplate."Email Subject";
        EmailBody := StrSubstNo(CarReplacementTemplate.GetEmailBody(), CustomerText.Get(1));


        if CUEnvInformation.IsProduction() then begin
            RecipientsBCC.Add('5313716@bcc.hubspot.com');
        end;

        TempBlob.CreateOutStream(OStream);
        Language.Reset();
        Language.SetRange(Code, Lang);
        IF Language.FindFirst() then
            SalesInvoiceReport.Language := Language."Windows Language ID";


        IF Report.SaveAs(GetReportID(), '', ReportFormat::Pdf, OStream, RecordRef) THEN
            //Report.SaveAs(117, '', ReportFormat::Pdf, OStream, RecordRef);
            TempBlob.CreateInStream(IStream);


        EmailMessageCU.Create(Recipients, EmailSubject, EmailBody, true, RecipientsCC, RecipientsBCC);
        EmailMessageCU.AddAttachment(FORMAT(SalesInvoice."No.") + '.pdf', 'PDF', IStream);

        EmailCU.Send(EmailMessageCU, Enum::"Email Scenario"::"Sales Invoice");



    end;

    procedure GetReportID(): Integer
    var
        DummyReportSelections: Record "Report Selections";
    begin
        DummyReportSelections.Reset();
        DummyReportSelections.SetRange(DummyReportSelections.Usage, DummyReportSelections.Usage::"S.Invoice");
        DummyReportSelections.Find('-');
        exit(DummyReportSelections."Report ID");

    end;

    var
        EmailMessageCU: Codeunit "Email Message";
        EmailCU: Codeunit Email;

        OStream: OutStream;
        IStream: InStream;
        CUFileManagment: Codeunit "File Management";
        TempBlob: codeunit "Temp Blob";
        SalesInvoiceReport: Report 50035;
        Language: Record Language;
        DummyBool: Boolean;
        CompanyInfo: Record "Company Information";
        Lang: code[10];
        CUEnvInformation: Codeunit "Environment Information";
        Text001: Label 'For customer  %1 you need to populate email address !';
        CUReminder: record "Reminder Header";

        SalesInvoice: record "Sales Invoice Header";
}

