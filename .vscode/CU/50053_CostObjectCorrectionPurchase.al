/// <summary>
/// Vukoje Ljubisa
/// 23.12.2022
/// ljubisa.vukoje@holyerp.rs
/// CU for handling Cost Objects corrections for Cost Accounting for Purchase Documents
/// </summary>
codeunit 50053 CostObjectCorrectionPurchCU
{

    trigger OnRun()
    begin
        MarkedRecordsForProcessing();
    end;



    procedure CorrectDimension(var CorrectionObject: Record CostObjectCorrectionPurchTable): Boolean
    var
        DimensionCorrection: Record "Dimension Correction";
        DimensionCorrectionMgt: Codeunit "Dimension Correction Mgt";
        DimensionCorrectionChange: Record "Dim Correction Change";
        CostAccountingSetup: Record "Cost Accounting Setup";
        JobQueueEntry: Record "Job Queue Entry";
        CostJournalLine: Record "Cost Journal Line";
        GLEntry: Record "G/L Entry";
        GlAcc: Record "G/L Account";
        PurchaseInvoice: record "Purch. Inv. Header";
        SalesInvoice: Record "Sales Invoice Header";
        CostEntries: Record "Cost Entry";
        CreateCostJournal: Boolean;
        DefaultDimension: Record "Default Dimension";

    begin

        DimensionValue.Reset();
        DimensionValue.SetRange("Dimension Value Type", DimensionValue."Dimension Value Type"::Standard);
        DimensionValue.SetRange(Carvolution_ID, CorrectionObject.Carvolution_ID);

        //check if CarvolutionID exist in dimension value table
        if not DimensionValue.Find('-') then begin
            CorrectionObject."Error Text" := StrSubstNo(CarvolutionIDLbl, CorrectionObject.Carvolution_ID);
            CorrectionObject."Cost Object Status" := CorrectionObject."Cost Object Status"::Error;
            CorrectionObject.Modify();
            EXIT(FALSE);
        end;

        //check if dimension is blocked
        if DimensionValue.Blocked then begin
            CorrectionObject."Error Text" := 'Dimension value ' + DimensionValue.Code + ' is blocked';
            CorrectionObject."Cost Object Status" := CorrectionObject."Cost Object Status"::Error;
            CorrectionObject.Modify();
            EXIT(FALSE);
        end;


        //skiped same dimension
        //if old dimension value are equal to new dimension value
        //dimension correctio throw exception
        //NOT SKIPPED JUST MARK AS Dim. Correction = true
        GLEntry.Reset();
        GLEntry.SetRange("Document No.", CorrectionObject."Document No.");
        GLEntry.SetFilter("Global Dimension 2 Code", '<>%1', '');
        GLEntry.SetFilter("Global Dimension 2 Code", '<>%1', DimensionValue.Code);
        if not GLEntry.Find('-') then begin
            // CorrectionObject."Error Text" := StrSubstNo(COLbl, GLEntry."Global Dimension 2 Code", DimensionValue.Code);
            CorrectionObject."Cost Object Status" := CorrectionObject."Cost Object Status"::"Completed Prev.";
            CorrectionObject.Corrected := true;
            CorrectionObject."Cost Journal Posted" := true;
            CorrectionObject.Modify();
            EXIT(FALSE);
        end
        else begin
            CorrectionObject."Cost Object Status" := CorrectionObject."Cost Object Status"::"In Process";
            CorrectionObject."Old Dimension Value" := GLEntry."Global Dimension 2 Code";
            CorrectionObject.Modify();
        end;


        repeat
            GlAcc.Reset();
            GlAcc.Get(GLEntry."G/L Account No.");
            //check if cost center is put as default dimension on g/l acc.
            DefaultDimension.Reset();
            DefaultDimension.SetRange("Table ID", 15);
            DefaultDimension.SetRange("No.", GlAcc."No.");
            DefaultDimension.SetRange("Dimension Code", 'KOSTENSTELLE');
            DefaultDimension.SetFilter("Value Posting", '<>%1', DefaultDimension."Value Posting"::"No Code");
            If DefaultDimension.Find('-') then begin
                CorrectionObject."Cost Object Status" := CorrectionObject."Cost Object Status"::Error;
                CorrectionObject."Error Text" := StrSubstNo(GlAccDefDimLbl, GlAcc."No.");
                CorrectionObject.Modify();
                exit(false);
            end;
            //Robin says that update all glentries BS & PL
            //if GlAcc."Income/Balance" = GlAcc."Income/Balance"::"Income Statement" then begin
            if GlAcc.Blocked then begin
                CorrectionObject."Error Text" := StrSubstNo(GLAccLbl, GLEntry, GLEntry."G/L Account No.");
                CorrectionObject."Cost Object Status" := CorrectionObject."Cost Object Status"::Error;
                CorrectionObject.Modify();
                EXIT(FALSE);
            end;
            GLEntry.Mark(true);
        // end;
        until GLEntry.Next() = 0;

        GLEntry.MarkedOnly(true);



        CorrectionObject."Old Dimension Value" := GLEntry."Global Dimension 2 Code";
        CorrectionObject."New Dimension Value" := DimensionValue.Code;
        CorrectionObject."Error Text" := '';
        CorrectionObject.Modify();

        //Creating Cost Object Correction Task
        //and Schedule execution in Job queie entrie
        if CorrectionObject."Cost Object Status" = CorrectionObject."Cost Object Status"::"In Process" then begin

            CreateDimensionCorrection(GLEntry, CorrectionObject);
            EXIT(TRUE);
        end
        //if new dimension equal old dimension check if gl/entries with document exist on cost entry table
        else begin
            if CorrectionObject."Cost Object Status" = CorrectionObject."Cost Object Status"::"Completed Prev." then begin
                CreateCostJournal := False;
                GLEntry.Reset();
                GLEntry.SetRange("Document No.", CorrectionObject."Document No.");
                if GLEntry.Find('-') then begin
                    repeat
                        CostEntries.Reset();
                        CostEntries.SetRange("Document No.", CorrectionObject."Document No.");
                        CostEntries.SetRange("G/L Entry No.", GLEntry."Entry No.");
                        CostEntries.SetFilter("Batch Name", '<>%1', '');
                        CostEntries.SetCurrentKey("Entry No.");
                        //check if in cost entries table already exist record for that g/l entry
                        //ad is it last in the table
                        if CostEntries.Find('+') then begin
                            if CostEntries."Cost Object Code" <> DimensionValue.Code then begin
                                GlAcc.Reset();
                                GlAcc.Get(GLEntry."G/L Account No.");
                                if GlAcc."Income/Balance" = GlAcc."Income/Balance"::"Income Statement" then begin
                                    GLEntry.Mark(true);
                                    CreateCostJournal := True;
                                end;
                            end;
                        end
                        else begin//if there is no cost entries for cost object 1. cost entry
                            GlAcc.Reset();
                            GlAcc.Get(GLEntry."G/L Account No.");
                            if GlAcc."Income/Balance" = GlAcc."Income/Balance"::"Income Statement" then begin
                                GLEntry.Mark(true);
                                CreateCostJournal := True;
                            end;
                        end;
                    until GLEntry.Next() = 0;
                end;


                GLEntry.MarkedOnly(true);

                if not CreateCostJournal then begin
                    CorrectionObject."Cost Journal Posted" := true;
                    CorrectionObject."Cost Object Status" := CorrectionObject."Cost Object Status"::Complete;
                    CorrectionObject.Modify();
                    exit(true);
                end;
                exit(TRUE);
            end;
        end;
        EXIT(TRUE);
    end;


    /// <summary>
    /// Create Job Queue Entry for Purchase DOcument
    /// </summary>
    /// <param name="GLEntry"></param>
    /// <param name="CorrectionObject"></param>
    procedure CreateDimensionCorrection(var GLEntry: Record "G/L Entry"; var CorrectionObject: Record CostObjectCorrectionPurchTable)
    var
        DimensionCorrection: Record "Dimension Correction";
        DimensionCorrectionMgt: Codeunit "Dimension Correction Mgt";
        DimensionCorrectionChange: Record "Dim Correction Change";
        JobQueueEntry: Record "Job Queue Entry";
    begin

        DimensionCorrectionMgt.VerifyCanStartJob(DimensionCorrection);
        DimensionCorrectionMgt.CreateCorrectionFromSelection(GLEntry, DimensionCorrection);

        DimensionCorrectionMgt.VerifyCanValidateDimensionCorrection(DimensionCorrection);


        DimensionCorrectionChange.SetRange("Dimension Correction Entry No.", DimensionCorrection."Entry No.");

        if DimensionCorrectionChange.Find('-') then begin
            DimensionCorrectionChange.Validate("Dimension Code", 'KOSTENTRÄGER');
            DimensionCorrectionChange.Validate("New Value", DimensionValue.Code);

            DimensionCorrectionChange.Modify();



        end
        else begin

            DimensionCorrectionChange.Reset();
            DimensionCorrectionChange.Init();
            DimensionCorrectionChange.Validate("Dimension Correction Entry No.", DimensionCorrection."Entry No.");
            DimensionCorrectionChange."Change Type" := DimensionCorrectionChange."Change Type"::Add;
            DimensionCorrectionChange.Validate("Dimension Code", 'KOSTENTRÄGER');
            DimensionCorrectionChange.Validate("New Value", DimensionValue.Code);

            DimensionCorrectionChange.Insert();

        end;

        DimensionCorrection.Status := DimensionCorrection.Status::Draft;
        DimensionCorrection."Update Analysis Views" := true;
        DimensionCorrection.CostCorrectionID := CorrectionObject."Entry No.";
        DimensionCorrection.Modify();
        CreateUniqueJobQueue(Codeunit::"Dim Correction Run", DimensionCorrection, JobQueueEntry, CorrectionObject);
        Commit();

    end;

    procedure GetDimensionCOrrectionEntryNo(): Integer
    var
        DimensionCorrection: Record "Dimension Correction";
    begin
        DimensionCorrection.Reset();
        DimensionCorrection.SetCurrentKey("Entry No.");
        if DimensionCorrection.Find('+') then
            exit(DimensionCorrection."Entry No." + 1)
        else
            exit(1);

    end;

    procedure MarkedRecordsForProcessing(): Boolean
    var
        CostObjectCorrection: Record CostObjectCorrectionPurchTable;
        Counter: Integer;
        JobQueueEntry: Record "Job Queue Entry";
        JobQueDisp: Codeunit "Job Queue Dispatcher";
        RecCount: integer;

    begin


        //check if there is some unfinished process
        JobQueueEntry.Reset();
        JobQueueEntry.SetRange("Job Queue Category Code", 'COC-P');
        JobQueueEntry.SetFilter(Status, '%1|%2', JobQueueEntry.Status::Ready, JobQueueEntry.Status::"On Hold");
        if JobQueueEntry.Find('-') then
            exit(false);



        //create new processes
        Counter := 0;
        RecCount := 10;
        CostObjectCorrection.Reset();
        CostObjectCorrection.SetRange("Cost Object Status", CostObjectCorrection."Cost Object Status"::Open);
        CostObjectCorrection.SetCurrentKey("Entry No.");
        if CostObjectCorrection.Find('-') then begin
            if CostObjectCorrection.Count < 10 then
                RecCount := CostObjectCorrection.Count;
            repeat
                Counter := Counter + 1;
                if IsValidated(CostObjectCorrection) then begin
                    CostObjectCorrection."Cost Object Status" := "Cost Correction Status"::"In Process";
                    CostObjectCorrection.USERID := UserId;
                    CostObjectCorrection.Modify(true);
                    Commit();
                    CorrectDimension(CostObjectCorrection);
                end;
                CostObjectCorrection.Next();
            until Counter = RecCount;
            exit(true);
        end;
        exit(False);
    end;



    procedure IsValidated(var CostObjectCorrection: Record CostObjectCorrectionPurchTable): Boolean
    var
        SalesInvoiceRec: Record "Sales Invoice Header";
        PurchaseInvoice: Record "Purch. Inv. Header";
        PurchaseInvLinesRec: Record "Purch. Inv. Line";
        GlEntries: Record "G/L Entry";

    begin

        if CostObjectCorrection."Cost Object Status" <> CostObjectCorrection."Cost Object Status"::Open then
            exit(false);


        //check if purchase document exist in database
        PurchaseInvoice.Reset();
        PurchaseInvoice.SetRange("No.", CostObjectCorrection."Document No.");
        if PurchaseInvoice.Find('-') then begin
            if not CheckIfPostingDateInRange(PurchaseInvoice."Posting Date") then begin
                CostObjectCorrection."Error Text" := StrSubstNo(PostingDateLbl, CostObjectCorrection."Document No.");
                CostObjectCorrection."Cost Object Status" := "Cost Correction Status"::Error;
                CostObjectCorrection.Modify();
                exit(false);
            end;



            //check if cost center already assigned on document
            PurchaseInvLinesRec.Reset();
            PurchaseInvLinesRec.SetRange("Document No.", PurchaseInvoice."No.");
            PurchaseInvLinesRec.SetFilter("Shortcut Dimension 1 Code", '<>%1', '');
            if PurchaseInvLinesRec.Find('-') then begin
                CostObjectCorrection."Error Text" := CostCenterAssignedLbl;
                CostObjectCorrection."Cost Object Status" := "Cost Correction Status"::Error;
                CostObjectCorrection.Modify();
                exit(false);

            end;

        end
        else begin
            SalesInvoiceRec.Reset();
            SalesInvoiceRec.SetRange("No.", CostObjectCorrection."Document No.");
            if SalesInvoiceRec.Find('-') then begin
                CostObjectCorrection."Error Text" := StrSubstNo(SwitchedDocLbl, CostObjectCorrection."Document No.", 'Purchase Invoice', 'Sales Invoice');
                CostObjectCorrection."Cost Object Status" := "Cost Correction Status"::Error;
                CostObjectCorrection.Modify();
                exit(false);
            end
            else begin
                CostObjectCorrection."Error Text" := StrSubstNo(GLEntryLbl, CostObjectCorrection."Document No.");
                CostObjectCorrection."Cost Object Status" := "Cost Correction Status"::Error;
                CostObjectCorrection.Modify();
                exit(false);
            end;

        end;

        //check if new cost object are equal like the od one
        GlEntries.Reset();
        GlEntries.SetRange("Document No.", CostObjectCorrection."Document No.");
        GlEntries.SetFilter("Global Dimension 2 Code", '=%1', CostObjectCorrection."New Dimension Value");
        if GlEntries.Find('-') then begin
            CostObjectCorrection."Error Text" := '';
            CostObjectCorrection."Cost Object Status" := "Cost Correction Status"::"Completed Prev.";
            CostObjectCorrection.Modify();
            exit(false);
        end;

        exit(true);
    end;

    local procedure CreateUniqueJobQueue(CodeunitID: Integer; var DimensionCorrection: Record "Dimension Correction"; var JobQueueEntry: Record "Job Queue Entry"; var CorrectionObject: Record CostObjectCorrectionPurchTable)
    var
        JobQueueExist: Boolean;
        EarliestDT: DateTime;

    begin

        JobQueueEntry.Init();
        JobQueueEntry."Cost Correction Record ID" := DimensionCorrection."Entry No.";
        JobQueueEntry."Object Type to Run" := JobQueueEntry."Object Type to Run"::Codeunit;
        JobQueueEntry."Object ID to Run" := CodeunitID;
        JobQueueEntry."Maximum No. of Attempts to Run" := 1;
        JobQueueEntry."Recurring Job" := false;
        JobQueueEntry."Record ID to Process" := DimensionCorrection.RecordId;
        JobQueueEntry."No. of Minutes between Runs" := 1;
        JobQueueEntry."Rerun Delay (sec.)" := 5;
        JobQueueEntry."Job Timeout" := 5 * 60000;
        JobQueueEntry."Job Queue Category Code" := 'COC-P';
        JobQueueEntry."Cost Record ID" := CorrectionObject."Entry No.";
        Clear(JobQueueEntry."Error Message");
        Clear(JobQueueEntry."Error Message Register Id");
        JobQueueEntry.Description := CopyStr(StrSubstNo(JobQueueEntryDescTxt, DimensionCorrection."Entry No."), 1, MaxStrLen(JobQueueEntry.Description));
        JobQueueEntry.Insert(true);

        JobQueueEntry.Validate("Earliest Start Date/Time", CurrentDateTime);
        JobQueueEntry.SetStatus(JobQueueEntry.Status::Ready);
        JobQueueEntry.Modify();

        CorrectionObject."Job Queue Entry ID" := JobQueueEntry.ID;
        CorrectionObject."DimensionCorrection ID" := DimensionCorrection."Entry No.";
        CorrectionObject.Modify();

    end;

    /// <summary>
    /// if error occur send error email
    /// </summary>
    procedure SendErrorEmail(var CorrectionObject: Record CostObjectCorrectionTable)
    var
        EmailSubject: text;
        EmailBody: text;
        Recipients: List of [Text];
        RecipientsCC: List of [Text];
        RecipientsBCC: List of [Text];
    begin
        CLear(EmailCU);
        Clear(EmailMessageCU);
        Clear(Recipients);
        Clear(RecipientsCC);
        Clear(RecipientsBCC);

        Recipients.Add('robin.schmid@carvolution.com');
        Recipients.Add('ljubisa.vukoje@holyerp.rs');


        EmailSubject := 'Cost Correction Error';
        EmailBody := 'Cost Correction Error : \nRecord ID : ' + Format(CorrectionObject."Entry No.") + '\n' + 'Error : ' + GetLastErrorText();


        EmailMessageCU.Create(Recipients, EmailSubject, EmailBody, true, RecipientsCC, RecipientsBCC);
        EmailCU.Send(EmailMessageCU, Enum::"Email Scenario"::Reminder);

    end;


    procedure CheckIfPostingDateInRange(var PostingDate: Date): Boolean
    begin
        GLSetup.Get();

        if (PostingDate < GLSetup."Allow Posting From") OR (PostingDate > GLSetup."Allow Posting To") then
            EXIT(false);

        EXIT(true);

    end;

    procedure FixCorrectionLineAmt(var CostCorrLine: record CostObjectCorrectionPurchTable)
    var
        GlEntries: record "G/L Entry";
        CostEntries: record "Cost Entry";
        GlAcc: Record "G/L Account";
        PostCorrJournalCU: Codeunit "Post Cost Corr. To Cost Acc.";
        ErrorLbl: Label 'You can use this function only for record where field Dimension Correction ID <> 0 !';

    begin

        // if CostCorrLine."DimensionCorrection ID" = 0 then
        //     error(ErrorLbl);

        CostCorrLine.CalcFields(CostEntriesAmt);
        CostCorrLine.PurchInvAmt := CostCorrLine.CalcInvAmount();
        CostCorrLine.DiffAmount := CostCorrLine.PurchInvAmt - CostCorrLine.CostEntriesAmt;
        CostCorrLine.Modify();

        //if is open than it is not corrected yet
        if CostCorrLine."Cost Object Status" = CostCorrLine."Cost Object Status"::Open then
            exit;

        GlEntries.Reset();
        GlEntries.SetRange("Document No.", CostCorrLine."Document No.");
        GlEntries.SetFilter(GlEntries."Global Dimension 1 Code", '<>%1', '');
        if GlEntries.Find('-') then begin
            //delete cost object entries if for some reason cost center and cost object are posted to cost acc.
            GlEntries.Reset();
            GlEntries.SetRange("Document No.", CostCorrLine."Document No.");
            GlEntries.SetFilter(GlEntries."Global Dimension 2 Code", '<>%1', '');
            if GlEntries.Find('-') then begin
                GlEntries.Reset();
                GlEntries.SetRange("Document No.", CostCorrLine."Document No.");
                GlEntries.SetFilter("Global Dimension 2 Code", '<>%1', '');
                if GlEntries.Find('-') then begin
                    repeat
                        CostEntries.Reset();
                        CostEntries.SetRange("G/L Entry No.", GlEntries."Entry No.");
                        CostEntries.SetRange("Document No.", GlEntries."Document No.");
                        CostEntries.SetRange("Cost Object Code", GlEntries."Global Dimension 2 Code");
                        if CostEntries.Find('-') then begin
                            repeat
                                UpdateCostRegister(CostEntries);
                                CostEntries.Delete();
                            until CostEntries.Next() = 0;
                        end;

                    until GlEntries.Next() = 0;
                end;
            end;
            CostCorrLine."Error Text" := CostCenterAssignedLbl;
            CostCorrLine."Cost Object Status" := "Cost Correction Status"::Error;
            CostCorrLine.Modify();
            exit;
        end;

        if CostCorrLine.DiffAmount = 0 then begin
            CostCorrLine."Error Text" := '';
            CostCorrLine."Cost Object Status" := CostCorrLine."Cost Object Status"::"Completed Prev.";
            CostCorrLine.Modify();
            exit;
        end;

        GlEntries.Reset();
        GlEntries.SetRange("Document No.", CostCorrLine."Document No.");
        GlEntries.SetRange("Gen. Posting Type", GlEntries."Gen. Posting Type"::Purchase);
        if not GlEntries.Find('-') then
            exit;

        GlEntries.Reset();
        GlEntries.SetRange("Document No.", CostCorrLine."Document No.");
        GlEntries.SetFilter("Global Dimension 2 Code", '<>%1', '');

        if GlEntries.Find('-') then begin
            DimensionValue.Reset();
            DimensionValue.SetRange("Dimension Value Type", DimensionValue."Dimension Value Type"::Standard);
            DimensionValue.SetRange(Code, GlEntries."Global Dimension 2 Code");

            //find CravolutionID in dimension value table
            if not DimensionValue.Find('-') then begin
                CostCorrLine."Error Text" := StrSubstNo(CarvolutionIDLbl, CostCorrLine.Carvolution_ID);
                CostCorrLine."Cost Object Status" := CostCorrLine."Cost Object Status"::Error;
                CostCorrLine.Modify();
                EXIT;
            end;


            if DimensionValue.Carvolution_ID <> CostCorrLine.Carvolution_ID then begin
                CostCorrLine."Error Text" := DiffCostObjectOnEntriesLbl;
                CostCorrLine."Cost Object Status" := CostCorrLine."Cost Object Status"::Error;
                CostCorrLine.Modify();
                exit;
            end;

            CostCorrLine."Error Text" := '';
            CostCorrLine.Modify();
        end;



        GlEntries.Reset();
        GlEntries.SetRange("Document No.", CostCorrLine."Document No.");
        GlEntries.SetRange("Global Dimension 2 Code", CostCorrLine."New Dimension Value");
        if GlEntries.Find('-') then begin
            repeat
                CostEntries.Reset();
                CostEntries.SetRange("G/L Entry No.", GlEntries."Entry No.");
                if CostEntries.Find('-') then begin
                    repeat
                        UpdateCostRegister(CostEntries);
                        CostEntries.Delete();
                    until CostEntries.Next() = 0;

                end;
            until GlEntries.Next() = 0;
        end;


        if GlEntries.Find('-') then begin
            repeat
                GlAcc.Reset();
                GlAcc.Get(GlEntries."G/L Account No.");
                if GlAcc."Income/Balance" = GlAcc."Income/Balance"::"Income Statement" then
                    GlEntries.Mark(true);

            until GlEntries.Next() = 0;

            GlEntries.MarkedOnly(true);

            Clear(PostCorrJournalCU);
            PostCorrJournalCU.CreateCostJournalPurchase(GlEntries, CostCorrLine);

        end;

    end;


    procedure UpdateCostRegister(var CostEntry: Record "Cost Entry")
    var
        TCostRegister: Record "Cost Register";
    begin
        TCostRegister.Reset();
        TCostRegister.SetFilter("From Cost Entry No.", '>=%1', CostEntry."Entry No.");
        if TCostRegister.Find('-') then begin
            TCostRegister."Deleted From COC" := true;
            TCostRegister.Modify();
        end
        else begin
            TCostRegister.Reset();
            TCostRegister.SetFilter(TCostRegister."From Cost Entry No.", '=<%1', CostEntry."Entry No.");
            if TCostRegister.Find('-') then begin
                TCostRegister."Deleted From COC" := true;
                TCostRegister.Modify();
            end;
        end;
    end;

    var
        GLSetup: Record "General Ledger Setup";
        EmailMessageCU: Codeunit "Email Message";
        EmailCU: Codeunit Email;

        NoSeriesMgt: Codeunit NoSeriesManagement;
        DimensionValue: Record "Dimension Value";
        PostCostJournal: Codeunit "Post Cost Corr. To Cost Acc.";
        CarvolutionIDLbl: label 'CarvolutionID %1 does not exist in database !';
        GLEntryLbl: label 'Document No. %1 does not exist in database !';
        COLbl: label 'Old Dimension %1 , should be <> than %2 !';
        GLAccLbl: Label 'Gl. Account %1 is blocked !';
        JobQueueIsRunningErr: Label 'The job queue entry is already running. Stop the existing job queue entry to schedule a new one.';
        JobQueueCategoryCodeTxt: Label 'DIMCORRECT', Locked = true;
        JobQueueEntryDescTxt: Label 'Dimension Correction - %1.', Comment = '%1 - Unique number of the correction';
        UpdateCOLbl: Label 'Cost object %1 must be <> than cost object %2 !';
        SwitchedDocLbl: label 'Document No. %1 is not %2.If you want to make a Cost Object Correction for %3, please use the respective action  !';
        ProcessAlreadyRunningLbl: Label 'Process Triggered by: %1 . Try later or contact triggering user.';
        PostingDateLbl: Label 'Posting Date for Document No. %1 is not in allowed posting range in G/L !';

        CostCenterAssignedLbl: Label 'You can not assign cost object on the document where cost center is already assigned !';
        DiffCostObjectOnEntriesLbl: label 'Cost object in Gl/Entries and Cost Entries must be the same !';
        GlAccDefDimLbl: label 'Default dimension error for G/L Acc. %1 !';

}