tableextension 50017 SalesReceivablesSetup extends "Sales & Receivables Setup"
{

    fields
    {
        field(50013; "G/L Account VISA"; code[20])
        {
            CaptionML = ENU = 'G/L Account VISA', DES = 'Fibukonto VISA';
            TableRelation = "G/L Account";
            DataClassification = ToBeClassified;
        }
        field(50014; "G/L Account Mastercard"; code[20])
        {
            CaptionML = ENU = 'G/L Account Mastercard', DES = 'Fibukonto Mastercard';
            TableRelation = "G/L Account";
            DataClassification = ToBeClassified;
        }
        field(50018; "Auxiliary Account Creditcard"; code[20])
        {
            CaptionML = ENU = 'Auxiliary Account Creditcard', DES = 'Durchlaufkonto Kreditkarten';
            TableRelation = "G/L Account";
            DataClassification = ToBeClassified;
        }

        field(50019; "Creditcard Clearing New"; Boolean)
        {
            CaptionML = ENU = 'Creditcard Clearing New', DES = 'Kreditkartenabwicklung Neu';
            DataClassification = ToBeClassified;


        }

    }


}