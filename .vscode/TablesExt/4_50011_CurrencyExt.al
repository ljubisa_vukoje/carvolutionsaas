tableextension 50011 ExtCurrency extends Currency
{

    fields
    {
        field(50000; "General Bank Account Code"; code[20])
        {

            TableRelation = "Bank Account";
            CaptionML = ENU = 'General Bank Account Code', DES = 'Haupt Bankkonto Code';
        }
    }

}