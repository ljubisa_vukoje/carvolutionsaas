tableextension 50019 CustLedgEntries extends "Cust. Ledger Entry"
{

    fields
    {
        //SCM1
        field(50000; "Debt Collection"; boolean)
        {
            CaptionML = ENU = 'Debt Collection', DES = 'Inkasso';
            Editable = true;
            DataClassification = ToBeClassified;
            trigger OnValidate()
            begin
                if "Debt Collection" = false then
                    "Transfered to Debt Collection" := 0D;
                Rec.Modify();
            end;


        }

        //SCM1
        field(50001; "Transfered to Debt Collection"; Date)
        {
            CaptionML = ENU = 'Transfered to Debt Collection', DES = 'An Inkasso übergeben';
            Editable = true;
            DataClassification = ToBeClassified;
            trigger OnValidate()
            begin
                Rec.Modify();
            end;

        }

        field(50002; "Last Registered Reminder Level"; Integer)
        {
            CaptionML = ENU = 'Last Registered Reminder Level', DES = 'Letzte registrierte Mahnstufe';
            Editable = false;
            DataClassification = ToBeClassified;


        }
        field(50003; "Exclude From Reminder"; Boolean)
        {
            CaptionML = ENU = 'Exclude From Reminder', DES = 'Von Erinnerung ausschließen';
            Editable = true;
            DataClassification = ToBeClassified;

            trigger OnValidate()
            begin
                if Rec."Remaining Amount" <> 0 then begin
                    /* if Rec."Document Type" = Rec."Document Type"::Invoice then begin */
                    if xRec."Exclude From Reminder" <> Rec."Exclude From Reminder" then
                        if xRec."Exclude From Reminder" = true then begin
                            if Rec."On Hold" = 'HOL' then
                                Rec.Validate(Rec."On Hold", '');
                            Rec.UserRSId := '';
                            Rec."Reminder Stop Notes" := '';
                        end

                        else begin
                            if Rec."On Hold" = '' then
                                Rec.Validate(Rec."On Hold", 'HOL');
                            Rec.UserRSId := USERID;
                        end;

                end
                else
                    Error(Err_001, Rec."Entry No.", FieldCaption(Rec."Document No."), Rec."Document No.");

                Rec.Modify();
            end;
        }

        field(50004; "End Date of the Dunning Stop"; Date)
        {
            CaptionML = ENU = 'End Date of the Dunning Stop', DES = 'Enddatum des Mahnstops';
            Editable = true;
            DataClassification = ToBeClassified;

            trigger OnValidate()
            begin
                Rec.Modify();
            end;

        }

        field(50005; "UserRSId"; Code[50])
        {
            CaptionML = ENU = 'User Id of Reminder Stop', DES = 'User Id für  Mahnstop';
        }

        field(50006; "Reminder Stop Notes"; Text[250])
        {
            CaptionML = ENU = 'Reminder Stop Notes', DES = 'Mahnstop Notiz';

            trigger OnValidate()
            begin
                Rec.Modify();
            end;

        }



    }

    var
        issue: report "Issue Reminders";
        Err_001: Label 'The selected entry %1 for %2: %3 does not have an overdue amount, please select a line where an open amount exists.';
}

