tableextension 50025 CostAccountingSetup extends "Cost Accounting Setup"
{
    fields
    {
        field(50000; "Cost Journal Nos."; Code[20])
        {
            Caption = 'Cost Journal Nos.';
            TableRelation = "No. Series";
        }

    }
}