tableextension 50012 ExtCompanyInfo extends "Company Information"
{

    fields
    {
        field(50010; Picture2; Blob)
        {
            ObsoleteState = Pending;
            CaptionML = ENU = 'Picture 2', DES = 'Bild 2';

        }
        field(50011; "Picture 2"; Blob)
        {
            CaptionML = ENU = 'Picture 2', DES = 'Bild 2';

        }
        /// <summary>
        /// ISO20022 BEGIN
        /// </summary>
        field(82800; "ISO active"; Boolean)
        {
            DataClassification = ToBeClassified;
            CaptionML = DEU = 'ISO umgestellt', ENU = 'changed to ISO', ITA = 'cambiato a ISO', DES = 'ISO aktiv', ITS = 'Cambiato a ISO', FRS = 'ISO converti';
        }
        field(82801; "Credit Transfer Msg. Nos."; COde[10])
        {
            TableRelation = "No. Series";
            DataClassification = ToBeClassified;
            CaptionML = ENU = 'Credit Transfer Msg. Nos.', DES = 'Kreditübertragung Nachr.-IDs', ITS = 'Nr. messaggi bonifico', FRS = 'N° msg. virement';
        }

        field(82802; "Software Version"; Text[50])
        {
            DataClassification = ToBeClassified;
            CaptionML = DEU = 'Software Version', ENU = 'Software Version', ITA = 'Versione software', DES = 'Software Version', FRS = 'Version de logiciel';
        }
        field(82803; "Software Name"; Text[50])
        {
            DataClassification = ToBeClassified;
            CaptionML = DEU = 'Software Name', ENU = 'Software name', ITA = 'Nome del Software', DES = 'Software Name', FRS = 'Nom du logiciel';
        }
        field(82804; "Manufacturer"; Text[70])
        {
            DataClassification = ToBeClassified;
            CaptionML = ENU = 'Producer company', DES = 'Hersteller Firma', ITS = 'firma produttore', FRS = 'Entreprise de fabrication';
        }
    }

    procedure CheckCompanyNameIsFarieOrFarieFleet(): Boolean
    begin
        if GET then
            if (Rec.Name in ['Farie AG', 'Farie Fleet AG']) then
                exit(true)
            else
                exit(false);
    end;

    procedure SetIsVisible(var pIsVisible: Boolean)
    begin
        pIsVisible := CheckCompanyNameIsFarieOrFarieFleet();
    end;
    /// <summary>
    /// ISO20022  END
    /// </summary>


}