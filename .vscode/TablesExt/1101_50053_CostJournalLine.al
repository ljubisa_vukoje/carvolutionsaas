tableextension 50053 "Cost Journal Line" extends "Cost Journal Line"
{
    fields
    {
        field(50010; "Created From COC"; Boolean)
        {
            DataClassification = ToBeClassified;
        }

        field(50011; "Customer No."; Code[50])
        {
            TableRelation = Customer;
            Caption = 'Customer No.';

        }
        field(50012; "Customer Name"; Text[250])
        {
            Caption = 'Customer Name';
        }

        field(50013; "Vendor No."; Code[50])
        {
            TableRelation = Vendor;
            Caption = 'Vendor No.';

        }
        field(50014; "Vendor Name"; Text[250])
        {
            Caption = 'Vendor Name';

        }
        field(50015; "Document Type"; Enum "Gen. Journal Document Type")
        {
            Caption = 'Document Type';
        }
    }

}