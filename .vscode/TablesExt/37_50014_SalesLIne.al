tableextension 50100 SalesLinexExt extends "Sales Line"

{


    fields
    {

        field(50100; "Header Id"; Guid)
        {
            TableRelation = "Sales Header".SystemId;
        }

        field(50101; "Deferral  Start Date"; Date)
        {

        }
        field(50102; "Kostenträger Code"; Code[50])
        {


        }

    }



    trigger OnAfterInsert()
    var
        deferralCodeHeader: Record "Deferral Header";

    begin
        if ("Deferral Code" <> '') and ("Deferral  Start Date" <> 0D) then begin
            deferralCodeHeader.Reset();
            deferralCodeHeader.SetRange("Document No.", "Document No.");
            deferralCodeHeader.SetRange("Line No.", "Line No.");
            if (deferralCodeHeader.FindFirst()) then begin
                deferralCodeHeader.Validate("Start Date", "Deferral  Start Date");
                deferralCodeHeader.Validate("No. of Periods", 1);
                deferralCodeHeader.Modify();

                deferralCodeHeader.CalculateSchedule();

            end;


        end;
    end;


}