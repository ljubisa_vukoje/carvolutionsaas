tableextension 50021 SalesInvoiceHeader extends "Sales Invoice Header"
{

    fields
    {
        field(50003; "Remark"; Text[500])
        {
            CaptionML = ENU = 'Remark', DES = 'Anmerkung', DEU = 'Anmerkung', FRS = 'Remarque';


        }
        field(50004; WebOrder; Boolean)
        {
            CaptionML = ENU = 'Carvolution order', DES = 'Carvolution order', DEU = 'Carvolution order', FRS = 'Carvolution order';


        }
        field(50005; "APISalesPersonCode"; Code[20])
        {
            CaptionML = ENU = 'APISalesPersonCode', DES = 'APISalesPersonCode', DEU = 'APISalesPersonCode', FRS = 'APISalesPersonCode';
            trigger OnValidate()
            var
                SalesPerson: Record "Salesperson/Purchaser";
            begin
                if (APISalesPersonCode <> '') then begin
                    SalesPerson.SetRange(Code, APISalesPersonCode);
                    if not SalesPerson.Find('-') then begin
                        SalesPerson.Init();
                        SalesPerson.Code := APISalesPersonCode;
                        SalesPerson.Insert();
                    end;

                end;
                Validate("Salesperson Code", APISalesPersonCode);

            end;

        }
    }

}