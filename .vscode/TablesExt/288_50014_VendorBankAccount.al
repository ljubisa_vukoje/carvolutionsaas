tableextension 50014 "Vendor Bank Account" extends "Vendor Bank Account"
{
    fields
    {

        field(82800; SEPA; Boolean)
        {


        }

        field(50000; "Payment Form Enum"; enum "Payment Form")
        {
            CaptionML = ENU = 'Paymet Form ISO', DES = 'Zahlungsformular ISO';

        }

        modify("IBAN")
        {
            trigger OnAfterValidate()
            var
                BankDirectory: Record "Bank Directory";
                clearingNoCOde: Code[5];
                ClearingNo: Integer;
                IbanNO: Text;
                QRIbanNO: Text;
                DummyOK: Boolean;
                VendorLE: Record "Vendor Bank Account";
                QRIbanSwiftBank: record "QR Iban Swift Bank List";
            begin

                if IBAN = '' then begin
                    "SWIFT Code" := '';
                    "Clearing No." := '';
                    Modify(true);
                    exit;
                end;

                if "Payment Form" <> "Payment Form"::"Bank Payment Domestic" then begin
                    "SWIFT Code" := '';
                    "Clearing No." := '';
                    Modify(true);
                    exit;
                end;

                IbanNo := IBAN;
                IbanNO := DelChr(IbanNO, '=', ' ');
                QRIbanNO := CopyStr(IbanNO, 5, 1);
                clearingNoCOde := CopyStr(IbanNO, 5, 5);
                DummyOK := EVALUATE(ClearingNo, clearingNoCOde);

                if (QRIbanNO = '3')
                then begin

                    QRIbanSwiftBank.Reset();
                    QRIbanSwiftBank.SetRange("Bank Code", clearingNoCOde);
                    If QRIbanSwiftBank.FindFirst() then begin
                        "SWIFT Code" := QRIbanSwiftBank."Swift Code";
                        //BankDirectory.Reset();
                        //BankDirectory.SetRange("Swift address", QRIbanSwiftBank."Swift Code");
                        //IF BankDirectory.FindFirst() then begin
                        //   Validate("Clearing No.", BankDirectory."Clearing Main Office");

                        //end;
                        Modify(true);
                        exit;
                    end
                    else begin
                        "SWIFT Code" := '';
                        "Clearing No." := '';
                        Modify(true);
                    end;
                end
                else begin

                    BankDirectory.Reset();
                    BankDirectory.SetRange("Clearing No.", FORMAT(ClearingNo));
                    IF BankDirectory.FindFirst() then begin
                        "SWIFT Code" := BankDirectory."SWIFT Address";
                        // Validate("Clearing No.", BankDirectory."Clearing No.");
                        Modify(true);
                    end
                    else begin
                        "SWIFT Code" := '';
                        "Clearing No." := '';
                        Modify(true);
                    end;
                end;
            end;
        }
    }

}