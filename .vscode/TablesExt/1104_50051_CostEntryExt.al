tableextension 50051 "Cost Entry Ext" extends "Cost Entry"
{
    fields
    {
        field(50100; "Customer No."; Code[50])
        {
            TableRelation = Customer;
            Caption = 'Customer No.';

        }
        field(50101; "Customer Name"; Text[250])
        {
            Caption = 'Customer Name';
        }

        field(50102; "Vendor No."; Code[50])
        {
            TableRelation = Vendor;
            Caption = 'Vendor No.';

        }
        field(50103; "Vendor Name"; Text[250])
        {
            Caption = 'Vendor Name';

        }

        field(50104; "Document Type"; Text[50])
        {
            ObsoleteState = Removed;
            ObsoleteReason = 'Changed from Text data type to Enum which is not allowed';
            Caption = 'Document Type';
        }

        field(50105; "Document Type New"; Enum "Gen. Journal Document Type")
        {
            Caption = 'Document Type';
        }

        field(50106; "Voided Comment"; Text[50])
        {
            Caption = 'Voided Comment';
        }

    }

}