
// ISO 20022
// - Neues Feld
//   - "IBAN Recipient"
// - Neue Globale VAR
//   - CompanyInfo 
//   ljubisa.vukoje@holyerp.rs 29.09.2021
tableextension 50016 ExtESRSetup extends "ESR Setup"
{

    fields
    {

        /// <summary>
        /// ISO 20022 BEGIN
        /// </summary>
        field(82800; "IBAN Recipient"; Code[50])
        {
            DataClassification = ToBeClassified;
            CaptionML = ENU = 'DTA Sender IBAN', DES = 'IBAN Empfänger', ITS = 'IBAN Mittente DTA',
            FRS = 'DTA IBAN expéditeur';

            trigger OnValidate()
            begin
                CompanyInfo.CheckIBAN("IBAN Recipient");
            end;
        }


        /// <summary>
        /// ISO 20022 END
        /// </summary>

    }



    var

        CompanyInfo: Record "Company Information";

}