tableextension 50024 CostObject extends "Cost Object"
{
    fields
    {
        field(50000; VIN; Code[30])
        {
            CaptionML = ENU = 'Vehicle VIN', DES = 'Fahrzeug-VIN';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup("Dimension Value"."Vehicle VIN" where(Code = field(Code)));

        }
        field(50001; "Vehicle Status"; Code[30])
        {
            CaptionML = ENU = 'Vehicle Status', DES = 'Fahrzeugstatus';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup("Dimension Value".Vehicle_Status where(Code = field(Code)));

        }
        field(50002; "Vehicle Category"; Code[30])
        {
            CaptionML = ENU = 'Vehicle Category', DES = 'Fahrzeugkategorie';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup("Dimension Value".Vehicle_Category where(Code = field(Code)));

        }
        field(50003; "Carvolution Model ID"; Code[10])
        {
            CaptionML = ENU = 'Carvolution Model ID', DES = 'Carvolution-Modell-ID';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup("Dimension Value".CarvolutionModel_ID where(Code = field(Code)));

        }
        field(50004; "Carvolution ID"; Code[10])
        {
            CaptionML = ENU = 'Carvolution ID', DES = 'Carvolution-ID';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup("Dimension Value".Carvolution_ID where(Code = field(Code)));

        }
        field(50005; "Licence plate"; Code[20])
        {
            CaptionML = ENU = 'Licence plate', DES = 'Nummernschild';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup("Dimension Value"."Licence plate" where(Code = field(Code)));

        }
    }

}