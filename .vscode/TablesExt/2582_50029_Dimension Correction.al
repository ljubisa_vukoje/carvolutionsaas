tableextension 50029 "Dimension Correction" extends "Dimension Correction"
{
    fields
    {
        field(50000; "Posted To Cost Acc."; Boolean)
        {

        }

        field(50001; "CostCorrectionID"; Integer)
        {
            TableRelation = CostObjectCorrectionTable;
        }

        field(50002; "Updated Dim. On Posted Doc."; Boolean)
        {

        }
    }
}