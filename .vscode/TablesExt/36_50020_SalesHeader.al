tableextension 50020 SalesHeader extends "Sales Header"
{

    fields
    {
        modify("No.")
        {
            trigger OnAfterValidate()
            var
                DummyInt: Integer;
            begin
                if StrLen("No.") > 8 then
                    error(LblDocNo);

                EVALUATE(DummyInt, "No.");
            end;

        }
        field(50003; "Remark"; Text[500])
        {
            CaptionML = ENU = 'Remark', DES = 'Anmerkung', DEU = 'Anmerkung', FRS = 'Remarque';


        }
        field(50004; WebOrder; Boolean)
        {
            CaptionML = ENU = 'Carvolution order', DES = 'Carvolution order', DEU = 'Carvolution order', FRS = 'Carvolution order';


        }
        field(50005; "APISalesPersonCode"; Code[20])
        {
            CaptionML = ENU = 'APISalesPersonCode', DES = 'APISalesPersonCode', DEU = 'APISalesPersonCode', FRS = 'APISalesPersonCode';
            trigger OnValidate()
            var
                SalesPerson: Record "Salesperson/Purchaser";
            begin
                if (APISalesPersonCode <> '') then begin
                    SalesPerson.SetRange(Code, APISalesPersonCode);
                    if not SalesPerson.Find('-') then begin
                        SalesPerson.Init();
                        SalesPerson.Code := APISalesPersonCode;
                        SalesPerson.Insert();
                    end;
                    Validate("Salesperson Code", APISalesPersonCode);
                end;
            end;

        }

        field(50006; "APISalesDocumentDate"; Date)
        {
            CaptionML = ENU = 'APISalesDocumentDate', DES = 'APISalesDocumentDate', DEU = 'APISalesDocumentDate', FRS = 'APISalesDocumentDate';
            trigger OnValidate()
            begin
                Validate("Document Date", APISalesDocumentDate);
            end;
        }
        field(50007; CarvolutionVehicleId; Code[20])
        {
            CaptionML = ENU = 'CarvolutionVehicleId', DES = 'CarvolutionVehicleId', DEU = 'CarvolutionVehicleId', FRS = 'CarvolutionVehicleId';
            trigger OnValidate()
            var
                DimensionValues: Record "Dimension Value";
            begin

                DimensionValues.Reset();
                DimensionValues.SetRange("Dimension Code", 'KOSTENTRÄGER');
                DimensionValues.SetRange(Carvolution_ID, CarvolutionVehicleId);
                DimensionValues.SetRange("Dimension Value Type", DimensionValues."Dimension Value Type"::Standard);
                if not DimensionValues.Find('-') then
                    Error(StrSubstNo('CarvolutionId %1 does not exist in database !', CarvolutionVehicleId))
                else
                    Rec."Shortcut Dimension 2 Code" := DimensionValues.Code;
            end;
        }

    }

    procedure SetNewDocumentDate(var SalesHeader: Record "Sales Header"; NewDocumentDate: Date)
    begin
        SalesHeader.Validate(SalesHeader."Document Date", NewDocumentDate);
    end;

    var
        LblDocNo: Label 'Document number must contains max 8 characters !';
}