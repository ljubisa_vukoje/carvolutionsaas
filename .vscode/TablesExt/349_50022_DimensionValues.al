tableextension 50022 DimensionValueTableExt extends "Dimension Value"
{

    fields
    {
        /// <summary>
        /// Old data field
        /// </summary>
        field(50000; "CarvolutionID"; Integer)
        {
            ObsoleteState = Removed;
            ObsoleteReason = 'Wrong data type';
            CaptionML = ENU = 'Carvolution ID Old', DES = 'Carvolution ID Old';
        }
        field(50001; "CarvolutionModelID"; Integer)
        {
            ObsoleteState = Removed;
            ObsoleteReason = 'Wrong data type';
            CaptionML = ENU = 'Carvolution Model ID Old', DES = 'Carvolution Model ID Old';
        }
        /// <summary>
        /// out of use
        /// </summary>
        field(50002; "Vehicle Category"; Enum "Vehicle Category")
        {
            ObsoleteState = Removed;
            ObsoleteReason = 'Duplicate field';
            CaptionML = ENU = 'Vehicle Category Old', DES = 'Fahrzeugkategorie Old';

        }
        /// <summary>
        /// out of use
        /// </summary>
        field(50003; "Vehicle Status"; Enum "Vehicle Status")
        {
            ObsoleteState = Removed;
            ObsoleteReason = 'Duplicate field';
            CaptionML = ENU = 'Vehicle Status Old', DES = 'Fahrzeugstatus Old';
        }
        /// <summary>
        /// New data fields
        /// </summary>
        field(50010; "Carvolution_ID"; Code[30])
        {
            CaptionML = ENU = 'Carvolution ID', DES = 'Carvolution ID';
        }
        field(50011; "CarvolutionModel_ID"; Code[10])
        {
            CaptionML = ENU = 'Carvolution Model ID', DES = 'Carvolution Model ID';
        }
        field(50004; "Vehicle VIN"; Code[25])
        {
            CaptionML = ENU = 'Vehicle VIN', DES = 'Fahrzeug-VIN';
        }
        field(50005; "Brand Name"; Text[40])
        {
            CaptionML = ENU = 'Brand Name', DES = 'Markenname';
        }

        field(50006; "Model Name"; Text[100])
        {
            CaptionML = ENU = 'Model Name', DES = 'Modellname';

        }
        field(50007; "Vehicle_Category"; code[30])
        {
            CaptionML = ENU = 'Vehicle Category', DES = 'Fahrzeugkategorie';
        }
        field(50008; "Vehicle_Status"; Code[30])
        {
            CaptionML = ENU = 'Vehicle Status', DES = 'Fahrzeugstatus';
        }

        field(50009; "Licence plate"; Text[25])
        {
            CaptionML = ENU = 'Licence plate', DES = 'Nummernschild';

        }
    }

    keys
    {
        key(Carolution_ID; Carvolution_ID)
        {

        }
        key(LicencePlate; "Licence plate")
        {

        }
    }


}