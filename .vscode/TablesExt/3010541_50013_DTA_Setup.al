
//*************************************
/* ISO 20022
 - Table umbenannt
  - von "DTA Setup" nach "DTA/ISO Setup"
- Feld DTA/EZAG
  - umbenannt von DTA/EZAG nach DTA/EZAG/ISO
  - zusätzliche Option ISO
- Neue Felder 
  - ab ID 82800
- Neue globale VAR
  - ISOMgt
- "Backup Folder" -OnValidate und -OnLookUp
  - Explorer Ordnerauswahl */
//*************************************
//UPGRADE FROM NAV 2018 Vukoje Ljubisa 23.9.2021 ljubisa.vukoje@holyerp.rs

tableextension 50013 EXtDTASetup extends "DTA Setup"
{

    fields
    {

        field(50000; "DTA/EZAG/ISO"; Enum "DTA/EZAG/ISO")
        {
            trigger OnValidate()
            begin

                case "DTA/EZAG/ISO" of
                    "DTA/EZAG/ISO"::DTA:
                        "DTA/EZAG" := "DTA/EZAG"::DTA;
                    "DTA/EZAG/ISO"::EZAG:
                        "DTA/EZAG" := "DTA/EZAG"::EZAG;
                end;

            end;

        }
        /// <summary>
        /// BEGIN ISO 20022
        /// </summary>
        field(82800; "ISO Sender IBAN"; Code[50])
        {

        }
        field(82801; "ISO SWIFT Code (BIC)"; Code[50])
        {

        }
        field(82802; "ISO File Folder"; Text[100])
        {

            trigger OnValidate()
            begin
                i := StrLen("ISO File Folder");
                if i <> 0 then begin
                    if copystr("ISO File Folder", i, 1) <> '\' then
                        "ISO File Folder" := "ISO File Folder" + '\';

                end;
            end;

            trigger OnLookup()
            begin


            end;

        }
        field(82803; "ISO Filename"; Text[100])
        {

        }
        /// <summary>
        /// END ISO 20022
        /// </summary>

    }

    var
        i: Integer;
        DummyVariant: Variant;
        TmpFolderName: Text;


}