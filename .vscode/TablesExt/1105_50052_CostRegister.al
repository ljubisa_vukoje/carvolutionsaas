tableextension 50052 "Cost Register" extends "Cost Register"
{
    fields
    {
        field(50010; "Created From COC"; Boolean)
        {
            DataClassification = ToBeClassified;
        }
        field(50011; "Deleted From COC"; Boolean)
        {
            DataClassification = ToBeClassified;
        }
    }
}