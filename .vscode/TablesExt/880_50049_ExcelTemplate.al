/// <summary>
/// TableExtension ExcelTemplate (ID 50049) extends Record Excel Template Storage.
/// </summary>
tableextension 50049 ExcelTemplate extends "Excel Template Storage"
{
    fields
    {
        field(50000; SheetName; Text[50])
        {
            Caption = 'Excel Sheet Name';
        }
        field(50001; DataExchangeCode; code[10])
        {
            caption = 'Data Exchange Definition';
            TableRelation = "Data Exch. Def";
        }
        field(50002; DefaultFileName; text[30])
        {
            Caption = 'Default Filename for Export';
        }
    }
}