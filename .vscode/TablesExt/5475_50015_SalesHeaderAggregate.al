tableextension 50015 SalesHeaderAggExtTable extends "Sales Invoice Entity Aggregate"
{

    fields
    {
        field(50000; "Posting No. Series"; Code[20])
        {
            Caption = 'Posting No. Series';
            TableRelation = "No. Series";

        }

        field(50001; "Posting No."; Code[20])
        {
            Caption = 'Posting No.';
        }

        field(50002; "Last Posting No."; Code[20])
        {
            Caption = 'Last Posting No.';
            Editable = false;
            TableRelation = "Sales Invoice Header";
        }
        field(50003; "Remark"; Text[500])
        {
            CaptionML = ENU = 'Remark', DES = 'Anmerkung', DEU = 'Anmerkung', FRS = 'Remarque';


        }
        field(50004; WebOrder; Boolean)
        {
            CaptionML = ENU = 'Carvolution order', DES = 'Carvolution order', DEU = 'Carvolution order', FRS = 'Carvolution order';


        }
        field(50007; CarvolutionVehicleId; Code[20])
        {
            CaptionML = ENU = 'CarvolutionVehicleId', DES = 'CarvolutionVehicleId', DEU = 'CarvolutionVehicleId', FRS = 'CarvolutionVehicleId';

        }

    }

}