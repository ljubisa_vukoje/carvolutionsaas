tableextension 50026 "Job Queue Entry" extends "Job Queue Entry"
{
    fields
    {
        field(50000; "Cost Correction Record ID"; Integer)
        {
            Caption = 'Dim. Correction Record ID';

        }
        field(50001; "Cost Record ID"; Integer)
        {
            Caption = 'Cost Table Correction Record ID';

        }

    }


    trigger OnDelete()
    var
        CostCorrection: Record CostObjectCorrectionTable;
        CostCorrectionPO: Record CostObjectCorrectionPurchTable;
    begin

        CostCorrection.Reset();
        CostCorrection.SetRange("Job Queue Entry ID", ID);
        if CostCorrection.Find('-') then begin
            if Status in [Status::Ready, Status::"In Process"] then
                error(DeleteErrorLbl);

            if Confirm(StrSubstNo(ConfirmDeletionLbl, 'Sales')) then
                CostCorrection.Delete();
        end;

        CostCorrectionPO.Reset();
        CostCorrectionPO.SetRange("Job Queue Entry ID", ID);
        if CostCorrectionPO.Find('-') then begin
            if Status in [Status::Ready, Status::"In Process"] then
                error(DeleteErrorLbl);

            if Confirm(StrSubstNo(ConfirmDeletionLbl, 'Purchase')) then
                CostCorrectionPO.Delete();
        end;
    end;



    var

        DeleteErrorLbl: Label 'Set Job Queue Entry Status to "OnHold"!';
        ConfirmDeletionLbl: Label 'There is related record in Cost Corection table %1 , which will be also deleted.\Confirm deletion ?';
        OperationCancelLbl: Label 'Operation canceled !';

}