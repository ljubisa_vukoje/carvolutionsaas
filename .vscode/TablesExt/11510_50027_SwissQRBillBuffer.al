/// <summary>
/// Vukoje Ljubisa
/// 08/25/2022
/// Extension for print Swiss QR Bill payment slip with invoice
/// remaining amount
/// </summary>
tableextension 50027 SwissQRBillBufferExt extends "Swiss QR-Bill Buffer"
{


    procedure SetSourceRecordRemainingAmount(CustomerLedgerEntryNo: Integer)
    var
        CustLedgerEntry: Record "Cust. Ledger Entry";
        PaymentMethod: Record "Payment Method";
        SwissQRBillLayout: Record "Swiss QR-Bill Layout";
        Customer: Record Customer;
    begin
        CustLedgerEntry.Get(CustomerLedgerEntryNo);
        "Customer Ledger Entry No." := CustLedgerEntry."Entry No.";
        "Source Record Printed" := CustLedgerEntry."Payment Reference" <> '';
        CustLedgerEntry.CalcFields("Remaining Amount");
        Amount := CustLedgerEntry."Remaining Amount";
        Currency := GetCurrency(CustLedgerEntry."Currency Code");
        Validate("Payment Reference", CustLedgerEntry."Payment Reference");
        if "Payment Reference" <> '' then
            if StrLen(DelChr("Payment Reference")) = 27 then
                Validate("IBAN Type", "IBAN Type"::"QR-IBAN")
            else
                Validate("IBAN Type", "IBAN Type"::"IBAN");

        if CustLedgerEntry."Payment Method Code" <> '' then
            if PaymentMethod.Get(CustLedgerEntry."Payment Method Code") then;

        LoadLayout(PaymentMethod."Swiss QR-Bill Layout");
        SwissQRBillLayout.Get("QR-Bill Layout");

        if CustLedgerEntry."Customer No." <> '' then
            if Customer.Get(CustLedgerEntry."Customer No.") then
                SetUltimateDebitorInfo(Customer);

        LoadSourceRecordBillingInformation(SwissQRBillLayout);
        if PaymentMethod."Swiss QR-Bill Bank Account No." <> '' then
            ValidateBankAccountIBAN(PaymentMethod."Swiss QR-Bill Bank Account No.");
    end;

    procedure LoadSourceRecordBillingInformation(SwissQRBillLayout: Record "Swiss QR-Bill Layout")
    var
        SwissQRBillBillingInfo: Record "Swiss QR-Bill Billing Info";
    begin
        if "Customer Ledger Entry No." <> 0 then
            if SwissQRBillLayout."Billing Information" <> '' then
                if SwissQRBillBillingInfo.Get(SwissQRBillLayout."Billing Information") then
                    "Billing Information" := GetBillingInformationRemainingAmount("Customer Ledger Entry No.");
    end;

    procedure ValidateBankAccountIBAN(BankAccountNo: Code[20])
    var
        BankAccount: Record "Bank Account";
    begin
        if BankAccount.Get(BankAccountNo) then
            if "IBAN Type" = "IBAN Type"::"QR-IBAN" then begin
                BankAccount.TestField("Swiss QR-Bill IBAN");
                IBAN := SwissQRBillMgt.FormatIBAN(BankAccount."Swiss QR-Bill IBAN");
            end else begin
                BankAccount.TestField(IBAN);
                IBAN := SwissQRBillMgt.FormatIBAN(BankAccount.IBAN);
            end;
    end;

    procedure SetUltimateDebitorInfo(Customer: Record Customer)
    var
        Language: Codeunit Language;
        LanguageId: Integer;
    begin
        "UDebtor Name" := CopyStr(Customer.Name, 1, MaxStrLen("Creditor Name"));
        "UDebtor Street Or AddrLine1" := CopyStr(Customer.Address, 1, MaxStrLen("Creditor Street Or AddrLine1"));
        "UDebtor BuildNo Or AddrLine2" := CopyStr(Customer."Address 2", 1, MaxStrLen("Creditor BuildNo Or AddrLine2"));
        "UDebtor Postal Code" := CopyStr(Customer."Post Code", 1, MaxStrLen("Creditor Postal Code"));
        "UDebtor City" := Customer.City;
        "UDebtor Country" := CopyStr(Customer."Country/Region Code", 1, MaxStrLen("Creditor Country"));

        LanguageId := Language.GetLanguageId(Customer."Language Code");

        case true of
            GetLanguagesIdDEU().Contains(Format(LanguageId)):
                "Language Code" := Language.GetLanguageCode(GetLanguageIdDEU());
            GetLanguagesIdFRA().Contains(Format(LanguageId)):
                "Language Code" := Language.GetLanguageCode(GetLanguageIdFRA());
            GetLanguagesIdITA().Contains(Format(LanguageId)):
                "Language Code" := Language.GetLanguageCode(GetLanguageIdITA());
            else
                "Language Code" := Language.GetLanguageCode(GetLanguageIdENU());
        end;
    end;

    internal procedure GetLanguageIdENU(): Integer
    begin
        exit(1033); // en-us
    end;

    internal procedure GetLanguageIdDEU(): Integer
    begin
        exit(2055); // de-ch
    end;

    internal procedure GetLanguageIdFRA(): Integer
    begin
        exit(4108); // fr-ch
    end;

    internal procedure GetLanguageIdITA(): Integer
    begin
        exit(2064); // it-ch
    end;

    internal procedure GetLanguagesIdDEU(): Text
    begin
        exit('1031|3079|2055');
    end;

    internal procedure GetLanguagesIdFRA(): Text
    begin
        exit('1036|2060|3084|4108');
    end;

    internal procedure GetLanguagesIdITA(): Text
    begin
        exit('1040|2064');
    end;

    internal procedure GetLanguageCodeENU(): Code[10]
    var
        Language: Codeunit Language;
    begin
        exit(Language.GetLanguageCode(GetLanguageIdENU()));
    end;

    internal procedure GetCurrency(CurrencyCode: Code[10]): Code[10]
    var
        GeneralLedgerSetup: Record "General Ledger Setup";
    begin
        if CurrencyCode = '' then begin
            GeneralLedgerSetup.Get();
            exit(GeneralLedgerSetup."LCY Code");
        end;

        exit(CurrencyCode);
    end;

    procedure LoadLayout(QRBillLayoutCode: Code[20])
    var
        SwissQRBillLayout: Record "Swiss QR-Bill Layout";
        OldLayoutCode: Code[20];
    begin
        OldLayoutCode := "QR-Bill Layout";

        if QRBillLayoutCode <> '' then
            "QR-Bill Layout" := QRBillLayoutCode
        else
            if "QR-Bill Layout" = '' then
                "QR-Bill Layout" := GetDefaultLayoutCodeRemainingAmount();
        SwissQRBillLayout.Get("QR-Bill Layout");

        if (OldLayoutCode <> "QR-Bill Layout") or (xRec."QR-Bill Layout" <> "QR-Bill Layout") then begin
            if not "Source Record Printed" then begin
                "IBAN Type" := SwissQRBillLayout."IBAN Type";
                Validate("Payment Reference Type", SwissQRBillLayout."Payment Reference Type");
            end;
            ValidateIBANRemainingAmount();
            SetCompanyInformationRemainingAmount();
            LoadAdditionalInformationRemainingAmount(SwissQRBillLayout);
        end;
    end;

    procedure LoadAdditionalInformationRemainingAmount(SwissQRBillLayout: Record "Swiss QR-Bill Layout")
    begin
        "Billing Information" := '';
        "Unstructured Message" := SwissQRBillLayout."Unstr. Message";
        LoadSourceRecordBillingInformationRemainingAmount(SwissQRBillLayout);
        "Alt. Procedure Name 1" := SwissQRBillLayout."Alt. Procedure Name 1";
        "Alt. Procedure Value 1" := SwissQRBillLayout."Alt. Procedure Value 1";
        "Alt. Procedure Name 2" := SwissQRBillLayout."Alt. Procedure Name 2";
        "Alt. Procedure Value 2" := SwissQRBillLayout."Alt. Procedure Value 2";
    end;

    local procedure LoadSourceRecordBillingInformationRemainingAmount(SwissQRBillLayout: Record "Swiss QR-Bill Layout")
    begin
        if "Customer Ledger Entry No." <> 0 then
            if SwissQRBillLayout."Billing Information" <> '' then
                if SwissQRBillBillingInfo.Get(SwissQRBillLayout."Billing Information") then
                    "Billing Information" := GetBillingInformationRemainingAmount("Customer Ledger Entry No.");
    end;

    procedure GetBillingInformationRemainingAmount(CustomerLedgerEntryNo: Integer): Text[140]
    var
        CompanyInformation: Record "Company Information";
        CustLedgerEntry: Record "Cust. Ledger Entry";
        SalesInvoiceLine: Record "Sales Invoice Line";
        ServiceInvoiceLine: Record "Service Invoice Line";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        ServiceInvoiceHeader: Record "Service Invoice Header";
        TempVATAmountLine: Record "VAT Amount Line" temporary;
        PaymentTermsCode: Code[10];
    begin
        CompanyInformation.Get();
        CustLedgerEntry.Get(CustomerLedgerEntryNo);
        if CustLedgerEntry."Document Type" = CustLedgerEntry."Document Type"::Invoice then
            case true of
                SalesInvoiceHeader.Get(CustLedgerEntry."Document No."):
                    begin
                        PaymentTermsCode := SalesInvoiceHeader."Payment Terms Code";
                        SalesInvoiceLine.CalcVATAmountLines(SalesInvoiceHeader, TempVATAmountLine);
                    end;
                FindServiceInvoiceFromLedgerEntryRemainingAmount(ServiceInvoiceHeader, CustLedgerEntry):
                    begin
                        PaymentTermsCode := ServiceInvoiceHeader."Payment Terms Code";
                        ServiceInvoiceLine.CalcVATAmountLines(ServiceInvoiceHeader, TempVATAmountLine);
                    end;
            end;

        exit(
            GetDocumentBillingInfo(
                CustLedgerEntry."Document No.",
                CustLedgerEntry."Document Date",
                CompanyInformation."VAT Registration No.",
                CustLedgerEntry."Posting Date",
                TempVATAmountLine,
                PaymentTermsCode));
    end;

    procedure GetDocumentBillingInfo(DoumentNo: Code[20]; DocumentDate: Date; VATRegNo: Text; VATDate: Date; var TempVATAmountLine: Record "VAT Amount Line"; PaymentTermsCode: Code[10]): Text[140]
    var
        TempSwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail" temporary;
        SwissQRBillBillingInfo: Record "Swiss QR-Bill Billing Info";
    begin
        if SwissQRBillBillingInfo."Document No." then
            AddDetailsIfNotBlanked(TempSwissQRBillBillingDetail, TempSwissQRBillBillingDetail."Tag Type"::"Document No.", DoumentNo);
        if SwissQRBillBillingInfo."Document Date" then
            AddDetailsIfNotBlanked(
                TempSwissQRBillBillingDetail,
                TempSwissQRBillBillingDetail."Tag Type"::"Document Date", FormatDate(DocumentDate));
        if SwissQRBillBillingInfo."VAT Number" then
            AddDetailsIfNotBlanked(
                TempSwissQRBillBillingDetail,
                TempSwissQRBillBillingDetail."Tag Type"::"VAT Registration No.", FormatVATRegNo(VATRegNo));
        if SwissQRBillBillingInfo."VAT Date" then
            AddDetailsIfNotBlanked(
                TempSwissQRBillBillingDetail,
                TempSwissQRBillBillingDetail."Tag Type"::"VAT Date", FormatDate(VATDate));
        if SwissQRBillBillingInfo."VAT Details" then
            AddDetailsIfNotBlanked(
                TempSwissQRBillBillingDetail,
                TempSwissQRBillBillingDetail."Tag Type"::"VAT Details", GetDocumentVATDetails(TempVATAmountLine));
        if SwissQRBillBillingInfo."Payment Terms" then
            AddDetailsIfNotBlanked(
                TempSwissQRBillBillingDetail,
                TempSwissQRBillBillingDetail."Tag Type"::"Payment Terms", GetDocumentPaymentTerms(PaymentTermsCode));

        exit(CreateBillingInfoString(TempSwissQRBillBillingDetail, 'S1'));
    end;

    procedure AddDetailsIfNotBlanked(var SwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail"; TagType: Enum "Swiss QR-Bill Billing Detail"; DetailsValue: Text)
    begin
        if DetailsValue <> '' then
            SwissQRBillBillingDetail.AddBufferRecord('S1', TagType, DetailsValue, '');
    end;

    procedure FindServiceInvoiceFromLedgerEntryRemainingAmount(var ServiceInvoiceHeader: Record "Service Invoice Header"; CustLedgerEntry: Record "Cust. Ledger Entry"): Boolean
    begin
        CustLedgerEntry.TestField("Entry No.");
        if CustLedgerEntry."Document Type" = CustLedgerEntry."Document Type"::Invoice then begin
            ServiceInvoiceHeader.SetRange("No.", CustLedgerEntry."Document No.");
            exit(ServiceInvoiceHeader.FindFirst());
        end;
    end;

    procedure SetCompanyInformationRemainingAmount()
    var
        CompanyInformation: Record "Company Information";
        TempCustomer: Record Customer temporary;
    begin
        CompanyInformation.Get();
        TempCustomer.Name := CompanyInformation.Name;
        TempCustomer.Address := CompanyInformation.Address;
        TempCustomer."Address 2" := CompanyInformation."Address 2";
        TempCustomer."Post Code" := CompanyInformation."Post Code";
        TempCustomer.City := CompanyInformation.City;
        TempCustomer."Country/Region Code" := CompanyInformation."Country/Region Code";
        SetCreditorInfoRemainingAmount(TempCustomer);
    end;

    procedure SetCreditorInfoRemainingAmount(Customer: Record Customer)
    begin
        "Creditor Name" := CopyStr(Customer.Name, 1, MaxStrLen("Creditor Name"));
        "Creditor Street Or AddrLine1" := CopyStr(Customer.Address, 1, MaxStrLen("Creditor Street Or AddrLine1"));
        "Creditor BuildNo Or AddrLine2" := CopyStr(Customer."Address 2", 1, MaxStrLen("Creditor BuildNo Or AddrLine2"));
        "Creditor Postal Code" := CopyStr(Customer."Post Code", 1, MaxStrLen("Creditor Postal Code"));
        "Creditor City" := Customer.City;
        "Creditor Country" := CopyStr(Customer."Country/Region Code", 1, MaxStrLen("Creditor Country"));
    end;

    local procedure GetDefaultLayoutCodeRemainingAmount(): Code[20]
    var
        SwissQRBillSetup: Record "Swiss QR-Bill Setup";
    begin
        SwissQRBillSetup.Get();
        SwissQRBillSetup.TestField("Default Layout");
        exit(SwissQRBillSetup."Default Layout");
    end;

    procedure ValidateIBANRemainingAmount()
    var
        CompanyInformation: Record "Company Information";
    begin
        OnBeforeValidateIBANRemainingAmount(IBAN);
        CompanyInformation.Get();
        if "IBAN Type" = "IBAN Type"::"QR-IBAN" then begin
            CompanyInformation.TestField("Swiss QR-Bill IBAN");
            IBAN := SwissQRBillMgt.FormatIBAN(CompanyInformation."Swiss QR-Bill IBAN");
        end else begin
            CompanyInformation.TestField(IBAN);
            IBAN := SwissQRBillMgt.FormatIBAN(CompanyInformation.IBAN);
        end;
        OnAfterValidateIBANRemainingAmount(IBAN);
    end;

    procedure CreateBillingInfoString(var SwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail"; FormatCode: Code[10]) Result: Text[140]
    var
        TempSwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail" temporary;
        AddText: Text;
    begin
        // compress by Tag Codes: "tag-codeX/value/tag-codeX/value2" to "tag-codeX/value1;value2"
        SwissQRBillBillingDetail.SetRange("Format Code", FormatCode);
        if SwissQRBillBillingDetail.FindSet() then
            repeat
                TempSwissQRBillBillingDetail.SetRange("Tag Code", SwissQRBillBillingDetail."Tag Code");
                if TempSwissQRBillBillingDetail.FindFirst() then begin
                    TempSwissQRBillBillingDetail."Tag Value" += ';' + SwissQRBillBillingDetail."Tag Value";
                    TempSwissQRBillBillingDetail.Modify();
                end else begin
                    TempSwissQRBillBillingDetail.SetRange("Tag Code");
                    if TempSwissQRBillBillingDetail.FindLast() then;
                    TempSwissQRBillBillingDetail.AddBufferRecord(FormatCode, SwissQRBillBillingDetail."Tag Code", SwissQRBillBillingDetail."Tag Value");
                end;
            until SwissQRBillBillingDetail.Next() = 0;

        // print in a following format: formatcode/tagcode1/value1/tagcode2/value2
        TempSwissQRBillBillingDetail.Reset();
        if TempSwissQRBillBillingDetail.FindSet() then begin
            Result := CopyStr('//' + FormatCode, 1, MaxStrLen(Result));
            repeat
                AddText := '/' + TempSwissQRBillBillingDetail."Tag Code" + '/' + TempSwissQRBillBillingDetail."Tag Value";
                if StrLen(Result + AddText) < 140 then
                    Result += '/' + TempSwissQRBillBillingDetail."Tag Code" + '/' + TempSwissQRBillBillingDetail."Tag Value";
            until TempSwissQRBillBillingDetail.Next() = 0;
        end;
    end;

    procedure GetDocumentVATDetails(var SourceVATAmountLine: Record "VAT Amount Line") Result: Text
    var
        TempTargetVATAmountLine: Record "VAT Amount Line" temporary;
    begin
        // format P vat% on A amount as "P:A"
        // single should be printed only % "P" (but zero % goes as "0:A")
        SumUpVATAmountLinesByVATPct(TempTargetVATAmountLine, SourceVATAmountLine);
        if TempTargetVATAmountLine.FindSet() then
            if (TempTargetVATAmountLine.Count() > 1) or (TempTargetVATAmountLine."VAT %" = 0) then
                repeat
                    if Result <> '' then
                        Result += ';';
                    Result += FormatAmount(TempTargetVATAmountLine."VAT %") + ':' + FormatAmount(TempTargetVATAmountLine."VAT Base");
                until TempTargetVATAmountLine.Next() = 0
            else
                exit(FormatAmount(TempTargetVATAmountLine."VAT %"));
    end;

    procedure SumUpVATAmountLinesByVATPct(var TargetVATAmountLine: Record "VAT Amount Line"; var SourceVATAmountLine: Record "VAT Amount Line")
    begin
        // reason: rounding goes to a different lines with the same VAT %. need to sum-up
        if SourceVATAmountLine.FindSet() then
            repeat
                TargetVATAmountLine.SetRange("VAT %", SourceVATAmountLine."VAT %");
                if TargetVATAmountLine.FindFirst() then begin
                    TargetVATAmountLine."VAT Base" += SourceVATAmountLine."VAT Base";
                    TargetVATAmountLine.Modify();
                end else begin
                    TargetVATAmountLine.SetRange("VAT %");
                    TargetVATAmountLine := SourceVATAmountLine;
                    TargetVATAmountLine.Insert();
                end;
            until SourceVATAmountLine.Next() = 0;
        TargetVATAmountLine.Reset();
    end;

    procedure GetDocumentPaymentTerms(PmtTermsCode: Code[10]): Text
    var
        PaymentTerms: Record "Payment Terms";
        DummyDateFormula: DateFormula;
    begin
        // format P discount% on D days as "P:D"
        if PmtTermsCode <> '' then
            if PaymentTerms.Get(PmtTermsCode) then
                if PaymentTerms."Discount Date Calculation" <> DummyDateFormula then
                    exit(Format(PaymentTerms."Discount %") + ':' + Format(CalcDate(PaymentTerms."Discount Date Calculation", WorkDate()) - WorkDate()));
    end;

    procedure DrillDownBillingInfo(BillingInfoText: Text)
    var
        TempSwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail" temporary;
        SwissQRBillBillingDetailsPage: Page "Swiss QR-Bill Billing Details";
    begin
        if BillingInfoText <> '' then
            if ParseBillingInfo(TempSwissQRBillBillingDetail, BillingInfoText) then begin
                SwissQRBillBillingDetailsPage.SetBuffer(TempSwissQRBillBillingDetail);
                SwissQRBillBillingDetailsPage.RunModal();
            end else
                Message(UnsupportedFormatMsg);
    end;

    procedure ParseBillingInfo(var SwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail"; BillingInfoText: Text): Boolean
    var
        TextList: List of [Text];
        TagValue: Text;
        FormatCode: Code[10];
        TagCode: Code[10];
        SlashString: Text;
        i: Integer;
    begin
        // source text = FormatCode/TagCode1/TagValue1/TagCode2/TagValue2
        if BillingInfoText <> '' then begin
            if BillingInfoText.StartsWith('//') then
                BillingInfoText := DelStr(BillingInfoText, 1, 2);
            SlashString := '*SLASH*';
            BillingInfoText := BillingInfoText.Replace('\\', '\');
            BillingInfoText := BillingInfoText.Replace('\/', SlashString);
            TextList := BillingInfoText.Split('/');
            for i := 1 to TextList.Count() do
                TextList.Set(i, TextList.Get(i).Replace(SlashString, '/'));

            if TextList.Count() > 2 then begin
                FormatCode := CopyStr(TextList.Get(1), 1, MaxStrLen(FormatCode));
                TextList.RemoveAt(1);
                if FormatCode <> '' then
                    while TextList.Count() > 1 do begin
                        TagCode := CopyStr(TextList.Get(1), 1, MaxStrLen(TagCode));
                        TagValue := TextList.Get(2);
                        if (TagCode <> '') and (TagValue <> '') then
                            SwissQRBillBillingDetail.AddBufferRecord(FormatCode, TagCode, TagValue);
                        TextList.RemoveRange(1, 2);
                    end;
            end;
            ParseDetailsDescriptions(SwissQRBillBillingDetail, FormatCode);
        end;
        exit(not SwissQRBillBillingDetail.IsEmpty())
    end;

    procedure ParseDetailsDescriptions(var SwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail"; FormatCode: Code[10])
    var
        TempSwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail" temporary;
    begin
        // try parse Swico "S1" format details
        // split multi values (e.g. VAT Details "P1:A1;P2:A2") into separate detail buffer records
        // copy source into buffer2 splitting multi-values
        if SwissQRBillBillingDetail.FindSet(true) then
            repeat
                case SwissQRBillBillingDetail."Tag Type" of
                    SwissQRBillBillingDetail."Tag Type"::"Document Date":
                        TempSwissQRBillBillingDetail.AddBufferRecord(FormatCode, SwissQRBillBillingDetail."Tag Type", SwissQRBillBillingDetail."Tag Value", ParseDate(SwissQRBillBillingDetail."Tag Value"));
                    SwissQRBillBillingDetail."Tag Type"::"VAT Registration No.":
                        TempSwissQRBillBillingDetail.AddBufferRecord(FormatCode, SwissQRBillBillingDetail."Tag Type", SwissQRBillBillingDetail."Tag Value", FormatVATRegNo(SwissQRBillBillingDetail."Tag Value"));
                    SwissQRBillBillingDetail."Tag Type"::"VAT Date":
                        TempSwissQRBillBillingDetail.AddBufferRecord(FormatCode, SwissQRBillBillingDetail."Tag Type", SwissQRBillBillingDetail."Tag Value", ParseDate(SwissQRBillBillingDetail."Tag Value"));
                    SwissQRBillBillingDetail."Tag Type"::"VAT Purely On Import":
                        TempSwissQRBillBillingDetail.AddBufferRecord(FormatCode, SwissQRBillBillingDetail."Tag Type", SwissQRBillBillingDetail."Tag Value", ParseAmount(SwissQRBillBillingDetail."Tag Value"));
                    SwissQRBillBillingDetail."Tag Type"::"VAT Details":
                        ParseVATDetails(TempSwissQRBillBillingDetail, SwissQRBillBillingDetail."Tag Value");
                    SwissQRBillBillingDetail."Tag Type"::"Payment Terms":
                        ParsePaymentTermsDetails(TempSwissQRBillBillingDetail, SwissQRBillBillingDetail."Tag Value");
                    SwissQRBillBillingDetail."Tag Type"::Unknown:
                        TempSwissQRBillBillingDetail.AddBufferRecord(FormatCode, SwissQRBillBillingDetail."Tag Code", SwissQRBillBillingDetail."Tag Value");
                    else
                        TempSwissQRBillBillingDetail.AddBufferRecord(FormatCode, SwissQRBillBillingDetail."Tag Type", SwissQRBillBillingDetail."Tag Value", SwissQRBillBillingDetail."Tag Value");
                end;
            until SwissQRBillBillingDetail.Next() = 0;

        // copy buffer2 back to source
        SwissQRBillBillingDetail.DeleteAll();
        if TempSwissQRBillBillingDetail.FindSet(true) then
            repeat
                SwissQRBillBillingDetail := TempSwissQRBillBillingDetail;
                SwissQRBillBillingDetail.Insert();
            until TempSwissQRBillBillingDetail.Next() = 0;
    end;

    procedure ParseVATDetails(var SwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail"; SourceText: Text)
    var
        PairsTextList: List of [Text];
        DetailsTextList: List of [Text];
        Percent: Decimal;
        Amount: Decimal;
    begin
        // source text = "P1:A1;P2:A2;P3"
        if SourceText <> '' then begin
            PairsTextList := SourceText.Split(';');
            while PairsTextList.Count() > 0 do begin
                DetailsTextList := PairsTextList.Get(1).Split(':');
                case DetailsTextList.Count() of
                    1:
                        if Evaluate(Percent, DetailsTextList.Get(1), 9) then
                            SwissQRBillBillingDetail.AddBufferRecord(
                                'S1', SwissQRBillBillingDetail."Tag Type"::"VAT Details",
                                PairsTextList.Get(1), StrSubstNo(VATDetailsTotalTxt, Percent));
                    2:
                        if Evaluate(Percent, DetailsTextList.Get(1), 9) and Evaluate(Amount, DetailsTextList.Get(2)) then
                            SwissQRBillBillingDetail.AddBufferRecord(
                                'S1', SwissQRBillBillingDetail."Tag Type"::"VAT Details",
                                PairsTextList.Get(1), StrSubstNo(VATDetailsTxt, Percent, Amount));
                end;
                PairsTextList.RemoveAt(1);
            end;
        end;
    end;

    procedure ParsePaymentTermsDetails(var SwissQRBillBillingDetail: Record "Swiss QR-Bill Billing Detail"; SourceText: Text)
    var
        PairsTextList: List of [Text];
        DetailsTextList: List of [Text];
        Percent: Decimal;
        Days: Integer;
    begin
        // source text = "P1:D1;P2:D2"
        if SourceText <> '' then begin
            PairsTextList := SourceText.Split(';');
            while PairsTextList.Count() > 0 do begin
                DetailsTextList := PairsTextList.Get(1).Split(':');
                if DetailsTextList.Count() = 2 then
                    if Evaluate(Percent, DetailsTextList.Get(1), 9) and Evaluate(Days, DetailsTextList.Get(2)) then
                        SwissQRBillBillingDetail.AddBufferRecord(
                            'S1', SwissQRBillBillingDetail."Tag Type"::"Payment Terms",
                            PairsTextList.Get(1), StrSubstNo(PaymentTermsTxt, Percent, Days));
                PairsTextList.RemoveAt(1);
            end;
        end;
    end;

    procedure ParseDate(DateText: Text): Text
    var
        ParsedDate: array[2] of Date;
    begin
        // source text = "YYMMDD" or "YYMMDDYYMMDD" (date range)
        case StrLen(DateText) of
            6:
                if ParseSingleDate(ParsedDate[1], DateText) then
                    exit(Format(ParsedDate[1]));
            12:
                if ParseSingleDate(ParsedDate[1], CopyStr(DateText, 1, 6)) and
                    ParseSingleDate(ParsedDate[2], CopyStr(DateText, 7, 6))
                then
                    exit(StrSubstNo(DateRangeTxt, ParsedDate[1], ParsedDate[2]));
        end;
    end;

    procedure ParseSingleDate(var ResultDate: Date; DateText: Text): Boolean
    var
        Year: Integer;
        Month: Integer;
        Day: Integer;
    begin
        // source text = "YYMMDD"
        if StrLen(DateText) = 6 then
            if Evaluate(Year, CopyStr(DateText, 1, 2)) and
                Evaluate(Month, CopyStr(DateText, 3, 2)) and
                Evaluate(Day, CopyStr(DateText, 5, 2))
            then begin
                ResultDate := DMY2Date(Day, Month, 2000 + Year);
                exit(true);
            end;
        exit(false);
    end;

    procedure ParseAmount(SourceText: Text): Text
    var
        Amount: Decimal;
    begin
        // parse as XML amount
        if SourceText <> '' then
            if Evaluate(Amount, SourceText, 9) then
                exit(Format(Amount));
    end;

    procedure FormatDate(SourceDate: Date): Text
    begin
        // YYMMDD
        exit(Format(SourceDate, 0, '<Year,2><Month,2><Day,2>'));
    end;

    procedure FormatAmount(Amount: Decimal): Text
    begin
        // XML amount format
        exit(Format(Round(Amount, 0.01), 0, '<Integer><Decimals><Comma,.>'));
    end;

    procedure FormatVATRegNo(VATNo: Text): Text
    begin
        // leave only digits
        exit(DelChr(VATNo, '=', DelChr(VATNo, '=', '0123456789')));
    end;

    var
        SwissQRBillMgt: Codeunit "Swiss QR-Bill Mgt.";
        SwissQRBillBillingInfo: Record "Swiss QR-Bill Billing Info";
        VATDetailsTotalTxt: Label '%1% for the total amount', Comment = '%1 - VAT percent (0..100)';
        VATDetailsTxt: Label '%1% on %2', Comment = '%1 - VAT percent (0..100), %2 - amount';
        DateRangeTxt: Label '%1 to %2', Comment = '%1 - start\from date, %2 - end\to date';
        PaymentTermsTxt: Label '%1% discount for %2 days', Comment = '%1 - percent value (0..100), %2 - number of days';
        UnsupportedFormatMsg: Label 'Unsupported billing format.';


    [IntegrationEvent(false, false)]
    local procedure OnBeforeValidateIBANRemainingAmount(var IBAN: Code[50])
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterValidateIBANRemainingAmount(var IBAN: Code[50])
    begin
    end;
}

